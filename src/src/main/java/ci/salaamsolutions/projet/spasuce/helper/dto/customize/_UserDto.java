
/*
 * Java dto for entity table user 
 * Created on 2020-01-03 ( Time 08:47:10 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.customize;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.dao.entity.Entreprise;
import ci.salaamsolutions.projet.spasuce.helper.contract.SearchParam;
import ci.salaamsolutions.projet.spasuce.helper.dto.EntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.FonctionnaliteDto;
import lombok.Data;
/**
 * DTO customize for table "user"
 * 
 * @author SFL Back-End developper
 *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _UserDto {
	
	protected String newPassword ;
	protected String typeEntrepriseCode ;
	protected String     link                  ;

	
	protected int compteurAppelDoffresNew = 0 ;
	protected int compteurAppelDoffresSelect = 0 ;
	protected int compteurAppelDoffresUpdate = 0 ;
	
	protected int compteurOffreNew = 0 ;
	protected int compteurOffreUpdate = 0 ;
	
	protected int totalCompteur = 0 ;

	
	
	protected EntrepriseDto dataEntreprise;
	protected Entreprise 	entityEntreprise;
	
	protected List<FonctionnaliteDto> datasFonctionnalite ;
	
	protected SearchParam<String>   typeEntrepriseCodeParam            ;                     


}
