
/*
 * Java dto for entity table appel_doffres 
 * Created on 2020-01-03 ( Time 08:47:09 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.customize;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.helper.dto.AppelDoffresRestreintDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.EntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.FichierDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.OffreDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteAppelDoffresDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.UserDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.ValeurCritereAppelDoffresDto;
import lombok.Data;
/**
 * DTO customize for table "appel_doffres"
 * 
 * @author SFL Back-End developper
 *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _AppelDoffresDto {
	
	protected List<ValeurCritereAppelDoffresDto> datasValeurCritereAppelDoffres;
	protected List<FichierDto> datasFichier;
	protected List<SecteurDactiviteAppelDoffresDto> datasSecteurDactiviteAppelDoffres;
	protected List<AppelDoffresRestreintDto> datasAppelDoffresRestreintx;
	protected List<OffreDto> datasOffre;
	protected List<UserDto> datasUserx;
	protected List<EntrepriseDto> datasEntreprise;
	
	//protected Integer nombreOffre ;
	protected String nombreOffre ;
	protected String nombreVisualisation ;

	protected String paysLibelle ;
	protected String paysCode ;
	protected int paysId ;

	
	protected OffreDto dataOffre ;
	

}
