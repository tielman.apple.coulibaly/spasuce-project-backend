

/*
 * Java transformer for entity table groupe_adresse_fournisseur 
 * Created on 2020-01-02 ( Time 17:59:59 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "groupe_adresse_fournisseur"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface GroupeAdresseFournisseurTransformer {

	GroupeAdresseFournisseurTransformer INSTANCE = Mappers.getMapper(GroupeAdresseFournisseurTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.groupe.id", target="groupeId"),
				@Mapping(source="entity.groupe.code", target="groupeCode"),
				@Mapping(source="entity.groupe.libelle", target="groupeLibelle"),
		@Mapping(source="entity.adresseFournisseur.id", target="adresseFournisseurId"),
				@Mapping(source="entity.adresseFournisseur.nom", target="adresseFournisseurNom"),
				@Mapping(source="entity.adresseFournisseur.prenoms", target="adresseFournisseurPrenoms"),
	})
	GroupeAdresseFournisseurDto toDto(GroupeAdresseFournisseur entity) throws ParseException;

    List<GroupeAdresseFournisseurDto> toDtos(List<GroupeAdresseFournisseur> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="groupe", target="groupe"),
		@Mapping(source="adresseFournisseur", target="adresseFournisseur"),
	})
    GroupeAdresseFournisseur toEntity(GroupeAdresseFournisseurDto dto, Groupe groupe, AdresseFournisseur adresseFournisseur) throws ParseException;

    //List<GroupeAdresseFournisseur> toEntities(List<GroupeAdresseFournisseurDto> dtos) throws ParseException;

}
