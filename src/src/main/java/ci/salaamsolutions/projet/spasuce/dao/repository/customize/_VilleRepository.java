package ci.salaamsolutions.projet.spasuce.dao.repository.customize;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;

/**
 * Repository customize : Ville.
 */
@Repository
public interface _VilleRepository {
	default List<String> _generateCriteria(VilleDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds Ville by using paysId as a search criteria.
	 *
	 * @param paysId
	 * @return A list of Object Ville whose paysId is equals to the given paysId. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.pays.id = :paysId and e.isDeleted = :isDeleted order by e.libelle asc")
	List<Ville> findByPaysIdAndOderByLibelle(@Param("paysId")Integer paysId, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Ville by using code and paysId as a search criteria.
	 *
	 * @param code, paysId
	 * @return An Object Ville whose code and paysId are equals to the given code and paysId. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.code = :code and e.pays.id = :paysId and e.isDeleted = :isDeleted")
	Ville findByCodeAndPaysId(@Param("code")String code, @Param("paysId")Integer paysId, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Ville by using libelle and paysId as a search criteria.
	 *
	 * @param libelle, paysId
	 * @return An Object Ville whose libelle and paysId are equals to the given libelle and paysId. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.libelle = :libelle and e.pays.id = :paysId and e.isDeleted = :isDeleted")
	Ville findByLibelleAndPaysId(@Param("libelle")String libelle, @Param("paysId")Integer paysId,  @Param("isDeleted")Boolean isDeleted);

}
