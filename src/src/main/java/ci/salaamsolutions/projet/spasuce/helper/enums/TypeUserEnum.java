package ci.salaamsolutions.projet.spasuce.helper.enums;

public class TypeUserEnum {
	public static final String	CLIENTX					= "Client";
	public static final String	ADMIN				    = "Administrateur";
	public static final String	FOURNISSEUR				= "Fournisseur";
	
	public static final String	ENTREPRISE_CLIENTE		= "CLIENTE";
	public static final String	ENTREPRISE_FOURNISSEUR	= "FOURNISSEUR";
	public static final String	ENTREPRISE_MIXTE		= "MIXTE";
	public static final String	USER_CLIENT				= "CLIENT";
	public static final String	USER_ADMIN				= "ADMINISTRATEUR";
	public static final String	USER_FOURNISSEUR		= "FOURNISSEUR";
	
}

