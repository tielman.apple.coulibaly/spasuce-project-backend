
/*
 * Java dto for entity table user
 * Created on 2020-01-20 ( Time 17:38:37 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.customize._UserDto;

import lombok.*;
/**
 * DTO for table "user"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class UserDto extends _UserDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private String     nom                  ;
	/*
	 * 
	 */
    private String     prenoms              ;
	/*
	 * 
	 */
    private String     login                ;
	/*
	 * 
	 */
    private String     password             ;
	/*
	 * 
	 */
    private String     telephone            ;
	/*
	 * 
	 */
    private String     email                ;
	/*
	 * 
	 */
    private Boolean    isLocked             ;
	/*
	 * 
	 */
    private Boolean    isSuUser             ;
	/*
	 * Valeur a setter si les inscription suivent un procès
	 */
    private Boolean    isValider            ;
	/*
	 * 
	 */
    private String     token                ;
	/*
	 * 
	 */
	private String     dateExpirationToken  ;
	/*
	 * 
	 */
    private Boolean    isTokenValide        ;
	/*
	 * 
	 */
    private Integer    roleId               ;
	/*
	 * 
	 */
    private Integer    entrepriseId         ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String roleCode;
	private String roleLibelle;
	private String entrepriseNom;
	private String entrepriseLogin;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   nomParam              ;                     
	private SearchParam<String>   prenomsParam          ;                     
	private SearchParam<String>   loginParam            ;                     
	private SearchParam<String>   passwordParam         ;                     
	private SearchParam<String>   telephoneParam        ;                     
	private SearchParam<String>   emailParam            ;                     
	private SearchParam<Boolean>  isLockedParam         ;                     
	private SearchParam<Boolean>  isSuUserParam         ;                     
	private SearchParam<Boolean>  isValiderParam        ;                     
	private SearchParam<String>   tokenParam            ;                     

		private SearchParam<String>   dateExpirationTokenParam;                     
	private SearchParam<Boolean>  isTokenValideParam    ;                     
	private SearchParam<Integer>  roleIdParam           ;                     
	private SearchParam<Integer>  entrepriseIdParam     ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   roleCodeParam         ;                     
	private SearchParam<String>   roleLibelleParam      ;                     
	private SearchParam<String>   entrepriseNomParam    ;                     
	private SearchParam<String>   entrepriseLoginParam  ;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		UserDto other = (UserDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
