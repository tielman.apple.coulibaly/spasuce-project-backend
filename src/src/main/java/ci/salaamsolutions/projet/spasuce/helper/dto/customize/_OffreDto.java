
/*
 * Java dto for entity table offre 
 * Created on 2020-01-08 ( Time 09:47:27 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.customize;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.helper.contract.SearchParam;
import ci.salaamsolutions.projet.spasuce.helper.dto.FichierDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.ValeurCritereOffreDto;
import lombok.Data;
/**
 * DTO customize for table "offre"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _OffreDto {
	
	protected List<ValeurCritereOffreDto> datasValeurCritereOffre ;
	protected List<FichierDto> datasFichier ;
	
	protected String entrepriseNom ;
	protected String paysLibelle ;
	protected SearchParam<String>  paysLibelleParam ; 

	
    private Integer    prixPrestation                 ;

	
	protected Boolean isAttribuerAppelDoffres ;
	
	protected Integer  userClientId ; 
	protected SearchParam<Integer>  userClientIdParam ; 
	
	
}
