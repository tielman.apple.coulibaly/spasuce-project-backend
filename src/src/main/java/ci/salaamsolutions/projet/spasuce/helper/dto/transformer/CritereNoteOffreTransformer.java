

/*
 * Java transformer for entity table critere_note_offre 
 * Created on 2020-01-02 ( Time 17:59:56 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "critere_note_offre"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface CritereNoteOffreTransformer {

	CritereNoteOffreTransformer INSTANCE = Mappers.getMapper(CritereNoteOffreTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.sousCritereNote.id", target="sousCritereNoteId"),
				@Mapping(source="entity.sousCritereNote.libelle", target="sousCritereNoteLibelle"),
		@Mapping(source="entity.offre.id", target="offreId"),
		@Mapping(source="entity.moyenCalcul.id", target="moyenCalculId"),
				@Mapping(source="entity.moyenCalcul.libelle", target="moyenCalculLibelle"),
		@Mapping(source="entity.critereNote.id", target="critereNoteId"),
				@Mapping(source="entity.critereNote.libelle", target="critereNoteLibelle"),
	})
	CritereNoteOffreDto toDto(CritereNoteOffre entity) throws ParseException;

    List<CritereNoteOffreDto> toDtos(List<CritereNoteOffre> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.valeurPartielle", target="valeurPartielle"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="sousCritereNote", target="sousCritereNote"),
		@Mapping(source="offre", target="offre"),
		@Mapping(source="moyenCalcul", target="moyenCalcul"),
		@Mapping(source="critereNote", target="critereNote"),
	})
    CritereNoteOffre toEntity(CritereNoteOffreDto dto, SousCritereNote sousCritereNote, Offre offre, MoyenCalcul moyenCalcul, CritereNote critereNote) throws ParseException;

    //List<CritereNoteOffre> toEntities(List<CritereNoteOffreDto> dtos) throws ParseException;

}
