
/*
 * Java dto for entity table domaine_entreprise 
 * Created on 2020-03-06 ( Time 21:33:12 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.customize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.dao.entity.Entreprise;
import ci.salaamsolutions.projet.spasuce.helper.contract.SearchParam;
import lombok.Data;
/**
 * DTO customize for table "domaine_entreprise"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _DomaineEntrepriseDto {
	

	protected Entreprise entityEntreprise ;
	protected String typeEntrepriseCode;
	protected SearchParam<String>  typeEntrepriseCodeParam;	

}
