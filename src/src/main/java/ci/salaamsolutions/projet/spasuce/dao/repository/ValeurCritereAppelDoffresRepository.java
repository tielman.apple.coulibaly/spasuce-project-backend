package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._ValeurCritereAppelDoffresRepository;

/**
 * Repository : ValeurCritereAppelDoffres.
 */
@Repository
public interface ValeurCritereAppelDoffresRepository extends JpaRepository<ValeurCritereAppelDoffres, Integer>, _ValeurCritereAppelDoffresRepository {
	/**
	 * Finds ValeurCritereAppelDoffres by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object ValeurCritereAppelDoffres whose id is equals to the given id. If
	 *         no ValeurCritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from ValeurCritereAppelDoffres e where e.id = :id and e.isDeleted = :isDeleted")
	ValeurCritereAppelDoffres findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ValeurCritereAppelDoffres by using valeur as a search criteria.
	 *
	 * @param valeur
	 * @return An Object ValeurCritereAppelDoffres whose valeur is equals to the given valeur. If
	 *         no ValeurCritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from ValeurCritereAppelDoffres e where e.valeur = :valeur and e.isDeleted = :isDeleted")
	List<ValeurCritereAppelDoffres> findByValeur(@Param("valeur")String valeur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereAppelDoffres by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object ValeurCritereAppelDoffres whose createdAt is equals to the given createdAt. If
	 *         no ValeurCritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from ValeurCritereAppelDoffres e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<ValeurCritereAppelDoffres> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereAppelDoffres by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object ValeurCritereAppelDoffres whose createdBy is equals to the given createdBy. If
	 *         no ValeurCritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from ValeurCritereAppelDoffres e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<ValeurCritereAppelDoffres> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereAppelDoffres by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object ValeurCritereAppelDoffres whose updatedAt is equals to the given updatedAt. If
	 *         no ValeurCritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from ValeurCritereAppelDoffres e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<ValeurCritereAppelDoffres> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereAppelDoffres by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object ValeurCritereAppelDoffres whose updatedBy is equals to the given updatedBy. If
	 *         no ValeurCritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from ValeurCritereAppelDoffres e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<ValeurCritereAppelDoffres> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereAppelDoffres by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object ValeurCritereAppelDoffres whose deletedAt is equals to the given deletedAt. If
	 *         no ValeurCritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from ValeurCritereAppelDoffres e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<ValeurCritereAppelDoffres> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereAppelDoffres by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object ValeurCritereAppelDoffres whose deletedBy is equals to the given deletedBy. If
	 *         no ValeurCritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from ValeurCritereAppelDoffres e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<ValeurCritereAppelDoffres> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereAppelDoffres by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object ValeurCritereAppelDoffres whose isDeleted is equals to the given isDeleted. If
	 *         no ValeurCritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from ValeurCritereAppelDoffres e where e.isDeleted = :isDeleted")
	List<ValeurCritereAppelDoffres> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ValeurCritereAppelDoffres by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object ValeurCritereAppelDoffres whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no ValeurCritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from ValeurCritereAppelDoffres e where e.appelDoffres.id = :appelDoffresId and e.isDeleted = :isDeleted")
	List<ValeurCritereAppelDoffres> findByAppelDoffresId(@Param("appelDoffresId")Integer appelDoffresId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one ValeurCritereAppelDoffres by using appelDoffresId as a search criteria.
   *
   * @param appelDoffresId
   * @return An Object ValeurCritereAppelDoffres whose appelDoffresId is equals to the given appelDoffresId. If
   *         no ValeurCritereAppelDoffres is found, this method returns null.
   */
  @Query("select e from ValeurCritereAppelDoffres e where e.appelDoffres.id = :appelDoffresId and e.isDeleted = :isDeleted")
  ValeurCritereAppelDoffres findValeurCritereAppelDoffresByAppelDoffresId(@Param("appelDoffresId")Integer appelDoffresId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds ValeurCritereAppelDoffres by using critereId as a search criteria.
	 *
	 * @param critereId
	 * @return A list of Object ValeurCritereAppelDoffres whose critereId is equals to the given critereId. If
	 *         no ValeurCritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from ValeurCritereAppelDoffres e where e.critereAppelDoffres.id = :critereId and e.isDeleted = :isDeleted")
	List<ValeurCritereAppelDoffres> findByCritereId(@Param("critereId")Integer critereId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one ValeurCritereAppelDoffres by using critereId as a search criteria.
   *
   * @param critereId
   * @return An Object ValeurCritereAppelDoffres whose critereId is equals to the given critereId. If
   *         no ValeurCritereAppelDoffres is found, this method returns null.
   */
  @Query("select e from ValeurCritereAppelDoffres e where e.critereAppelDoffres.id = :critereId and e.isDeleted = :isDeleted")
  ValeurCritereAppelDoffres findValeurCritereAppelDoffresByCritereId(@Param("critereId")Integer critereId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of ValeurCritereAppelDoffres by using valeurCritereAppelDoffresDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of ValeurCritereAppelDoffres
	 * @throws DataAccessException,ParseException
	 */
	public default List<ValeurCritereAppelDoffres> getByCriteria(Request<ValeurCritereAppelDoffresDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from ValeurCritereAppelDoffres e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<ValeurCritereAppelDoffres> query = em.createQuery(req, ValeurCritereAppelDoffres.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of ValeurCritereAppelDoffres by using valeurCritereAppelDoffresDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of ValeurCritereAppelDoffres
	 *
	 */
	public default Long count(Request<ValeurCritereAppelDoffresDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from ValeurCritereAppelDoffres e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<ValeurCritereAppelDoffresDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		ValeurCritereAppelDoffresDto dto = request.getData() != null ? request.getData() : new ValeurCritereAppelDoffresDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (ValeurCritereAppelDoffresDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(ValeurCritereAppelDoffresDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getValeur())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("valeur", dto.getValeur(), "e.valeur", "String", dto.getValeurParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getAppelDoffresId()!= null && dto.getAppelDoffresId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("appelDoffresId", dto.getAppelDoffresId(), "e.appelDoffres.id", "Integer", dto.getAppelDoffresIdParam(), param, index, locale));
			}
			if (dto.getCritereId()!= null && dto.getCritereId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("critereId", dto.getCritereId(), "e.critereAppelDoffres.id", "Integer", dto.getCritereIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getAppelDoffresLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("appelDoffresLibelle", dto.getAppelDoffresLibelle(), "e.appelDoffres.libelle", "String", dto.getAppelDoffresLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCritereAppelDoffresLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("critereAppelDoffresLibelle", dto.getCritereAppelDoffresLibelle(), "e.critereAppelDoffres.libelle", "String", dto.getCritereAppelDoffresLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
