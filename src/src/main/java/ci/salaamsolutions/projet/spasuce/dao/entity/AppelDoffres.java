/*
 * Created on 2020-03-03 ( Time 07:21:47 )
 * Generated by Telosys Tools Generator ( version 3.1.2 )
 */
// This Bean has a basic Primary Key (not composite) 

package ci.salaamsolutions.projet.spasuce.dao.entity;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import lombok.*;

/**
 * Persistent class for entity stored in table "appel_doffres"
 *
* @author Back-End developper
   *
 */

@Entity
@Table(name="appel_doffres" )
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
public class AppelDoffres implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
	/*
	 * 
	 */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
	/*
	 * 
	 */
    @Column(name="libelle", length=255)
    private String     libelle      ;

	/*
	 * 
	 */
    @Column(name="code", length=255)
    private String     code         ;

	/*
	 * 
	 */
    @Column(name="prix_prestation")
    private Integer    prixPrestation ;

	/*
	 * 
	 */
    @Column(name="nombre_type_duree")
    private Integer    nombreTypeDuree ;

	/*
	 * 
	 */
    @Column(name="description")
    private String     description  ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date_limite_depot")
    private Date       dateLimiteDepot ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date_creation")
    private Date       dateCreation ;

	/*
	 * 
	 */
    @Column(name="dure_realisation", length=255)
    private String     dureRealisation ;

	/*
	 * Setter quand le client décide de fermer son offre
	 */
    @Column(name="is_locked")
    private Boolean    isLocked     ;

	/*
	 * Pour dire que l'AO a des contenus d'offres non visualiser
	 */
    @Column(name="is_contenu_notification_dispo")
    private Boolean    isContenuNotificationDispo ;

	/*
	 * 
	 */
    @Column(name="is_attribuer")
    private Boolean    isAttribuer  ;

	/*
	 * Pour savoir si le client peut toujours modifier l'appel d'offres
	 */
    @Column(name="is_modifiable")
    private Boolean    isModifiable ;

	/*
	 * Pour dire que l'appel d'offres a été retirer ou le fournisseur a été retirer de l'appel d'offres
	 */
    @Column(name="is_retirer")
    private Boolean    isRetirer    ;

	/*
	 * Pour les suppressions logique ou partielle
	 */
    @Column(name="is_visible_by_client")
    private Boolean    isVisibleByClient ;

	/*
	 * Pour dire que l'AO a été modifier après soumission dans le système
	 */
    @Column(name="is_update")
    private Boolean    isUpdate     ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at")
    private Date       createdAt    ;

	/*
	 * 
	 */
    @Column(name="created_by")
    private Integer    createdBy    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_at")
    private Date       updatedAt    ;

	/*
	 * 
	 */
    @Column(name="updated_by")
    private Integer    updatedBy    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="deleted_at")
    private Date       deletedAt    ;

	/*
	 * 
	 */
    @Column(name="deleted_by")
    private Integer    deletedBy    ;

	/*
	 * 
	 */
    @Column(name="is_deleted")
    private Boolean    isDeleted    ;

	// "typeDureeId" (column "type_duree_id") is not defined by itself because used as FK in a link 
	// "villeId" (column "ville_id") is not defined by itself because used as FK in a link 
	// "typeAppelDoffresId" (column "type_appel_doffres_id") is not defined by itself because used as FK in a link 
	// "userClientId" (column "user_client_id") is not defined by itself because used as FK in a link 
	// "etatId" (column "etat_id") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="ville_id", referencedColumnName="id")
        private Ville ville;
    @ManyToOne
    @JoinColumn(name="etat_id", referencedColumnName="id")
        private Etat etat;
    @ManyToOne
    @JoinColumn(name="type_duree_id", referencedColumnName="id")
        private TypeDuree typeDuree;
    @ManyToOne
    @JoinColumn(name="type_appel_doffres_id", referencedColumnName="id")
        private TypeAppelDoffres typeAppelDoffres;
    @ManyToOne
    @JoinColumn(name="user_client_id", referencedColumnName="id")
        private User user;

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
