package ci.salaamsolutions.projet.spasuce.helper.enums;

public class MonthLetterEnum {
 
	public static final String	janvier			= "janvier";
	public static final String	fevrier			= "fevrier";
	public static final String	mars			= "mars";
	public static final String	avril			= "avril";
	public static final String	mai				= "mai";
	public static final String	juin			= "juin";
	public static final String	juillet			= "juillet";
	public static final String	aout			= "aout";
	public static final String	septembre		= "septembre";
	public static final String	octobre			= "octobre";
	public static final String	novembre		= "novembre";
	public static final String	decembre		= "decembre";
	
	public static final String	JANVIER			= "JANVIER";
	public static final String	FEVRIER			= "FEVRIER";
	public static final String	MARS			= "MARS";
	public static final String	AVRIL			= "AVRIL";
	public static final String	MAI				= "MAI";
	public static final String	JUIN			= "JUIN";
	public static final String	JUILLET			= "JUILLET";
	public static final String	AOUT			= "AOUT";
	public static final String	SEPTEMBRE		= "SEPTEMBRE";
	public static final String	OCTOBRE			= "OCTOBRE";
	public static final String	NOVEMBRE		= "NOVEMBRE";
	public static final String	DECEMBRE		= "DECEMBRE";
	
	public static final String	JANV			= "JANV";
	public static final String	FEV		= "FEV";
	public static final String	AVR			= "AVR";
	public static final String	JUIL		= "JUIL";
	public static final String	SEPT		= "SEPT";
	public static final String	OCT 	= "OCT";
	public static final String	NOV		= "NOV";
	public static final String	DEC 		= "DEC";
	
	
	public static final String	FÉVRIER			= "FÉVRIER";
	public static final String	AOÛT			= "AOÛT";
	public static final String	DÉCEMBRE		= "DÉCEMBRE";
	public static final String	février			= "février";
	public static final String	août			= "août";
	public static final String	décembre		= "décembre";
	
	public static final String	SPACE			= " ";
}
