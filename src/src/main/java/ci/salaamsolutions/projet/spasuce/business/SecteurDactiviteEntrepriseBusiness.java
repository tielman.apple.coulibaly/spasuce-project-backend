


/*
 * Java transformer for entity table secteur_dactivite_entreprise
 * Created on 2020-01-02 ( Time 18:00:03 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffres;
import ci.salaamsolutions.projet.spasuce.dao.entity.Entreprise;
import ci.salaamsolutions.projet.spasuce.dao.entity.SecteurDactivite;
import ci.salaamsolutions.projet.spasuce.dao.entity.SecteurDactiviteEntreprise;
import ci.salaamsolutions.projet.spasuce.dao.entity.User;
import ci.salaamsolutions.projet.spasuce.dao.repository.EntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.SecteurDactiviteEntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.SecteurDactiviteRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.UserRepository;
import ci.salaamsolutions.projet.spasuce.helper.ExceptionUtils;
import ci.salaamsolutions.projet.spasuce.helper.FunctionalError;
import ci.salaamsolutions.projet.spasuce.helper.TechnicalError;
import ci.salaamsolutions.projet.spasuce.helper.Utilities;
import ci.salaamsolutions.projet.spasuce.helper.Validate;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.dto.AppelDoffresDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.EntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteEntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.AppelDoffresTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.EntrepriseTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.SecteurDactiviteEntrepriseTransformer;

/**
BUSINESS for table "secteur_dactivite_entreprise"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class SecteurDactiviteEntrepriseBusiness implements IBasicBusiness<Request<SecteurDactiviteEntrepriseDto>, Response<SecteurDactiviteEntrepriseDto>> {

	private Response<SecteurDactiviteEntrepriseDto> response;
	@Autowired
	private SecteurDactiviteEntrepriseRepository secteurDactiviteEntrepriseRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private SecteurDactiviteRepository secteurDactiviteRepository;
	@Autowired
	private EntrepriseRepository entrepriseRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public SecteurDactiviteEntrepriseBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create SecteurDactiviteEntreprise by using SecteurDactiviteEntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SecteurDactiviteEntrepriseDto> create(Request<SecteurDactiviteEntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin create SecteurDactiviteEntreprise-----");

		response = new Response<SecteurDactiviteEntrepriseDto>();

		try {
//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}
			if (request.getUser() != null && request.getUser() > 0) {
				User utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
			}


			List<SecteurDactiviteEntreprise> items = new ArrayList<SecteurDactiviteEntreprise>();

			for (SecteurDactiviteEntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("valeurAutre", dto.getValeurAutre());
				fieldsToVerify.put("secteurDactiviteId", dto.getSecteurDactiviteId());
				fieldsToVerify.put("entrepriseId", dto.getEntrepriseId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if secteurDactiviteEntreprise to insert do not exist
				SecteurDactiviteEntreprise existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("secteurDactiviteEntreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if secteurDactivite exist
				SecteurDactivite existingSecteurDactivite = secteurDactiviteRepository.findById(dto.getSecteurDactiviteId(), false);
				if (existingSecteurDactivite == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("secteurDactivite -> " + dto.getSecteurDactiviteId(), locale));
					response.setHasError(true);
					return response;
				}
		
				// Verify if entreprise exist
				Entreprise existingEntreprise = entrepriseRepository.findById(dto.getEntrepriseId(), false);
				if (existingEntreprise == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("entreprise -> " + dto.getEntrepriseId(), locale));
					response.setHasError(true);
					return response;
				}
				
				if (items.size() > 0) {
					if (items.stream().anyMatch(a->a.getEntreprise().getId().equals(dto.getEntrepriseId()) && a.getSecteurDactivite().getId().equals(dto.getSecteurDactiviteId()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du secteurDactivite ->'" + dto.getSecteurDactiviteId()+"' pour l'entreprises -> "+ dto.getEntrepriseId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				SecteurDactiviteEntreprise entityToSave = null;
				entityToSave = SecteurDactiviteEntrepriseTransformer.INSTANCE.toEntity(dto, existingSecteurDactivite, existingEntreprise);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<SecteurDactiviteEntreprise> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = secteurDactiviteEntrepriseRepository.save((Iterable<SecteurDactiviteEntreprise>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("secteurDactiviteEntreprise", locale));
					response.setHasError(true);
					return response;
				}
				List<SecteurDactiviteEntrepriseDto> itemsDto = new ArrayList<SecteurDactiviteEntrepriseDto>();
				for (SecteurDactiviteEntreprise entity : itemsSaved) {
					SecteurDactiviteEntrepriseDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create SecteurDactiviteEntreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update SecteurDactiviteEntreprise by using SecteurDactiviteEntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SecteurDactiviteEntrepriseDto> update(Request<SecteurDactiviteEntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin update SecteurDactiviteEntreprise-----");

		response = new Response<SecteurDactiviteEntrepriseDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SecteurDactiviteEntreprise> items = new ArrayList<SecteurDactiviteEntreprise>();

			for (SecteurDactiviteEntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la secteurDactiviteEntreprise existe
				SecteurDactiviteEntreprise entityToSave = null;
				entityToSave = secteurDactiviteEntrepriseRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("secteurDactiviteEntreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if secteurDactivite exist
				if (dto.getSecteurDactiviteId() != null && dto.getSecteurDactiviteId() > 0){
					SecteurDactivite existingSecteurDactivite = secteurDactiviteRepository.findById(dto.getSecteurDactiviteId(), false);
					if (existingSecteurDactivite == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("secteurDactivite -> " + dto.getSecteurDactiviteId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setSecteurDactivite(existingSecteurDactivite);
				}
				// Verify if entreprise exist
				if (dto.getEntrepriseId() != null && dto.getEntrepriseId() > 0){
					Entreprise existingEntreprise = entrepriseRepository.findById(dto.getEntrepriseId(), false);
					if (existingEntreprise == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("entreprise -> " + dto.getEntrepriseId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEntreprise(existingEntreprise);
				}
				if (Utilities.notBlank(dto.getValeurAutre())) {
					entityToSave.setValeurAutre(dto.getValeurAutre());
				}
//				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
//					entityToSave.setCreatedBy(dto.getCreatedBy());
//				}
//				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
//					entityToSave.setUpdatedBy(dto.getUpdatedBy());
//				}
//				if (Utilities.notBlank(dto.getDeletedAt())) {
//					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
//				}
//				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
//					entityToSave.setDeletedBy(dto.getDeletedBy());
//				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<SecteurDactiviteEntreprise> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = secteurDactiviteEntrepriseRepository.save((Iterable<SecteurDactiviteEntreprise>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("secteurDactiviteEntreprise", locale));
					response.setHasError(true);
					return response;
				}
				List<SecteurDactiviteEntrepriseDto> itemsDto = new ArrayList<SecteurDactiviteEntrepriseDto>();
				for (SecteurDactiviteEntreprise entity : itemsSaved) {
					SecteurDactiviteEntrepriseDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update SecteurDactiviteEntreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete SecteurDactiviteEntreprise by using SecteurDactiviteEntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SecteurDactiviteEntrepriseDto> delete(Request<SecteurDactiviteEntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete SecteurDactiviteEntreprise-----");

		response = new Response<SecteurDactiviteEntrepriseDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SecteurDactiviteEntreprise> items = new ArrayList<SecteurDactiviteEntreprise>();

			for (SecteurDactiviteEntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la secteurDactiviteEntreprise existe
				SecteurDactiviteEntreprise existingEntity = null;
				existingEntity = secteurDactiviteEntrepriseRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("secteurDactiviteEntreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				secteurDactiviteEntrepriseRepository.save((Iterable<SecteurDactiviteEntreprise>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete SecteurDactiviteEntreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete SecteurDactiviteEntreprise by using SecteurDactiviteEntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<SecteurDactiviteEntrepriseDto> forceDelete(Request<SecteurDactiviteEntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete SecteurDactiviteEntreprise-----");

		response = new Response<SecteurDactiviteEntrepriseDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SecteurDactiviteEntreprise> items = new ArrayList<SecteurDactiviteEntreprise>();

			for (SecteurDactiviteEntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la secteurDactiviteEntreprise existe
				SecteurDactiviteEntreprise existingEntity = null;
				existingEntity = secteurDactiviteEntrepriseRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("secteurDactiviteEntreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				secteurDactiviteEntrepriseRepository.save((Iterable<SecteurDactiviteEntreprise>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete SecteurDactiviteEntreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get SecteurDactiviteEntreprise by using SecteurDactiviteEntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<SecteurDactiviteEntrepriseDto> getByCriteria(Request<SecteurDactiviteEntrepriseDto> request, Locale locale) {
		slf4jLogger.info("----begin get SecteurDactiviteEntreprise-----");

		response = new Response<SecteurDactiviteEntrepriseDto>();

		try {
			List<SecteurDactiviteEntreprise> items = null;
			items = secteurDactiviteEntrepriseRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<SecteurDactiviteEntrepriseDto> itemsDto = new ArrayList<SecteurDactiviteEntrepriseDto>();
				for (SecteurDactiviteEntreprise entity : items) {
					SecteurDactiviteEntrepriseDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(secteurDactiviteEntrepriseRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("secteurDactiviteEntreprise", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get SecteurDactiviteEntreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
	
	
	/**
	 * getCustom SecteurDactiviteAppelDoffres by using SecteurDactiviteAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	public Response<EntrepriseDto> getByCriteriaCustom(Request<SecteurDactiviteEntrepriseDto> request, Locale locale) {
		slf4jLogger.info("----begin getByCriteriaCustom SecteurDactiviteEntreprise-----");

		Response<EntrepriseDto> response = new Response<>();

		try {
			List<Entreprise> items = null;
			items = secteurDactiviteEntrepriseRepository.getByCriteriaCustom(request, em, locale);
			if (items != null && !items.isEmpty()) {
				
				Collections.sort(items, new Comparator<Entreprise>() {
					@Override
					public int compare(Entreprise o1, Entreprise o2) {
						// TODO Auto-generated method stub
						//return o1.getId().compareTo(o2.getId())* - 1;
						//return o2.getId().compareTo(o1.getId());

						return o2.getCreatedAt().compareTo(o1.getCreatedAt());
					}
				}); // trier la liste
				
				List<EntrepriseDto> itemsDto = new ArrayList<EntrepriseDto>();
				itemsDto = EntrepriseTransformer.INSTANCE.toDtos(items) ;
				
//				for (EntrepriseDto entrepriseDto : itemsDto) {
//					entrepriseDto.setEntrepriseNom(entrepriseDto.getNom());
//				}
				response.setItems(itemsDto);
				//response.setCount(secteurDactiviteAppelDoffresRepository.countCustom(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("SecteurDactiviteEntreprise", locale));
				response.setHasError(false);
				return response;
			}
			
			slf4jLogger.info("----end getByCriteriaCustom SecteurDactiviteEntreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * get full SecteurDactiviteEntrepriseDto by using SecteurDactiviteEntreprise as object.
	 *
	 * @param entity, locale
	 * @return SecteurDactiviteEntrepriseDto
	 *
	 */
	private SecteurDactiviteEntrepriseDto getFullInfos(SecteurDactiviteEntreprise entity, Integer size, Locale locale) throws Exception {
		SecteurDactiviteEntrepriseDto dto = SecteurDactiviteEntrepriseTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
