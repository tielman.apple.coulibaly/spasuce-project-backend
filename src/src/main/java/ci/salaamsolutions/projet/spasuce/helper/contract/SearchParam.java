
/*
 * Created on 2020-01-02 ( Time 18:00:15 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.contract;

/**
 * Search Param
 * 
 * @author SFL Back-End developper
 *
 */
public class SearchParam<T> {

	String	operator;
	T		start;
	T		end;

	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * @return the start
	 */
	public T getStart() {
		return start;
	}

	/**
	 * @return the end
	 */
	public T getEnd() {
		return end;
	}

	/**
	 * @param operator
	 *            the operator to set
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}

	/**
	 * @param start
	 *            the start to set
	 */
	public void setStart(T start) {
		this.start = start;
	}

	/**
	 * @param end
	 *            the end to set
	 */
	public void setEnd(T end) {
		this.end = end;
	}

}
