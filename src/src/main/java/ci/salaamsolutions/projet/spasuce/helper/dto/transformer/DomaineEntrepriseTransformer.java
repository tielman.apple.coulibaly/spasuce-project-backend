

/*
 * Java transformer for entity table domaine_entreprise 
 * Created on 2020-03-06 ( Time 21:33:12 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "domaine_entreprise"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface DomaineEntrepriseTransformer {

	DomaineEntrepriseTransformer INSTANCE = Mappers.getMapper(DomaineEntrepriseTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.entreprise.id", target="entrepriseId"),
				@Mapping(source="entity.entreprise.nom", target="entrepriseNom"),
				@Mapping(source="entity.entreprise.login", target="entrepriseLogin"),
		@Mapping(source="entity.domaine.id", target="domaineId"),
				@Mapping(source="entity.domaine.code", target="domaineCode"),
				@Mapping(source="entity.domaine.libelle", target="domaineLibelle"),
	})
	DomaineEntrepriseDto toDto(DomaineEntreprise entity) throws ParseException;

    List<DomaineEntrepriseDto> toDtos(List<DomaineEntreprise> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.valeurAutre", target="valeurAutre"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="entreprise", target="entreprise"),
		@Mapping(source="domaine", target="domaine"),
	})
    DomaineEntreprise toEntity(DomaineEntrepriseDto dto, Entreprise entreprise, Domaine domaine) throws ParseException;

    //List<DomaineEntreprise> toEntities(List<DomaineEntrepriseDto> dtos) throws ParseException;

}
