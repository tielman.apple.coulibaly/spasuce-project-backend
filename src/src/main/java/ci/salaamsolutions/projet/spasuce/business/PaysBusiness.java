


/*
 * Java transformer for entity table pays
 * Created on 2020-01-02 ( Time 18:00:01 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.dao.entity.Continent;
import ci.salaamsolutions.projet.spasuce.dao.entity.Pays;
import ci.salaamsolutions.projet.spasuce.dao.entity.User;
import ci.salaamsolutions.projet.spasuce.dao.entity.Ville;
import ci.salaamsolutions.projet.spasuce.dao.repository.AppelDoffresRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.ContinentRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.EntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.PaysRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.UserRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.VilleRepository;
import ci.salaamsolutions.projet.spasuce.helper.ExceptionUtils;
import ci.salaamsolutions.projet.spasuce.helper.FunctionalError;
import ci.salaamsolutions.projet.spasuce.helper.TechnicalError;
import ci.salaamsolutions.projet.spasuce.helper.Utilities;
import ci.salaamsolutions.projet.spasuce.helper.Validate;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.dto.PaysDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.PaysTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.VilleTransformer;

/**
BUSINESS for table "pays"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class PaysBusiness implements IBasicBusiness<Request<PaysDto>, Response<PaysDto>> {

	private Response<PaysDto> response;
	@Autowired
	private PaysRepository paysRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AppelDoffresRepository appelDoffresRepository;
	@Autowired
	private EntrepriseRepository entrepriseRepository;
	@Autowired
	private ContinentRepository continentRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private AppelDoffresBusiness appelDoffresBusiness;


	@Autowired
	private EntrepriseBusiness entrepriseBusiness;
	@Autowired
	private VilleRepository villeRepository;
	
	



	public PaysBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create Pays by using PaysDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<PaysDto> create(Request<PaysDto> request, Locale locale)  {
		slf4jLogger.info("----begin create Pays-----");

		response = new Response<PaysDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Pays> items = new ArrayList<>() ; 
			List<Pays> listes = new ArrayList<>() ; 

			for (PaysDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("code", dto.getCode());
				fieldsToVerify.put("libelle", dto.getLibelle());
				//fieldsToVerify.put("datasPays", dto.getDatasPays());
				//fieldsToVerify.put("continentId", dto.getContinentId());
				fieldsToVerify.put("continentLibelle", dto.getContinentLibelle());

				//				fieldsToVerify.put("createdBy", dto.getCreatedBy());
				//				fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
				//				fieldsToVerify.put("deletedAt", dto.getDeletedAt());
				//				fieldsToVerify.put("deletedBy", dto.getDeletedBy());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if pays to insert do not exist
				Pays existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("pays -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if continent exist
				//Continent existingContinent = continentRepository.findById(dto.getContinentId(), false);
				Continent existingContinent = continentRepository.findByLibelle(dto.getContinentLibelle(), false);
				if (existingContinent == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("continent -> " + dto.getContinentLibelle(), locale));
					response.setHasError(true);
					return response;
				}

				/*

				for (PaysDto data : dto.getDatasPays()) {

					if (Utilities.notBlank(data.getCode())) {
						existingEntity = paysRepository.findByCodeAndContinentId(data.getCode(), dto.getContinentId(), false);
						if (existingEntity != null) {
							response.setStatus(functionalError.DATA_EXIST("pays -> " + data.getCode(), locale));
							response.setHasError(true);
							return response;
						}
						if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(data.getCode()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + data.getCode()+"' pour les payss", locale));
							response.setHasError(true);
							return response;
						}
					}
					existingEntity = paysRepository.findByLibelleAndContinentId(data.getLibelle(), dto.getContinentId(),false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("pays -> " + data.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(data.getLibelle()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + data.getLibelle()+"' pour les payss", locale));
						response.setHasError(true);
						return response;
					}

					Pays entityToSave = null;
					entityToSave = PaysTransformer.INSTANCE.toEntity(data, existingContinent);
					entityToSave.setCreatedAt(Utilities.getCurrentDate());
					entityToSave.setCreatedBy(request.getUser());
					entityToSave.setIsDeleted(false);
					items.add(entityToSave);
				}
				
				//*/


				if (Utilities.notBlank(dto.getCode())) {
					existingEntity = paysRepository.findByCodeAndContinentId(dto.getCode(), dto.getContinentId(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("pays -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items == null ) {
						slf4jLogger.info("***********items*********"+ items);

					}
					if (listes == null ) {
						slf4jLogger.info("***********listes*********" + listes);

					}

					slf4jLogger.info("----items-----" +items);
					slf4jLogger.info("----listes-----" + listes);
					
					if (items != null && items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les payss", locale));
						response.setHasError(true);
						return response;
					}
				}
				existingEntity = paysRepository.findByLibelleAndContinentId(dto.getLibelle(), dto.getContinentId(),false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("pays -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items != null &&  items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les payss", locale));
					response.setHasError(true);
					return response;
				}

				Pays entityToSave = null;
				entityToSave = PaysTransformer.INSTANCE.toEntity(dto, existingContinent);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			

			}

			if (!items.isEmpty()) {
				List<Pays> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = paysRepository.save((Iterable<Pays>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("pays", locale));
					response.setHasError(true);
					return response;
				}
				List<PaysDto> itemsDto = new ArrayList<PaysDto>();
				for (Pays entity : itemsSaved) {
					PaysDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create Pays-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Pays by using PaysDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<PaysDto> update(Request<PaysDto> request, Locale locale)  {
		slf4jLogger.info("----begin update Pays-----");

		response = new Response<PaysDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Pays> items = new ArrayList<Pays>();

			for (PaysDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la pays existe
				Pays entityToSave = null;
				entityToSave = paysRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pays -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if continent exist
				if (Utilities.notBlank(dto.getContinentLibelle())){
					//if (dto.getContinentId() != null && dto.getContinentId() > 0){
					//Continent existingContinent = continentRepository.findById(dto.getContinentId(), false);
					Continent existingContinent = continentRepository.findByLibelle(dto.getContinentLibelle(), false);

					if (existingContinent == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("continent -> " + dto.getContinentLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setContinent(existingContinent);
				}
				
				Integer continentId = entityToSave.getContinent().getId();

				
				
				if (Utilities.notBlank(dto.getIso())) {
					entityToSave.setIso(dto.getIso());
				}
				if (Utilities.notBlank(dto.getCurrency())) {
					entityToSave.setCurrency(dto.getCurrency());
				}
				if (Utilities.notBlank(dto.getCode())) {
					Pays existingEntity = paysRepository.findByCodeAndContinentId(dto.getCode(), continentId,false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("pays -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les payss", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCode(dto.getCode());
				}
				if (Utilities.notBlank(dto.getLibelle())) {
					Pays existingEntity = paysRepository.findByLibelleAndContinentId(dto.getLibelle(), continentId,false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("pays -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les payss", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
//				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
//					entityToSave.setCreatedBy(dto.getCreatedBy());
//				}
//				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
//					entityToSave.setUpdatedBy(dto.getUpdatedBy());
//				}
//				if (Utilities.notBlank(dto.getDeletedAt())) {
//					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
//				}
//				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
//					entityToSave.setDeletedBy(dto.getDeletedBy());
//				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Pays> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = paysRepository.save((Iterable<Pays>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("pays", locale));
					response.setHasError(true);
					return response;
				}
				List<PaysDto> itemsDto = new ArrayList<PaysDto>();
				for (Pays entity : itemsSaved) {
					PaysDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update Pays-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete Pays by using PaysDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<PaysDto> delete(Request<PaysDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete Pays-----");

		response = new Response<PaysDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Pays> items = new ArrayList<Pays>();

			for (PaysDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la pays existe
				Pays existingEntity = null;
				existingEntity = paysRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pays -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// appelDoffres
//				List<AppelDoffres> listOfAppelDoffres = appelDoffresRepository.findByPaysId(existingEntity.getId(), false);
//				if (listOfAppelDoffres != null && !listOfAppelDoffres.isEmpty()){
//					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfAppelDoffres.size() + ")", locale));
//					response.setHasError(true);
//					return response;
//				}
//				// entreprise
//				List<Entreprise> listOfEntreprise = entrepriseRepository.findByPaysId(existingEntity.getId(), false);
//				if (listOfEntreprise != null && !listOfEntreprise.isEmpty()){
//					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfEntreprise.size() + ")", locale));
//					response.setHasError(true);
//					return response;
//				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				paysRepository.save((Iterable<Pays>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete Pays-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete Pays by using PaysDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<PaysDto> forceDelete(Request<PaysDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete Pays-----");

		response = new Response<PaysDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Pays> items = new ArrayList<Pays>();

			for (PaysDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la pays existe
				Pays existingEntity = null;
				existingEntity = paysRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pays -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

//				// appelDoffres
//				List<AppelDoffres> listOfAppelDoffres = appelDoffresRepository.findByPaysId(existingEntity.getId(), false);
//				if (listOfAppelDoffres != null && !listOfAppelDoffres.isEmpty()){
//					Request<AppelDoffresDto> deleteRequest = new Request<AppelDoffresDto>();
//					deleteRequest.setDatas(AppelDoffresTransformer.INSTANCE.toDtos(listOfAppelDoffres));
//					deleteRequest.setUser(request.getUser());
//					Response<AppelDoffresDto> deleteResponse = appelDoffresBusiness.delete(deleteRequest, locale);
//					if(deleteResponse.isHasError()){
//						response.setStatus(deleteResponse.getStatus());
//						response.setHasError(true);
//						return response;
//					}
//				}
//				// entreprise
//				List<Entreprise> listOfEntreprise = entrepriseRepository.findByPaysId(existingEntity.getId(), false);
//				if (listOfEntreprise != null && !listOfEntreprise.isEmpty()){
//					Request<EntrepriseDto> deleteRequest = new Request<EntrepriseDto>();
//					deleteRequest.setDatas(EntrepriseTransformer.INSTANCE.toDtos(listOfEntreprise));
//					deleteRequest.setUser(request.getUser());
//					Response<EntrepriseDto> deleteResponse = entrepriseBusiness.delete(deleteRequest, locale);
//					if(deleteResponse.isHasError()){
//						response.setStatus(deleteResponse.getStatus());
//						response.setHasError(true);
//						return response;
//					}
//				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				paysRepository.save((Iterable<Pays>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete Pays-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get Pays by using PaysDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<PaysDto> getByCriteria(Request<PaysDto> request, Locale locale) {
		slf4jLogger.info("----begin get Pays-----");

		response = new Response<PaysDto>();

		try {
			List<Pays> items = null;
			items = paysRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<PaysDto> itemsDto = new ArrayList<PaysDto>();
				for (Pays entity : items) {
					PaysDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(paysRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("pays", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get Pays-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full PaysDto by using Pays as object.
	 *
	 * @param entity, locale
	 * @return PaysDto
	 *
	 */
	private PaysDto getFullInfos(Pays entity, Integer size, Locale locale) throws Exception {
		PaysDto dto = PaysTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		// renvoi des payss associé au continent
		List<Ville> datasVille = new ArrayList<Ville>();
		datasVille = villeRepository.findByPaysIdAndOderByLibelle(dto.getId(), false);
		if (Utilities.isNotEmpty(datasVille)) {
			dto.setDatasVille(VilleTransformer.INSTANCE.toDtos(datasVille));
		}
		
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
