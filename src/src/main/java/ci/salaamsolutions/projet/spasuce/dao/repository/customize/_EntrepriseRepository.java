package ci.salaamsolutions.projet.spasuce.dao.repository.customize;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;

/**
 * Repository customize : Entreprise.
 */
@Repository
public interface _EntrepriseRepository {
	default List<String> _generateCriteria(EntrepriseDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds Entreprise by using numeroFix as a search criteria.
	 *
	 * @param numeroFix
	 * @return An Object Entreprise whose numeroFix is equals to the given numeroFix. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.numeroFix = :numeroFix and e.isDeleted = :isDeleted")
	Entreprise findEntrepriseByNumeroFix(@Param("numeroFix")String numeroFix, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using numeroFax as a search criteria.
	 *
	 * @param numeroFax
	 * @return An Object Entreprise whose numeroFax is equals to the given numeroFax. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.numeroFax = :numeroFax and e.isDeleted = :isDeleted")
	Entreprise findEntrepriseByNumeroFax(@Param("numeroFax")String numeroFax, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using password as a search criteria.
	 *
	 * @param password
	 * @return An Object Entreprise whose password is equals to the given password. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.password = :password and e.isDeleted = :isDeleted")
	Entreprise findEntrepriseByPassword(@Param("password")String password, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using numeroImmatriculation as a search criteria.
	 *
	 * @param numeroImmatriculation
	 * @return An Object Entreprise whose numeroImmatriculation is equals to the given numeroImmatriculation. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.numeroImmatriculation = :numeroImmatriculation and e.isDeleted = :isDeleted")
	Entreprise findEntrepriseByNumeroImmatriculation(@Param("numeroImmatriculation")String numeroImmatriculation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using nom as a search criteria.
	 *
	 * @param nom
	 * @return An Object Entreprise whose nom is equals to the given nom. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.nom = :nom and e.isDeleted = :isDeleted")
	Entreprise findEntrepriseByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using numeroDuns as a search criteria.
	 *
	 * @param numeroDuns
	 * @return An Object Entreprise whose numeroDuns is equals to the given numeroDuns. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.numeroDuns = :numeroDuns and e.isDeleted = :isDeleted")
	Entreprise findEntrepriseByNumeroDuns(@Param("numeroDuns")String numeroDuns, @Param("isDeleted")Boolean isDeleted);
	
}
