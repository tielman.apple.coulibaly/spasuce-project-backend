

/*
 * Java transformer for entity table appel_doffres_restreint 
 * Created on 2020-02-08 ( Time 09:20:27 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "appel_doffres_restreint"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface AppelDoffresRestreintTransformer {

	AppelDoffresRestreintTransformer INSTANCE = Mappers.getMapper(AppelDoffresRestreintTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.user.id", target="userFournisseurId"),
				@Mapping(source="entity.user.nom", target="userNom"),
				@Mapping(source="entity.user.prenoms", target="userPrenoms"),
				@Mapping(source="entity.user.login", target="userLogin"),
		@Mapping(source="entity.appelDoffres.id", target="appelDoffresId"),
		@Mapping(source="entity.appelDoffres.libelle", target="appelDoffresLibelle"),
		@Mapping(source="entity.appelDoffres.code", target="appelDoffresCode"),
		@Mapping(source="entity.appelDoffres.ville.libelle", target="villeAppelDoffres"),// ajouter
		@Mapping(source="entity.appelDoffres.dateCreation", dateFormat="dd/MM/yyyy", target="dateCreationAppelDoffres"),// ajouter
		@Mapping(source="entity.appelDoffres.dateLimiteDepot", dateFormat="dd/MM/yyyy", target="dateLimiteDepotAppelDoffres"),// ajouter
		@Mapping(source="entity.appelDoffres.etat.libelle", target="etatAppelDoffres"), // ajouter
		@Mapping(source="entity.appelDoffres.isAttribuer", target="isAttribuerAppelDoffres"), // ajouter
		@Mapping(source="entity.appelDoffres.isContenuNotificationDispo", target="isContenuNotificationDispo"), // ajouter
	})
	AppelDoffresRestreintDto toDto(AppelDoffresRestreint entity) throws ParseException;

    List<AppelDoffresRestreintDto> toDtos(List<AppelDoffresRestreint> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.isDesactiver", target="isDesactiver"),
		@Mapping(source="dto.isSelectedByClient", target="isSelectedByClient"),
		@Mapping(source="dto.isVisibleByFournisseur", target="isVisibleByFournisseur"),
		@Mapping(source="dto.isVisualiserNotifByFournisseur", target="isVisualiserNotifByFournisseur"),
		@Mapping(source="dto.isVisualiserContenuByFournisseur", target="isVisualiserContenuByFournisseur"),
		@Mapping(source="dto.isUpdateAppelDoffres", target="isUpdateAppelDoffres"),
		@Mapping(source="dto.isSoumissionnerByFournisseur", target="isSoumissionnerByFournisseur"),
		@Mapping(source="dto.isAppelRestreint", target="isAppelRestreint"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="user", target="user"),
		@Mapping(source="appelDoffres", target="appelDoffres"),
	})
    AppelDoffresRestreint toEntity(AppelDoffresRestreintDto dto, User user, AppelDoffres appelDoffres) throws ParseException;

    //List<AppelDoffresRestreint> toEntities(List<AppelDoffresRestreintDto> dtos) throws ParseException;

}
