package ci.salaamsolutions.projet.spasuce.helper.dto.customize;

import lombok.Data;

@Data
public class _Image {

	protected String fileBase64;
	protected String extension;
	protected String fileName;
	protected String fileType;
	
}
