package ci.salaamsolutions.projet.spasuce.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.dao.entity.Pays;
import ci.salaamsolutions.projet.spasuce.helper.dto.PaysDto;

/**
 * Repository customize : Pays.
 */
@Repository
public interface _PaysRepository {
	default List<String> _generateCriteria(PaysDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds Pays by using code and continentId as a search criteria.
	 *
	 * @param code, continentId
	 * @return An Object Pays whose code and continentId are equals to the given code and continentId. If
	 *         no Pays is found, this method returns null.
	 */
	@Query("select e from Pays e where e.code = :code and e.continent.id = :continentId and e.isDeleted = :isDeleted")
	Pays findByCodeAndContinentId(@Param("code")String code, @Param("continentId")Integer continentId, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Pays by using libelle and continentId as a search criteria.
	 *
	 * @param libelle, continentId
	 * @return An Object Pays whose libelle and continentId are equals to the given libelle and continentId. If
	 *         no Pays is found, this method returns null.
	 */
	@Query("select e from Pays e where e.libelle = :libelle and e.continent.id = :continentId and e.isDeleted = :isDeleted")
	Pays findByLibelleAndContinentId(@Param("libelle")String libelle, @Param("continentId")Integer continentId,  @Param("isDeleted")Boolean isDeleted);

}
