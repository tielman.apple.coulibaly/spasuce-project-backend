

/*
 * Java transformer for entity table adresse_fournisseur 
 * Created on 2020-01-02 ( Time 17:59:52 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "adresse_fournisseur"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface AdresseFournisseurTransformer {

	AdresseFournisseurTransformer INSTANCE = Mappers.getMapper(AdresseFournisseurTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.carnetDadresse.id", target="carnetDadresseId"),
				@Mapping(source="entity.carnetDadresse.libelle", target="carnetDadresseLibelle"),
	})
	AdresseFournisseurDto toDto(AdresseFournisseur entity) throws ParseException;

    List<AdresseFournisseurDto> toDtos(List<AdresseFournisseur> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.prenoms", target="prenoms"),
		@Mapping(source="dto.email", target="email"),
		@Mapping(source="dto.telephone", target="telephone"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="carnetDadresse", target="carnetDadresse"),
	})
    AdresseFournisseur toEntity(AdresseFournisseurDto dto, CarnetDadresse carnetDadresse) throws ParseException;

    //List<AdresseFournisseur> toEntities(List<AdresseFournisseurDto> dtos) throws ParseException;

}
