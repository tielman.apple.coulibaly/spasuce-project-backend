


                                                                                /*
 * Java transformer for entity table groupe
 * Created on 2020-01-02 ( Time 17:59:59 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "groupe"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class GroupeBusiness implements IBasicBusiness<Request<GroupeDto>, Response<GroupeDto>> {

  private Response<GroupeDto> response;
  @Autowired
  private GroupeRepository groupeRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
    private GroupeAdresseFournisseurRepository groupeAdresseFournisseurRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

                      
  @Autowired
  private GroupeAdresseFournisseurBusiness groupeAdresseFournisseurBusiness;

      

  public GroupeBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create Groupe by using GroupeDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<GroupeDto> create(Request<GroupeDto> request, Locale locale)  {
    slf4jLogger.info("----begin create Groupe-----");

    response = new Response<GroupeDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<Groupe> items = new ArrayList<Groupe>();

      for (GroupeDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("code", dto.getCode());
        fieldsToVerify.put("libelle", dto.getLibelle());
        fieldsToVerify.put("description", dto.getDescription());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if groupe to insert do not exist
        Groupe existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("groupe -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
        existingEntity = groupeRepository.findByCode(dto.getCode(), false);
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("groupe -> " + dto.getCode(), locale));
          response.setHasError(true);
          return response;
        }
        if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
          response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les groupes", locale));
          response.setHasError(true);
          return response;
        }
        existingEntity = groupeRepository.findByLibelle(dto.getLibelle(), false);
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("groupe -> " + dto.getLibelle(), locale));
          response.setHasError(true);
          return response;
        }
        if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
          response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les groupes", locale));
          response.setHasError(true);
          return response;
        }

        Groupe entityToSave = null;
        entityToSave = GroupeTransformer.INSTANCE.toEntity(dto);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        entityToSave.setIsDeleted(false);
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<Groupe> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = groupeRepository.save((Iterable<Groupe>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("groupe", locale));
          response.setHasError(true);
          return response;
        }
        List<GroupeDto> itemsDto = new ArrayList<GroupeDto>();
        for (Groupe entity : itemsSaved) {
          GroupeDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create Groupe-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update Groupe by using GroupeDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<GroupeDto> update(Request<GroupeDto> request, Locale locale)  {
    slf4jLogger.info("----begin update Groupe-----");

    response = new Response<GroupeDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<Groupe> items = new ArrayList<Groupe>();

      for (GroupeDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la groupe existe
        Groupe entityToSave = null;
        entityToSave = groupeRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("groupe -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        if (Utilities.notBlank(dto.getCode())) {
                        Groupe existingEntity = groupeRepository.findByCode(dto.getCode(), false);
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("groupe -> " + dto.getCode(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les groupes", locale));
                  response.setHasError(true);
                  return response;
                }
                  entityToSave.setCode(dto.getCode());
        }
        if (Utilities.notBlank(dto.getLibelle())) {
                        Groupe existingEntity = groupeRepository.findByLibelle(dto.getLibelle(), false);
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("groupe -> " + dto.getLibelle(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les groupes", locale));
                  response.setHasError(true);
                  return response;
                }
                  entityToSave.setLibelle(dto.getLibelle());
        }
        if (Utilities.notBlank(dto.getDescription())) {
                  entityToSave.setDescription(dto.getDescription());
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<Groupe> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = groupeRepository.save((Iterable<Groupe>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("groupe", locale));
          response.setHasError(true);
          return response;
        }
        List<GroupeDto> itemsDto = new ArrayList<GroupeDto>();
        for (Groupe entity : itemsSaved) {
          GroupeDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update Groupe-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete Groupe by using GroupeDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<GroupeDto> delete(Request<GroupeDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete Groupe-----");

    response = new Response<GroupeDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<Groupe> items = new ArrayList<Groupe>();

      for (GroupeDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la groupe existe
        Groupe existingEntity = null;
        existingEntity = groupeRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("groupe -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

        // groupeAdresseFournisseur
        List<GroupeAdresseFournisseur> listOfGroupeAdresseFournisseur = groupeAdresseFournisseurRepository.findByGroupeId(existingEntity.getId(), false);
        if (listOfGroupeAdresseFournisseur != null && !listOfGroupeAdresseFournisseur.isEmpty()){
          response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfGroupeAdresseFournisseur.size() + ")", locale));
          response.setHasError(true);
          return response;
        }


        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        existingEntity.setIsDeleted(true);
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        groupeRepository.save((Iterable<Groupe>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete Groupe-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete Groupe by using GroupeDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<GroupeDto> forceDelete(Request<GroupeDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete Groupe-----");

    response = new Response<GroupeDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<Groupe> items = new ArrayList<Groupe>();

      for (GroupeDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la groupe existe
        Groupe existingEntity = null;
          existingEntity = groupeRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("groupe -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

                            // groupeAdresseFournisseur
        List<GroupeAdresseFournisseur> listOfGroupeAdresseFournisseur = groupeAdresseFournisseurRepository.findByGroupeId(existingEntity.getId(), false);
        if (listOfGroupeAdresseFournisseur != null && !listOfGroupeAdresseFournisseur.isEmpty()){
          Request<GroupeAdresseFournisseurDto> deleteRequest = new Request<GroupeAdresseFournisseurDto>();
          deleteRequest.setDatas(GroupeAdresseFournisseurTransformer.INSTANCE.toDtos(listOfGroupeAdresseFournisseur));
          deleteRequest.setUser(request.getUser());
          Response<GroupeAdresseFournisseurDto> deleteResponse = groupeAdresseFournisseurBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }
    

                                                                  existingEntity.setDeletedAt(Utilities.getCurrentDate());
                    existingEntity.setDeletedBy(request.getUser());
              existingEntity.setIsDeleted(true);
              items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          groupeRepository.save((Iterable<Groupe>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete Groupe-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get Groupe by using GroupeDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<GroupeDto> getByCriteria(Request<GroupeDto> request, Locale locale) {
    slf4jLogger.info("----begin get Groupe-----");

    response = new Response<GroupeDto>();

    try {
      List<Groupe> items = null;
      items = groupeRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<GroupeDto> itemsDto = new ArrayList<GroupeDto>();
        for (Groupe entity : items) {
          GroupeDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(groupeRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("groupe", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get Groupe-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full GroupeDto by using Groupe as object.
   *
   * @param entity, locale
   * @return GroupeDto
   *
   */
  private GroupeDto getFullInfos(Groupe entity, Integer size, Locale locale) throws Exception {
    GroupeDto dto = GroupeTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
