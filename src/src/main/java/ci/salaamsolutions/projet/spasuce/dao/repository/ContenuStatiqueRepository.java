package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._ContenuStatiqueRepository;

/**
 * Repository : ContenuStatique.
 */
@Repository
public interface ContenuStatiqueRepository extends JpaRepository<ContenuStatique, Integer>, _ContenuStatiqueRepository {
	/**
	 * Finds ContenuStatique by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object ContenuStatique whose id is equals to the given id. If
	 *         no ContenuStatique is found, this method returns null.
	 */
	@Query("select e from ContenuStatique e where e.id = :id and e.isDeleted = :isDeleted")
	ContenuStatique findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ContenuStatique by using contenu as a search criteria.
	 *
	 * @param contenu
	 * @return An Object ContenuStatique whose contenu is equals to the given contenu. If
	 *         no ContenuStatique is found, this method returns null.
	 */
	@Query("select e from ContenuStatique e where e.contenu = :contenu and e.isDeleted = :isDeleted")
	List<ContenuStatique> findByContenu(@Param("contenu")String contenu, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ContenuStatique by using titre as a search criteria.
	 *
	 * @param titre
	 * @return An Object ContenuStatique whose titre is equals to the given titre. If
	 *         no ContenuStatique is found, this method returns null.
	 */
	@Query("select e from ContenuStatique e where e.titre = :titre and e.isDeleted = :isDeleted")
	List<ContenuStatique> findByTitre(@Param("titre")String titre, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ContenuStatique by using ordre as a search criteria.
	 *
	 * @param ordre
	 * @return An Object ContenuStatique whose ordre is equals to the given ordre. If
	 *         no ContenuStatique is found, this method returns null.
	 */
	@Query("select e from ContenuStatique e where e.ordre = :ordre and e.isDeleted = :isDeleted")
	List<ContenuStatique> findByOrdre(@Param("ordre")Integer ordre, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ContenuStatique by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object ContenuStatique whose createdAt is equals to the given createdAt. If
	 *         no ContenuStatique is found, this method returns null.
	 */
	@Query("select e from ContenuStatique e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<ContenuStatique> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ContenuStatique by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object ContenuStatique whose createdBy is equals to the given createdBy. If
	 *         no ContenuStatique is found, this method returns null.
	 */
	@Query("select e from ContenuStatique e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<ContenuStatique> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ContenuStatique by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object ContenuStatique whose updatedAt is equals to the given updatedAt. If
	 *         no ContenuStatique is found, this method returns null.
	 */
	@Query("select e from ContenuStatique e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<ContenuStatique> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ContenuStatique by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object ContenuStatique whose updatedBy is equals to the given updatedBy. If
	 *         no ContenuStatique is found, this method returns null.
	 */
	@Query("select e from ContenuStatique e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<ContenuStatique> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ContenuStatique by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object ContenuStatique whose deletedAt is equals to the given deletedAt. If
	 *         no ContenuStatique is found, this method returns null.
	 */
	@Query("select e from ContenuStatique e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<ContenuStatique> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ContenuStatique by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object ContenuStatique whose deletedBy is equals to the given deletedBy. If
	 *         no ContenuStatique is found, this method returns null.
	 */
	@Query("select e from ContenuStatique e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<ContenuStatique> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ContenuStatique by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object ContenuStatique whose isDeleted is equals to the given isDeleted. If
	 *         no ContenuStatique is found, this method returns null.
	 */
	@Query("select e from ContenuStatique e where e.isDeleted = :isDeleted")
	List<ContenuStatique> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ContenuStatique by using typeContenuStatiqueId as a search criteria.
	 *
	 * @param typeContenuStatiqueId
	 * @return A list of Object ContenuStatique whose typeContenuStatiqueId is equals to the given typeContenuStatiqueId. If
	 *         no ContenuStatique is found, this method returns null.
	 */
	@Query("select e from ContenuStatique e where e.typeContenuStatique.id = :typeContenuStatiqueId and e.isDeleted = :isDeleted")
	List<ContenuStatique> findByTypeContenuStatiqueId(@Param("typeContenuStatiqueId")Integer typeContenuStatiqueId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one ContenuStatique by using typeContenuStatiqueId as a search criteria.
   *
   * @param typeContenuStatiqueId
   * @return An Object ContenuStatique whose typeContenuStatiqueId is equals to the given typeContenuStatiqueId. If
   *         no ContenuStatique is found, this method returns null.
   */
  @Query("select e from ContenuStatique e where e.typeContenuStatique.id = :typeContenuStatiqueId and e.isDeleted = :isDeleted")
  ContenuStatique findContenuStatiqueByTypeContenuStatiqueId(@Param("typeContenuStatiqueId")Integer typeContenuStatiqueId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of ContenuStatique by using contenuStatiqueDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of ContenuStatique
	 * @throws DataAccessException,ParseException
	 */
	public default List<ContenuStatique> getByCriteria(Request<ContenuStatiqueDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from ContenuStatique e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		//req += " order by e.ordre asc";
		TypedQuery<ContenuStatique> query = em.createQuery(req, ContenuStatique.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}
	
	

	/**
	 * Finds count of ContenuStatique by using contenuStatiqueDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of ContenuStatique
	 *
	 */
	public default Long count(Request<ContenuStatiqueDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from ContenuStatique e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<ContenuStatiqueDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		ContenuStatiqueDto dto = request.getData() != null ? request.getData() : new ContenuStatiqueDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (ContenuStatiqueDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(ContenuStatiqueDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getContenu())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("contenu", dto.getContenu(), "e.contenu", "String", dto.getContenuParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTitre())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("titre", dto.getTitre(), "e.titre", "String", dto.getTitreParam(), param, index, locale));
			}
			if (dto.getOrdre()!= null && dto.getOrdre() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ordre", dto.getOrdre(), "e.ordre", "Integer", dto.getOrdreParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getTypeContenuStatiqueId()!= null && dto.getTypeContenuStatiqueId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeContenuStatiqueId", dto.getTypeContenuStatiqueId(), "e.typeContenuStatique.id", "Integer", dto.getTypeContenuStatiqueIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeContenuStatiqueCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeContenuStatiqueCode", dto.getTypeContenuStatiqueCode(), "e.typeContenuStatique.code", "String", dto.getTypeContenuStatiqueCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeContenuStatiqueLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeContenuStatiqueLibelle", dto.getTypeContenuStatiqueLibelle(), "e.typeContenuStatique.libelle", "String", dto.getTypeContenuStatiqueLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
