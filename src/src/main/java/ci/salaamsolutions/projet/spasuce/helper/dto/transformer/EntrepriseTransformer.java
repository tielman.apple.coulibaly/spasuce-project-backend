

/*
 * Java transformer for entity table entreprise 
 * Created on 2020-02-07 ( Time 09:02:06 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "entreprise"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface EntrepriseTransformer {

	EntrepriseTransformer INSTANCE = Mappers.getMapper(EntrepriseTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.ville.pays.id", target="paysId"), // ajouter
		@Mapping(source="entity.ville.id", target="villeId"),
				@Mapping(source="entity.ville.code", target="villeCode"),
				@Mapping(source="entity.ville.libelle", target="villeLibelle"),
				@Mapping(source="entity.ville.pays.libelle", target="paysLibelle"),
		@Mapping(source="entity.statutJuridique.id", target="statutJuridiqueId"),
				@Mapping(source="entity.statutJuridique.code", target="statutJuridiqueCode"),
				@Mapping(source="entity.statutJuridique.libelle", target="statutJuridiqueLibelle"),
		@Mapping(source="entity.classeNote.id", target="classeNoteId"),
				@Mapping(source="entity.classeNote.code", target="classeNoteCode"),
				@Mapping(source="entity.classeNote.libelle", target="classeNoteLibelle"),
		@Mapping(source="entity.typeEntreprise.id", target="typeEntrepriseId"),
				@Mapping(source="entity.typeEntreprise.code", target="typeEntrepriseCode"),
				@Mapping(source="entity.typeEntreprise.libelle", target="typeEntrepriseLibelle"),
	})
	EntrepriseDto toDto(Entreprise entity) throws ParseException;

    List<EntrepriseDto> toDtos(List<Entreprise> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.email", target="email"),
		@Mapping(source="dto.numeroImmatriculation", target="numeroImmatriculation"),
		@Mapping(source="dto.numeroDuns", target="numeroDuns"),
		@Mapping(source="dto.password", target="password"),
		@Mapping(source="dto.numeroFix", target="numeroFix"),
		@Mapping(source="dto.numeroFax", target="numeroFax"),
		@Mapping(source="dto.telephone", target="telephone"),
		@Mapping(source="dto.login", target="login"),
		@Mapping(source="dto.urlLogo", target="urlLogo"),
		@Mapping(source="dto.extensionLogo", target="extensionLogo"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.note", target="note"),
		@Mapping(source="dto.isLocked", target="isLocked"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="ville", target="ville"),
		@Mapping(source="statutJuridique", target="statutJuridique"),
		@Mapping(source="classeNote", target="classeNote"),
		@Mapping(source="typeEntreprise", target="typeEntreprise"),
	})
    Entreprise toEntity(EntrepriseDto dto, Ville ville, StatutJuridique statutJuridique, ClasseNote classeNote, TypeEntreprise typeEntreprise) throws ParseException;

    //List<Entreprise> toEntities(List<EntrepriseDto> dtos) throws ParseException;

}
