


/*
 * Java transformer for entity table contenu_statique
 * Created on 2020-01-02 ( Time 17:59:54 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "contenu_statique"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class ContenuStatiqueBusiness implements IBasicBusiness<Request<ContenuStatiqueDto>, Response<ContenuStatiqueDto>> {

	private Response<ContenuStatiqueDto> response;
	@Autowired
	private ContenuStatiqueRepository contenuStatiqueRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TypeContenuStatiqueRepository typeContenuStatiqueRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public ContenuStatiqueBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create ContenuStatique by using ContenuStatiqueDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ContenuStatiqueDto> create(Request<ContenuStatiqueDto> request, Locale locale)  {
		slf4jLogger.info("----begin create ContenuStatique-----");

		response = new Response<ContenuStatiqueDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ContenuStatique> items = new ArrayList<ContenuStatique>();

			for (ContenuStatiqueDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("contenu", dto.getContenu());
				fieldsToVerify.put("titre", dto.getTitre());
				fieldsToVerify.put("ordre", dto.getOrdre());
				fieldsToVerify.put("typeContenuStatiqueLibelle", dto.getTypeContenuStatiqueLibelle());
				//fieldsToVerify.put("typeContenuStatiqueId", dto.getTypeContenuStatiqueId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if contenuStatique to insert do not exist
				ContenuStatique existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("contenuStatique -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if typeContenuStatique exist
				TypeContenuStatique existingTypeContenuStatique = typeContenuStatiqueRepository.findByLibelle(dto.getTypeContenuStatiqueLibelle(), false);
				//TypeContenuStatique existingTypeContenuStatique = typeContenuStatiqueRepository.findById(dto.getTypeContenuStatiqueId(), false);
				if (existingTypeContenuStatique == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeContenuStatique -> " + dto.getTypeContenuStatiqueLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				
//				existingEntity = contenuStatiqueRepository.findContenuStatiqueByTypeContenuStatiqueLibelle(dto.getTypeContenuStatiqueLibelle(), false);
//				if (existingEntity != null) {
//					response.setStatus(functionalError.DATA_EXIST("contenuStatique -> " + dto.getTypeContenuStatiqueLibelle(), locale));
//					response.setHasError(true);
//					return response;
//				}
				
				
				ContenuStatique entityToSave = null;
				entityToSave = ContenuStatiqueTransformer.INSTANCE.toEntity(dto, existingTypeContenuStatique);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ContenuStatique> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = contenuStatiqueRepository.save((Iterable<ContenuStatique>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("contenuStatique", locale));
					response.setHasError(true);
					return response;
				}
				List<ContenuStatiqueDto> itemsDto = new ArrayList<ContenuStatiqueDto>();
				for (ContenuStatique entity : itemsSaved) {
					ContenuStatiqueDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create ContenuStatique-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update ContenuStatique by using ContenuStatiqueDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ContenuStatiqueDto> update(Request<ContenuStatiqueDto> request, Locale locale)  {
		slf4jLogger.info("----begin update ContenuStatique-----");

		response = new Response<ContenuStatiqueDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ContenuStatique> items = new ArrayList<ContenuStatique>();

			for (ContenuStatiqueDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la contenuStatique existe
				ContenuStatique entityToSave = null;
				entityToSave = contenuStatiqueRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("contenuStatique -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();
				
				

				// Verify if typeContenuStatique exist
				if ( Utilities.notBlank(dto.getTypeContenuStatiqueLibelle())){
					
					//if (dto.getTypeContenuStatiqueId() != null && dto.getTypeContenuStatiqueId() > 0){
					//TypeContenuStatique existingTypeContenuStatique = typeContenuStatiqueRepository.findById(dto.getTypeContenuStatiqueId(), false);
					TypeContenuStatique existingTypeContenuStatique = typeContenuStatiqueRepository.findByLibelle(dto.getTypeContenuStatiqueLibelle(), false);
					if (existingTypeContenuStatique == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("typeContenuStatique -> " + dto.getTypeContenuStatiqueLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					
//					ContenuStatique existingEntity = contenuStatiqueRepository.findContenuStatiqueByTypeContenuStatiqueLibelle(dto.getTypeContenuStatiqueLibelle(), false);
//					if (existingEntity != null && !existingEntity.getId().equals(entityToSaveId)) {
//						response.setStatus(functionalError.DATA_EXIST("contenuStatique -> " + dto.getTypeContenuStatiqueLibelle(), locale));
//						response.setHasError(true);
//						return response;
//					}
					entityToSave.setTypeContenuStatique(existingTypeContenuStatique);
				}
				if (Utilities.notBlank(dto.getContenu())) {
					entityToSave.setContenu(dto.getContenu());
				}
				if (Utilities.notBlank(dto.getTitre())) {
					entityToSave.setTitre(dto.getTitre());
				}
				if (dto.getOrdre() != null && dto.getOrdre() > 0) {
					entityToSave.setOrdre(dto.getOrdre());
				}
				
				
//				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
//					entityToSave.setCreatedBy(dto.getCreatedBy());
//				}
//				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
//					entityToSave.setUpdatedBy(dto.getUpdatedBy());
//				}
//				if (Utilities.notBlank(dto.getDeletedAt())) {
//					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
//				}
//				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
//					entityToSave.setDeletedBy(dto.getDeletedBy());
//				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ContenuStatique> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = contenuStatiqueRepository.save((Iterable<ContenuStatique>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("contenuStatique", locale));
					response.setHasError(true);
					return response;
				}
				List<ContenuStatiqueDto> itemsDto = new ArrayList<ContenuStatiqueDto>();
				for (ContenuStatique entity : itemsSaved) {
					ContenuStatiqueDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update ContenuStatique-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete ContenuStatique by using ContenuStatiqueDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ContenuStatiqueDto> delete(Request<ContenuStatiqueDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete ContenuStatique-----");

		response = new Response<ContenuStatiqueDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ContenuStatique> items = new ArrayList<ContenuStatique>();

			for (ContenuStatiqueDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la contenuStatique existe
				ContenuStatique existingEntity = null;
				existingEntity = contenuStatiqueRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("contenuStatique -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				contenuStatiqueRepository.save((Iterable<ContenuStatique>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete ContenuStatique-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete ContenuStatique by using ContenuStatiqueDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<ContenuStatiqueDto> forceDelete(Request<ContenuStatiqueDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete ContenuStatique-----");

		response = new Response<ContenuStatiqueDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ContenuStatique> items = new ArrayList<ContenuStatique>();

			for (ContenuStatiqueDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la contenuStatique existe
				ContenuStatique existingEntity = null;
				existingEntity = contenuStatiqueRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("contenuStatique -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				contenuStatiqueRepository.save((Iterable<ContenuStatique>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete ContenuStatique-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get ContenuStatique by using ContenuStatiqueDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<ContenuStatiqueDto> getByCriteria(Request<ContenuStatiqueDto> request, Locale locale) {
		slf4jLogger.info("----begin get ContenuStatique-----");

		response = new Response<ContenuStatiqueDto>();

		try {
			List<ContenuStatique> items = null;
			items = contenuStatiqueRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<ContenuStatiqueDto> itemsDto = new ArrayList<ContenuStatiqueDto>();
				for (ContenuStatique entity : items) {
					ContenuStatiqueDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(contenuStatiqueRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("contenuStatique", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get ContenuStatique-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
	/**
	 * get ContenuStatique by using ContenuStatiqueDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	public Response<ContenuStatiqueDto> getByCriteriaCustom(Request<ContenuStatiqueDto> request, Locale locale) {
		slf4jLogger.info("----begin getByCriteriaCustom ContenuStatique-----");

		response = new Response<ContenuStatiqueDto>();

		try {
			List<ContenuStatique> items = null;
			items = contenuStatiqueRepository.getByCriteriaCustom(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<ContenuStatiqueDto> itemsDto = new ArrayList<ContenuStatiqueDto>();
				for (ContenuStatique entity : items) {
					ContenuStatiqueDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(contenuStatiqueRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("contenuStatique", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end getByCriteriaCustom ContenuStatique-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ContenuStatiqueDto by using ContenuStatique as object.
	 *
	 * @param entity, locale
	 * @return ContenuStatiqueDto
	 *
	 */
	private ContenuStatiqueDto getFullInfos(ContenuStatique entity, Integer size, Locale locale) throws Exception {
		ContenuStatiqueDto dto = ContenuStatiqueTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		
		//dto.setContenu(Utilities.cleanHtmlFrom(dto.getContenu()));
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
