
/*
 * Java dto for entity table appel_doffres
 * Created on 2020-03-03 ( Time 07:21:47 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.customize._AppelDoffresDto;

import lombok.*;
/**
 * DTO for table "appel_doffres"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class AppelDoffresDto extends _AppelDoffresDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private String     libelle              ;
	/*
	 * 
	 */
    private String     code                 ;
	/*
	 * 
	 */
    private Integer    prixPrestation       ;
	/*
	 * 
	 */
    private Integer    nombreTypeDuree      ;
	/*
	 * 
	 */
    private String     description          ;
	/*
	 * 
	 */
	private String     dateLimiteDepot      ;
	/*
	 * 
	 */
	private String     dateCreation         ;
	/*
	 * 
	 */
    private String     dureRealisation      ;
	/*
	 * Setter quand le client décide de fermer son offre
	 */
    private Boolean    isLocked             ;
	/*
	 * Pour dire que l'AO a des contenus d'offres non visualiser
	 */
    private Boolean    isContenuNotificationDispo ;
	/*
	 * 
	 */
    private Boolean    isAttribuer          ;
	/*
	 * Pour savoir si le client peut toujours modifier l'appel d'offres
	 */
    private Boolean    isModifiable         ;
	/*
	 * Pour dire que l'appel d'offres a été retirer ou le fournisseur a été retirer de l'appel d'offres
	 */
    private Boolean    isRetirer            ;
	/*
	 * Pour les suppressions logique ou partielle
	 */
    private Boolean    isVisibleByClient    ;
	/*
	 * Pour dire que l'AO a été modifier après soumission dans le système
	 */
    private Boolean    isUpdate             ;
	/*
	 * 
	 */
    private Integer    typeDureeId          ;
	/*
	 * 
	 */
    private Integer    villeId              ;
	/*
	 * 
	 */
    private Integer    typeAppelDoffresId   ;
	/*
	 * 
	 */
    private Integer    userClientId         ;
	/*
	 * 
	 */
    private Integer    etatId               ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String villeCode;
	private String villeLibelle;
	private String etatCode;
	private String etatLibelle;
	private String typeDureeLibelle;
	private String typeDureeCode;
	private String typeAppelDoffresCode;
	private String typeAppelDoffresLibelle;
	private String userNom;
	private String userPrenoms;
	private String userLogin;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   libelleParam          ;                     
	private SearchParam<String>   codeParam             ;                     
	private SearchParam<Integer>  prixPrestationParam   ;                     
	private SearchParam<Integer>  nombreTypeDureeParam  ;                     
	private SearchParam<String>   descriptionParam      ;                     

		private SearchParam<String>   dateLimiteDepotParam  ;                     

		private SearchParam<String>   dateCreationParam     ;                     
	private SearchParam<String>   dureRealisationParam  ;                     
	private SearchParam<Boolean>  isLockedParam         ;                     
	private SearchParam<Boolean>  isContenuNotificationDispoParam;                     
	private SearchParam<Boolean>  isAttribuerParam      ;                     
	private SearchParam<Boolean>  isModifiableParam     ;                     
	private SearchParam<Boolean>  isRetirerParam        ;                     
	private SearchParam<Boolean>  isVisibleByClientParam;                     
	private SearchParam<Boolean>  isUpdateParam         ;                     
	private SearchParam<Integer>  typeDureeIdParam      ;                     
	private SearchParam<Integer>  villeIdParam          ;                     
	private SearchParam<Integer>  typeAppelDoffresIdParam;                     
	private SearchParam<Integer>  userClientIdParam     ;                     
	private SearchParam<Integer>  etatIdParam           ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   villeCodeParam        ;                     
	private SearchParam<String>   villeLibelleParam     ;                     
	private SearchParam<String>   etatCodeParam         ;                     
	private SearchParam<String>   etatLibelleParam      ;                     
	private SearchParam<String>   typeDureeLibelleParam ;                     
	private SearchParam<String>   typeDureeCodeParam    ;                     
	private SearchParam<String>   typeAppelDoffresCodeParam;                     
	private SearchParam<String>   typeAppelDoffresLibelleParam;                     
	private SearchParam<String>   userNomParam          ;                     
	private SearchParam<String>   userPrenomsParam      ;                     
	private SearchParam<String>   userLoginParam        ;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		AppelDoffresDto other = (AppelDoffresDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
