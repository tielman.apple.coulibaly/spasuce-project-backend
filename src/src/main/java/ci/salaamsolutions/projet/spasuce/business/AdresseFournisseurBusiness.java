


                                                                                            /*
 * Java transformer for entity table adresse_fournisseur
 * Created on 2020-01-02 ( Time 17:59:52 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "adresse_fournisseur"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class AdresseFournisseurBusiness implements IBasicBusiness<Request<AdresseFournisseurDto>, Response<AdresseFournisseurDto>> {

  private Response<AdresseFournisseurDto> response;
  @Autowired
  private AdresseFournisseurRepository adresseFournisseurRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
    private GroupeAdresseFournisseurRepository groupeAdresseFournisseurRepository;
  @Autowired
  private CarnetDadresseRepository carnetDadresseRepository;
  
  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

                      
  @Autowired
  private GroupeAdresseFournisseurBusiness groupeAdresseFournisseurBusiness;

          

  public AdresseFournisseurBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create AdresseFournisseur by using AdresseFournisseurDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<AdresseFournisseurDto> create(Request<AdresseFournisseurDto> request, Locale locale)  {
    slf4jLogger.info("----begin create AdresseFournisseur-----");

    response = new Response<AdresseFournisseurDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<AdresseFournisseur> items = new ArrayList<AdresseFournisseur>();

      for (AdresseFournisseurDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("nom", dto.getNom());
        fieldsToVerify.put("prenoms", dto.getPrenoms());
        fieldsToVerify.put("email", dto.getEmail());
        fieldsToVerify.put("telephone", dto.getTelephone());
        fieldsToVerify.put("carnetDadresseId", dto.getCarnetDadresseId());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if adresseFournisseur to insert do not exist
        AdresseFournisseur existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("adresseFournisseur -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
        existingEntity = adresseFournisseurRepository.findByEmail(dto.getEmail(), false);
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("adresseFournisseur -> " + dto.getEmail(), locale));
          response.setHasError(true);
          return response;
        }
        if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()))) {
          response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les adresseFournisseurs", locale));
          response.setHasError(true);
          return response;
        }
        existingEntity = adresseFournisseurRepository.findByTelephone(dto.getTelephone(), false);
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("adresseFournisseur -> " + dto.getTelephone(), locale));
          response.setHasError(true);
          return response;
        }
        if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()))) {
          response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les adresseFournisseurs", locale));
          response.setHasError(true);
          return response;
        }

        // Verify if carnetDadresse exist
                CarnetDadresse existingCarnetDadresse = carnetDadresseRepository.findById(dto.getCarnetDadresseId(), false);
                if (existingCarnetDadresse == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("carnetDadresse -> " + dto.getCarnetDadresseId(), locale));
          response.setHasError(true);
          return response;
        }
        AdresseFournisseur entityToSave = null;
        entityToSave = AdresseFournisseurTransformer.INSTANCE.toEntity(dto, existingCarnetDadresse);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        entityToSave.setIsDeleted(false);
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<AdresseFournisseur> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = adresseFournisseurRepository.save((Iterable<AdresseFournisseur>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("adresseFournisseur", locale));
          response.setHasError(true);
          return response;
        }
        List<AdresseFournisseurDto> itemsDto = new ArrayList<AdresseFournisseurDto>();
        for (AdresseFournisseur entity : itemsSaved) {
          AdresseFournisseurDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create AdresseFournisseur-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update AdresseFournisseur by using AdresseFournisseurDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<AdresseFournisseurDto> update(Request<AdresseFournisseurDto> request, Locale locale)  {
    slf4jLogger.info("----begin update AdresseFournisseur-----");

    response = new Response<AdresseFournisseurDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<AdresseFournisseur> items = new ArrayList<AdresseFournisseur>();

      for (AdresseFournisseurDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la adresseFournisseur existe
        AdresseFournisseur entityToSave = null;
        entityToSave = adresseFournisseurRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("adresseFournisseur -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if carnetDadresse exist
        if (dto.getCarnetDadresseId() != null && dto.getCarnetDadresseId() > 0){
                    CarnetDadresse existingCarnetDadresse = carnetDadresseRepository.findById(dto.getCarnetDadresseId(), false);
          if (existingCarnetDadresse == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("carnetDadresse -> " + dto.getCarnetDadresseId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setCarnetDadresse(existingCarnetDadresse);
        }
        if (Utilities.notBlank(dto.getNom())) {
                  entityToSave.setNom(dto.getNom());
        }
        if (Utilities.notBlank(dto.getPrenoms())) {
                  entityToSave.setPrenoms(dto.getPrenoms());
        }
        if (Utilities.notBlank(dto.getEmail())) {
                        AdresseFournisseur existingEntity = adresseFournisseurRepository.findByEmail(dto.getEmail(), false);
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("adresseFournisseur -> " + dto.getEmail(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les adresseFournisseurs", locale));
                  response.setHasError(true);
                  return response;
                }
                  entityToSave.setEmail(dto.getEmail());
        }
        if (Utilities.notBlank(dto.getTelephone())) {
                        AdresseFournisseur existingEntity = adresseFournisseurRepository.findByTelephone(dto.getTelephone(), false);
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("adresseFournisseur -> " + dto.getTelephone(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les adresseFournisseurs", locale));
                  response.setHasError(true);
                  return response;
                }
                  entityToSave.setTelephone(dto.getTelephone());
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<AdresseFournisseur> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = adresseFournisseurRepository.save((Iterable<AdresseFournisseur>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("adresseFournisseur", locale));
          response.setHasError(true);
          return response;
        }
        List<AdresseFournisseurDto> itemsDto = new ArrayList<AdresseFournisseurDto>();
        for (AdresseFournisseur entity : itemsSaved) {
          AdresseFournisseurDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update AdresseFournisseur-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete AdresseFournisseur by using AdresseFournisseurDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<AdresseFournisseurDto> delete(Request<AdresseFournisseurDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete AdresseFournisseur-----");

    response = new Response<AdresseFournisseurDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<AdresseFournisseur> items = new ArrayList<AdresseFournisseur>();

      for (AdresseFournisseurDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la adresseFournisseur existe
        AdresseFournisseur existingEntity = null;
        existingEntity = adresseFournisseurRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("adresseFournisseur -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

        // groupeAdresseFournisseur
        List<GroupeAdresseFournisseur> listOfGroupeAdresseFournisseur = groupeAdresseFournisseurRepository.findByAdresseFournisseurId(existingEntity.getId(), false);
        if (listOfGroupeAdresseFournisseur != null && !listOfGroupeAdresseFournisseur.isEmpty()){
          response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfGroupeAdresseFournisseur.size() + ")", locale));
          response.setHasError(true);
          return response;
        }


        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        existingEntity.setIsDeleted(true);
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        adresseFournisseurRepository.save((Iterable<AdresseFournisseur>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete AdresseFournisseur-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete AdresseFournisseur by using AdresseFournisseurDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<AdresseFournisseurDto> forceDelete(Request<AdresseFournisseurDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete AdresseFournisseur-----");

    response = new Response<AdresseFournisseurDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<AdresseFournisseur> items = new ArrayList<AdresseFournisseur>();

      for (AdresseFournisseurDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la adresseFournisseur existe
        AdresseFournisseur existingEntity = null;
          existingEntity = adresseFournisseurRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("adresseFournisseur -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

                            // groupeAdresseFournisseur
        List<GroupeAdresseFournisseur> listOfGroupeAdresseFournisseur = groupeAdresseFournisseurRepository.findByAdresseFournisseurId(existingEntity.getId(), false);
        if (listOfGroupeAdresseFournisseur != null && !listOfGroupeAdresseFournisseur.isEmpty()){
          Request<GroupeAdresseFournisseurDto> deleteRequest = new Request<GroupeAdresseFournisseurDto>();
          deleteRequest.setDatas(GroupeAdresseFournisseurTransformer.INSTANCE.toDtos(listOfGroupeAdresseFournisseur));
          deleteRequest.setUser(request.getUser());
          Response<GroupeAdresseFournisseurDto> deleteResponse = groupeAdresseFournisseurBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }
        

                                                                          existingEntity.setDeletedAt(Utilities.getCurrentDate());
                    existingEntity.setDeletedBy(request.getUser());
              existingEntity.setIsDeleted(true);
              items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          adresseFournisseurRepository.save((Iterable<AdresseFournisseur>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete AdresseFournisseur-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get AdresseFournisseur by using AdresseFournisseurDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<AdresseFournisseurDto> getByCriteria(Request<AdresseFournisseurDto> request, Locale locale) {
    slf4jLogger.info("----begin get AdresseFournisseur-----");

    response = new Response<AdresseFournisseurDto>();

    try {
      List<AdresseFournisseur> items = null;
      items = adresseFournisseurRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<AdresseFournisseurDto> itemsDto = new ArrayList<AdresseFournisseurDto>();
        for (AdresseFournisseur entity : items) {
          AdresseFournisseurDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(adresseFournisseurRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("adresseFournisseur", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get AdresseFournisseur-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full AdresseFournisseurDto by using AdresseFournisseur as object.
   *
   * @param entity, locale
   * @return AdresseFournisseurDto
   *
   */
  private AdresseFournisseurDto getFullInfos(AdresseFournisseur entity, Integer size, Locale locale) throws Exception {
    AdresseFournisseurDto dto = AdresseFournisseurTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
