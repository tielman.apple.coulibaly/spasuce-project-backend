


/*
 * Java transformer for entity table secteur_dactivite_appel_doffres
 * Created on 2020-01-31 ( Time 08:34:48 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffres;
import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffresRestreint;
import ci.salaamsolutions.projet.spasuce.dao.entity.Entreprise;
import ci.salaamsolutions.projet.spasuce.dao.entity.Offre;
import ci.salaamsolutions.projet.spasuce.dao.entity.SecteurDactivite;
import ci.salaamsolutions.projet.spasuce.dao.entity.SecteurDactiviteAppelDoffres;
import ci.salaamsolutions.projet.spasuce.dao.entity.SecteurDactiviteEntreprise;
import ci.salaamsolutions.projet.spasuce.dao.entity.User;
import ci.salaamsolutions.projet.spasuce.dao.entity.ValeurCritereOffre;
import ci.salaamsolutions.projet.spasuce.dao.repository.AppelDoffresRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.AppelDoffresRestreintRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.EntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.OffreRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.SecteurDactiviteAppelDoffresRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.SecteurDactiviteEntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.SecteurDactiviteRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.UserRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.ValeurCritereOffreRepository;
import ci.salaamsolutions.projet.spasuce.helper.ExceptionUtils;
import ci.salaamsolutions.projet.spasuce.helper.FunctionalError;
import ci.salaamsolutions.projet.spasuce.helper.TechnicalError;
import ci.salaamsolutions.projet.spasuce.helper.Utilities;
import ci.salaamsolutions.projet.spasuce.helper.Validate;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.contract.SearchParam;
import ci.salaamsolutions.projet.spasuce.helper.dto.AppelDoffresDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.CritereFiltreDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.EntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.PaysDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteAppelDoffresDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteEntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.AppelDoffresTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.SecteurDactiviteAppelDoffresTransformer;
import ci.salaamsolutions.projet.spasuce.helper.enums.EtatEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.GlobalEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.OperatorEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeAppelDoffresEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeUserEnum;

/**
BUSINESS for table "secteur_dactivite_appel_doffres"
 *
 * @author Back-End developper
 *
 */
@Component
public class SecteurDactiviteAppelDoffresBusiness implements IBasicBusiness<Request<SecteurDactiviteAppelDoffresDto>, Response<SecteurDactiviteAppelDoffresDto>> {

	private Response<SecteurDactiviteAppelDoffresDto> response;
	@Autowired
	private SecteurDactiviteAppelDoffresRepository secteurDactiviteAppelDoffresRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private SecteurDactiviteRepository secteurDactiviteRepository;
	@Autowired
	private AppelDoffresRepository appelDoffresRepository;
	@Autowired
	private SecteurDactiviteEntrepriseRepository secteurDactiviteEntrepriseRepository;
	@Autowired
	private OffreRepository offreRepository;

	@Autowired
	private AppelDoffresRestreintRepository appelDoffresRestreintRepository ;
	@Autowired
	private SecteurDactiviteEntrepriseBusiness secteurDactiviteEntrepriseBusiness ;
	@Autowired
	private ValeurCritereOffreRepository valeurCritereOffreRepository ;
	@Autowired
	private EntrepriseRepository entrepriseRepository ;

	
	







	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateFormatNew;



	public SecteurDactiviteAppelDoffresBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		dateFormatNew = new SimpleDateFormat("dd/MM/yyyy");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create SecteurDactiviteAppelDoffres by using SecteurDactiviteAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SecteurDactiviteAppelDoffresDto> create(Request<SecteurDactiviteAppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin create SecteurDactiviteAppelDoffres-----");

		response = new Response<SecteurDactiviteAppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			// type de user autorisé
			if (utilisateur.getEntreprise().getTypeEntreprise().getCode().equals(TypeUserEnum.ENTREPRISE_FOURNISSEUR)) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un client" , locale));
				response.setHasError(true);
				return response;
			}

			List<SecteurDactiviteAppelDoffres> items = new ArrayList<SecteurDactiviteAppelDoffres>();

			for (SecteurDactiviteAppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("appelDoffresId", dto.getAppelDoffresId());
				fieldsToVerify.put("secteurDactiviteId", dto.getSecteurDactiviteId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if secteurDactiviteAppelDoffres to insert do not exist
				SecteurDactiviteAppelDoffres existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("secteurDactiviteAppelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if secteurDactivite exist
				SecteurDactivite existingSecteurDactivite = secteurDactiviteRepository.findById(dto.getSecteurDactiviteId(), false);
				if (existingSecteurDactivite == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("secteurDactivite -> " + dto.getSecteurDactiviteId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if appelDoffres exist
				AppelDoffres existingAppelDoffres = appelDoffresRepository.findById(dto.getAppelDoffresId(), false);
				if (existingAppelDoffres == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getAppelDoffresId(), locale));
					response.setHasError(true);
					return response;
				}
				// doublons de secteurDactivite pour les appelDoffres
				if (!items.isEmpty()) {
					if (items.stream().anyMatch(a->a.getSecteurDactivite().getId().equals(dto.getSecteurDactiviteId()) && a.getAppelDoffres().getId().equals(dto.getAppelDoffresId()) )) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du secteur d'activité '" + dto.getSecteurDactiviteId()+"' pour les appelDoffress", locale));
						response.setHasError(true);
						return response;
					}
				}
				SecteurDactiviteAppelDoffres entityToSave = null;
				entityToSave = SecteurDactiviteAppelDoffresTransformer.INSTANCE.toEntity(dto, existingSecteurDactivite, existingAppelDoffres);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<SecteurDactiviteAppelDoffres> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = secteurDactiviteAppelDoffresRepository.save((Iterable<SecteurDactiviteAppelDoffres>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("secteurDactiviteAppelDoffres", locale));
					response.setHasError(true);
					return response;
				}
				List<SecteurDactiviteAppelDoffresDto> itemsDto = new ArrayList<SecteurDactiviteAppelDoffresDto>();
				for (SecteurDactiviteAppelDoffres entity : itemsSaved) {
					SecteurDactiviteAppelDoffresDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create SecteurDactiviteAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update SecteurDactiviteAppelDoffres by using SecteurDactiviteAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SecteurDactiviteAppelDoffresDto> update(Request<SecteurDactiviteAppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin update SecteurDactiviteAppelDoffres-----");

		response = new Response<SecteurDactiviteAppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SecteurDactiviteAppelDoffres> items = new ArrayList<SecteurDactiviteAppelDoffres>();

			for (SecteurDactiviteAppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la secteurDactiviteAppelDoffres existe
				SecteurDactiviteAppelDoffres entityToSave = null;
				entityToSave = secteurDactiviteAppelDoffresRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("secteurDactiviteAppelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if secteurDactivite exist
				if (dto.getSecteurDactiviteId() != null && dto.getSecteurDactiviteId() > 0){
					SecteurDactivite existingSecteurDactivite = secteurDactiviteRepository.findById(dto.getSecteurDactiviteId(), false);
					if (existingSecteurDactivite == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("secteurDactivite -> " + dto.getSecteurDactiviteId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setSecteurDactivite(existingSecteurDactivite);
				}
				// Verify if appelDoffres exist
				if (dto.getAppelDoffresId() != null && dto.getAppelDoffresId() > 0){
					AppelDoffres existingAppelDoffres = appelDoffresRepository.findById(dto.getAppelDoffresId(), false);
					if (existingAppelDoffres == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getAppelDoffresId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setAppelDoffres(existingAppelDoffres);
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				if (Utilities.notBlank(dto.getDeletedAt())) {
					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
				}
				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
					entityToSave.setDeletedBy(dto.getDeletedBy());
				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<SecteurDactiviteAppelDoffres> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = secteurDactiviteAppelDoffresRepository.save((Iterable<SecteurDactiviteAppelDoffres>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("secteurDactiviteAppelDoffres", locale));
					response.setHasError(true);
					return response;
				}
				List<SecteurDactiviteAppelDoffresDto> itemsDto = new ArrayList<SecteurDactiviteAppelDoffresDto>();
				for (SecteurDactiviteAppelDoffres entity : itemsSaved) {
					SecteurDactiviteAppelDoffresDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update SecteurDactiviteAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete SecteurDactiviteAppelDoffres by using SecteurDactiviteAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SecteurDactiviteAppelDoffresDto> delete(Request<SecteurDactiviteAppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete SecteurDactiviteAppelDoffres-----");

		response = new Response<SecteurDactiviteAppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SecteurDactiviteAppelDoffres> items = new ArrayList<SecteurDactiviteAppelDoffres>();

			for (SecteurDactiviteAppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la secteurDactiviteAppelDoffres existe
				SecteurDactiviteAppelDoffres existingEntity = null;
				existingEntity = secteurDactiviteAppelDoffresRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("secteurDactiviteAppelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				secteurDactiviteAppelDoffresRepository.save((Iterable<SecteurDactiviteAppelDoffres>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete SecteurDactiviteAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete SecteurDactiviteAppelDoffres by using SecteurDactiviteAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<SecteurDactiviteAppelDoffresDto> forceDelete(Request<SecteurDactiviteAppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete SecteurDactiviteAppelDoffres-----");

		response = new Response<SecteurDactiviteAppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SecteurDactiviteAppelDoffres> items = new ArrayList<SecteurDactiviteAppelDoffres>();

			for (SecteurDactiviteAppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la secteurDactiviteAppelDoffres existe
				SecteurDactiviteAppelDoffres existingEntity = null;
				existingEntity = secteurDactiviteAppelDoffresRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("secteurDactiviteAppelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				secteurDactiviteAppelDoffresRepository.save((Iterable<SecteurDactiviteAppelDoffres>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete SecteurDactiviteAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get SecteurDactiviteAppelDoffres by using SecteurDactiviteAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<SecteurDactiviteAppelDoffresDto> getByCriteria(Request<SecteurDactiviteAppelDoffresDto> request, Locale locale) {
		slf4jLogger.info("----begin get SecteurDactiviteAppelDoffres-----");

		response = new Response<SecteurDactiviteAppelDoffresDto>();

		try {
			List<SecteurDactiviteAppelDoffres> items = null;
			items = secteurDactiviteAppelDoffresRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<SecteurDactiviteAppelDoffresDto> itemsDto = new ArrayList<SecteurDactiviteAppelDoffresDto>();
				for (SecteurDactiviteAppelDoffres entity : items) {
					SecteurDactiviteAppelDoffresDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(secteurDactiviteAppelDoffresRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("secteurDactiviteAppelDoffres", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get SecteurDactiviteAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * getCustom SecteurDactiviteAppelDoffres by using SecteurDactiviteAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	public Response<AppelDoffresDto> getByCriteriaCustom(Request<SecteurDactiviteAppelDoffresDto> request, Locale locale) {
		slf4jLogger.info("----begin getByCriteriaCustom SecteurDactiviteAppelDoffres-----");

		Response<AppelDoffresDto> response = new Response<>();

		try {
			List<AppelDoffres> items = null;
			items = secteurDactiviteAppelDoffresRepository.getByCriteriaCustom(request, em, locale);
			if (items != null && !items.isEmpty()) {

				Collections.sort(items, new Comparator<AppelDoffres>() {
					@Override
					public int compare(AppelDoffres o1, AppelDoffres o2) {
						// TODO Auto-generated method stub
						//return o1.getId().compareTo(o2.getId())* - 1;
						//return o2.getId().compareTo(o1.getId());

						if (o2.getDateCreation() != null && o1.getDateCreation() != null ) {
							return o2.getDateCreation().compareTo(o1.getDateCreation());
						}
						return o2.getCreatedAt().compareTo(o1.getCreatedAt());
					}
				}); // trier la liste

				// faire les tris selons les dates

				if (Utilities.isNotEmpty(request.getCritereFiltreDtos())) {

					for (CritereFiltreDto data  : request.getCritereFiltreDtos()) {

						slf4jLogger.info("----begin items-----" + items);

						switch (data.getCode()) {
						case GlobalEnum.par_date_limite:
							Collections.sort(items, new Comparator<AppelDoffres>() {
								@Override
								public int compare(AppelDoffres o1, AppelDoffres o2) {
									// TODO Auto-generated method stub
									//return o1.getId().compareTo(o2.getId())* - 1;
									//return o2.getId().compareTo(o1.getId());

									//return o2.getDateLimiteDepot().compareTo(o1.getDateLimiteDepot());
									return o1.getDateLimiteDepot().compareTo(o2.getDateLimiteDepot());
								}
							}); // trier la liste


							slf4jLogger.info("----items apres tri par_date_limite -----" + items);


							break;

						case GlobalEnum.plus_ancien:
							Collections.sort(items, new Comparator<AppelDoffres>() {
								@Override
								public int compare(AppelDoffres o1, AppelDoffres o2) {
									// TODO Auto-generated method stub
									//return o1.getId().compareTo(o2.getId())* - 1;
									//return o2.getId().compareTo(o1.getId());

									if (o2.getDateCreation() != null && o1.getDateCreation() != null ) {
										return o1.getDateCreation().compareTo(o2.getDateCreation());
									}
									return o1.getCreatedAt().compareTo(o2.getCreatedAt());
								}
							}); // trier la liste
							slf4jLogger.info("----items apres tri plus_ancien -----" + items);

							break;

							//						case GlobalEnum.plus_recent:
							//							Collections.sort(items, new Comparator<AppelDoffres>() {
							//								@Override
							//								public int compare(AppelDoffres o1, AppelDoffres o2) {
							//									// TODO Auto-generated method stub
							//									//return o1.getId().compareTo(o2.getId())* - 1;
							//									//return o2.getId().compareTo(o1.getId());
							//
							//									if (o2.getDateCreation() != null && o1.getDateCreation() != null ) {
							//										return o2.getDateCreation().compareTo(o1.getDateCreation());
							//									}
							//									return o2.getCreatedAt().compareTo(o1.getCreatedAt());
							//								}
							//							}); // trier la liste
							//							slf4jLogger.info("----items apres tri plus_recent -----" + items);
							//
							//							break;

						default:
							break;
						}

					}

				}



				List<AppelDoffresDto> itemsDto = new ArrayList<AppelDoffresDto>();
				for (AppelDoffres entity : items) {
					AppelDoffresDto dto = AppelDoffresTransformer.INSTANCE.toDto(entity);
					if (dto == null) continue;

					//SecteurDactiviteAppelDoffresDto dto = getFullInfos(entity, items.size(), locale);
					//if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(secteurDactiviteAppelDoffresRepository.countCustom(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("secteurDactiviteAppelDoffres", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end getByCriteriaCustom SecteurDactiviteAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * getCustom SecteurDactiviteAppelDoffres by using SecteurDactiviteAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	public Response<SecteurDactiviteAppelDoffresDto> getStatistiques(Request<SecteurDactiviteAppelDoffresDto> request, Locale locale) {
		slf4jLogger.info("----begin getStatistiques SecteurDactiviteAppelDoffres-----");

		Response<SecteurDactiviteAppelDoffresDto> response = new Response<>();

		try {
			List<AppelDoffres> items = null;
			SecteurDactiviteAppelDoffresDto itemDto = new SecteurDactiviteAppelDoffresDto();
			
			//Integer nbreMixte = 0 ;
			//Integer nbreMixte = 0 ;


			// formation des datas envoyées

			SecteurDactiviteAppelDoffresDto dto = request.getData() ;
			
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("dateDebut", dto.getDateDebut());
			fieldsToVerify.put("dateFin", dto.getDateFin());
			//fieldsToVerify.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}
			
			
			Date dateDebut = dateFormatNew.parse(dto.getDateDebut());
			Date dateFin = dateFormatNew.parse(dto.getDateFin());
			if (dateDebut.after(dateFin)) {
				response.setStatus(functionalError.INVALID_DATA("La date de debut soit etre <= à la date de fin", locale));
				response.setHasError(true);
				return response;
			}

			
			Request<SecteurDactiviteEntrepriseDto> req = new Request<>();
			Response<EntrepriseDto> res = new Response<>();
			Request<SecteurDactiviteAppelDoffresDto> reqAO = new Request<>();
			//Response<EntrepriseDto> res = new Response<>();
			List<SecteurDactiviteEntrepriseDto> datas = new ArrayList<>();
			List<SecteurDactiviteAppelDoffresDto> datasSecteurDactiviteAppelDoffres = new ArrayList<>();

			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(dateDebut);
			//calendar.add(Calendar.MONTH, -11);
			//String currentDateString = "01" + dateFormatToSearch.format(calendar.getTime());			
			//Date dateFacture= dateFormatFront.parse(currentDateString);

			
			
			// 
			
			String dateDebutNew = "" ;
			String dateFinNew = "" ;
			Boolean isAnneeEcart = false ;
			
			Integer nbreClienteGlobal = 0 ;
			Integer nbreFournisseurGlobal = 0 ;
			Integer nbreMixteGlobal = 0 ;


			// verifier si l'ecart depasse des annees
			calendar.add(Calendar.YEAR, 1); // annee suivante
			if (dateFin.after(calendar.getTime()) || calendar.getTime().equals(dateFin)) {
				isAnneeEcart = true ;
			}
			calendar.add(Calendar.YEAR, -1); // annee normale


			// declaration des listes 
			List<Integer> nbreClienteY  = new ArrayList<>();
			 List<Integer> nbreFournisseurY = new ArrayList<>();
			 List<Integer> nbreMixteY = new ArrayList<>();

			 List<Integer> gainRecuY  = new ArrayList<>();
			 List<Integer> gainPrevuY = new ArrayList<>();
			
			 List<Integer> nbreAOY = new ArrayList<>();
			 List<Integer> nbreAORestreintY = new ArrayList<>() ;
			 List<Integer> nbreAOGeneralY= new ArrayList<>() ;
			 List<Integer> nbreAOConluY = new ArrayList<>();
			 List<Integer> nbreOffreY = new ArrayList<>();
			
			 List<String> abscisseGraphe = new ArrayList<>();
			
			
			while(dateFin.after(calendar.getTime())) {
				
				SecteurDactiviteEntrepriseDto defaultDto = new SecteurDactiviteEntrepriseDto() ;
				SecteurDactiviteAppelDoffresDto defaultDto2 = new SecteurDactiviteAppelDoffresDto() ;
				
				
				
				Integer nbreCliente = 0 ;
				Integer nbreFournisseur = 0 ;
				Integer nbreMixte = 0 ;


				Integer gainRecu = 0 ;
				Integer gainPrevu = 0 ;


				//Integer nbreAO = 0 ;
				Integer nbreAORestreint = 0 ;
				Integer nbreAOGeneral = 0 ;
				Integer nbreAOConlu = 0 ;
				Integer nbreOffre = 0 ;
				
				
				String xGraphe = "" ;
				
				
				dateDebutNew = dateFormatNew.format(calendar.getTime());
				
				if (isAnneeEcart) {
					xGraphe = ""+calendar.get(Calendar.YEAR) ;
					calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR)); // setter le dernier jour de l'annee

					//calendar.add(Calendar.YEAR, 1); // annee suivante
				}else {
					xGraphe = Utilities.getLibelleMonthByMonthId(calendar.get(Calendar.MONTH)) ;
					calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));// setter la date du mois à une valeur. La méthode est generale 

					//calendar.add(Calendar.MONTH, 1); // mois suivant
				}
				
				 
				if (calendar.getTime().after(dateFin) || calendar.getTime().equals(dateFin)) { // voir si le mois suivant est inferieur à la date de fin
					dateFinNew = dto.getDateFin() ;
				}else {					
					dateFinNew = dateFormatNew.format(calendar.getTime());					
				}
				
				// setter le calendar a un jour suivant
				calendar.add(Calendar.DAY_OF_YEAR, 1); // mois suivant


				defaultDto.setDateDebut(dateDebutNew);
				defaultDto.setDateFin(dateFinNew);
				
				System.out.println("dateDebutNew " +  dateDebutNew);
				System.out.println("dateFinNew " +  dateFinNew);
				
				
				defaultDto2.setDateDebut(dateDebutNew);
				defaultDto2.setDateFin(dateFinNew);
				
				req.setData(defaultDto) ;
				reqAO.setData(defaultDto2) ;
				
				
				// diviser la date
				
				
				if (Utilities.isNotEmpty(dto.getDatasSecteurDactivite()) || Utilities.isNotEmpty(dto.getDatasPays())) {

					if (Utilities.isNotEmpty(dto.getDatasSecteurDactivite())) {
						for (SecteurDactiviteDto data : dto.getDatasSecteurDactivite()) {

							SecteurDactiviteEntrepriseDto secteurDactiviteEntrepriseDto = new SecteurDactiviteEntrepriseDto();
							SecteurDactiviteAppelDoffresDto secteurDactiviteAppelDoffresDto = new SecteurDactiviteAppelDoffresDto();

							secteurDactiviteEntrepriseDto.setSecteurDactiviteLibelle(data.getLibelle());
							secteurDactiviteEntrepriseDto.setDateDebut(dateDebutNew);
							secteurDactiviteEntrepriseDto.setDateFin(dateFinNew);
							datas.add(secteurDactiviteEntrepriseDto);

							secteurDactiviteAppelDoffresDto.setSecteurDactiviteLibelle(data.getLibelle());
							secteurDactiviteAppelDoffresDto.setEtatCode(EtatEnum.ENTRANT);
							secteurDactiviteAppelDoffresDto.setDateDebut(dateDebutNew);
							secteurDactiviteAppelDoffresDto.setDateFin(dateFinNew);
							datasSecteurDactiviteAppelDoffres.add(secteurDactiviteAppelDoffresDto);
						}

						req.setData(datas.get(0));
						if (datas.size() > 1 ) {
							req.setDatas(datas.subList(1, datas.size())) ;
						}

						reqAO.setData(datasSecteurDactiviteAppelDoffres.get(0));
						if (datasSecteurDactiviteAppelDoffres.size() > 1 ) {
							reqAO.setDatas(datasSecteurDactiviteAppelDoffres.subList(1, datasSecteurDactiviteAppelDoffres.size())) ;
						}
					}

					//res = secteurDactiviteEntrepriseBusiness.getByCriteriaCustom(req, locale);
					//res = secteurDactiviteEntrepriseBusiness.getByCriteriaCustom(req, locale);
					List<Entreprise> itemsEntreprise = null;
					itemsEntreprise = secteurDactiviteEntrepriseRepository.getByCriteriaCustom(req, em, locale);
					if (Utilities.isNotEmpty(itemsEntreprise)) {
						if (Utilities.isNotEmpty(dto.getDatasPays())) {
							for (PaysDto data : dto.getDatasPays()) {
								for (Entreprise entreprise : itemsEntreprise) {
									if (entreprise.getVille() != null 
											&& entreprise.getVille().getPays().getLibelle().equals(data.getLibelle())) {

										switch (entreprise.getTypeEntreprise().getCode()) {
										case TypeUserEnum.ENTREPRISE_CLIENTE:
											nbreCliente ++ ;
											break;
										case TypeUserEnum.ENTREPRISE_FOURNISSEUR:
											nbreFournisseur ++ ;
											break;
										case TypeUserEnum.ENTREPRISE_MIXTE:
											nbreMixte ++ ;
											break;

										default:
											break;
										}
									}
								}
							}
						}else {
							for (Entreprise entreprise : itemsEntreprise) {

								switch (entreprise.getTypeEntreprise().getCode()) {
								case TypeUserEnum.ENTREPRISE_CLIENTE:
									nbreCliente ++ ;
									break;
								case TypeUserEnum.ENTREPRISE_FOURNISSEUR:
									nbreFournisseur ++ ;
									break;
								case TypeUserEnum.ENTREPRISE_MIXTE:
									nbreMixte ++ ;
									break;

								default:
									break;
								}
							}
						}
					}

					//				if (Utilities.isNotEmpty(datasSecteurDactiviteAppelDoffres)) {
					//					
					//					
					//					
					//					
					//					dto.isAttribuer = false ; 
					//				    dto.etatCode = environment.ENTRANT  ; 
					//				    dto.typeAppelDoffresCode = environment.GENERAL  ; 
					//				    dto.secteurDactiviteLibelle = data.libelle ;
					//
					//					
					//				}

					// les AO et OF
					List<AppelDoffres> itemsAO = null;
					itemsAO = secteurDactiviteAppelDoffresRepository.getByCriteriaCustom(reqAO, em, locale);
					if (Utilities.isNotEmpty(itemsAO)){
						if (Utilities.isNotEmpty(dto.getDatasPays())) {
							for (PaysDto data : dto.getDatasPays()) {
								for (AppelDoffres appelDoffres : itemsAO) {
									if (appelDoffres.getVille() != null 
											&& appelDoffres.getVille().getPays().getLibelle().equals(data.getLibelle())) {

										switch (appelDoffres.getTypeAppelDoffres().getCode()) {
										case TypeAppelDoffresEnum.RESTREINT:
											nbreAORestreint ++ ;
											break;
										case TypeAppelDoffresEnum.GENERAL:
											nbreAOGeneral ++ ;
											break;
										default:
											break;
										}

										List<Offre> offres = new ArrayList<>() ;

										//offres = offreRepository.findByAppelDoffresId(appelDoffres.getId(), false);
										offres = offreRepository.findByAppelDoffresIdAndDateSoumission(appelDoffres.getId(), dateFormatNew.parse(dateDebutNew), dateFormatNew.parse(dateFinNew), false);
										if (Utilities.isNotEmpty(offres)) {
											nbreOffre += offres.size() ;
										}

										if (appelDoffres.getIsAttribuer()!= null && appelDoffres.getIsAttribuer()) {

											List<Offre> offresConclu = new ArrayList<>() ;

											offresConclu = offreRepository.findByAppelDoffresIdAndIsSelectedByClient(appelDoffres.getId(), true, false) ;
											if (Utilities.isNotEmpty(offresConclu)) {
												for (Offre offre : offresConclu) {
													ValeurCritereOffre valeurCritereOffre = new ValeurCritereOffre();
													valeurCritereOffre = valeurCritereOffreRepository.findValeurCritereOffreByOffreIdAndCritereAppelDoffresCode(offre.getId(), GlobalEnum.prix, false) ;

													gainPrevu += Integer.valueOf(valeurCritereOffre.getValeur()) ;
													if (offre.getIsPayer() != null && offre.getIsPayer()) {
														gainRecu += Integer.valueOf(valeurCritereOffre.getValeur()) ;
													}
												}
											}
											nbreAOConlu ++ ;
										}
									}
								}
							}
						}else {
							for (AppelDoffres appelDoffres : itemsAO) {
								switch (appelDoffres.getTypeAppelDoffres().getCode()) {
								case TypeAppelDoffresEnum.RESTREINT:
									nbreAORestreint ++ ;
									break;
								case TypeAppelDoffresEnum.GENERAL:
									nbreAOGeneral ++ ;
									break;
								default:
									break;
								}

								List<Offre> offres = new ArrayList<>() ;

								//offres = offreRepository.findByAppelDoffresId(appelDoffres.getId(), false);
								offres = offreRepository.findByAppelDoffresIdAndDateSoumission(appelDoffres.getId(), dateFormatNew.parse(dateDebutNew), dateFormatNew.parse(dateFinNew), false);
								if (Utilities.isNotEmpty(offres)) {
									nbreOffre += offres.size() ;
								}

								if (appelDoffres.getIsAttribuer()!= null && appelDoffres.getIsAttribuer()) {


									List<Offre> offresConclu = new ArrayList<>() ;

									offresConclu = offreRepository.findByAppelDoffresIdAndIsSelectedByClient(appelDoffres.getId(), true, false) ;
									if (Utilities.isNotEmpty(offresConclu)) {
										for (Offre offre : offresConclu) {
											ValeurCritereOffre valeurCritereOffre = new ValeurCritereOffre();
											valeurCritereOffre = valeurCritereOffreRepository.findValeurCritereOffreByOffreIdAndCritereAppelDoffresCode(offre.getId(), GlobalEnum.prix, false) ;

											gainPrevu += Integer.valueOf(valeurCritereOffre.getValeur()) ;
											if (offre.getIsPayer() != null && offre.getIsPayer()) {
												gainRecu += Integer.valueOf(valeurCritereOffre.getValeur()) ;
											}
										}
									}
									nbreAOConlu ++ ;
								}
							}
						}
					}
				}else {

					List<Entreprise> itemsEntreprise = null;
					//itemsEntreprise = secteurDactiviteEntrepriseRepository.getByCriteriaCustom(req, em, locale);
					
					Request<EntrepriseDto> reqLoc = new Request<>() ;
					EntrepriseDto data = new EntrepriseDto() ;
					SearchParam<String> createdAtParam = new SearchParam<String>() ;
					createdAtParam.setOperator(OperatorEnum.BETWEEN);
					data.setCreatedAt(dateDebutNew);
					createdAtParam.setStart(dateDebutNew);
					createdAtParam.setEnd(dateFinNew);
					data.setCreatedAtParam(createdAtParam);
					reqLoc.setData(data);
					itemsEntreprise = entrepriseRepository.getByCriteria(reqLoc, em, locale);
					if (Utilities.isNotEmpty(itemsEntreprise)) {
						for (Entreprise entreprise : itemsEntreprise) {

							switch (entreprise.getTypeEntreprise().getCode()) {
							case TypeUserEnum.ENTREPRISE_CLIENTE:
								nbreCliente ++ ;
								break;
							case TypeUserEnum.ENTREPRISE_FOURNISSEUR:
								nbreFournisseur ++ ;
								break;
							case TypeUserEnum.ENTREPRISE_MIXTE:
								nbreMixte ++ ;
								break;

							default:
								break;
							}
						}
					}

					
					List<AppelDoffres> itemsAO = null;
					//itemsAO = secteurDactiviteAppelDoffresRepository.getByCriteriaCustom(reqAO, em, locale);
					
					Request<AppelDoffresDto> reqLocAO = new Request<>() ;
					AppelDoffresDto dataAO = new AppelDoffresDto() ;
					SearchParam<String> dateCreationParam = new SearchParam<String>() ;
					dateCreationParam.setOperator(OperatorEnum.BETWEEN);
					dataAO.setDateCreation(dateDebutNew);
					dateCreationParam.setStart(dateDebutNew);
					dateCreationParam.setEnd(dateFinNew);
					dataAO.setDateCreationParam(dateCreationParam);
					reqLocAO.setData(dataAO);
					itemsAO = appelDoffresRepository.getByCriteria(reqLocAO, em, locale);

					if (Utilities.isNotEmpty(itemsAO)){

						for (AppelDoffres appelDoffres : itemsAO) {
							switch (appelDoffres.getTypeAppelDoffres().getCode()) {
							case TypeAppelDoffresEnum.RESTREINT:
								nbreAORestreint ++ ;
								break;
							case TypeAppelDoffresEnum.GENERAL:
								nbreAOGeneral ++ ;
								break;
							default:
								break;
							}

							List<Offre> offres = new ArrayList<>() ;
							
							//offres = offreRepository.findByAppelDoffresId(appelDoffres.getId(), false);
							offres = offreRepository.findByAppelDoffresIdAndDateSoumission(appelDoffres.getId(), dateFormatNew.parse(dateDebutNew), dateFormatNew.parse(dateFinNew), false);
							if (Utilities.isNotEmpty(offres)) {
								nbreOffre += offres.size() ;
							}

							if (appelDoffres.getIsAttribuer()!= null && appelDoffres.getIsAttribuer()) {


								List<Offre> offresConclu = new ArrayList<>() ;

								offresConclu = offreRepository.findByAppelDoffresIdAndIsSelectedByClient(appelDoffres.getId(), true, false) ;
								if (Utilities.isNotEmpty(offresConclu)) {
									for (Offre offre : offresConclu) {
										ValeurCritereOffre valeurCritereOffre = new ValeurCritereOffre();
										valeurCritereOffre = valeurCritereOffreRepository.findValeurCritereOffreByOffreIdAndCritereAppelDoffresCode(offre.getId(), GlobalEnum.prix, false) ;

										gainPrevu += Integer.valueOf(valeurCritereOffre.getValeur()) ;
										if (offre.getIsPayer() != null && offre.getIsPayer()) {
											gainRecu += Integer.valueOf(valeurCritereOffre.getValeur()) ;
										}
									}
								}
								nbreAOConlu ++ ;
							}
						}
					}
				}

				// trois grandes listes

				// subdivision de l'interval de temps dans l'unité voulu
				
				
				abscisseGraphe.add(xGraphe) ;
				
				nbreAOY.add(nbreAORestreint + nbreAOGeneral) ;
				nbreAOConluY.add(nbreAOConlu) ;
				nbreAOGeneralY.add(nbreAOGeneral) ;
				nbreAORestreintY.add(nbreAORestreint) ;

				nbreClienteY.add(nbreCliente) ;
				nbreMixteY.add(nbreMixte) ;
				nbreFournisseurY.add(nbreFournisseur) ;
				
				nbreClienteGlobal += nbreCliente; 
				nbreMixteGlobal += nbreMixte ;
				nbreFournisseurGlobal += nbreFournisseur ;
				
				
				nbreOffreY.add(nbreOffre) ;
				
				gainPrevuY.add(gainPrevu) ;
				gainRecuY.add(gainRecu) ;

				
//				itemDto.setNbreAO(nbreAO);
//				itemDto.setNbreAOConlu(nbreAOConlu);
//				itemDto.setNbreAOGeneral(nbreAOGeneral);
//				itemDto.setNbreAORestreint(nbreAORestreint);
//				
//				itemDto.setNbreCliente(nbreCliente);
//				itemDto.setNbreMixte(nbreMixte);
//				itemDto.setNbreFournisseur(nbreFournisseur);
//				
//				itemDto.setNbreOffre(nbreOffre);
//				
//				itemDto.setGainPrevu(gainPrevu);
//				itemDto.setGainRecu(gainRecu);
			

			}
			
			
			itemDto.setAbscisseGraphe(abscisseGraphe);
			itemDto.setNbreAOY(nbreAOY);
			itemDto.setNbreAOConluY(nbreAOConluY);
			itemDto.setNbreAOGeneralY(nbreAOGeneralY);
			itemDto.setNbreAORestreintY(nbreAORestreintY);
			
			itemDto.setNbreMixteY(nbreMixteY);
			itemDto.setNbreFournisseurY(nbreFournisseurY);
			itemDto.setNbreClienteY(nbreClienteY);
			
			itemDto.setNbreMixte(nbreMixteGlobal);
			itemDto.setNbreFournisseur(nbreFournisseurGlobal);
			itemDto.setNbreCliente(nbreClienteGlobal);
			
			itemDto.setNbreOffreY(nbreOffreY);
			itemDto.setGainPrevuY(gainPrevuY);
			itemDto.setGainRecuY(gainRecuY);
			
			response.setItems(Arrays.asList(itemDto));
			response.setHasError(false);

			slf4jLogger.info("----end getStatistiques SecteurDactiviteAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * getCustom SecteurDactiviteAppelDoffres by using SecteurDactiviteAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	public Response<AppelDoffresDto> getByCriteriaCustomOld(Request<SecteurDactiviteAppelDoffresDto> request, Locale locale) {
		slf4jLogger.info("----begin getByCriteriaCustom SecteurDactiviteAppelDoffres-----");

		Response<AppelDoffresDto> response = new Response<>();

		try {

			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			// type de user autorisé
			if (utilisateur.getEntreprise().getTypeEntreprise().getCode().equals(TypeUserEnum.ENTREPRISE_CLIENTE)) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" est un client ou administrateur" , locale));
				response.setHasError(true);
				return response;
			}
			List<AppelDoffres> items = new ArrayList<>();
			List<AppelDoffres> itemsGeneral = new ArrayList<>();
			List<AppelDoffres> itemsGeneralSelect = new ArrayList<>();
			List<AppelDoffresRestreint> itemsRestreint = new ArrayList<>();
			List<AppelDoffresRestreint> itemsRestreintSelect = new ArrayList<>();

			// formation de la request 
			SecteurDactiviteAppelDoffresDto data = request.getData();

			Integer userFounisseurId = request.getUser();

			if (data != null) {



				List<SecteurDactiviteEntreprise> secteurDactiviteEntreprises = secteurDactiviteEntrepriseRepository.findByEntrepriseId(utilisateur.getEntreprise().getId(), false);
				if (Utilities.isNotEmpty(secteurDactiviteEntreprises)) {
					List<SecteurDactiviteAppelDoffresDto> datas = new ArrayList<>();

					for (SecteurDactiviteEntreprise secteurDactiviteEntreprise : secteurDactiviteEntreprises) {

						data.setSecteurDactiviteId(secteurDactiviteEntreprise.getSecteurDactivite().getId());
						data.setTypeAppelDoffresCode(TypeAppelDoffresEnum.GENERAL);
						//TODO : il marche mais il faut bien le comprendre
						SecteurDactiviteAppelDoffresDto dataCloner = (SecteurDactiviteAppelDoffresDto) data.clone();
						datas.add(dataCloner);
					}
					request.setData(datas.get(0)); // afin de ne pas prendre un data qui n'a pas toutes les conditions
					datas.remove(0); // important pour eviter des recupereantion en doublon
					request.setDatas(datas);
					//request.setIsAnd(false); SETTER A FALSE PAR DEFAUT
				}
			}

			slf4jLogger.info("-------request-------"+ request);
			slf4jLogger.info("-------request-------"+ request.toString());
			slf4jLogger.info("-------request-------"+ request.getData());
			slf4jLogger.info("-------request-------"+ request.getDatas());


			itemsGeneral = secteurDactiviteAppelDoffresRepository.getByCriteriaCustom(request, em, locale);
			if (Utilities.isNotEmpty(request.getDatas())) {
				List<SecteurDactiviteAppelDoffresDto> datas = new ArrayList<>();
				for (SecteurDactiviteAppelDoffresDto secteurDactiviteAppelDoffresDto : request.getDatas()) {
					slf4jLogger.info("contenu avant : " + secteurDactiviteAppelDoffresDto.getIsAttribuer());

					secteurDactiviteAppelDoffresDto.setIsAttribuer(true);

					slf4jLogger.info("contenu apres : " + secteurDactiviteAppelDoffresDto.getIsAttribuer());

					datas.add(secteurDactiviteAppelDoffresDto);
				}
				//slf4jLogger.info("request.getDatas avant : " + request.getDatas());

				request.setData(datas.get(0));
				datas.remove(0); // important pour eviter des recupereantion en doublon
				request.setDatas(datas);
				//slf4jLogger.info("request.getDatas apres : " + request.getDatas());

				itemsGeneralSelect = secteurDactiviteAppelDoffresRepository.getByCriteriaCustom(request, em, locale);
			}

			slf4jLogger.info("itemsGeneral : " + itemsGeneral.size());
			slf4jLogger.info("itemsGeneralSelect : " + itemsGeneralSelect.size());
			slf4jLogger.info("itemsRestreint : " + itemsRestreint.size());
			slf4jLogger.info("itemsRestreintSelect : " + itemsRestreintSelect.size());


			itemsRestreint 		 = appelDoffresRestreintRepository.findByUserFournisseurIdAndIsAttribuerOffreAndIsVisibleByFournisseurAndIsDesactiver(userFounisseurId, false, true, false, false);
			itemsRestreintSelect = appelDoffresRestreintRepository.findByUserFournisseurIdAndIsAttribuerOffreAndIsVisibleByFournisseurAndIsDesactiver(userFounisseurId, true,  true, false, false);

			if (Utilities.isNotEmpty(itemsRestreint)) {
				for (AppelDoffresRestreint appelDoffresRestreint : itemsRestreint) {
					items.add(appelDoffresRestreint.getAppelDoffres());
				}
			}
			if (Utilities.isNotEmpty(itemsRestreintSelect)) {
				for (AppelDoffresRestreint appelDoffresRestreint : itemsRestreintSelect) {
					items.add(appelDoffresRestreint.getAppelDoffres());
				}
			}
			if (Utilities.isNotEmpty(itemsGeneral)) {
				items.addAll(itemsGeneral);
			}
			if (Utilities.isNotEmpty(itemsGeneralSelect)) {
				for (AppelDoffres appelDoffres : itemsGeneralSelect) {
					List<Offre> offres = offreRepository.findByAppelDoffresIdAnduserFournisseurIdAndIsSelectedByClient(appelDoffres.getId(), userFounisseurId, true, false) ; 
					if (!Utilities.isNotEmpty(offres)) {
						continue ;
					}
					items.add(appelDoffres);
				}
			}

			if (items != null && !items.isEmpty()) {
				Collections.sort(items, new Comparator<AppelDoffres>() {
					@Override
					public int compare(AppelDoffres o1, AppelDoffres o2) {
						// TODO Auto-generated method stub
						//return o1.getId().compareTo(o2.getId())* - 1;
						//return o2.getId().compareTo(o1.getId());

						if (o2.getDateCreation() != null ) {
							return o2.getDateCreation().compareTo(o1.getDateCreation());
						}
						return o2.getCreatedAt().compareTo(o1.getCreatedAt());
					}
				}); // trier la liste



				List<AppelDoffresDto> itemsDto = new ArrayList<>();
				for (AppelDoffres entity : items) {
					AppelDoffresDto dto = AppelDoffresTransformer.INSTANCE.toDto(entity);
					if (dto == null) continue;
					List<Offre> offres = offreRepository.findByAppelDoffresId(dto.getId(), false);
					dto.setNombreOffre(Utilities.isNotEmpty(offres) ? ( offres.size() == 1 ?  offres.size() + " offre" : offres.size() + " offres") : "Aucune offre");
					// pas besoin de setter le isMOdifiable car il sera setter apres sooumission d'une offre
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount((long) itemsDto.size());
				//response.setCount(secteurDactiviteAppelDoffresRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("secteurDactiviteAppelDoffres", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end getByCriteriaCustom SecteurDactiviteAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full SecteurDactiviteAppelDoffresDto by using SecteurDactiviteAppelDoffres as object.
	 *
	 * @param entity, locale
	 * @return SecteurDactiviteAppelDoffresDto
	 *
	 */
	private SecteurDactiviteAppelDoffresDto getFullInfos(SecteurDactiviteAppelDoffres entity, Integer size, Locale locale) throws Exception {
		SecteurDactiviteAppelDoffresDto dto = SecteurDactiviteAppelDoffresTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
