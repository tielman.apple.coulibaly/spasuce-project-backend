package ci.salaamsolutions.projet.spasuce.helper.enums;

public enum MonthEnum {
	
	
	JANVIER(1),
	FEVRIER(2),
	MARS(3),
	AVRIL(4),
	MAI(5),
	JUIN(6),
	JUILLET(7),
	AOUT(8),
	SEPTEMBRE(9),
	OCTOBRE(10),
	NOVEMBRE(11),
	DECEMBRE(12);
	private final Integer value;

	MonthEnum(Integer str) {
		value = str;
	}

	public Integer getValue() {
		return value;
	}

}
