
/*
 * Java dto for entity table secteur_dactivite_appel_doffres 
 * Created on 2020-01-31 ( Time 08:34:48 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.customize;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffres;
import ci.salaamsolutions.projet.spasuce.helper.contract.SearchParam;
import ci.salaamsolutions.projet.spasuce.helper.dto.PaysDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteDto;
import lombok.Data;
/**
 * DTO customize for table "secteur_dactivite_appel_doffres"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _SecteurDactiviteAppelDoffresDto {
	protected AppelDoffres entityAppelDoffres ;
	protected Integer    userFournisseurId       ;
	
	
	// variable pour les stats
	protected List<PaysDto>    datasPays       ;
	protected List<SecteurDactiviteDto>    datasSecteurDactivite     ;
	protected String    secteurDactiviteLibelle       ;
	protected String    dateDebut       ;
	protected String    dateFin       ;
	protected Boolean 	isDetailUser ;
	protected Boolean 	isDetailGain ;
	protected Boolean 	isDetailAoOf ;
	protected SearchParam<String>   createdAtAppelDoffresParam        ;   
	
	protected Integer nbreCliente;
	protected Integer nbreFournisseur ;
	protected Integer nbreMixte;
	
	protected Integer gainRecu;
	protected Integer gainPrevu ;


	protected Integer nbreAO ;
	protected Integer nbreAORestreint  ;
	protected Integer nbreAOGeneral ;
	protected Integer nbreAOConlu ;
	protected Integer nbreOffre ;
	
	
	protected List<Integer> nbreClienteY  ;
	protected List<Integer> nbreFournisseurY ;
	protected List<Integer> nbreMixteY ;

	protected List<Integer> gainRecuY  ;
	protected List<Integer> gainPrevuY ;
	
	protected List<Integer> nbreAOY ;
	protected List<Integer> nbreAORestreintY  ;
	protected List<Integer> nbreAOGeneralY ;
	protected List<Integer> nbreAOConluY ;
	protected List<Integer> nbreOffreY ;
	
	protected List<String> abscisseGraphe ;

	


	

	
	// les champs d'appels d'offres
	protected Boolean isVisibleByClient ;
	protected Boolean isRetirer ;
	protected Boolean isAttribuer ;
	protected Boolean isLocked ;
	protected Boolean isModifiable ;
	protected Boolean isUpdate ;
	protected Boolean isDeletedAppelDoffres ;
	protected SearchParam<Boolean> isVisibleByClientParam ;
	protected SearchParam<Boolean> isRetirerParam ;
	protected SearchParam<Boolean> isAttribuerParam ;
	protected SearchParam<Boolean> isLockedParam ;
	protected SearchParam<Boolean> isModifiableParam ;
	protected SearchParam<Boolean> isUpdateParam ;
	protected SearchParam<Boolean> isDeletedAppelDoffresParam ;


	
	
	
	protected String typeAppelDoffresCode;
	protected SearchParam<String>  typeAppelDoffresCodeParam;
	protected String etatCode;
	protected SearchParam<String>  etatCodeParam;

}
