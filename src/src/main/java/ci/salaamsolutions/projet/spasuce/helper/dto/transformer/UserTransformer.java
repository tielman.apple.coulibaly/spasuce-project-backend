

/*
 * Java transformer for entity table user 
 * Created on 2020-01-02 ( Time 18:00:05 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "user"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface UserTransformer {

	UserTransformer INSTANCE = Mappers.getMapper(UserTransformer.class);

	@Mappings({

		@Mapping(source="entity.dateExpirationToken", dateFormat="dd/MM/yyyy",target="dateExpirationToken"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.role.id", target="roleId"),
				@Mapping(source="entity.role.code", target="roleCode"),
				@Mapping(source="entity.role.libelle", target="roleLibelle"),
		@Mapping(source="entity.entreprise.id", target="entrepriseId"),
		@Mapping(source="entity.entreprise.nom", target="entrepriseNom"), // ajouter
		@Mapping(source="entity.entreprise.typeEntreprise.code", target="typeEntrepriseCode"),// ajouter
				@Mapping(source="entity.entreprise.login", target="entrepriseLogin"),
	})
	UserDto toDto(User entity) throws ParseException;

    List<UserDto> toDtos(List<User> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.prenoms", target="prenoms"),
		@Mapping(source="dto.login", target="login"),
		@Mapping(source="dto.password", target="password"),
		@Mapping(source="dto.telephone", target="telephone"),
		@Mapping(source="dto.email", target="email"),
		@Mapping(source="dto.isLocked", target="isLocked"),
		@Mapping(source="dto.isSuUser", target="isSuUser"),
		@Mapping(source="dto.isValider", target="isValider"),
		@Mapping(source="dto.token", target="token"),
		@Mapping(source="dto.dateExpirationToken", dateFormat="dd/MM/yyyy",target="dateExpirationToken"),
		@Mapping(source="dto.isTokenValide", target="isTokenValide"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="role", target="role"),
		@Mapping(source="entreprise", target="entreprise"),
	})
    User toEntity(UserDto dto, Role role, Entreprise entreprise) throws ParseException;

    //List<User> toEntities(List<UserDto> dtos) throws ParseException;

}
