package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._AdresseFournisseurRepository;

/**
 * Repository : AdresseFournisseur.
 */
@Repository
public interface AdresseFournisseurRepository extends JpaRepository<AdresseFournisseur, Integer>, _AdresseFournisseurRepository {
	/**
	 * Finds AdresseFournisseur by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object AdresseFournisseur whose id is equals to the given id. If
	 *         no AdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from AdresseFournisseur e where e.id = :id and e.isDeleted = :isDeleted")
	AdresseFournisseur findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AdresseFournisseur by using nom as a search criteria.
	 *
	 * @param nom
	 * @return An Object AdresseFournisseur whose nom is equals to the given nom. If
	 *         no AdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from AdresseFournisseur e where e.nom = :nom and e.isDeleted = :isDeleted")
	List<AdresseFournisseur> findByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AdresseFournisseur by using prenoms as a search criteria.
	 *
	 * @param prenoms
	 * @return An Object AdresseFournisseur whose prenoms is equals to the given prenoms. If
	 *         no AdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from AdresseFournisseur e where e.prenoms = :prenoms and e.isDeleted = :isDeleted")
	List<AdresseFournisseur> findByPrenoms(@Param("prenoms")String prenoms, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AdresseFournisseur by using email as a search criteria.
	 *
	 * @param email
	 * @return An Object AdresseFournisseur whose email is equals to the given email. If
	 *         no AdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from AdresseFournisseur e where e.email = :email and e.isDeleted = :isDeleted")
	AdresseFournisseur findByEmail(@Param("email")String email, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AdresseFournisseur by using telephone as a search criteria.
	 *
	 * @param telephone
	 * @return An Object AdresseFournisseur whose telephone is equals to the given telephone. If
	 *         no AdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from AdresseFournisseur e where e.telephone = :telephone and e.isDeleted = :isDeleted")
	AdresseFournisseur findByTelephone(@Param("telephone")String telephone, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AdresseFournisseur by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object AdresseFournisseur whose createdAt is equals to the given createdAt. If
	 *         no AdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from AdresseFournisseur e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<AdresseFournisseur> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AdresseFournisseur by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object AdresseFournisseur whose createdBy is equals to the given createdBy. If
	 *         no AdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from AdresseFournisseur e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<AdresseFournisseur> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AdresseFournisseur by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object AdresseFournisseur whose updatedAt is equals to the given updatedAt. If
	 *         no AdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from AdresseFournisseur e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<AdresseFournisseur> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AdresseFournisseur by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object AdresseFournisseur whose updatedBy is equals to the given updatedBy. If
	 *         no AdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from AdresseFournisseur e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<AdresseFournisseur> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AdresseFournisseur by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object AdresseFournisseur whose deletedAt is equals to the given deletedAt. If
	 *         no AdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from AdresseFournisseur e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<AdresseFournisseur> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AdresseFournisseur by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object AdresseFournisseur whose deletedBy is equals to the given deletedBy. If
	 *         no AdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from AdresseFournisseur e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<AdresseFournisseur> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AdresseFournisseur by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object AdresseFournisseur whose isDeleted is equals to the given isDeleted. If
	 *         no AdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from AdresseFournisseur e where e.isDeleted = :isDeleted")
	List<AdresseFournisseur> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AdresseFournisseur by using carnetDadresseId as a search criteria.
	 *
	 * @param carnetDadresseId
	 * @return A list of Object AdresseFournisseur whose carnetDadresseId is equals to the given carnetDadresseId. If
	 *         no AdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from AdresseFournisseur e where e.carnetDadresse.id = :carnetDadresseId and e.isDeleted = :isDeleted")
	List<AdresseFournisseur> findByCarnetDadresseId(@Param("carnetDadresseId")Integer carnetDadresseId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one AdresseFournisseur by using carnetDadresseId as a search criteria.
   *
   * @param carnetDadresseId
   * @return An Object AdresseFournisseur whose carnetDadresseId is equals to the given carnetDadresseId. If
   *         no AdresseFournisseur is found, this method returns null.
   */
  @Query("select e from AdresseFournisseur e where e.carnetDadresse.id = :carnetDadresseId and e.isDeleted = :isDeleted")
  AdresseFournisseur findAdresseFournisseurByCarnetDadresseId(@Param("carnetDadresseId")Integer carnetDadresseId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of AdresseFournisseur by using adresseFournisseurDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of AdresseFournisseur
	 * @throws DataAccessException,ParseException
	 */
	public default List<AdresseFournisseur> getByCriteria(Request<AdresseFournisseurDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from AdresseFournisseur e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<AdresseFournisseur> query = em.createQuery(req, AdresseFournisseur.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of AdresseFournisseur by using adresseFournisseurDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of AdresseFournisseur
	 *
	 */
	public default Long count(Request<AdresseFournisseurDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from AdresseFournisseur e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<AdresseFournisseurDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		AdresseFournisseurDto dto = request.getData() != null ? request.getData() : new AdresseFournisseurDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (AdresseFournisseurDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(AdresseFournisseurDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nom", dto.getNom(), "e.nom", "String", dto.getNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPrenoms())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prenoms", dto.getPrenoms(), "e.prenoms", "String", dto.getPrenomsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEmail())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("email", dto.getEmail(), "e.email", "String", dto.getEmailParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTelephone())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephone", dto.getTelephone(), "e.telephone", "String", dto.getTelephoneParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCarnetDadresseId()!= null && dto.getCarnetDadresseId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carnetDadresseId", dto.getCarnetDadresseId(), "e.carnetDadresse.id", "Integer", dto.getCarnetDadresseIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCarnetDadresseLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carnetDadresseLibelle", dto.getCarnetDadresseLibelle(), "e.carnetDadresse.libelle", "String", dto.getCarnetDadresseLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
