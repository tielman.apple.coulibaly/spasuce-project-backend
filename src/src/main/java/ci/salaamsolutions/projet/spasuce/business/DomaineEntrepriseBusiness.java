


/*
 * Java transformer for entity table domaine_entreprise
 * Created on 2020-03-06 ( Time 21:33:12 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "domaine_entreprise"
 *
 * @author Back-End developper
 *
 */
@Component
public class DomaineEntrepriseBusiness implements IBasicBusiness<Request<DomaineEntrepriseDto>, Response<DomaineEntrepriseDto>> {

	private Response<DomaineEntrepriseDto> response;
	@Autowired
	private DomaineEntrepriseRepository domaineEntrepriseRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EntrepriseRepository entrepriseRepository;
	@Autowired
	private DomaineRepository domaineRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public DomaineEntrepriseBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create DomaineEntreprise by using DomaineEntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<DomaineEntrepriseDto> create(Request<DomaineEntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin create DomaineEntreprise-----");

		response = new Response<DomaineEntrepriseDto>();

		try {
//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}
			if (request.getUser() != null && request.getUser() > 0) {
				User utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
			}

			List<DomaineEntreprise> items = new ArrayList<DomaineEntreprise>();

			for (DomaineEntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("valeurAutre", dto.getValeurAutre());
				fieldsToVerify.put("entrepriseId", dto.getEntrepriseId());
				fieldsToVerify.put("domaineId", dto.getDomaineId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if domaineEntreprise to insert do not exist
				DomaineEntreprise existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("domaineEntreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if entreprise exist
				Entreprise existingEntreprise = entrepriseRepository.findById(dto.getEntrepriseId(), false);
				if (existingEntreprise == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("entreprise -> " + dto.getEntrepriseId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if domaine exist
				Domaine existingDomaine = domaineRepository.findById(dto.getDomaineId(), false);
				if (existingDomaine == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("domaine -> " + dto.getDomaineId(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.size() > 0) {
					if (items.stream().anyMatch(a->a.getEntreprise().getId().equals(dto.getEntrepriseId()) && a.getDomaine().getId().equals(dto.getDomaineId()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du domiane ->'" + dto.getDomaineId()+"' pour l'entreprises -> "+ dto.getEntrepriseId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				DomaineEntreprise entityToSave = null;
				entityToSave = DomaineEntrepriseTransformer.INSTANCE.toEntity(dto, existingEntreprise, existingDomaine);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<DomaineEntreprise> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = domaineEntrepriseRepository.save((Iterable<DomaineEntreprise>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("domaineEntreprise", locale));
					response.setHasError(true);
					return response;
				}
				List<DomaineEntrepriseDto> itemsDto = new ArrayList<DomaineEntrepriseDto>();
				for (DomaineEntreprise entity : itemsSaved) {
					DomaineEntrepriseDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create DomaineEntreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update DomaineEntreprise by using DomaineEntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<DomaineEntrepriseDto> update(Request<DomaineEntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin update DomaineEntreprise-----");

		response = new Response<DomaineEntrepriseDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<DomaineEntreprise> items = new ArrayList<DomaineEntreprise>();

			for (DomaineEntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la domaineEntreprise existe
				DomaineEntreprise entityToSave = null;
				entityToSave = domaineEntrepriseRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("domaineEntreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if entreprise exist
				if (dto.getEntrepriseId() != null && dto.getEntrepriseId() > 0){
					Entreprise existingEntreprise = entrepriseRepository.findById(dto.getEntrepriseId(), false);
					if (existingEntreprise == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("entreprise -> " + dto.getEntrepriseId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEntreprise(existingEntreprise);
				}
				// Verify if domaine exist
				if (dto.getDomaineId() != null && dto.getDomaineId() > 0){
					Domaine existingDomaine = domaineRepository.findById(dto.getDomaineId(), false);
					if (existingDomaine == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("domaine -> " + dto.getDomaineId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setDomaine(existingDomaine);
				}
				if (Utilities.notBlank(dto.getValeurAutre())) {
					entityToSave.setValeurAutre(dto.getValeurAutre());
				}
//				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
//					entityToSave.setCreatedBy(dto.getCreatedBy());
//				}
//				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
//					entityToSave.setUpdatedBy(dto.getUpdatedBy());
//				}
//				if (Utilities.notBlank(dto.getDeletedAt())) {
//					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
//				}
//				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
//					entityToSave.setDeletedBy(dto.getDeletedBy());
//				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<DomaineEntreprise> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = domaineEntrepriseRepository.save((Iterable<DomaineEntreprise>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("domaineEntreprise", locale));
					response.setHasError(true);
					return response;
				}
				List<DomaineEntrepriseDto> itemsDto = new ArrayList<DomaineEntrepriseDto>();
				for (DomaineEntreprise entity : itemsSaved) {
					DomaineEntrepriseDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update DomaineEntreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete DomaineEntreprise by using DomaineEntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<DomaineEntrepriseDto> delete(Request<DomaineEntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete DomaineEntreprise-----");

		response = new Response<DomaineEntrepriseDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<DomaineEntreprise> items = new ArrayList<DomaineEntreprise>();

			for (DomaineEntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la domaineEntreprise existe
				DomaineEntreprise existingEntity = null;
				existingEntity = domaineEntrepriseRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("domaineEntreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				domaineEntrepriseRepository.save((Iterable<DomaineEntreprise>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete DomaineEntreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete DomaineEntreprise by using DomaineEntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<DomaineEntrepriseDto> forceDelete(Request<DomaineEntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete DomaineEntreprise-----");

		response = new Response<DomaineEntrepriseDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<DomaineEntreprise> items = new ArrayList<DomaineEntreprise>();

			for (DomaineEntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la domaineEntreprise existe
				DomaineEntreprise existingEntity = null;
				existingEntity = domaineEntrepriseRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("domaineEntreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				domaineEntrepriseRepository.save((Iterable<DomaineEntreprise>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete DomaineEntreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get DomaineEntreprise by using DomaineEntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<DomaineEntrepriseDto> getByCriteria(Request<DomaineEntrepriseDto> request, Locale locale) {
		slf4jLogger.info("----begin get DomaineEntreprise-----");

		response = new Response<DomaineEntrepriseDto>();

		try {
			List<DomaineEntreprise> items = null;
			items = domaineEntrepriseRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<DomaineEntrepriseDto> itemsDto = new ArrayList<DomaineEntrepriseDto>();
				for (DomaineEntreprise entity : items) {
					DomaineEntrepriseDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(domaineEntrepriseRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("domaineEntreprise", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get DomaineEntreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full DomaineEntrepriseDto by using DomaineEntreprise as object.
	 *
	 * @param entity, locale
	 * @return DomaineEntrepriseDto
	 *
	 */
	private DomaineEntrepriseDto getFullInfos(DomaineEntreprise entity, Integer size, Locale locale) throws Exception {
		DomaineEntrepriseDto dto = DomaineEntrepriseTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
