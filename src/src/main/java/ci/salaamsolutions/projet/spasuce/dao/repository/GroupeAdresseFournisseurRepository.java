package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._GroupeAdresseFournisseurRepository;

/**
 * Repository : GroupeAdresseFournisseur.
 */
@Repository
public interface GroupeAdresseFournisseurRepository extends JpaRepository<GroupeAdresseFournisseur, Integer>, _GroupeAdresseFournisseurRepository {
	/**
	 * Finds GroupeAdresseFournisseur by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object GroupeAdresseFournisseur whose id is equals to the given id. If
	 *         no GroupeAdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from GroupeAdresseFournisseur e where e.id = :id and e.isDeleted = :isDeleted")
	GroupeAdresseFournisseur findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds GroupeAdresseFournisseur by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object GroupeAdresseFournisseur whose createdAt is equals to the given createdAt. If
	 *         no GroupeAdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from GroupeAdresseFournisseur e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<GroupeAdresseFournisseur> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GroupeAdresseFournisseur by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object GroupeAdresseFournisseur whose createdBy is equals to the given createdBy. If
	 *         no GroupeAdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from GroupeAdresseFournisseur e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<GroupeAdresseFournisseur> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GroupeAdresseFournisseur by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object GroupeAdresseFournisseur whose updatedAt is equals to the given updatedAt. If
	 *         no GroupeAdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from GroupeAdresseFournisseur e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<GroupeAdresseFournisseur> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GroupeAdresseFournisseur by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object GroupeAdresseFournisseur whose updatedBy is equals to the given updatedBy. If
	 *         no GroupeAdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from GroupeAdresseFournisseur e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<GroupeAdresseFournisseur> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GroupeAdresseFournisseur by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object GroupeAdresseFournisseur whose deletedAt is equals to the given deletedAt. If
	 *         no GroupeAdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from GroupeAdresseFournisseur e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<GroupeAdresseFournisseur> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GroupeAdresseFournisseur by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object GroupeAdresseFournisseur whose deletedBy is equals to the given deletedBy. If
	 *         no GroupeAdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from GroupeAdresseFournisseur e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<GroupeAdresseFournisseur> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GroupeAdresseFournisseur by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object GroupeAdresseFournisseur whose isDeleted is equals to the given isDeleted. If
	 *         no GroupeAdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from GroupeAdresseFournisseur e where e.isDeleted = :isDeleted")
	List<GroupeAdresseFournisseur> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds GroupeAdresseFournisseur by using groupeId as a search criteria.
	 *
	 * @param groupeId
	 * @return A list of Object GroupeAdresseFournisseur whose groupeId is equals to the given groupeId. If
	 *         no GroupeAdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from GroupeAdresseFournisseur e where e.groupe.id = :groupeId and e.isDeleted = :isDeleted")
	List<GroupeAdresseFournisseur> findByGroupeId(@Param("groupeId")Integer groupeId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one GroupeAdresseFournisseur by using groupeId as a search criteria.
   *
   * @param groupeId
   * @return An Object GroupeAdresseFournisseur whose groupeId is equals to the given groupeId. If
   *         no GroupeAdresseFournisseur is found, this method returns null.
   */
  @Query("select e from GroupeAdresseFournisseur e where e.groupe.id = :groupeId and e.isDeleted = :isDeleted")
  GroupeAdresseFournisseur findGroupeAdresseFournisseurByGroupeId(@Param("groupeId")Integer groupeId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds GroupeAdresseFournisseur by using adresseFournisseurId as a search criteria.
	 *
	 * @param adresseFournisseurId
	 * @return A list of Object GroupeAdresseFournisseur whose adresseFournisseurId is equals to the given adresseFournisseurId. If
	 *         no GroupeAdresseFournisseur is found, this method returns null.
	 */
	@Query("select e from GroupeAdresseFournisseur e where e.adresseFournisseur.id = :adresseFournisseurId and e.isDeleted = :isDeleted")
	List<GroupeAdresseFournisseur> findByAdresseFournisseurId(@Param("adresseFournisseurId")Integer adresseFournisseurId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one GroupeAdresseFournisseur by using adresseFournisseurId as a search criteria.
   *
   * @param adresseFournisseurId
   * @return An Object GroupeAdresseFournisseur whose adresseFournisseurId is equals to the given adresseFournisseurId. If
   *         no GroupeAdresseFournisseur is found, this method returns null.
   */
  @Query("select e from GroupeAdresseFournisseur e where e.adresseFournisseur.id = :adresseFournisseurId and e.isDeleted = :isDeleted")
  GroupeAdresseFournisseur findGroupeAdresseFournisseurByAdresseFournisseurId(@Param("adresseFournisseurId")Integer adresseFournisseurId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of GroupeAdresseFournisseur by using groupeAdresseFournisseurDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of GroupeAdresseFournisseur
	 * @throws DataAccessException,ParseException
	 */
	public default List<GroupeAdresseFournisseur> getByCriteria(Request<GroupeAdresseFournisseurDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from GroupeAdresseFournisseur e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<GroupeAdresseFournisseur> query = em.createQuery(req, GroupeAdresseFournisseur.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of GroupeAdresseFournisseur by using groupeAdresseFournisseurDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of GroupeAdresseFournisseur
	 *
	 */
	public default Long count(Request<GroupeAdresseFournisseurDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from GroupeAdresseFournisseur e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<GroupeAdresseFournisseurDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		GroupeAdresseFournisseurDto dto = request.getData() != null ? request.getData() : new GroupeAdresseFournisseurDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (GroupeAdresseFournisseurDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(GroupeAdresseFournisseurDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getGroupeId()!= null && dto.getGroupeId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("groupeId", dto.getGroupeId(), "e.groupe.id", "Integer", dto.getGroupeIdParam(), param, index, locale));
			}
			if (dto.getAdresseFournisseurId()!= null && dto.getAdresseFournisseurId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("adresseFournisseurId", dto.getAdresseFournisseurId(), "e.adresseFournisseur.id", "Integer", dto.getAdresseFournisseurIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getGroupeCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("groupeCode", dto.getGroupeCode(), "e.groupe.code", "String", dto.getGroupeCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getGroupeLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("groupeLibelle", dto.getGroupeLibelle(), "e.groupe.libelle", "String", dto.getGroupeLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getAdresseFournisseurNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("adresseFournisseurNom", dto.getAdresseFournisseurNom(), "e.adresseFournisseur.nom", "String", dto.getAdresseFournisseurNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getAdresseFournisseurPrenoms())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("adresseFournisseurPrenoms", dto.getAdresseFournisseurPrenoms(), "e.adresseFournisseur.prenoms", "String", dto.getAdresseFournisseurPrenomsParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
