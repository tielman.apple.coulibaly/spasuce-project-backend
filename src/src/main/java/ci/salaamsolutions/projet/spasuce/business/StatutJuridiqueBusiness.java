


/*
 * Java transformer for entity table statut_juridique
 * Created on 2020-01-02 ( Time 18:00:04 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "statut_juridique"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class StatutJuridiqueBusiness implements IBasicBusiness<Request<StatutJuridiqueDto>, Response<StatutJuridiqueDto>> {

	private Response<StatutJuridiqueDto> response;
	@Autowired
	private StatutJuridiqueRepository statutJuridiqueRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EntrepriseRepository entrepriseRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private EntrepriseBusiness entrepriseBusiness;



	public StatutJuridiqueBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create StatutJuridique by using StatutJuridiqueDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<StatutJuridiqueDto> create(Request<StatutJuridiqueDto> request, Locale locale)  {
		slf4jLogger.info("----begin create StatutJuridique-----");

		response = new Response<StatutJuridiqueDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<StatutJuridique> items = new ArrayList<StatutJuridique>();

			for (StatutJuridiqueDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("code", dto.getCode());
				fieldsToVerify.put("libelle", dto.getLibelle());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if statutJuridique to insert do not exist
				StatutJuridique existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("statutJuridique -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				if (Utilities.notBlank(dto.getCode())) {
					existingEntity = statutJuridiqueRepository.findByCode(dto.getCode(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("statutJuridique -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les statutJuridiques", locale));
						response.setHasError(true);
						return response;
					}
				}
				existingEntity = statutJuridiqueRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("statutJuridique -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les statutJuridiques", locale));
					response.setHasError(true);
					return response;
				}

				StatutJuridique entityToSave = null;
				entityToSave = StatutJuridiqueTransformer.INSTANCE.toEntity(dto);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<StatutJuridique> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = statutJuridiqueRepository.save((Iterable<StatutJuridique>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("statutJuridique", locale));
					response.setHasError(true);
					return response;
				}
				List<StatutJuridiqueDto> itemsDto = new ArrayList<StatutJuridiqueDto>();
				for (StatutJuridique entity : itemsSaved) {
					StatutJuridiqueDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create StatutJuridique-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update StatutJuridique by using StatutJuridiqueDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<StatutJuridiqueDto> update(Request<StatutJuridiqueDto> request, Locale locale)  {
		slf4jLogger.info("----begin update StatutJuridique-----");

		response = new Response<StatutJuridiqueDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<StatutJuridique> items = new ArrayList<StatutJuridique>();

			for (StatutJuridiqueDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la statutJuridique existe
				StatutJuridique entityToSave = null;
				entityToSave = statutJuridiqueRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("statutJuridique -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				if (Utilities.notBlank(dto.getCode())) {
					StatutJuridique existingEntity = statutJuridiqueRepository.findByCode(dto.getCode(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("statutJuridique -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les statutJuridiques", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCode(dto.getCode());
				}
				if (Utilities.notBlank(dto.getLibelle())) {
					StatutJuridique existingEntity = statutJuridiqueRepository.findByLibelle(dto.getLibelle(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("statutJuridique -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les statutJuridiques", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
//				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
//					entityToSave.setCreatedBy(dto.getCreatedBy());
//				}
//				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
//					entityToSave.setUpdatedBy(dto.getUpdatedBy());
//				}
//				if (Utilities.notBlank(dto.getDeletedAt())) {
//					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
//				}
//				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
//					entityToSave.setDeletedBy(dto.getDeletedBy());
//				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<StatutJuridique> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = statutJuridiqueRepository.save((Iterable<StatutJuridique>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("statutJuridique", locale));
					response.setHasError(true);
					return response;
				}
				List<StatutJuridiqueDto> itemsDto = new ArrayList<StatutJuridiqueDto>();
				for (StatutJuridique entity : itemsSaved) {
					StatutJuridiqueDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update StatutJuridique-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete StatutJuridique by using StatutJuridiqueDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<StatutJuridiqueDto> delete(Request<StatutJuridiqueDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete StatutJuridique-----");

		response = new Response<StatutJuridiqueDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<StatutJuridique> items = new ArrayList<StatutJuridique>();

			for (StatutJuridiqueDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la statutJuridique existe
				StatutJuridique existingEntity = null;
				existingEntity = statutJuridiqueRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("statutJuridique -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// entreprise
				List<Entreprise> listOfEntreprise = entrepriseRepository.findByStatutJuridiqueId(existingEntity.getId(), false);
				if (listOfEntreprise != null && !listOfEntreprise.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfEntreprise.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				statutJuridiqueRepository.save((Iterable<StatutJuridique>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete StatutJuridique-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete StatutJuridique by using StatutJuridiqueDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<StatutJuridiqueDto> forceDelete(Request<StatutJuridiqueDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete StatutJuridique-----");

		response = new Response<StatutJuridiqueDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<StatutJuridique> items = new ArrayList<StatutJuridique>();

			for (StatutJuridiqueDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la statutJuridique existe
				StatutJuridique existingEntity = null;
				existingEntity = statutJuridiqueRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("statutJuridique -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// entreprise
				List<Entreprise> listOfEntreprise = entrepriseRepository.findByStatutJuridiqueId(existingEntity.getId(), false);
				if (listOfEntreprise != null && !listOfEntreprise.isEmpty()){
					Request<EntrepriseDto> deleteRequest = new Request<EntrepriseDto>();
					deleteRequest.setDatas(EntrepriseTransformer.INSTANCE.toDtos(listOfEntreprise));
					deleteRequest.setUser(request.getUser());
					Response<EntrepriseDto> deleteResponse = entrepriseBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				statutJuridiqueRepository.save((Iterable<StatutJuridique>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete StatutJuridique-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get StatutJuridique by using StatutJuridiqueDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<StatutJuridiqueDto> getByCriteria(Request<StatutJuridiqueDto> request, Locale locale) {
		slf4jLogger.info("----begin get StatutJuridique-----");

		response = new Response<StatutJuridiqueDto>();

		try {
			List<StatutJuridique> items = null;
			items = statutJuridiqueRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<StatutJuridiqueDto> itemsDto = new ArrayList<StatutJuridiqueDto>();
				for (StatutJuridique entity : items) {
					StatutJuridiqueDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(statutJuridiqueRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("statutJuridique", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get StatutJuridique-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full StatutJuridiqueDto by using StatutJuridique as object.
	 *
	 * @param entity, locale
	 * @return StatutJuridiqueDto
	 *
	 */
	private StatutJuridiqueDto getFullInfos(StatutJuridique entity, Integer size, Locale locale) throws Exception {
		StatutJuridiqueDto dto = StatutJuridiqueTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
