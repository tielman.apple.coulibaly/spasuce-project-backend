package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._OffreRepository;

/**
 * Repository : Offre.
 */
@Repository
public interface OffreRepository extends JpaRepository<Offre, Integer>, _OffreRepository {
	/**
	 * Finds Offre by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object Offre whose id is equals to the given id. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.id = :id and e.isDeleted = :isDeleted")
	Offre findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds Offre by using code as a search criteria.
	 *
	 * @param code
	 * @return An Object Offre whose code is equals to the given code. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.code = :code and e.isDeleted = :isDeleted")
	Offre findByCode(@Param("code")String code, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds Offre by using codeContrat as a search criteria.
	 *
	 * @param codeContrat
	 * @return An Object Offre whose codeContrat is equals to the given codeContrat. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.codeContrat = :codeContrat and e.isDeleted = :isDeleted")
	Offre findByCodeContrat(@Param("codeContrat")String codeContrat, @Param("isDeleted")Boolean isDeleted);
	

	/**
	 * Finds Offre by using messageClient as a search criteria.
	 *
	 * @param messageClient
	 * @return An Object Offre whose messageClient is equals to the given messageClient. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.messageClient = :messageClient and e.isDeleted = :isDeleted")
	List<Offre> findByMessageClient(@Param("messageClient")String messageClient, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using isSelectedByClient as a search criteria.
	 *
	 * @param isSelectedByClient
	 * @return An Object Offre whose isSelectedByClient is equals to the given isSelectedByClient. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.isSelectedByClient = :isSelectedByClient and e.isDeleted = :isDeleted")
	List<Offre> findByIsSelectedByClient(@Param("isSelectedByClient")Boolean isSelectedByClient, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using commentaire as a search criteria.
	 *
	 * @param commentaire
	 * @return An Object Offre whose commentaire is equals to the given commentaire. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.commentaire = :commentaire and e.isDeleted = :isDeleted")
	List<Offre> findByCommentaire(@Param("commentaire")String commentaire, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using noteOffre as a search criteria.
	 *
	 * @param noteOffre
	 * @return An Object Offre whose noteOffre is equals to the given noteOffre. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.noteOffre = :noteOffre and e.isDeleted = :isDeleted")
	List<Offre> findByNoteOffre(@Param("noteOffre")Float noteOffre, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using dateSoumission as a search criteria.
	 *
	 * @param dateSoumission
	 * @return An Object Offre whose dateSoumission is equals to the given dateSoumission. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.dateSoumission = :dateSoumission and e.isDeleted = :isDeleted")
	List<Offre> findByDateSoumission(@Param("dateSoumission")Date dateSoumission, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using isPayer as a search criteria.
	 *
	 * @param isPayer
	 * @return An Object Offre whose isPayer is equals to the given isPayer. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.isPayer = :isPayer and e.isDeleted = :isDeleted")
	List<Offre> findByIsPayer(@Param("isPayer")Boolean isPayer, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using isNoterByClient as a search criteria.
	 *
	 * @param isNoterByClient
	 * @return An Object Offre whose isNoterByClient is equals to the given isNoterByClient. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.isNoterByClient = :isNoterByClient and e.isDeleted = :isDeleted")
	List<Offre> findByIsNoterByClient(@Param("isNoterByClient")Boolean isNoterByClient, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using isVisualiserNoteByFournisseur as a search criteria.
	 *
	 * @param isVisualiserNoteByFournisseur
	 * @return An Object Offre whose isVisualiserNoteByFournisseur is equals to the given isVisualiserNoteByFournisseur. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.isVisualiserNoteByFournisseur = :isVisualiserNoteByFournisseur and e.isDeleted = :isDeleted")
	List<Offre> findByIsVisualiserNoteByFournisseur(@Param("isVisualiserNoteByFournisseur")Boolean isVisualiserNoteByFournisseur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using isVisualiserNotifByClient as a search criteria.
	 *
	 * @param isVisualiserNotifByClient
	 * @return An Object Offre whose isVisualiserNotifByClient is equals to the given isVisualiserNotifByClient. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.isVisualiserNotifByClient = :isVisualiserNotifByClient and e.isDeleted = :isDeleted")
	List<Offre> findByIsVisualiserNotifByClient(@Param("isVisualiserNotifByClient")Boolean isVisualiserNotifByClient, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using isVisualiserContenuByClient as a search criteria.
	 *
	 * @param isVisualiserContenuByClient
	 * @return An Object Offre whose isVisualiserContenuByClient is equals to the given isVisualiserContenuByClient. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.isVisualiserContenuByClient = :isVisualiserContenuByClient and e.isDeleted = :isDeleted")
	List<Offre> findByIsVisualiserContenuByClient(@Param("isVisualiserContenuByClient")Boolean isVisualiserContenuByClient, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using isModifiable as a search criteria.
	 *
	 * @param isModifiable
	 * @return An Object Offre whose isModifiable is equals to the given isModifiable. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.isModifiable = :isModifiable and e.isDeleted = :isDeleted")
	List<Offre> findByIsModifiable(@Param("isModifiable")Boolean isModifiable, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using isUpdate as a search criteria.
	 *
	 * @param isUpdate
	 * @return An Object Offre whose isUpdate is equals to the given isUpdate. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.isUpdate = :isUpdate and e.isDeleted = :isDeleted")
	List<Offre> findByIsUpdate(@Param("isUpdate")Boolean isUpdate, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using isRetirer as a search criteria.
	 *
	 * @param isRetirer
	 * @return An Object Offre whose isRetirer is equals to the given isRetirer. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.isRetirer = :isRetirer and e.isDeleted = :isDeleted")
	List<Offre> findByIsRetirer(@Param("isRetirer")Boolean isRetirer, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using prix as a search criteria.
	 *
	 * @param prix
	 * @return An Object Offre whose prix is equals to the given prix. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.prix = :prix and e.isDeleted = :isDeleted")
	List<Offre> findByPrix(@Param("prix")Integer prix, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object Offre whose createdAt is equals to the given createdAt. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Offre> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object Offre whose createdBy is equals to the given createdBy. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Offre> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object Offre whose updatedAt is equals to the given updatedAt. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Offre> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object Offre whose updatedBy is equals to the given updatedBy. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Offre> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object Offre whose deletedAt is equals to the given deletedAt. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Offre> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object Offre whose deletedBy is equals to the given deletedBy. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Offre> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Offre by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object Offre whose isDeleted is equals to the given isDeleted. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.isDeleted = :isDeleted")
	List<Offre> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Offre by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object Offre whose etatId is equals to the given etatId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
	List<Offre> findByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Offre by using etatId as a search criteria.
   *
   * @param etatId
   * @return An Object Offre whose etatId is equals to the given etatId. If
   *         no Offre is found, this method returns null.
   */
  @Query("select e from Offre e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
  Offre findOffreByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Offre by using classeNoteId as a search criteria.
	 *
	 * @param classeNoteId
	 * @return A list of Object Offre whose classeNoteId is equals to the given classeNoteId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.classeNote.id = :classeNoteId and e.isDeleted = :isDeleted")
	List<Offre> findByClasseNoteId(@Param("classeNoteId")Integer classeNoteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Offre by using classeNoteId as a search criteria.
   *
   * @param classeNoteId
   * @return An Object Offre whose classeNoteId is equals to the given classeNoteId. If
   *         no Offre is found, this method returns null.
   */
  @Query("select e from Offre e where e.classeNote.id = :classeNoteId and e.isDeleted = :isDeleted")
  Offre findOffreByClasseNoteId(@Param("classeNoteId")Integer classeNoteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Offre by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object Offre whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.user.id = :userFournisseurId and e.isDeleted = :isDeleted")
	List<Offre> findByUserFournisseurId(@Param("userFournisseurId")Integer userFournisseurId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Offre by using userFournisseurId as a search criteria.
   *
   * @param userFournisseurId
   * @return An Object Offre whose userFournisseurId is equals to the given userFournisseurId. If
   *         no Offre is found, this method returns null.
   */
  @Query("select e from Offre e where e.user.id = :userFournisseurId and e.isDeleted = :isDeleted")
  Offre findOffreByUserFournisseurId(@Param("userFournisseurId")Integer userFournisseurId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Offre by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object Offre whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.appelDoffres.id = :appelDoffresId and e.isDeleted = :isDeleted")
	List<Offre> findByAppelDoffresId(@Param("appelDoffresId")Integer appelDoffresId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Offre by using appelDoffresId as a search criteria.
   *
   * @param appelDoffresId
   * @return An Object Offre whose appelDoffresId is equals to the given appelDoffresId. If
   *         no Offre is found, this method returns null.
   */
  @Query("select e from Offre e where e.appelDoffres.id = :appelDoffresId and e.isDeleted = :isDeleted")
  Offre findOffreByAppelDoffresId(@Param("appelDoffresId")Integer appelDoffresId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of Offre by using offreDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Offre
	 * @throws DataAccessException,ParseException
	 */
	public default List<Offre> getByCriteria(Request<OffreDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Offre e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Offre> query = em.createQuery(req, Offre.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Offre by using offreDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Offre
	 *
	 */
	public default Long count(Request<OffreDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Offre e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<OffreDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		OffreDto dto = request.getData() != null ? request.getData() : new OffreDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (OffreDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(OffreDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCodeContrat())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("codeContrat", dto.getCodeContrat(), "e.codeContrat", "String", dto.getCodeContratParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("code", dto.getCode(), "e.code", "String", dto.getCodeParam(), param, index, locale));
			}
			
			
			
			if (Utilities.notBlank(dto.getMessageClient())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("messageClient", dto.getMessageClient(), "e.messageClient", "String", dto.getMessageClientParam(), param, index, locale));
			}
			if (dto.getIsSelectedByClient()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isSelectedByClient", dto.getIsSelectedByClient(), "e.isSelectedByClient", "Boolean", dto.getIsSelectedByClientParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCommentaire())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("commentaire", dto.getCommentaire(), "e.commentaire", "String", dto.getCommentaireParam(), param, index, locale));
			}
			if (dto.getNoteOffre()!= null && dto.getNoteOffre() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("noteOffre", dto.getNoteOffre(), "e.noteOffre", "Float", dto.getNoteOffreParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateSoumission())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateSoumission", dto.getDateSoumission(), "e.dateSoumission", "Date", dto.getDateSoumissionParam(), param, index, locale));
			}
			if (dto.getIsPayer()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isPayer", dto.getIsPayer(), "e.isPayer", "Boolean", dto.getIsPayerParam(), param, index, locale));
			}
			if (dto.getIsNoterByClient()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isNoterByClient", dto.getIsNoterByClient(), "e.isNoterByClient", "Boolean", dto.getIsNoterByClientParam(), param, index, locale));
			}
			if (dto.getIsVisualiserNoteByFournisseur()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isVisualiserNoteByFournisseur", dto.getIsVisualiserNoteByFournisseur(), "e.isVisualiserNoteByFournisseur", "Boolean", dto.getIsVisualiserNoteByFournisseurParam(), param, index, locale));
			}
			if (dto.getIsVisualiserNotifByClient()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isVisualiserNotifByClient", dto.getIsVisualiserNotifByClient(), "e.isVisualiserNotifByClient", "Boolean", dto.getIsVisualiserNotifByClientParam(), param, index, locale));
			}
			if (dto.getIsVisualiserContenuByClient()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isVisualiserContenuByClient", dto.getIsVisualiserContenuByClient(), "e.isVisualiserContenuByClient", "Boolean", dto.getIsVisualiserContenuByClientParam(), param, index, locale));
			}
			if (dto.getIsModifiable()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isModifiable", dto.getIsModifiable(), "e.isModifiable", "Boolean", dto.getIsModifiableParam(), param, index, locale));
			}
			if (dto.getIsUpdate()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isUpdate", dto.getIsUpdate(), "e.isUpdate", "Boolean", dto.getIsUpdateParam(), param, index, locale));
			}
			if (dto.getIsRetirer()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isRetirer", dto.getIsRetirer(), "e.isRetirer", "Boolean", dto.getIsRetirerParam(), param, index, locale));
			}
			if (dto.getPrix()!= null && dto.getPrix() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prix", dto.getPrix(), "e.prix", "Integer", dto.getPrixParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getEtatId()!= null && dto.getEtatId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatId", dto.getEtatId(), "e.etat.id", "Integer", dto.getEtatIdParam(), param, index, locale));
			}
			if (dto.getClasseNoteId()!= null && dto.getClasseNoteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("classeNoteId", dto.getClasseNoteId(), "e.classeNote.id", "Integer", dto.getClasseNoteIdParam(), param, index, locale));
			}
			if (dto.getUserFournisseurId()!= null && dto.getUserFournisseurId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userFournisseurId", dto.getUserFournisseurId(), "e.user.id", "Integer", dto.getUserFournisseurIdParam(), param, index, locale));
			}
			if (dto.getAppelDoffresId()!= null && dto.getAppelDoffresId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("appelDoffresId", dto.getAppelDoffresId(), "e.appelDoffres.id", "Integer", dto.getAppelDoffresIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatCode", dto.getEtatCode(), "e.etat.code", "String", dto.getEtatCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatLibelle", dto.getEtatLibelle(), "e.etat.libelle", "String", dto.getEtatLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getClasseNoteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("classeNoteCode", dto.getClasseNoteCode(), "e.classeNote.code", "String", dto.getClasseNoteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getClasseNoteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("classeNoteLibelle", dto.getClasseNoteLibelle(), "e.classeNote.libelle", "String", dto.getClasseNoteLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserPrenoms())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenoms", dto.getUserPrenoms(), "e.user.prenoms", "String", dto.getUserPrenomsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getAppelDoffresLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("appelDoffresLibelle", dto.getAppelDoffresLibelle(), "e.appelDoffres.libelle", "String", dto.getAppelDoffresLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
