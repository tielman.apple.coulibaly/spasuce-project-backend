package ci.salaamsolutions.projet.spasuce.helper.enums;

public class TypeDocumentEnum {
	public static final String	IMAGE			= "image";
	public static final String	VIDEO			= "video";
	public static final String	DOC_TEXT		= "document_text";
	public static final String	OTHER		    = "autre";

}
