


                                                                                /*
 * Java transformer for entity table sous_critere_note
 * Created on 2020-01-02 ( Time 18:00:03 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "sous_critere_note"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class SousCritereNoteBusiness implements IBasicBusiness<Request<SousCritereNoteDto>, Response<SousCritereNoteDto>> {

  private Response<SousCritereNoteDto> response;
  @Autowired
  private SousCritereNoteRepository sousCritereNoteRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private CritereNoteRepository critereNoteRepository;
    @Autowired
    private CritereNoteOffreRepository critereNoteOffreRepository;
  @Autowired
    private MoyenCalculRepository moyenCalculRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

                          
  @Autowired
  private CritereNoteOffreBusiness critereNoteOffreBusiness;

                          
  @Autowired
  private MoyenCalculBusiness moyenCalculBusiness;

      

  public SousCritereNoteBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create SousCritereNote by using SousCritereNoteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<SousCritereNoteDto> create(Request<SousCritereNoteDto> request, Locale locale)  {
    slf4jLogger.info("----begin create SousCritereNote-----");

    response = new Response<SousCritereNoteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<SousCritereNote> items = new ArrayList<SousCritereNote>();

      for (SousCritereNoteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("libelle", dto.getLibelle());
        fieldsToVerify.put("coefficient", dto.getCoefficient());
        fieldsToVerify.put("critereNoteId", dto.getCritereNoteId());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if sousCritereNote to insert do not exist
        SousCritereNote existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("sousCritereNote -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
        existingEntity = sousCritereNoteRepository.findByLibelle(dto.getLibelle(), false);
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("sousCritereNote -> " + dto.getLibelle(), locale));
          response.setHasError(true);
          return response;
        }
        if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
          response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les sousCritereNotes", locale));
          response.setHasError(true);
          return response;
        }

        // Verify if critereNote exist
                CritereNote existingCritereNote = critereNoteRepository.findById(dto.getCritereNoteId(), false);
                if (existingCritereNote == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("critereNote -> " + dto.getCritereNoteId(), locale));
          response.setHasError(true);
          return response;
        }
        SousCritereNote entityToSave = null;
        entityToSave = SousCritereNoteTransformer.INSTANCE.toEntity(dto, existingCritereNote);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        entityToSave.setIsDeleted(false);
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<SousCritereNote> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = sousCritereNoteRepository.save((Iterable<SousCritereNote>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("sousCritereNote", locale));
          response.setHasError(true);
          return response;
        }
        List<SousCritereNoteDto> itemsDto = new ArrayList<SousCritereNoteDto>();
        for (SousCritereNote entity : itemsSaved) {
          SousCritereNoteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create SousCritereNote-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update SousCritereNote by using SousCritereNoteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<SousCritereNoteDto> update(Request<SousCritereNoteDto> request, Locale locale)  {
    slf4jLogger.info("----begin update SousCritereNote-----");

    response = new Response<SousCritereNoteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<SousCritereNote> items = new ArrayList<SousCritereNote>();

      for (SousCritereNoteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la sousCritereNote existe
        SousCritereNote entityToSave = null;
        entityToSave = sousCritereNoteRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("sousCritereNote -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if critereNote exist
        if (dto.getCritereNoteId() != null && dto.getCritereNoteId() > 0){
                    CritereNote existingCritereNote = critereNoteRepository.findById(dto.getCritereNoteId(), false);
          if (existingCritereNote == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("critereNote -> " + dto.getCritereNoteId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setCritereNote(existingCritereNote);
        }
        if (Utilities.notBlank(dto.getLibelle())) {
                        SousCritereNote existingEntity = sousCritereNoteRepository.findByLibelle(dto.getLibelle(), false);
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("sousCritereNote -> " + dto.getLibelle(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les sousCritereNotes", locale));
                  response.setHasError(true);
                  return response;
                }
                  entityToSave.setLibelle(dto.getLibelle());
        }
        if (dto.getCoefficient() != null && dto.getCoefficient() > 0) {
          entityToSave.setCoefficient(dto.getCoefficient());
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<SousCritereNote> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = sousCritereNoteRepository.save((Iterable<SousCritereNote>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("sousCritereNote", locale));
          response.setHasError(true);
          return response;
        }
        List<SousCritereNoteDto> itemsDto = new ArrayList<SousCritereNoteDto>();
        for (SousCritereNote entity : itemsSaved) {
          SousCritereNoteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update SousCritereNote-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete SousCritereNote by using SousCritereNoteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<SousCritereNoteDto> delete(Request<SousCritereNoteDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete SousCritereNote-----");

    response = new Response<SousCritereNoteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<SousCritereNote> items = new ArrayList<SousCritereNote>();

      for (SousCritereNoteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la sousCritereNote existe
        SousCritereNote existingEntity = null;
        existingEntity = sousCritereNoteRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("sousCritereNote -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

        // critereNoteOffre
        List<CritereNoteOffre> listOfCritereNoteOffre = critereNoteOffreRepository.findBySousCritereNoteId(existingEntity.getId(), false);
        if (listOfCritereNoteOffre != null && !listOfCritereNoteOffre.isEmpty()){
          response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfCritereNoteOffre.size() + ")", locale));
          response.setHasError(true);
          return response;
        }
        // moyenCalcul
        List<MoyenCalcul> listOfMoyenCalcul = moyenCalculRepository.findBySousCritereNoteId(existingEntity.getId(), false);
        if (listOfMoyenCalcul != null && !listOfMoyenCalcul.isEmpty()){
          response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfMoyenCalcul.size() + ")", locale));
          response.setHasError(true);
          return response;
        }


        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        existingEntity.setIsDeleted(true);
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        sousCritereNoteRepository.save((Iterable<SousCritereNote>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete SousCritereNote-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete SousCritereNote by using SousCritereNoteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<SousCritereNoteDto> forceDelete(Request<SousCritereNoteDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete SousCritereNote-----");

    response = new Response<SousCritereNoteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<SousCritereNote> items = new ArrayList<SousCritereNote>();

      for (SousCritereNoteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la sousCritereNote existe
        SousCritereNote existingEntity = null;
          existingEntity = sousCritereNoteRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("sousCritereNote -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

                                            // critereNoteOffre
        List<CritereNoteOffre> listOfCritereNoteOffre = critereNoteOffreRepository.findBySousCritereNoteId(existingEntity.getId(), false);
        if (listOfCritereNoteOffre != null && !listOfCritereNoteOffre.isEmpty()){
          Request<CritereNoteOffreDto> deleteRequest = new Request<CritereNoteOffreDto>();
          deleteRequest.setDatas(CritereNoteOffreTransformer.INSTANCE.toDtos(listOfCritereNoteOffre));
          deleteRequest.setUser(request.getUser());
          Response<CritereNoteOffreDto> deleteResponse = critereNoteOffreBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }
                        // moyenCalcul
        List<MoyenCalcul> listOfMoyenCalcul = moyenCalculRepository.findBySousCritereNoteId(existingEntity.getId(), false);
        if (listOfMoyenCalcul != null && !listOfMoyenCalcul.isEmpty()){
          Request<MoyenCalculDto> deleteRequest = new Request<MoyenCalculDto>();
          deleteRequest.setDatas(MoyenCalculTransformer.INSTANCE.toDtos(listOfMoyenCalcul));
          deleteRequest.setUser(request.getUser());
          Response<MoyenCalculDto> deleteResponse = moyenCalculBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }
    

                                                          existingEntity.setDeletedAt(Utilities.getCurrentDate());
                    existingEntity.setDeletedBy(request.getUser());
              existingEntity.setIsDeleted(true);
              items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          sousCritereNoteRepository.save((Iterable<SousCritereNote>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete SousCritereNote-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get SousCritereNote by using SousCritereNoteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<SousCritereNoteDto> getByCriteria(Request<SousCritereNoteDto> request, Locale locale) {
    slf4jLogger.info("----begin get SousCritereNote-----");

    response = new Response<SousCritereNoteDto>();

    try {
      List<SousCritereNote> items = null;
      items = sousCritereNoteRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<SousCritereNoteDto> itemsDto = new ArrayList<SousCritereNoteDto>();
        for (SousCritereNote entity : items) {
          SousCritereNoteDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(sousCritereNoteRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("sousCritereNote", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get SousCritereNote-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full SousCritereNoteDto by using SousCritereNote as object.
   *
   * @param entity, locale
   * @return SousCritereNoteDto
   *
   */
  private SousCritereNoteDto getFullInfos(SousCritereNote entity, Integer size, Locale locale) throws Exception {
    SousCritereNoteDto dto = SousCritereNoteTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
