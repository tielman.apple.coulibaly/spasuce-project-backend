
/*
 * Java dto for entity table offre
 * Created on 2020-03-03 ( Time 13:45:01 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.customize._OffreDto;

import lombok.*;
/**
 * DTO for table "offre"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class OffreDto extends _OffreDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private String     code                 ;
	/*
	 * 
	 */
    private String     codeContrat          ;
	/*
	 * 
	 */
    private String     messageClient        ;
	/*
	 * 
	 */
    private Boolean    isSelectedByClient   ;
	/*
	 * 
	 */
    private String     commentaire          ;
	/*
	 * 
	 */
    private Float      noteOffre            ;
	/*
	 * 
	 */
	private String     dateSoumission       ;
	/*
	 * 
	 */
    private Boolean    isPayer              ;
	/*
	 * Pour savoir si l'offre à été notée par le client 
	 */
    private Boolean    isNoterByClient      ;
	/*
	 * Pour savoir si le fournisseur à vu sa note pour eviter toutes modifications apres
	 */
    private Boolean    isVisualiserNoteByFournisseur ;
	/*
	 * Quand le client aura  cliqué sur la notification
	 */
    private Boolean    isVisualiserNotifByClient ;
	/*
	 * Quand le client aura lu le contenu de la notification	
	 */
    private Boolean    isVisualiserContenuByClient ;
	/*
	 * Pour savoir si le fournisseur peut toujours modifier l'offre
	 */
    private Boolean    isModifiable         ;
	/*
	 * Pour dire que l'offre a été modifier après soumission dans le système
	 */
    private Boolean    isUpdate             ;
	/*
	 * Pour dire que l'offre a été retirer par le fournisseur concernant cet appel d'offres
	 */
    private Boolean    isRetirer            ;
	/*
	 * 
	 */
    private Integer    prix                 ;
	/*
	 * 
	 */
    private Integer    appelDoffresId       ;
	/*
	 * 
	 */
    private Integer    classeNoteId         ;
	/*
	 * 
	 */
    private Integer    userFournisseurId    ;
	/*
	 * 
	 */
    private Integer    etatId               ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String etatCode;
	private String etatLibelle;
	private String classeNoteCode;
	private String classeNoteLibelle;
	private String userNom;
	private String userPrenoms;
	private String userLogin;
	private String appelDoffresLibelle;
	private String appelDoffresCode;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   codeParam             ;                     
	private SearchParam<String>   codeContratParam      ;                     
	private SearchParam<String>   messageClientParam    ;                     
	private SearchParam<Boolean>  isSelectedByClientParam;                     
	private SearchParam<String>   commentaireParam      ;                     
	private SearchParam<Float>    noteOffreParam        ;                     

		private SearchParam<String>   dateSoumissionParam   ;                     
	private SearchParam<Boolean>  isPayerParam          ;                     
	private SearchParam<Boolean>  isNoterByClientParam  ;                     
	private SearchParam<Boolean>  isVisualiserNoteByFournisseurParam;                     
	private SearchParam<Boolean>  isVisualiserNotifByClientParam;                     
	private SearchParam<Boolean>  isVisualiserContenuByClientParam;                     
	private SearchParam<Boolean>  isModifiableParam     ;                     
	private SearchParam<Boolean>  isUpdateParam         ;                     
	private SearchParam<Boolean>  isRetirerParam        ;                     
	private SearchParam<Integer>  prixParam             ;                     
	private SearchParam<Integer>  appelDoffresIdParam   ;                     
	private SearchParam<Integer>  classeNoteIdParam     ;                     
	private SearchParam<Integer>  userFournisseurIdParam;                     
	private SearchParam<Integer>  etatIdParam           ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   etatCodeParam         ;                     
	private SearchParam<String>   etatLibelleParam      ;                     
	private SearchParam<String>   classeNoteCodeParam   ;                     
	private SearchParam<String>   classeNoteLibelleParam;                     
	private SearchParam<String>   userNomParam          ;                     
	private SearchParam<String>   userPrenomsParam      ;                     
	private SearchParam<String>   userLoginParam        ;                     
	private SearchParam<String>   appelDoffresLibelleParam;                     
	private SearchParam<String>   appelDoffresCodeParam ;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		OffreDto other = (OffreDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
