

/*
 * Java transformer for entity table valeur_critere_offre 
 * Created on 2020-01-02 ( Time 18:00:06 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "valeur_critere_offre"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface ValeurCritereOffreTransformer {

	ValeurCritereOffreTransformer INSTANCE = Mappers.getMapper(ValeurCritereOffreTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.offre.id", target="offreId"),
		
		@Mapping(source="entity.offre.commentaire", target="commentaire"), //ajouter
		@Mapping(source="entity.offre.prix", target="prix"), //ajouter
		@Mapping(source="entity.offre.user.entreprise.nom", target="entrepriseNom"), //ajouter
		@Mapping(source="entity.offre.dateSoumission", dateFormat="dd/MM/yyyy",target="dateSoumission"), //ajouter

		@Mapping(source="entity.critereAppelDoffres.id", target="critereId"),
		@Mapping(source="entity.critereAppelDoffres.libelle", target="critereAppelDoffresLibelle"),
		@Mapping(source="entity.critereAppelDoffres.typeCritereAppelDoffres.code", target="typeCritereAppelDoffresCode"), //Ajouter 

	})
	ValeurCritereOffreDto toDto(ValeurCritereOffre entity) throws ParseException;

    List<ValeurCritereOffreDto> toDtos(List<ValeurCritereOffre> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.valeur", target="valeur"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="offre", target="offre"),
		@Mapping(source="critereAppelDoffres", target="critereAppelDoffres"),
	})
    ValeurCritereOffre toEntity(ValeurCritereOffreDto dto, Offre offre, CritereAppelDoffres critereAppelDoffres) throws ParseException;

    //List<ValeurCritereOffre> toEntities(List<ValeurCritereOffreDto> dtos) throws ParseException;

}
