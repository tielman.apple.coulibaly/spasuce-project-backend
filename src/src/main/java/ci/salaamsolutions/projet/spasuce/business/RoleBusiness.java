


/*
 * Java transformer for entity table role
 * Created on 2020-01-02 ( Time 18:00:02 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.dao.entity.Fonctionnalite;
import ci.salaamsolutions.projet.spasuce.dao.entity.Role;
import ci.salaamsolutions.projet.spasuce.dao.entity.RoleFonctionnalite;
import ci.salaamsolutions.projet.spasuce.dao.entity.User;
import ci.salaamsolutions.projet.spasuce.dao.repository.FonctionnaliteRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.RoleFonctionnaliteRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.RoleRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.UserRepository;
import ci.salaamsolutions.projet.spasuce.helper.ExceptionUtils;
import ci.salaamsolutions.projet.spasuce.helper.FunctionalError;
import ci.salaamsolutions.projet.spasuce.helper.TechnicalError;
import ci.salaamsolutions.projet.spasuce.helper.Utilities;
import ci.salaamsolutions.projet.spasuce.helper.Validate;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.dto.FonctionnaliteDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.RoleDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.RoleFonctionnaliteDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.UserDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.FonctionnaliteTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.RoleFonctionnaliteTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.RoleTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.UserTransformer;

/**
BUSINESS for table "role"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class RoleBusiness implements IBasicBusiness<Request<RoleDto>, Response<RoleDto>> {

	private Response<RoleDto> response;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleFonctionnaliteRepository roleFonctionnaliteRepository;
	@Autowired
	private FonctionnaliteRepository fonctionnaliteRepository;



	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private UserBusiness userBusiness;


	@Autowired
	private RoleFonctionnaliteBusiness roleFonctionnaliteBusiness;



	public RoleBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create Role by using RoleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<RoleDto> create(Request<RoleDto> request, Locale locale)  {
		slf4jLogger.info("----begin create Role-----");

		response = new Response<RoleDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Role> items = new ArrayList<Role>();
			List<RoleFonctionnalite> itemsRoleFonctionnalite = new ArrayList<>();

			for (RoleDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("code", dto.getCode());
				fieldsToVerify.put("libelle", dto.getLibelle());
				//fieldsToVerify.put("datasFonctionnalite", dto.getDatasFonctionnalite());
				//        fieldsToVerify.put("createdBy", dto.getCreatedBy());
				//        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
				//        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
				//        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				//        if (dto.getDatasFonctionnalite().isEmpty()) {
				//            response.setStatus(functionalError.FIELD_EMPTY("datasFonctionnalite", locale));
				//            response.setHasError(true);
				//            return response;
				//          }

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if role to insert do not exist
				Role existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("role -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = roleRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("role -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les roles", locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = roleRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("role -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les roles", locale));
					response.setHasError(true);
					return response;
				}

				Role entityToSave = null;
				entityToSave = RoleTransformer.INSTANCE.toEntity(dto);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);


				if (Utilities.isNotEmpty(dto.getDatasFonctionnalite())) {
					List<Fonctionnalite> itemsFonctionnalite = new ArrayList<>();

					for (FonctionnaliteDto data : dto.getDatasFonctionnalite()) {
						Fonctionnalite fonctionnalite = fonctionnaliteRepository.findById(data.getId(), false);
						if (fonctionnalite == null) {
							response.setStatus(functionalError.DATA_NOT_EXIST("fonctionnalite -> " + data.getId(), locale));
							response.setHasError(true);
							return response;
						}
						if (itemsFonctionnalite.stream().anyMatch(a->a.getId().equals(data.getId()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication de la fonctionnalite '" + fonctionnalite.getLibelle()+"' pour le role '" + dto.getLibelle()+"'", locale));
							response.setHasError(true);
							return response;
						}

						RoleFonctionnalite roleFonctionnalite = new RoleFonctionnalite();
						roleFonctionnalite.setRole(entityToSave);
						roleFonctionnalite.setFonctionnalite(fonctionnalite);
						roleFonctionnalite.setCreatedAt(Utilities.getCurrentDate());
						roleFonctionnalite.setCreatedBy(request.getUser());
						roleFonctionnalite.setIsDeleted(false);

						itemsRoleFonctionnalite.add(roleFonctionnalite);

					}
				}

			}

			if (!items.isEmpty()) {
				List<Role> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = roleRepository.save((Iterable<Role>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("role", locale));
					response.setHasError(true);
					return response;
				}

				if (!itemsRoleFonctionnalite.isEmpty()) {
					List<RoleFonctionnalite> itemsRoleFonctionnaliteSaved = null;
					// inserer les donnees en base de donnees
					itemsRoleFonctionnaliteSaved = roleFonctionnaliteRepository.save((Iterable<RoleFonctionnalite>) itemsRoleFonctionnalite);
					if (itemsRoleFonctionnaliteSaved == null || itemsRoleFonctionnaliteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("roleFonctionnalite", locale));
						response.setHasError(true);
						return response;
					}

				}
				List<RoleDto> itemsDto = new ArrayList<RoleDto>();
				for (Role entity : itemsSaved) {
					RoleDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create Role-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Role by using RoleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<RoleDto> update(Request<RoleDto> request, Locale locale)  {
		slf4jLogger.info("----begin update Role-----");

		response = new Response<RoleDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Role> items = new ArrayList<Role>();
			List<RoleFonctionnalite> itemsRoleFonctionnaliteToCreate = new ArrayList<>();
			List<RoleFonctionnalite> itemsRoleFonctionnaliteToDelete = new ArrayList<>();


			for (RoleDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la role existe
				Role entityToSave = null;
				entityToSave = roleRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				if (Utilities.notBlank(dto.getCode())) {
					Role existingEntity = roleRepository.findByCode(dto.getCode(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("role -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les roles", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCode(dto.getCode());
				}
				if (Utilities.notBlank(dto.getLibelle())) {
					Role existingEntity = roleRepository.findByLibelle(dto.getLibelle(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("role -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les roles", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
				
				
				 if (Utilities.isNotEmpty(dto.getDatasFonctionnalite())) {
					List<Fonctionnalite> itemsFonctionnalite = new ArrayList<>();

					List<RoleFonctionnalite> roleFonctionnalites = roleFonctionnaliteRepository.findByRoleId(entityToSaveId, false);
					
					
					if (Utilities.isNotEmpty(roleFonctionnalites)) {
						itemsRoleFonctionnaliteToDelete.addAll(roleFonctionnalites);
					}
					
		        	 for (FonctionnaliteDto data : dto.getDatasFonctionnalite()) {
		        		Fonctionnalite fonctionnalite = fonctionnaliteRepository.findById(data.getId(), false);
		 				if (fonctionnalite == null) {
		 					response.setStatus(functionalError.DATA_NOT_EXIST("fonctionnalite -> " + data.getId(), locale));
		 					response.setHasError(true);
		 					return response;
		 				}
		 				if (itemsFonctionnalite.stream().anyMatch(a->a.getId().equals(data.getId()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication de la fonctionnalite '" + fonctionnalite.getLibelle()+"' pour le role '" + dto.getLibelle()+"'", locale));
							response.setHasError(true);
							return response;
						}
		 				
		 				RoleFonctionnalite roleFonctionnalite = new RoleFonctionnalite();
		 				roleFonctionnalite.setRole(entityToSave);
		 				roleFonctionnalite.setFonctionnalite(fonctionnalite);
		 				roleFonctionnalite.setCreatedAt(Utilities.getCurrentDate());
		 				roleFonctionnalite.setCreatedBy(request.getUser());
		 				roleFonctionnalite.setIsDeleted(false);
		 				
		 				itemsRoleFonctionnaliteToCreate.add(roleFonctionnalite);
		     		}
				}
//				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
//					entityToSave.setCreatedBy(dto.getCreatedBy());
//				}
//				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
//					entityToSave.setUpdatedBy(dto.getUpdatedBy());
//				}
//				if (Utilities.notBlank(dto.getDeletedAt())) {
//					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
//				}
//				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
//					entityToSave.setDeletedBy(dto.getDeletedBy());
//				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Role> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = roleRepository.save((Iterable<Role>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("role", locale));
					response.setHasError(true);
					return response;
				}
				
				if (!itemsRoleFonctionnaliteToDelete.isEmpty()) {
					
					for (RoleFonctionnalite data : itemsRoleFonctionnaliteToDelete) {
						data.setDeletedAt(Utilities.getCurrentDate());
						data.setDeletedBy(request.getUser());
						data.setIsDeleted(true);
					}
					List<RoleFonctionnalite> itemsRoleFonctionnaliteToDeleteSaved = null;
					// inserer les donnees en base de donnees
					itemsRoleFonctionnaliteToDeleteSaved = roleFonctionnaliteRepository.save((Iterable<RoleFonctionnalite>) itemsRoleFonctionnaliteToDelete);
					if (itemsRoleFonctionnaliteToDeleteSaved == null || itemsRoleFonctionnaliteToDeleteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("roleFonctionnalite", locale));
						response.setHasError(true);
						return response;
					}

				}
				
				if (!itemsRoleFonctionnaliteToCreate.isEmpty()) {
					List<RoleFonctionnalite> itemsRoleFonctionnaliteToCreateSaved = null;
					// inserer les donnees en base de donnees
					itemsRoleFonctionnaliteToCreateSaved = roleFonctionnaliteRepository.save((Iterable<RoleFonctionnalite>) itemsRoleFonctionnaliteToCreate);
					if (itemsRoleFonctionnaliteToCreateSaved == null || itemsRoleFonctionnaliteToCreateSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("roleFonctionnalite", locale));
						response.setHasError(true);
						return response;
					}

				}
				List<RoleDto> itemsDto = new ArrayList<RoleDto>();
				for (Role entity : itemsSaved) {
					RoleDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update Role-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete Role by using RoleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<RoleDto> delete(Request<RoleDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete Role-----");

		response = new Response<RoleDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Role> items = new ArrayList<Role>();

			for (RoleDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la role existe
				Role existingEntity = null;
				existingEntity = roleRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// user
				List<User> listOfUser = userRepository.findByRoleId(existingEntity.getId(), false);
				if (listOfUser != null && !listOfUser.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfUser.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// roleFonctionnalite
				List<RoleFonctionnalite> listOfRoleFonctionnalite = roleFonctionnaliteRepository.findByRoleId(existingEntity.getId(), false);
				if (listOfRoleFonctionnalite != null && !listOfRoleFonctionnalite.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfRoleFonctionnalite.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				roleRepository.save((Iterable<Role>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete Role-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete Role by using RoleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<RoleDto> forceDelete(Request<RoleDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete Role-----");

		response = new Response<RoleDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Role> items = new ArrayList<Role>();

			for (RoleDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la role existe
				Role existingEntity = null;
				existingEntity = roleRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// user
				List<User> listOfUser = userRepository.findByRoleId(existingEntity.getId(), false);
				if (listOfUser != null && !listOfUser.isEmpty()){
					Request<UserDto> deleteRequest = new Request<UserDto>();
					deleteRequest.setDatas(UserTransformer.INSTANCE.toDtos(listOfUser));
					deleteRequest.setUser(request.getUser());
					Response<UserDto> deleteResponse = userBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// roleFonctionnalite
				List<RoleFonctionnalite> listOfRoleFonctionnalite = roleFonctionnaliteRepository.findByRoleId(existingEntity.getId(), false);
				if (listOfRoleFonctionnalite != null && !listOfRoleFonctionnalite.isEmpty()){
					Request<RoleFonctionnaliteDto> deleteRequest = new Request<RoleFonctionnaliteDto>();
					deleteRequest.setDatas(RoleFonctionnaliteTransformer.INSTANCE.toDtos(listOfRoleFonctionnalite));
					deleteRequest.setUser(request.getUser());
					Response<RoleFonctionnaliteDto> deleteResponse = roleFonctionnaliteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				roleRepository.save((Iterable<Role>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete Role-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get Role by using RoleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<RoleDto> getByCriteria(Request<RoleDto> request, Locale locale) {
		slf4jLogger.info("----begin get Role-----");

		response = new Response<RoleDto>();

		try {
			List<Role> items = null;
			items = roleRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<RoleDto> itemsDto = new ArrayList<RoleDto>();
				for (Role entity : items) {
					RoleDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(roleRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("role", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get Role-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full RoleDto by using Role as object.
	 *
	 * @param entity, locale
	 * @return RoleDto
	 *
	 */
	private RoleDto getFullInfos(Role entity, Integer size, Locale locale) throws Exception {
		RoleDto dto = RoleTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		List<RoleFonctionnalite> roleFonctionnalites =  roleFonctionnaliteRepository.findByRoleId(dto.getId(), false);

		if (Utilities.isNotEmpty(roleFonctionnalites)) {
			List<Fonctionnalite> datas = new ArrayList<>();

			for (RoleFonctionnalite data : roleFonctionnalites) {
				datas.add(data.getFonctionnalite());
			}
			if (!datas.isEmpty()) {
				dto.setDatasFonctionnalite(FonctionnaliteTransformer.INSTANCE.toDtos(datas));
			}
		}

		return dto;
	}
}
