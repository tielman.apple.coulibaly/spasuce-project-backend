//
//
//
///*
// * Java transformer for entity table fichier
// * Created on 2020-01-02 ( Time 17:59:58 )
// * Generator tool : Telosys Tools Generator ( version 3.1.2 )
// * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
// */
//
//package ci.salaamsolutions.projet.spasuce.business;
//
//import java.text.SimpleDateFormat;
//import java.util.List;
//import java.util.Date;
//import java.util.Locale;
//import java.util.Map;
//import java.util.ArrayList;
//import java.util.HashMap;
//
//import javax.persistence.PersistenceContext;
//import javax.persistence.EntityManager;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.dao.DataAccessException;
//import org.springframework.dao.DataAccessResourceFailureException;
//import org.springframework.dao.PermissionDeniedDataAccessException;
//import org.springframework.stereotype.Component;
//import org.springframework.transaction.annotation.Transactional;
//
//import ci.salaamsolutions.projet.spasuce.helper.dto.*;
//import ci.salaamsolutions.projet.spasuce.helper.dto.customize._Image;
//import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
//import ci.salaamsolutions.projet.spasuce.helper.enums.GlobalEnum;
//import ci.salaamsolutions.projet.spasuce.helper.contract.*;
//import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
//import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
//import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
//import ci.salaamsolutions.projet.spasuce.dao.entity.*;
//import ci.salaamsolutions.projet.spasuce.helper.*;
//import ci.salaamsolutions.projet.spasuce.dao.repository.*;
//
///**
//BUSINESS for table "fichier"
// *
// * @author SFL Back-End developper
// *
// */
//@Component
//public class FichierBusinessOld implements IBasicBusiness<Request<FichierDto>, Response<FichierDto>> {
//
//	private Response<FichierDto> response;
//	@Autowired
//	private FichierRepository fichierRepository;
//	@Autowired
//	private UserRepository userRepository;
//	@Autowired
//	private AppelDoffresRepository appelDoffresRepository;
//	@Autowired
//	private OffreRepository offreRepository;
//
//	@Autowired
//	private FunctionalError functionalError;
//	@Autowired
//	private TechnicalError technicalError;
//	@Autowired
//	private ExceptionUtils exceptionUtils;
//	@PersistenceContext
//	private EntityManager em;
//
//	private Logger slf4jLogger;
//	private SimpleDateFormat dateFormat;
//
//
//	@Autowired
//	private ParamsUtils paramsUtils;
//
//
//
//	public FichierBusinessOld() {
//		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//		slf4jLogger = LoggerFactory.getLogger(getClass());
//	}
//
//
//	/**
//	 * create Fichier by using FichierDto as object.
//	 *
//	 * @param request
//	 * @return response
//	 *
//	 */
//	@SuppressWarnings("unused")
//	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
//	@Override
//	public Response<FichierDto> create(Request<FichierDto> request, Locale locale)  {
//		slf4jLogger.info("----begin create Fichier-----");
//
//		response = new Response<FichierDto>();
//		List<String> listOfFilesCreate = new 	ArrayList<>();
//
//
//		try {
//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}
//
//			User utilisateur = userRepository.findById(request.getUser(), false);
//			if (utilisateur == null) {
//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
//				response.setHasError(true);
//				return response;
//			}
//			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
//				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
//				response.setHasError(true);
//				return response;
//			}
//
//			List<Fichier> items = new ArrayList<Fichier>();
//
//			for (FichierDto dto : request.getDatas()) {
//				// Definir les parametres obligatoires
//				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
//				fieldsToVerify.put("extension", dto.getExtension());
//				fieldsToVerify.put("name", dto.getName());
//				fieldsToVerify.put("fichierBase64", dto.getFichierBase64());
//				fieldsToVerify.put("source", dto.getSource());
//				fieldsToVerify.put("sourceId", dto.getSourceId());
//				
//				//fieldsToVerify.put("isFichierOffre", dto.getIsFichierOffre());
//
//				//fieldsToVerify.put("appelDoffresId", dto.getAppelDoffresId());
//				//fieldsToVerify.put("offreId", dto.getOffreId());
//				//fieldsToVerify.put("user", request.getUser());
//				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
//					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//					response.setHasError(true);
//					return response;
//				}
//
//				/*
//        // Verify if utilisateur exist
//        User utilisateur = userRepository.findById(request.getUser(), false);
//        if (utilisateur == null) {
//          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
//          response.setHasError(true);
//          return response;
//        }
//				 */
//
//				// Verify if fichier to insert do not exist
//				Fichier existingEntity = null;
//				//TODO: add/replace by the best method
//				if (existingEntity != null) {
//					response.setStatus(functionalError.DATA_EXIST("fichier -> " + dto.getId(), locale));
//					response.setHasError(true);
//					return response;
//				}
//				//        existingEntity = fichierRepository.findByName(dto.getName(), false);
//				//        if (existingEntity != null) {
//				//          response.setStatus(functionalError.DATA_EXIST("fichier -> " + dto.getName(), locale));
//				//          response.setHasError(true);
//				//          return response;
//				//        }
//				//        if (items.stream().anyMatch(a->a.getName().equalsIgnoreCase(dto.getName()))) {
//				//          response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du name '" + dto.getName()+"' pour les fichiers", locale));
//				//          response.setHasError(true);
//				//          return response;
//				//        }
//
//				// Verify if appelDoffres exist
//				AppelDoffres existingAppelDoffres = null ;
//				if (dto.getAppelDoffresId() != null && dto.getAppelDoffresId() > 0) {
//					existingAppelDoffres = appelDoffresRepository.findById(dto.getAppelDoffresId(), false);
//					if (existingAppelDoffres == null) {
//						response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getAppelDoffresId(), locale));
//						response.setHasError(true);
//						return response;
//					}
//				}
//				
//				
//
//				// Verify if offre exist
//				Offre existingOffre = null ;
//				if (dto.getOffreId() != null && dto.getOffreId() > 0) {
//					existingOffre = offreRepository.findById(dto.getOffreId(), false);
//					if (existingOffre == null) {
//						response.setStatus(functionalError.DATA_NOT_EXIST("offre -> " + dto.getOffreId(), locale));
//						response.setHasError(true);
//						return response;
//					}
//				}
//				
//				
//				switch (dto.getSource()) {
//				case value:
//					
//					break;
//case value:
//					
//					break;
//case value:
//	
//	break;
//case value:
//	
//	break;
//
//				default:
//					break;
//				}
//
//
//				// TODO : recuperation du base64 et le stocker sur le serveur
//				//if (Utilities.notBlank(dto.getFichierBase64()) && Utilities.notBlank(dto.getExtension()) && Utilities.notBlank(dto.getName()) ) {
//
//				// gestion du logo associe à l'entreprise
//				String cheminFile = null;
//				_Image image2 = new _Image();
//
//				image2.setExtension(dto.getExtension());
//				image2.setFileBase64(dto.getFichierBase64());
//				image2.setFileName(dto.getName());
//				if(image2 != null){
//
//					cheminFile =  Utilities.saveFile(image2, paramsUtils);
////					if (dto.getIsFichierOffre()) {
////						cheminFile =  Utilities.saveFileCustom(image2, GlobalEnum.appel_doffres, paramsUtils);
////					}else {
////						cheminFile =  Utilities.saveFileCustom(image2, GlobalEnum.offres, paramsUtils);
////					}
//
//					if(cheminFile==null){
//						response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
//						response.setHasError(true);
//						return response;
//					}
//					dto.setName(cheminFile);
//					listOfFilesCreate.add(cheminFile);
//				}
//				//Utilities.saveImage(data.getImageBase64(), "urlFichier", data.getImageExtension());
//				//image.setImageUrl(data.getImageUrl());
//				//}
//				Fichier entityToSave = null;
//				entityToSave = FichierTransformer.INSTANCE.toEntity(dto, existingAppelDoffres, existingOffre);
//				entityToSave.setCreatedAt(Utilities.getCurrentDate());
//				entityToSave.setCreatedBy(request.getUser());
//				entityToSave.setIsDeleted(false);
//				items.add(entityToSave);
//			}
//
//			if (!items.isEmpty()) {
//				List<Fichier> itemsSaved = null;
//				// inserer les donnees en base de donnees
//				itemsSaved = fichierRepository.save((Iterable<Fichier>) items);
//				if (itemsSaved == null || itemsSaved.isEmpty()) {
//					response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
//					response.setHasError(true);
//					return response;
//				}
//				List<FichierDto> itemsDto = new ArrayList<FichierDto>();
//				for (Fichier entity : itemsSaved) {
//					FichierDto dto = getFullInfos(entity, itemsSaved.size(), locale);
//					if (dto == null) continue;
//					itemsDto.add(dto);
//				}
//				response.setItems(itemsDto);
//				response.setHasError(false);
//			}
//
//			slf4jLogger.info("----end create Fichier-----");
//		} catch (PermissionDeniedDataAccessException e) {
//			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
//		} catch (DataAccessResourceFailureException e) {
//			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
//		} catch (DataAccessException e) {
//			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
//		} catch (RuntimeException e) {
//			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
//		} catch (Exception e) {
//			exceptionUtils.EXCEPTION(response, locale, e);
//		} finally {
//			if (response.isHasError() && response.getStatus() != null) {
//				Utilities.deleteFileOnSever(listOfFilesCreate, paramsUtils);
//				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
//				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
//			}
//		}
//		return response;
//	}
//
//	/**
//	 * update Fichier by using FichierDto as object.
//	 *
//	 * @param request
//	 * @return response
//	 *
//	 */
//	@SuppressWarnings("unused")
//	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
//	@Override
//	public Response<FichierDto> update(Request<FichierDto> request, Locale locale)  {
//		slf4jLogger.info("----begin update Fichier-----");
//
//		response = new Response<FichierDto>();
//
//		try {
//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}
//
//			User utilisateur = userRepository.findById(request.getUser(), false);
//			if (utilisateur == null) {
//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
//				response.setHasError(true);
//				return response;
//			}
//			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
//				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
//				response.setHasError(true);
//				return response;
//			}
//
//			List<Fichier> items = new ArrayList<Fichier>();
//
//			for (FichierDto dto : request.getDatas()) {
//				// Definir les parametres obligatoires
//				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
//				fieldsToVerify.put("id", dto.getId());
//				//fieldsToVerify.put("user", request.getUser());
//				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
//					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//					response.setHasError(true);
//					return response;
//				}
//
//				/*
//        // Verify if utilisateur exist
//        User utilisateur = userRepository.findById(request.getUser(), false);
//        if (utilisateur == null) {
//          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
//          response.setHasError(true);
//          return response;
//        }
//				 */
//
//				// Verifier si la fichier existe
//				Fichier entityToSave = null;
//				entityToSave = fichierRepository.findById(dto.getId(), false);
//				if (entityToSave == null) {
//					response.setStatus(functionalError.DATA_NOT_EXIST("fichier -> " + dto.getId(), locale));
//					response.setHasError(true);
//					return response;
//				}
//
//
//				Integer entityToSaveId = entityToSave.getId();
//
//				// Verify if appelDoffres exist
//				if (dto.getAppelDoffresId() != null && dto.getAppelDoffresId() > 0){
//					AppelDoffres existingAppelDoffres = appelDoffresRepository.findById(dto.getAppelDoffresId(), false);
//					if (existingAppelDoffres == null) {
//						response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getAppelDoffresId(), locale));
//						response.setHasError(true);
//						return response;
//					}
//					entityToSave.setAppelDoffres(existingAppelDoffres);
//				}
//				// Verify if offre exist
//				if (dto.getOffreId() != null && dto.getOffreId() > 0){
//					Offre existingOffre = offreRepository.findById(dto.getOffreId(), false);
//					if (existingOffre == null) {
//						response.setStatus(functionalError.DATA_NOT_EXIST("offre -> " + dto.getOffreId(), locale));
//						response.setHasError(true);
//						return response;
//					}
//					entityToSave.setOffre(existingOffre);
//				}
//				if (Utilities.notBlank(dto.getExtension())) {
//					entityToSave.setExtension(dto.getExtension());
//				}
//				if (Utilities.notBlank(dto.getName())) {
//					entityToSave.setName(dto.getName());
//				}
//				//        if (Utilities.notBlank(dto.getName())) {
//				//                        Fichier existingEntity = fichierRepository.findByName(dto.getName(), false);
//				//                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
//				//                  response.setStatus(functionalError.DATA_EXIST("fichier -> " + dto.getName(), locale));
//				//                  response.setHasError(true);
//				//                  return response;
//				//                }
//				//                if (items.stream().anyMatch(a->a.getName().equalsIgnoreCase(dto.getName()) && !a.getId().equals(entityToSaveId))) {
//				//                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du name '" + dto.getName()+"' pour les fichiers", locale));
//				//                  response.setHasError(true);
//				//                  return response;
//				//                }
//				//                  entityToSave.setName(dto.getName());
//				//        }
//				if (dto.getIsFichierOffre() != null) {
//					entityToSave.setIsFichierOffre(dto.getIsFichierOffre());
//				}
//				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
//					entityToSave.setCreatedBy(dto.getCreatedBy());
//				}
//				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
//					entityToSave.setUpdatedBy(dto.getUpdatedBy());
//				}
//				if (Utilities.notBlank(dto.getDeletedAt())) {
//					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
//				}
//				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
//					entityToSave.setDeletedBy(dto.getDeletedBy());
//				}
//				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
//				entityToSave.setUpdatedBy(request.getUser());
//				items.add(entityToSave);
//			}
//
//			if (!items.isEmpty()) {
//				List<Fichier> itemsSaved = null;
//				// maj les donnees en base
//				itemsSaved = fichierRepository.save((Iterable<Fichier>) items);
//				if (itemsSaved == null || itemsSaved.isEmpty()) {
//					response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
//					response.setHasError(true);
//					return response;
//				}
//				List<FichierDto> itemsDto = new ArrayList<FichierDto>();
//				for (Fichier entity : itemsSaved) {
//					FichierDto dto = getFullInfos(entity, itemsSaved.size(), locale);
//					if (dto == null) continue;
//					itemsDto.add(dto);
//				}
//				response.setItems(itemsDto);
//				response.setHasError(false);
//			}
//
//			slf4jLogger.info("----end update Fichier-----");
//		} catch (PermissionDeniedDataAccessException e) {
//			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
//		} catch (DataAccessResourceFailureException e) {
//			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
//		} catch (DataAccessException e) {
//			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
//		} catch (RuntimeException e) {
//			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
//		} catch (Exception e) {
//			exceptionUtils.EXCEPTION(response, locale, e);
//		} finally {
//			if (response.isHasError() && response.getStatus() != null) {
//				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
//				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
//			}
//		}
//		return response;
//	}
//
//
//	/**
//	 * delete Fichier by using FichierDto as object.
//	 *
//	 * @param request
//	 * @return response
//	 *
//	 */
//	@SuppressWarnings("unused")
//	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
//	@Override
//	public Response<FichierDto> delete(Request<FichierDto> request, Locale locale)  {
//		slf4jLogger.info("----begin delete Fichier-----");
//
//		response = new Response<FichierDto>();
//		List<String> listOfFilesCreate = new 	ArrayList<>();
//
//		try {
//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}
//
//			User utilisateur = userRepository.findById(request.getUser(), false);
//			if (utilisateur == null) {
//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
//				response.setHasError(true);
//				return response;
//			}
//			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
//				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
//				response.setHasError(true);
//				return response;
//			}
//
//			List<Fichier> items = new ArrayList<Fichier>();
//
//			for (FichierDto dto : request.getDatas()) {
//				// Definir les parametres obligatoires
//				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
//				fieldsToVerify.put("id", dto.getId());
//				//fieldsToVerify.put("user", request.getUser());
//				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
//					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//					response.setHasError(true);
//					return response;
//				}
//
//				/*
//        // Verify if utilisateur exist
//        User utilisateur = userRepository.findById(request.getUser(), false);
//        if (utilisateur == null) {
//          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
//          response.setHasError(true);
//          return response;
//        }
//				 */
//
//				// Verifier si la fichier existe
//				Fichier existingEntity = null;
//				existingEntity = fichierRepository.findById(dto.getId(), false);
//				if (existingEntity == null) {
//					response.setStatus(functionalError.DATA_NOT_EXIST("fichier -> " + dto.getId(), locale));
//					response.setHasError(true);
//					return response;
//				}
//
//				// -----------------------------------------------------------------------
//				// ----------- CHECK IF DATA IS USED
//				// -----------------------------------------------------------------------
//
//				listOfFilesCreate.add(existingEntity.getName());
//
//				existingEntity.setDeletedAt(Utilities.getCurrentDate());
//				existingEntity.setDeletedBy(request.getUser());
//				existingEntity.setIsDeleted(true);
//				items.add(existingEntity);
//			}
//
//			if (!items.isEmpty()) {
//				// supprimer les donnees en base
//				fichierRepository.save((Iterable<Fichier>) items);
//
//				response.setHasError(false);
//			}
//
//			slf4jLogger.info("----end delete Fichier-----");
//		} catch (PermissionDeniedDataAccessException e) {
//			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
//		} catch (DataAccessResourceFailureException e) {
//			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
//		} catch (DataAccessException e) {
//			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
//		} catch (RuntimeException e) {
//			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
//		} catch (Exception e) {
//			exceptionUtils.EXCEPTION(response, locale, e);
//		} finally {
//			if (response.isHasError() && response.getStatus() != null) {
//				Utilities.deleteFileOnSever(listOfFilesCreate, paramsUtils);
//				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
//				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
//			}
//		}
//		return response;
//	}
//
//
//	/**
//	 * forceDelete Fichier by using FichierDto as object.
//	 *
//	 * @param request
//	 * @return response
//	 *
//	 */
//	@SuppressWarnings("unused")
//	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
//	//@Override
//	public Response<FichierDto> forceDelete(Request<FichierDto> request, Locale locale)  {
//		slf4jLogger.info("----begin forceDelete Fichier-----");
//
//		response = new Response<FichierDto>();
//
//		try {
//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}
//
//			User utilisateur = userRepository.findById(request.getUser(), false);
//			if (utilisateur == null) {
//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
//				response.setHasError(true);
//				return response;
//			}
//			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
//				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
//				response.setHasError(true);
//				return response;
//			}
//
//			List<Fichier> items = new ArrayList<Fichier>();
//
//			for (FichierDto dto : request.getDatas()) {
//				// Definir les parametres obligatoires
//				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
//				fieldsToVerify.put("id", dto.getId());
//				//fieldsToVerify.put("user", request.getUser());
//				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
//					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//					response.setHasError(true);
//					return response;
//				}
//
//				/*
//        // Verify if utilisateur exist
//        User utilisateur = userRepository.findById(request.getUser(), false);
//        if (utilisateur == null) {
//          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
//          response.setHasError(true);
//          return response;
//        }
//				 */
//
//				// Verifier si la fichier existe
//				Fichier existingEntity = null;
//				existingEntity = fichierRepository.findById(dto.getId(), false);
//				if (existingEntity == null) {
//					response.setStatus(functionalError.DATA_NOT_EXIST("fichier -> " + dto.getId(), locale));
//					response.setHasError(true);
//					return response;
//				}
//
//				// -----------------------------------------------------------------------
//				// ----------- CHECK IF DATA IS USED
//				// -----------------------------------------------------------------------
//
//
//
//				existingEntity.setDeletedAt(Utilities.getCurrentDate());
//				existingEntity.setDeletedBy(request.getUser());
//				existingEntity.setIsDeleted(true);
//				items.add(existingEntity);
//			}
//
//			if (!items.isEmpty()) {
//				// supprimer les donnees en base
//				fichierRepository.save((Iterable<Fichier>) items);
//
//				response.setHasError(false);
//			}
//
//			slf4jLogger.info("----end forceDelete Fichier-----");
//		} catch (PermissionDeniedDataAccessException e) {
//			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
//		} catch (DataAccessResourceFailureException e) {
//			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
//		} catch (DataAccessException e) {
//			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
//		} catch (RuntimeException e) {
//			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
//		} catch (Exception e) {
//			exceptionUtils.EXCEPTION(response, locale, e);
//		} finally {
//			if (response.isHasError() && response.getStatus() != null) {
//				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
//				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
//			}
//		}
//		return response;
//	}
//
//
//
//	/**
//	 * get Fichier by using FichierDto as object.
//	 *
//	 * @param request
//	 * @return response
//	 *
//	 */
//	@SuppressWarnings("unused")
//	@Override
//	public Response<FichierDto> getByCriteria(Request<FichierDto> request, Locale locale) {
//		slf4jLogger.info("----begin get Fichier-----");
//
//		response = new Response<FichierDto>();
//
//		try {
//			List<Fichier> items = null;
//			items = fichierRepository.getByCriteria(request, em, locale);
//			if (items != null && !items.isEmpty()) {
//				List<FichierDto> itemsDto = new ArrayList<FichierDto>();
//				for (Fichier entity : items) {
//					FichierDto dto = getFullInfos(entity, items.size(), locale);
//					if (dto == null) continue;
//					itemsDto.add(dto);
//				}
//				response.setItems(itemsDto);
//				response.setCount(fichierRepository.count(request, em, locale));
//				response.setHasError(false);
//			} else {
//				response.setStatus(functionalError.DATA_EMPTY("fichier", locale));
//				response.setHasError(false);
//				return response;
//			}
//
//			slf4jLogger.info("----end get Fichier-----");
//		} catch (PermissionDeniedDataAccessException e) {
//			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
//		} catch (DataAccessResourceFailureException e) {
//			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
//		} catch (DataAccessException e) {
//			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
//		} catch (RuntimeException e) {
//			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
//		} catch (Exception e) {
//			exceptionUtils.EXCEPTION(response, locale, e);
//		} finally {
//			if (response.isHasError() && response.getStatus() != null) {
//				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
//				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
//			}
//		}
//		return response;
//	}
//
//	
//	public void supprimerFichier (Integer id, Boolean isFichierOffre) {
//		
//		if (id != null && id > 0) {
//			List<Fichier> fichiers = new ArrayList<>();
//			if (isFichierOffre != null && isFichierOffre) {
//				fichiers = fichierRepository.findByAppelDoffresId(id, false);
//			}else {
//				fichiers = fichierRepository.findByOffreId(id, false);
//			}
//			if (!fichiers.isEmpty()) {
//				List<String> listOfFiles = new ArrayList<String>();
//				for (Fichier fichier : fichiers) {
//					listOfFiles.add(fichier.getName());
//				}
//				Utilities.deleteFileOnSever(listOfFiles, paramsUtils);
//			}
//		}
//	}
//	/**
//	 * get full FichierDto by using Fichier as object.
//	 *
//	 * @param entity, locale
//	 * @return FichierDto
//	 *
//	 */
//	private FichierDto getFullInfos(Fichier entity, Integer size, Locale locale) throws Exception {
//		FichierDto dto = FichierTransformer.INSTANCE.toDto(entity);
//		if (dto == null){
//			return null;
//		}
//		if (size > 1) {
//			return dto;
//		}
//
//		return dto;
//	}
//}
