
/*
 * Java dto for entity table secteur_dactivite_entreprise 
 * Created on 2020-01-03 ( Time 08:47:10 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.customize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.dao.entity.Entreprise;
import ci.salaamsolutions.projet.spasuce.helper.contract.SearchParam;
import lombok.Data;
/**
 * DTO customize for table "secteur_dactivite_entreprise"
 * 
 * @author SFL Back-End developper
 *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _SecteurDactiviteEntrepriseDto {
	
	protected Entreprise entityEntreprise ;
	protected String typeEntrepriseCode;
	protected SearchParam<String>  typeEntrepriseCodeParam;
	private SearchParam<String>   createdAtEntrepriseParam        ;                     

	
	// pr les stats
	protected String    dateDebut       ;
	protected String    dateFin       ;
	protected Boolean 	isDetailUser ;
	protected Boolean 	isDetailGain ;
	protected Boolean 	isDetailAoOf ;
	
}
