package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._PaiementRepository;

/**
 * Repository : Paiement.
 */
@Repository
public interface PaiementRepository extends JpaRepository<Paiement, Integer>, _PaiementRepository {
	/**
	 * Finds Paiement by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object Paiement whose id is equals to the given id. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.id = :id and e.isDeleted = :isDeleted")
	Paiement findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Paiement by using code as a search criteria.
	 *
	 * @param code
	 * @return An Object Paiement whose code is equals to the given code. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.code = :code and e.isDeleted = :isDeleted")
	Paiement findByCode(@Param("code")String code, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object Paiement whose libelle is equals to the given libelle. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	Paiement findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using isPayer as a search criteria.
	 *
	 * @param isPayer
	 * @return An Object Paiement whose isPayer is equals to the given isPayer. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.isPayer = :isPayer and e.isDeleted = :isDeleted")
	List<Paiement> findByIsPayer(@Param("isPayer")Boolean isPayer, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using description as a search criteria.
	 *
	 * @param description
	 * @return An Object Paiement whose description is equals to the given description. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.description = :description and e.isDeleted = :isDeleted")
	List<Paiement> findByDescription(@Param("description")String description, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object Paiement whose createdAt is equals to the given createdAt. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Paiement> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object Paiement whose createdBy is equals to the given createdBy. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Paiement> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object Paiement whose updatedAt is equals to the given updatedAt. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Paiement> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object Paiement whose updatedBy is equals to the given updatedBy. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Paiement> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object Paiement whose deletedAt is equals to the given deletedAt. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Paiement> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object Paiement whose deletedBy is equals to the given deletedBy. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Paiement> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object Paiement whose isDeleted is equals to the given isDeleted. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.isDeleted = :isDeleted")
	List<Paiement> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Paiement by using offreId as a search criteria.
	 *
	 * @param offreId
	 * @return A list of Object Paiement whose offreId is equals to the given offreId. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.offre.id = :offreId and e.isDeleted = :isDeleted")
	List<Paiement> findByOffreId(@Param("offreId")Integer offreId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Paiement by using offreId as a search criteria.
   *
   * @param offreId
   * @return An Object Paiement whose offreId is equals to the given offreId. If
   *         no Paiement is found, this method returns null.
   */
  @Query("select e from Paiement e where e.offre.id = :offreId and e.isDeleted = :isDeleted")
  Paiement findPaiementByOffreId(@Param("offreId")Integer offreId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of Paiement by using paiementDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Paiement
	 * @throws DataAccessException,ParseException
	 */
	public default List<Paiement> getByCriteria(Request<PaiementDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Paiement e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Paiement> query = em.createQuery(req, Paiement.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Paiement by using paiementDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Paiement
	 *
	 */
	public default Long count(Request<PaiementDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Paiement e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<PaiementDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		PaiementDto dto = request.getData() != null ? request.getData() : new PaiementDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (PaiementDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(PaiementDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("code", dto.getCode(), "e.code", "String", dto.getCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (dto.getIsPayer()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isPayer", dto.getIsPayer(), "e.isPayer", "Boolean", dto.getIsPayerParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDescription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getOffreId()!= null && dto.getOffreId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("offreId", dto.getOffreId(), "e.offre.id", "Integer", dto.getOffreIdParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
