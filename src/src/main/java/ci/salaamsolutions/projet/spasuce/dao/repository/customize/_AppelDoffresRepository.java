package ci.salaamsolutions.projet.spasuce.dao.repository.customize;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;

/**
 * Repository customize : AppelDoffres.
 */
@Repository
public interface _AppelDoffresRepository {
	default List<String> _generateCriteria(AppelDoffresDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds AppelDoffres by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object AppelDoffres whose etatId is equals to the given etatId. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.etat.code = :etatCode and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByEtatCode(@Param("etatCode")String etatCode, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AppelDoffres by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object AppelDoffres whose etatId is equals to the given etatId. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.etat.code = :etatCode")
	List<AppelDoffres> findByEtatCodeAll(@Param("etatCode")String etatCode);

}
