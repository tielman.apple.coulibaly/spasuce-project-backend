

/*
 * Java transformer for entity table avertissement_offre 
 * Created on 2020-01-02 ( Time 17:59:54 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "avertissement_offre"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface AvertissementOffreTransformer {

	AvertissementOffreTransformer INSTANCE = Mappers.getMapper(AvertissementOffreTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.offre.id", target="offreId"),
	})
	AvertissementOffreDto toDto(AvertissementOffre entity) throws ParseException;

    List<AvertissementOffreDto> toDtos(List<AvertissementOffre> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="offre", target="offre"),
	})
    AvertissementOffre toEntity(AvertissementOffreDto dto, Offre offre) throws ParseException;

    //List<AvertissementOffre> toEntities(List<AvertissementOffreDto> dtos) throws ParseException;

}
