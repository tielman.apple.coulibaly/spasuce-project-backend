/*
 * Created on 2020-01-02 ( Time 18:00:15 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.contract;

import java.util.List;

import ci.salaamsolutions.projet.spasuce.dao.entity.CritereFiltre;
import ci.salaamsolutions.projet.spasuce.helper.dto.CritereFiltreDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Request Base
 * 
 * @author SFL Back-End developper
 *
 */

@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
public class RequestBase {

	protected String	sessionUser;
	protected Integer	size;
	protected Integer	index;
	protected String	lang;
	protected String	typeEntreprise;
	protected String	businessLineCode;
	protected String	caseEngine;
	protected Boolean	isAnd;
	protected Integer	user;
	protected List<CritereFiltreDto> critereFiltreDtos ;
	
	
	
	public String getBusinessLineCode() {
		return businessLineCode;
	}

	public void setBusinessLineCode(String businessLineCode) {
		this.businessLineCode = businessLineCode;
	}

	public String getCaseEngine() {
		return caseEngine;
	}

	public void setCaseEngine(String caseEngine) {
		this.caseEngine = caseEngine;
	}

	public String getSessionUser() {
		return sessionUser;
	}

	public void setSessionUser(String sessionUser) {
		this.sessionUser = sessionUser;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Boolean getIsAnd() {
		return isAnd;
	}

	public void setIsAnd(Boolean isAnd) {
		this.isAnd = isAnd;
	}
	
	public Integer getUser() {
		return user;
	}

	public void setUser(Integer user) {
		this.user = user;
	}
}