
/*
 * Java dto for entity table appel_doffres_restreint 
 * Created on 2020-01-03 ( Time 08:47:09 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.customize;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffres;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;

import lombok.*;
/**
 * DTO customize for table "appel_doffres_restreint"
 * 
 * @author SFL Back-End developper
 *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _AppelDoffresRestreintDto {

	protected AppelDoffres entityAppelDoffres ;

	protected Boolean isAttribuerAppelDoffres ;
	protected Boolean isContenuNotificationDispo ;

	protected String villeAppelDoffres ;
	protected String dateCreationAppelDoffres ;
	protected String dateLimiteDepotAppelDoffres ;
	protected String etatAppelDoffres ;


	//protected Integer nombreOffre ;
	protected String nombreOffre ;
	protected String nombreVisualisation ;



	// les champs d'appels d'offres
	protected Boolean isVisibleByClient ;
	protected Boolean isRetirer ;
	protected Boolean isAttribuer ;
	protected Boolean isLocked ;
	protected Boolean isModifiable ;
	protected Boolean isUpdate ;
	protected SearchParam<Boolean> isVisibleByClientParam ;
	protected SearchParam<Boolean> isRetirerParam ;
	protected SearchParam<Boolean> isAttribuerParam ;
	protected SearchParam<Boolean> isLockedParam ;
	protected SearchParam<Boolean> isModifiableParam ;
	protected SearchParam<Boolean> isUpdateParam ;
	protected String typeAppelDoffresCode;
	protected SearchParam<String>  typeAppelDoffresCodeParam;

	protected String etatCode;
	protected SearchParam<String>  etatCodeParam;


}
