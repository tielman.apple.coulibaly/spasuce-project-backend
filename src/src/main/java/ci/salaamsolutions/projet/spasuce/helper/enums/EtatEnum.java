package ci.salaamsolutions.projet.spasuce.helper.enums;

public class EtatEnum {
	public static final String	BROUILLON				= "BROUILLON";
	public static final String	ENTRANT				    = "ENTRANT";
	public static final String	REFUSER					= "REFUSER";
	
	public static final String	EN_COURS				= "EN_COURS";
	public static final String	ACCEPTER				= "ACCEPTER";
}

