

/*
 * Java transformer for entity table pays 
 * Created on 2020-03-21 ( Time 13:11:14 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "pays"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface PaysTransformer {

	PaysTransformer INSTANCE = Mappers.getMapper(PaysTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.continent.id", target="continentId"),
				@Mapping(source="entity.continent.libelle", target="continentLibelle"),
				@Mapping(source="entity.continent.code", target="continentCode"),
	})
	PaysDto toDto(Pays entity) throws ParseException;

    List<PaysDto> toDtos(List<Pays> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.currency", target="currency"),
		@Mapping(source="dto.iso", target="iso"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="continent", target="continent"),
	})
    Pays toEntity(PaysDto dto, Continent continent) throws ParseException;

    //List<Pays> toEntities(List<PaysDto> dtos) throws ParseException;

}
