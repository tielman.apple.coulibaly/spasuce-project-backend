


/*
 * Java transformer for entity table entreprise
 * Created on 2020-01-02 ( Time 17:59:57 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffres;
import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffresRestreint;
import ci.salaamsolutions.projet.spasuce.dao.entity.ClasseNote;
import ci.salaamsolutions.projet.spasuce.dao.entity.Domaine;
import ci.salaamsolutions.projet.spasuce.dao.entity.DomaineEntreprise;
import ci.salaamsolutions.projet.spasuce.dao.entity.Entreprise;
import ci.salaamsolutions.projet.spasuce.dao.entity.Fichier;
import ci.salaamsolutions.projet.spasuce.dao.entity.PartenaireFournisseur;
import ci.salaamsolutions.projet.spasuce.dao.entity.SecteurDactivite;
import ci.salaamsolutions.projet.spasuce.dao.entity.SecteurDactiviteEntreprise;
import ci.salaamsolutions.projet.spasuce.dao.entity.StatutJuridique;
import ci.salaamsolutions.projet.spasuce.dao.entity.TypeEntreprise;
import ci.salaamsolutions.projet.spasuce.dao.entity.User;
import ci.salaamsolutions.projet.spasuce.dao.entity.Ville;
import ci.salaamsolutions.projet.spasuce.dao.repository.AppelDoffresRestreintRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.ClasseNoteRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.DomaineEntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.EntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.FichierRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.PartenaireFournisseurRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.SecteurDactiviteAppelDoffresRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.SecteurDactiviteEntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.StatutJuridiqueRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.TypeEntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.UserRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.VilleRepository;
import ci.salaamsolutions.projet.spasuce.helper.ExceptionUtils;
import ci.salaamsolutions.projet.spasuce.helper.FunctionalError;
import ci.salaamsolutions.projet.spasuce.helper.HostingUtils;
import ci.salaamsolutions.projet.spasuce.helper.ParamsUtils;
import ci.salaamsolutions.projet.spasuce.helper.TechnicalError;
import ci.salaamsolutions.projet.spasuce.helper.Utilities;
import ci.salaamsolutions.projet.spasuce.helper.Validate;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.dto.DomaineEntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.EntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.FichierDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.PartenaireFournisseurDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteAppelDoffresDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteEntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.UserDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.customize._Image;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.DomaineTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.EntrepriseTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.FichierTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.PartenaireFournisseurTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.SecteurDactiviteEntrepriseTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.SecteurDactiviteTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.UserTransformer;
import ci.salaamsolutions.projet.spasuce.helper.enums.EtatEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.GlobalEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeAppelDoffresEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeUserEnum;

/**
BUSINESS for table "entreprise"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class EntrepriseBusiness implements IBasicBusiness<Request<EntrepriseDto>, Response<EntrepriseDto>> {

	private Response<EntrepriseDto> response;
	@Autowired
	private EntrepriseRepository entrepriseRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private VilleRepository villeRepository;
	@Autowired
	private ClasseNoteRepository classeNoteRepository;
	@Autowired
	private StatutJuridiqueRepository statutJuridiqueRepository;
	@Autowired
	private PartenaireFournisseurRepository partenaireFournisseurRepository;
	@Autowired
	private SecteurDactiviteEntrepriseRepository secteurDactiviteEntrepriseRepository;
	@Autowired
	private TypeEntrepriseRepository typeEntrepriseRepository;
	@Autowired
	private FichierRepository fichierRepository;

	@Autowired
	private AppelDoffresRestreintRepository appelDoffresRestreintRepository ;
	
	@Autowired
	private SecteurDactiviteAppelDoffresRepository secteurDactiviteAppelDoffresRepository ;


	@Autowired
	private DomaineEntrepriseRepository domaineEntrepriseRepository ;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private PartenaireFournisseurBusiness partenaireFournisseurBusiness;


	@Autowired
	private SecteurDactiviteEntrepriseBusiness secteurDactiviteEntrepriseBusiness;
	
	@Autowired
	private DomaineEntrepriseBusiness domaineEntrepriseBusiness;
	
	

	@Autowired
	private FichierBusiness fichierBusiness;



	@Autowired
	private UserBusiness userBusiness;

	@Autowired
	private ParamsUtils paramsUtils;

	private Context context;

	@Autowired
	private HostingUtils hostingUtils;



	public EntrepriseBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create Entreprise by using EntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<EntrepriseDto> create(Request<EntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin create Entreprise-----");

		response = new Response<EntrepriseDto>();
		List<String> listOfFilesCreate = new 	ArrayList<>();


		try {
			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}


			if (request.getUser() != null && request.getUser() > 0) {
				User utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
			}



			List<Entreprise> items = new ArrayList<Entreprise>();

			List<SecteurDactiviteEntrepriseDto> datasSecteurDactiviteEntreprise = new ArrayList<>();
			List<DomaineEntrepriseDto> datasDomaineEntreprise = new ArrayList<>();
			List<FichierDto> datasFichierDescription = new ArrayList<>();
			List<FichierDto> datasFichierPartenaire = new ArrayList<>();


			for (EntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("nom", dto.getNom());
				fieldsToVerify.put("email", dto.getEmail());
				//fieldsToVerify.put("numeroImmatriculation", dto.getNumeroImmatriculation());
				fieldsToVerify.put("password", dto.getPassword());
				//fieldsToVerify.put("numeroFix", dto.getNumeroFix());
				//fieldsToVerify.put("numeroFax", dto.getNumeroFax());
				fieldsToVerify.put("telephone", dto.getTelephone());
				//fieldsToVerify.put("login", dto.getLogin());
				//fieldsToVerify.put("urlLogo", dto.getUrlLogo());
				//fieldsToVerify.put("extensionLogo", dto.getExtensionLogo());
				//fieldsToVerify.put("description", dto.getDescription());
				//fieldsToVerify.put("isLocked", dto.getIsLocked());
				fieldsToVerify.put("villeId", dto.getVilleId());
				fieldsToVerify.put("typeEntrepriseId", dto.getTypeEntrepriseId());
				fieldsToVerify.put("statutJuridiqueId", dto.getStatutJuridiqueId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}



				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if entreprise to insert do not exist
				Entreprise existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = entrepriseRepository.findByEmail(dto.getEmail(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getEmail(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les entreprises", locale));
					response.setHasError(true);
					return response;
				}
				// TODO : à revoir pour les autres ville
				if (!Utilities.isValidateIvorianPhoneNumber(dto.getTelephone())){
					response.setStatus(functionalError.INVALID_DATA("telephone -> " + dto.getTelephone(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = entrepriseRepository.findByTelephone(dto.getTelephone(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getTelephone(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les entreprises", locale));
					response.setHasError(true);
					return response;
				}

				existingEntity = entrepriseRepository.findEntrepriseByNom(dto.getNom(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getNom(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getNom().equalsIgnoreCase(dto.getNom()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du nom '" + dto.getNom()+"' pour les entreprises", locale));
					response.setHasError(true);
					return response;
				}

//				if (!Utilities.notBlank(dto.getNumeroImmatriculation()) && !Utilities.notBlank(dto.getNumeroDuns())) {
//					response.setStatus(functionalError.FIELD_EMPTY("numeroImmatriculation & numeroDuns", locale));
//					response.setHasError(true);
//					return response;
//				}else {
//					if (Utilities.notBlank(dto.getNumeroImmatriculation())) {
//						existingEntity = entrepriseRepository.findEntrepriseByNumeroImmatriculation(dto.getNumeroImmatriculation(), false);
//						if (existingEntity != null) {
//							response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getNumeroImmatriculation(), locale));
//							response.setHasError(true);
//							return response;
//						}
//						if (items.stream().anyMatch(a->a.getNumeroImmatriculation().equalsIgnoreCase(dto.getNumeroImmatriculation()))) {
//							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du numeroImmatriculation '" + dto.getNumeroImmatriculation()+"' pour les entreprises", locale));
//							response.setHasError(true);
//							return response;
//						}
//					}
//					if (Utilities.notBlank(dto.getNumeroDuns())) {
//						existingEntity = entrepriseRepository.findEntrepriseByNumeroDuns(dto.getNumeroDuns(), false);
//						if (existingEntity != null) {
//							response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getNumeroDuns(), locale));
//							response.setHasError(true);
//							return response;
//						}
//						if (items.stream().anyMatch(a->a.getNumeroDuns().equalsIgnoreCase(dto.getNumeroDuns()))) {
//							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du numeroDuns '" + dto.getNumeroDuns()+"' pour les entreprises", locale));
//							response.setHasError(true);
//							return response;
//						}
//					}
//				}
				if (Utilities.notBlank(dto.getNumeroImmatriculation())) {
					existingEntity = entrepriseRepository.findEntrepriseByNumeroImmatriculation(dto.getNumeroImmatriculation(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getNumeroImmatriculation(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getNumeroImmatriculation().equalsIgnoreCase(dto.getNumeroImmatriculation()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du numeroImmatriculation '" + dto.getNumeroImmatriculation()+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (Utilities.notBlank(dto.getNumeroDuns())) {
					existingEntity = entrepriseRepository.findEntrepriseByNumeroDuns(dto.getNumeroDuns(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getNumeroDuns(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getNumeroDuns().equalsIgnoreCase(dto.getNumeroDuns()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du numeroDuns '" + dto.getNumeroDuns()+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}
				}




				//				existingEntity = entrepriseRepository.findEntrepriseByNumeroImmatriculation(dto.getNumeroImmatriculation(), false);
				//				if (existingEntity != null) {
				//					response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getNumeroImmatriculation(), locale));
				//					response.setHasError(true);
				//					return response;
				//				}
				//				if (items.stream().anyMatch(a->a.getNumeroImmatriculation().equalsIgnoreCase(dto.getNumeroImmatriculation()))) {
				//					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du numeroImmatriculation '" + dto.getNumeroImmatriculation()+"' pour les entreprises", locale));
				//					response.setHasError(true);
				//					return response;
				//				}
				if (Utilities.notBlank(dto.getNumeroFax())) {
					// TODO : à revoir pour les autres ville
					if (!Utilities.isValidateIvorianPhoneNumber(dto.getNumeroFax())){
						response.setStatus(functionalError.INVALID_DATA("numeroFax -> " + dto.getNumeroFax(), locale));
						response.setHasError(true);
						return response;
					}
					existingEntity = entrepriseRepository.findEntrepriseByNumeroFax(dto.getNumeroFax(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getNumeroFax(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getNumeroFax().equalsIgnoreCase(dto.getNumeroFax()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du numéro fax '" + dto.getNumeroFax()+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (Utilities.notBlank(dto.getNumeroFix())) {
					// TODO : à revoir pour les autres ville
					if (!Utilities.isValidateIvorianPhoneNumber(dto.getNumeroFix())){
						response.setStatus(functionalError.INVALID_DATA("numeroFix -> " + dto.getNumeroFix(), locale));
						response.setHasError(true);
						return response;
					}
					existingEntity = entrepriseRepository.findEntrepriseByNumeroFix(dto.getNumeroFix(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getNumeroFix(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getNumeroFix().equalsIgnoreCase(dto.getNumeroFix()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du numéro fix '" + dto.getNumeroFix()+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}
				}
				String password = dto.getPassword() ;
				// HASHAGE du password avant de le mettre dans la BD
				dto.setPassword(Utilities.encrypt(dto.getPassword()));
				existingEntity = entrepriseRepository.findEntrepriseByPassword(dto.getPassword(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("entreprise -> " + password, locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getPassword().equalsIgnoreCase(dto.getPassword()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du password '" + password+"' pour les entreprises", locale));
					response.setHasError(true);
					return response;
				}
				if (Utilities.notBlank(dto.getLogin())) {
					existingEntity = entrepriseRepository.findByLogin(dto.getLogin(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getLogin(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getLogin()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getLogin()+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}
				}

				// Verify if ville exist
				Ville existingVille = villeRepository.findById(dto.getVilleId(), false);
				if (existingVille == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ville -> " + dto.getVilleId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if classeNote exist
				ClasseNote existingClasseNote = null;
				if (Utilities.notBlank(dto.getClasseNoteCode())) {
					//if (dto.getClasseNoteId() != null && dto.getClasseNoteId() > 0) {
					existingClasseNote = classeNoteRepository.findByCode(dto.getClasseNoteCode(), false);
					if (existingClasseNote == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("classeNote -> " + dto.getClasseNoteCode(), locale));
						response.setHasError(true);
						return response;
					}
				}

				// Verify if statutJuridique exist
				StatutJuridique existingStatutJuridique = statutJuridiqueRepository.findById(dto.getStatutJuridiqueId(), false);
				if (existingStatutJuridique == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("statutJuridique -> " + dto.getStatutJuridiqueId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if typeEntreprise exist
				TypeEntreprise existingTypeEntreprise = typeEntrepriseRepository.findById(dto.getTypeEntrepriseId(), false);
				if (existingTypeEntreprise == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeEntreprise -> " + dto.getTypeEntrepriseId(), locale));
					response.setHasError(true);
					return response;
				}


				// TODO : recuperation du base64 et le stocker sur le serveur
				if (Utilities.notBlank(dto.getFichierBase64()) && Utilities.notBlank(dto.getExtensionLogo()) && Utilities.notBlank(dto.getUrlLogo()) ) {

					// gestion du logo associe à l'entreprise
					String cheminFile = null;
					_Image image2 = new _Image();

					image2.setExtension(dto.getExtensionLogo());
					image2.setFileBase64(dto.getFichierBase64());
					image2.setFileName(dto.getUrlLogo());
					if(image2 != null){

						cheminFile =  Utilities.saveFile(image2, paramsUtils);
						//cheminFile =  Utilities.saveFileCustom(image2, GlobalEnum.profils, paramsUtils);
						if(cheminFile==null){
							response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
							response.setHasError(true);
							return response;
						}
						dto.setUrlLogo(cheminFile.split("."+dto.getExtensionLogo())[0]);
						//dto.setUrlLogo(cheminFile);
						listOfFilesCreate.add(cheminFile);
					}
					//Utilities.saveImage(data.getImageBase64(), "urlFichier", data.getImageExtension());
					//image.setImageUrl(data.getImageUrl());
				}
				dto.setIsLocked(false); // par defaut l'entreprise n'est pas bloquée
				Entreprise entityToSave = null;
				entityToSave = EntrepriseTransformer.INSTANCE.toEntity(dto, existingVille, existingStatutJuridique, existingClasseNote, existingTypeEntreprise);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);

				// 
				if (Utilities.isNotEmpty(dto.getDatasSecteurDactiviteEntreprise())) {
					for (SecteurDactiviteEntrepriseDto data : dto.getDatasSecteurDactiviteEntreprise()) {
						data.setEntityEntreprise(entityToSave);
					}
					datasSecteurDactiviteEntreprise.addAll(dto.getDatasSecteurDactiviteEntreprise());
				}
				
				// pour les secteurs d'activites
				if (Utilities.isNotEmpty(dto.getDatasDomaineEntreprise())) {
					for (DomaineEntrepriseDto data : dto.getDatasDomaineEntreprise()) {
						data.setEntityEntreprise(entityToSave);
					}
					datasDomaineEntreprise.addAll(dto.getDatasDomaineEntreprise());
				}

				
				if (Utilities.isNotEmpty(dto.getDatasFichierDescription())) {
					for (FichierDto data : dto.getDatasFichierDescription()) {
						data.setEntityEntreprise(entityToSave);
						//data.setIsFichierOffre(false);
					}
					datasFichierDescription.addAll(dto.getDatasFichierDescription());
				}

				if (Utilities.isNotEmpty(dto.getDatasFichierPartenaire())) {
					for (FichierDto data : dto.getDatasFichierPartenaire()) {
						data.setEntityEntreprise(entityToSave);
						//data.setIsFichierOffre(false);
					}
					datasFichierPartenaire.addAll(dto.getDatasFichierPartenaire());
				}



			}

			if (!items.isEmpty()) {
				List<Entreprise> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = entrepriseRepository.save((Iterable<Entreprise>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("entreprise", locale));
					response.setHasError(true);
					return response;
				}

				// creation des domaines
				if (Utilities.isNotEmpty(datasDomaineEntreprise)) {
					for (DomaineEntrepriseDto data : datasDomaineEntreprise) {
						data.setEntrepriseId(data.getEntityEntreprise().getId());
					}
					Request<DomaineEntrepriseDto> req = new Request<>();
					Response<DomaineEntrepriseDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasDomaineEntreprise);

					res = domaineEntrepriseBusiness.create(req, locale);

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
					
				}
				
				// creation des SecteurDactiviteEntreprise
				if (Utilities.isNotEmpty(datasSecteurDactiviteEntreprise)) {
					for (SecteurDactiviteEntrepriseDto data : datasSecteurDactiviteEntreprise) {
						data.setEntrepriseId(data.getEntityEntreprise().getId());
					}
					Request<SecteurDactiviteEntrepriseDto> req = new Request<>();
					Response<SecteurDactiviteEntrepriseDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasSecteurDactiviteEntreprise);

					res = secteurDactiviteEntrepriseBusiness.create(req, locale);

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
					
				}



				// creation des Fichier 
				if (Utilities.isNotEmpty(datasFichierDescription)) {
					for (FichierDto data : datasFichierDescription) {
						data.setSource(GlobalEnum.fichiers_ins_description);
						data.setSourceId(data.getEntityEntreprise().getId());
						//data.setOffreId(data.getEntityOffre().getId());
					}
					Request<FichierDto> reqFichier = new Request<>();
					Response<FichierDto> resFichier = new Response<>();
					reqFichier.setUser(request.getUser());
					reqFichier.setDatas(datasFichierDescription);

					resFichier = fichierBusiness.create(reqFichier, locale);

					if (resFichier != null && resFichier.isHasError()) {
						response.setStatus(resFichier.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				// creation des Fichier 
				if (Utilities.isNotEmpty(datasFichierPartenaire)) {
					for (FichierDto data : datasFichierPartenaire) {
						data.setSource(GlobalEnum.fichiers_ins_partenaire);
						data.setSourceId(data.getEntityEntreprise().getId());
						//data.setOffreId(data.getEntityOffre().getId());
					}
					Request<FichierDto> reqFichierPar = new Request<>();
					Response<FichierDto> resFichierPar = new Response<>();
					reqFichierPar.setUser(request.getUser());
					reqFichierPar.setDatas(datasFichierPartenaire);

					resFichierPar = fichierBusiness.create(reqFichierPar, locale);

					if (resFichierPar != null && resFichierPar.isHasError()) {
						response.setStatus(resFichierPar.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				
				List<EntrepriseDto> itemsDto = new ArrayList<EntrepriseDto>();
				for (Entreprise entity : itemsSaved) {
					EntrepriseDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);



				// envoi de mail 
				/*
				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user" ,  paramsUtils.getSmtpMailUser());

					// choisir la vraie url
					String appLink = "";
					//			if (entityToSave.getAdmin() != null ) {
					//				appLink = paramsUtils.getUrlRootAdmin();
					//			}else {
					//				appLink = paramsUtils.getUrlRootUser();
					//			}

					appLink = paramsUtils.getUrlRootUser();

					for (Entreprise user : itemsSaved) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_CREATION_COMPTE;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateCreationCompteParUser();
						//		        	context.setVariable("email", user.getEmail());
						//					context.setVariable("login", user.getLogin());
						//					context.setVariable("token", user.getToken());
						//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable(EmailEnum.nomEntreprise, EmailEnum.NOM_ENTREPRISE);
						context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail

					}
				}
				//*/
			}

			slf4jLogger.info("----end create Entreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				Utilities.deleteFileOnSever(listOfFilesCreate, paramsUtils);
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Entreprise by using EntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<EntrepriseDto> update(Request<EntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin update Entreprise-----");

		response = new Response<EntrepriseDto>();
		List<String> listOfOldFiles = new 	ArrayList<>();
		List<String> listOfFilesCreate = new 	ArrayList<>();
		List<FichierDto> datasFichierDescription = new ArrayList<>();
		List<FichierDto> datasFichierPartenaire = new ArrayList<>();
		List<Fichier> itemsFichierToDelete = new ArrayList<>();


		try {
			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			User utilisateur = null ;
			if (request.getUser() != null && request.getUser() > 0) {
				utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
			}

			List<Entreprise> items = new ArrayList<Entreprise>();

			List<SecteurDactiviteEntrepriseDto> datasSecteurDactiviteEntreprise = new ArrayList<>();
			List<DomaineEntrepriseDto> datasDomaineEntreprise = new ArrayList<>();

			
			List<SecteurDactiviteEntreprise> itemsSecteurDactiviteEntrepriseToDelete = new ArrayList<>();
			List<DomaineEntreprise> itemsDomaineEntrepriseToDelete = new ArrayList<>();


			for (EntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la entreprise existe
				Entreprise entityToSave = null;
				entityToSave = entrepriseRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("entreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				if (utilisateur != null && utilisateur.getEntreprise() != null) { // seul le user qui a crée l'entreprise peut la modififer
					if (!entityToSaveId.equals(utilisateur.getEntreprise().getId()) || (entityToSave.getCreatedBy() != null &&  !entityToSave.getCreatedBy().equals(utilisateur.getId()))) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'a pas ce droit" , locale));
						response.setHasError(true);
						return response;
					}
				}

				// Verify if ville exist
				if (dto.getVilleId() != null && dto.getVilleId() > 0){
					Ville existingVille = villeRepository.findById(dto.getVilleId(), false);
					if (existingVille == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("ville -> " + dto.getVilleId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setVille(existingVille);
				}
				// Verify if statutJuridique exist
				if (dto.getStatutJuridiqueId() != null && dto.getStatutJuridiqueId() > 0){
					StatutJuridique existingStatutJuridique = statutJuridiqueRepository.findById(dto.getStatutJuridiqueId(), false);
					if (existingStatutJuridique == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("statutJuridique -> " + dto.getStatutJuridiqueId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setStatutJuridique(existingStatutJuridique);
				}
				// Verify if classeNote exist
				if (Utilities.notBlank(dto.getClasseNoteCode())){

					//if (dto.getClasseNoteId() != null && dto.getClasseNoteId() > 0) {
					ClasseNote existingClasseNote = classeNoteRepository.findByCode(dto.getClasseNoteCode(), false);
					if (existingClasseNote == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("classeNote -> " + dto.getClasseNoteCode(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setClasseNote(existingClasseNote);
				}
				// Verify if typeEntreprise exist
				if (dto.getTypeEntrepriseId() != null && dto.getTypeEntrepriseId() > 0){
					TypeEntreprise existingTypeEntreprise = typeEntrepriseRepository.findById(dto.getTypeEntrepriseId(), false);
					if (existingTypeEntreprise == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("typeEntreprise -> " + dto.getTypeEntrepriseId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setTypeEntreprise(existingTypeEntreprise);
				}
				if (Utilities.notBlank(dto.getNom())) {
					Entreprise existingEntity = entrepriseRepository.findEntrepriseByNom(dto.getNom(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getNom(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getNom().equalsIgnoreCase(dto.getNom()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du nom '" + dto.getNom()+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setNom(dto.getNom());
				}
				if (Utilities.notBlank(dto.getEmail())) {
					Entreprise existingEntity = entrepriseRepository.findByEmail(dto.getEmail(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getEmail(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEmail(dto.getEmail());
				}	

				if (Utilities.notBlank(dto.getNumeroImmatriculation())) {
					Entreprise existingEntity = entrepriseRepository.findEntrepriseByNumeroImmatriculation(dto.getNumeroImmatriculation(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getNumeroImmatriculation(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getNumeroImmatriculation().equalsIgnoreCase(dto.getNumeroImmatriculation()) && !a.getId().equals(entityToSaveId) )) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du numeroImmatriculation '" + dto.getNumeroImmatriculation()+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setNumeroImmatriculation(dto.getNumeroImmatriculation());
				}
				if (Utilities.notBlank(dto.getNumeroDuns())) {
					Entreprise existingEntity = entrepriseRepository.findEntrepriseByNumeroDuns(dto.getNumeroDuns(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getNumeroDuns(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getNumeroDuns().equalsIgnoreCase(dto.getNumeroDuns()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du numeroDuns '" + dto.getNumeroDuns()+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setNumeroDuns(dto.getNumeroDuns());
				}
				//				if (Utilities.notBlank(dto.getPassword())) {
				//					entityToSave.setPassword(dto.getPassword());
				//				}
				//				if (Utilities.notBlank(dto.getNumeroFix())) {
				//					entityToSave.setNumeroFix(dto.getNumeroFix());
				//				}
				//				if (Utilities.notBlank(dto.getNumeroFax())) {
				//					entityToSave.setNumeroFax(dto.getNumeroFax());
				//				}
				if (Utilities.notBlank(dto.getTelephone())) {
					// TODO : à revoir pour les autres ville
					if (!Utilities.isValidateIvorianPhoneNumber(dto.getTelephone())){
						response.setStatus(functionalError.INVALID_DATA("telephone -> " + dto.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}
					Entreprise existingEntity = entrepriseRepository.findByTelephone(dto.getTelephone(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setTelephone(dto.getTelephone());
				}


				if (Utilities.notBlank(dto.getNumeroFax())) {
					// TODO : à revoir pour les autres ville
					if (!Utilities.isValidateIvorianPhoneNumber(dto.getNumeroFax())){
						response.setStatus(functionalError.INVALID_DATA("numeroFax -> " + dto.getNumeroFax(), locale));
						response.setHasError(true);
						return response;
					}
					Entreprise existingEntity = entrepriseRepository.findEntrepriseByNumeroFax(dto.getNumeroFax(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getNumeroFax(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getNumeroFax().equalsIgnoreCase(dto.getNumeroFax()) && !a.getId().equals(entityToSaveId) )) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du numéro fax '" + dto.getNumeroFax()+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setNumeroFax(dto.getNumeroFax());
				}
				if (Utilities.notBlank(dto.getNumeroFix())) {
					// TODO : à revoir pour les autres pays
					if (!Utilities.isValidateIvorianPhoneNumber(dto.getNumeroFix())){
						response.setStatus(functionalError.INVALID_DATA("numeroFix -> " + dto.getNumeroFix(), locale));
						response.setHasError(true);
						return response;
					}
					Entreprise existingEntity = entrepriseRepository.findEntrepriseByNumeroFix(dto.getNumeroFix(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getNumeroFix(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getNumeroFix().equalsIgnoreCase(dto.getNumeroFix()) && !a.getId().equals(entityToSaveId) )) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du numéro fix '" + dto.getNumeroFix()+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}				
					entityToSave.setNumeroFix(dto.getNumeroFix());
				}/*
				if (Utilities.notBlank(dto.getLogin())) {
					Entreprise existingEntity = entrepriseRepository.findByLogin(dto.getLogin(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + dto.getLogin(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getLogin()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getLogin()+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLogin(dto.getLogin());
				}
				if (Utilities.notBlank(dto.getPassword())) {
					String password = dto.getPassword() ;

					dto.setPassword(Utilities.encrypt(dto.getPassword()));
					Entreprise existingEntity = entrepriseRepository.findEntrepriseByPassword(dto.getPassword(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("entreprise -> " + password, locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getLogin()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du password '" + password+"' pour les entreprises", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setPassword(dto.getPassword());
				}*/

				// TODO : recuperation du base64 et le stocker sur le serveur
				if (Utilities.notBlank(dto.getFichierBase64()) && Utilities.notBlank(dto.getExtensionLogo()) && Utilities.notBlank(dto.getUrlLogo()) ) {

					// TODO : reste la suppression du fichier sur le disque dur
					//listOfOldFiles.add(entityToSave.getUrlLogo());
					String fileNameByExtension = Utilities.addExtensionInFileName( entityToSave.getUrlLogo() , entityToSave.getExtensionLogo()) ;

					listOfOldFiles.add(fileNameByExtension);

					// gestion du logo associe à l'entreprise
					String cheminFile = null;
					_Image image2 = new _Image();

					image2.setExtension(dto.getExtensionLogo());
					image2.setFileBase64(dto.getFichierBase64());
					image2.setFileName(dto.getUrlLogo());

					cheminFile =  Utilities.saveFile(image2, paramsUtils);
					//cheminFile =  Utilities.saveFileCustom(image2, GlobalEnum.profils, paramsUtils);
					if(cheminFile==null){
						response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
						response.setHasError(true);
						return response;
					}
					//entityToSave.setUrlLogo(cheminFile);
					entityToSave.setUrlLogo(cheminFile.split("."+dto.getExtensionLogo())[0]);

					entityToSave.setExtensionLogo(dto.getExtensionLogo());
					listOfFilesCreate.add(cheminFile);

					//Utilities.saveImage(data.getImageBase64(), "urlFichier", data.getImageExtension());
					//image.setImageUrl(data.getImageUrl());
				}
				//				if (Utilities.notBlank(dto.getUrlLogo())) {
				//					entityToSave.setUrlLogo(dto.getUrlLogo());
				//				}
				//				if (Utilities.notBlank(dto.getExtensionLogo())) {
				//					entityToSave.setExtensionLogo(dto.getExtensionLogo());
				//				}
				if (Utilities.notBlank(dto.getDescription())) {
					entityToSave.setDescription(dto.getDescription());
				}

				if (dto.getNote() != null && dto.getNote() > 0) {
					entityToSave.setNote(dto.getNote());
				}
				//				if (dto.getIsLocked() != null) {
				//					entityToSave.setIsLocked(dto.getIsLocked());
				//					
				//					List<User> usersEntreprise = userRepository.findByEntrepriseId(entityToSaveId, false);
				//					
				//					if (Utilities.isNotEmpty(usersEntreprise)) {
				//						for (User user : usersEntreprise) {
				//							user.setIsLocked(isLocked);
				//						}
				//						
				//					}
				//				
				//				}
				//				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				//					entityToSave.setCreatedBy(dto.getCreatedBy());
				//				}
				//				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				//					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				//				}
				//				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				//					entityToSave.setDeletedBy(dto.getDeletedBy());
				//				}
				//				if (Utilities.notBlank(dto.getDeletedAt())) {
				//					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
				//				}
				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);

				// pour les domaines d'activites
				if (Utilities.isNotEmpty(dto.getDatasSecteurDactiviteEntreprise())) {

					// recuperation des anciens pour suppression
					List<SecteurDactiviteEntreprise> secteurDactiviteEntreprise = secteurDactiviteEntrepriseRepository.findByEntrepriseId(entityToSaveId, false);
					if (Utilities.isNotEmpty(secteurDactiviteEntreprise)) {
						itemsSecteurDactiviteEntrepriseToDelete.addAll(secteurDactiviteEntreprise);
					}
					for (SecteurDactiviteEntrepriseDto data : dto.getDatasSecteurDactiviteEntreprise()) {
						data.setEntrepriseId(entityToSaveId);
					}
					datasSecteurDactiviteEntreprise.addAll(dto.getDatasSecteurDactiviteEntreprise());
				}

				// pour les secteurs d'activites
				if (Utilities.isNotEmpty(dto.getDatasDomaineEntreprise())) {
					
					// recuperation des anciens pour suppression
					List<DomaineEntreprise> domaineEntreprise = domaineEntrepriseRepository.findByEntrepriseId(entityToSaveId, false);
					if (Utilities.isNotEmpty(domaineEntreprise)) {
						itemsDomaineEntrepriseToDelete.addAll(domaineEntreprise);
					}
					for (DomaineEntrepriseDto data : dto.getDatasDomaineEntreprise()) {
						data.setEntrepriseId(entityToSaveId);
					}
					datasDomaineEntreprise.addAll(dto.getDatasDomaineEntreprise());
				}

				if (Utilities.isNotEmpty(dto.getDatasFichierDescription())) {
					List<Fichier> fichiers = fichierRepository.findBySourceIdAndSource(entityToSaveId, GlobalEnum.fichiers_ins_description, false);

					if (Utilities.isNotEmpty(fichiers)) {
						itemsFichierToDelete.addAll(fichiers);
					}

					for (FichierDto data : dto.getDatasFichierDescription()){
						//data.setAppelDoffresId(entityToSaveId);
						//data.setOffreId(entityToSaveId);
						//data.setIsFichierOffre(true);
						data.setSource(GlobalEnum.fichiers_ins_description);
						data.setSourceId(entityToSaveId);

					}
					datasFichierDescription.addAll(dto.getDatasFichierDescription());
				}

				if (Utilities.isNotEmpty(dto.getDatasFichierPartenaire())) {
					List<Fichier> fichiers = fichierRepository.findBySourceIdAndSource(entityToSaveId, GlobalEnum.fichiers_ins_partenaire, false);

					if (Utilities.isNotEmpty(fichiers)) {
						itemsFichierToDelete.addAll(fichiers);
					}

					for (FichierDto data : dto.getDatasFichierPartenaire()){
						//data.setAppelDoffresId(entityToSaveId);
						//data.setOffreId(entityToSaveId);
						//data.setIsFichierOffre(true);
						data.setSource(GlobalEnum.fichiers_ins_partenaire);
						data.setSourceId(entityToSaveId);

					}
					datasFichierPartenaire.addAll(dto.getDatasFichierPartenaire());
				}
			}

			if (!items.isEmpty()) {
				List<Entreprise> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = entrepriseRepository.save((Iterable<Entreprise>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("entreprise", locale));
					response.setHasError(true);
					return response;
				}

				// creation des SecteurDactiviteEntreprise (domaine)
				if (!itemsSecteurDactiviteEntrepriseToDelete.isEmpty()) {
					List<SecteurDactiviteEntreprise> itemsToDelete = null;
					for (SecteurDactiviteEntreprise data : itemsSecteurDactiviteEntrepriseToDelete) {
						data.setIsDeleted(true);
						data.setDeletedBy(request.getUser());
						data.setDeletedAt(Utilities.getCurrentDate());
					}
					// maj les donnees en base
					itemsToDelete = secteurDactiviteEntrepriseRepository.save((Iterable<SecteurDactiviteEntreprise>) itemsSecteurDactiviteEntrepriseToDelete);
					if (itemsToDelete == null || itemsToDelete.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!datasSecteurDactiviteEntreprise.isEmpty()) {

					Request<SecteurDactiviteEntrepriseDto> req = new Request<>();
					Response<SecteurDactiviteEntrepriseDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasSecteurDactiviteEntreprise);

					res = secteurDactiviteEntrepriseBusiness.create(req, locale);

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}

				}

				// creation des DomaineEntreprise (secteur d'activite)
				if (!itemsDomaineEntrepriseToDelete.isEmpty()) {
					List<DomaineEntreprise> itemsToDelete = null;
					for (DomaineEntreprise data : itemsDomaineEntrepriseToDelete) {
						data.setIsDeleted(true);
						data.setDeletedBy(request.getUser());
						data.setDeletedAt(Utilities.getCurrentDate());
					}
					// maj les donnees en base
					itemsToDelete = domaineEntrepriseRepository.save((Iterable<DomaineEntreprise>) itemsDomaineEntrepriseToDelete);
					if (itemsToDelete == null || itemsToDelete.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!datasDomaineEntreprise.isEmpty()) {

					Request<DomaineEntrepriseDto> req = new Request<>();
					Response<DomaineEntrepriseDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasDomaineEntreprise);

					res = domaineEntrepriseBusiness.create(req, locale);

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}

				}

				// creation des Fichier 
				if (!itemsFichierToDelete.isEmpty()) {
					for (Fichier data : itemsFichierToDelete) {
						data.setIsDeleted(true);
						data.setDeletedBy(request.getUser());
						data.setDeletedAt(Utilities.getCurrentDate());
						//listOfOldFiles.add(data.getName());
						String fileNameByExtension = Utilities.addExtensionInFileName( data.getName() , data.getExtension()) ;

						listOfOldFiles.add(fileNameByExtension);
					}
					List<Fichier> itemsToDelete = null;
					// maj les donnees en base
					itemsToDelete = fichierRepository.save((Iterable<Fichier>) itemsFichierToDelete);
					if (itemsToDelete == null || itemsToDelete.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!datasFichierPartenaire.isEmpty()) {

					Request<FichierDto> req = new Request<>();
					Response<FichierDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasFichierPartenaire);

					res = fichierBusiness.create(req, locale);

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				if (!datasFichierDescription.isEmpty()) {

					Request<FichierDto> req = new Request<>();
					Response<FichierDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasFichierDescription);

					res = fichierBusiness.create(req, locale);

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
				}

				List<EntrepriseDto> itemsDto = new ArrayList<EntrepriseDto>();
				for (Entreprise entity : itemsSaved) {
					EntrepriseDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update Entreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				Utilities.deleteFileOnSever(listOfFilesCreate, paramsUtils);
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}else {
				// suppression des anciens fichiers quand le processus de update c'est bien dérouler

				slf4jLogger.info("listOfOldFiles ... " + listOfOldFiles);
				slf4jLogger.info("listOfOldFiles.toString() ... " + listOfOldFiles.toString());

				Utilities.deleteFileOnSever(listOfOldFiles, paramsUtils);
			}
		}
		return response;
	}


	/**
	 * delete Entreprise by using EntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<EntrepriseDto> delete(Request<EntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete Entreprise-----");

		response = new Response<EntrepriseDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Entreprise> items = new ArrayList<Entreprise>();

			for (EntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la entreprise existe
				Entreprise existingEntity = null;
				existingEntity = entrepriseRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("entreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				//				// partenaireFournisseur
				//				List<PartenaireFournisseur> listOfPartenaireFournisseur = partenaireFournisseurRepository.findByEntrepriseId(existingEntity.getId(), false);
				//				if (listOfPartenaireFournisseur != null && !listOfPartenaireFournisseur.isEmpty()){
				//					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPartenaireFournisseur.size() + ")", locale));
				//					response.setHasError(true);
				//					return response;
				//				}
				//				// secteurDactiviteEntreprise
				//				List<SecteurDactiviteEntreprise> listOfSecteurDactiviteEntreprise = secteurDactiviteEntrepriseRepository.findByEntrepriseId(existingEntity.getId(), false);
				//				if (listOfSecteurDactiviteEntreprise != null && !listOfSecteurDactiviteEntreprise.isEmpty()){
				//					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfSecteurDactiviteEntreprise.size() + ")", locale));
				//					response.setHasError(true);
				//					return response;
				//				}
				//				// user
				//				List<User> listOfUser = userRepository.findByEntrepriseId(existingEntity.getId(), false);
				//				if (listOfUser != null && !listOfUser.isEmpty()){
				//					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfUser.size() + ")", locale));
				//					response.setHasError(true);
				//					return response;
				//				}

				existingEntity.setIsLocked(true);
				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				entrepriseRepository.save((Iterable<Entreprise>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete Entreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
	
	/**
	 * updateSecteurDactivite Entreprise by using EntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<EntrepriseDto> updateSecteurDactivite(Request<EntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin updateSecteurDactivite Entreprise-----");

		response = new Response<EntrepriseDto>();
	

		try {
			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			User utilisateur = null ;
			if (request.getUser() != null && request.getUser() > 0) {
				utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
			}

			List<Entreprise> items = new ArrayList<Entreprise>();

			List<SecteurDactiviteEntrepriseDto> datasSecteurDactiviteEntreprise = new ArrayList<>();
			List<SecteurDactiviteEntreprise> itemsSecteurDactiviteEntrepriseToDelete = new ArrayList<>();


			for (EntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la entreprise existe
				Entreprise entityToSave = null;
				entityToSave = entrepriseRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("entreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				if (utilisateur != null && utilisateur.getEntreprise() != null) {
					if (!entityToSaveId.equals(utilisateur.getEntreprise().getId())) {
						//if (!entityToSaveId.equals(utilisateur.getEntreprise().getId()) || !entityToSave.getCreatedBy().equals(utilisateur.getId()) ) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'a pas ce droit" , locale));
						response.setHasError(true);
						return response;
					}
					
				}
				
				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);


				if (Utilities.isNotEmpty(dto.getDatasSecteurDactiviteEntreprise())) {

					// recuperation des anciens pour suppression
					List<SecteurDactiviteEntreprise> secteurDactiviteEntreprise = secteurDactiviteEntrepriseRepository.findByEntrepriseId(entityToSaveId, false);
					if (Utilities.isNotEmpty(secteurDactiviteEntreprise)) {
						itemsSecteurDactiviteEntrepriseToDelete.addAll(secteurDactiviteEntreprise);
					}
					for (SecteurDactiviteEntrepriseDto data : dto.getDatasSecteurDactiviteEntreprise()) {
						data.setEntrepriseId(entityToSaveId);
					}
					datasSecteurDactiviteEntreprise.addAll(dto.getDatasSecteurDactiviteEntreprise());
				}
			}

			if (!items.isEmpty()) {
				List<Entreprise> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = entrepriseRepository.save((Iterable<Entreprise>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("entreprise", locale));
					response.setHasError(true);
					return response;
				}

				// creation des SecteurDactiviteEntreprise
				if (!itemsSecteurDactiviteEntrepriseToDelete.isEmpty()) {
					List<SecteurDactiviteEntreprise> itemsToDelete = null;
					for (SecteurDactiviteEntreprise data : itemsSecteurDactiviteEntrepriseToDelete) {
						data.setIsDeleted(true);
						data.setDeletedBy(request.getUser());
						data.setDeletedAt(Utilities.getCurrentDate());
					}
					// maj les donnees en base
					itemsToDelete = secteurDactiviteEntrepriseRepository.save((Iterable<SecteurDactiviteEntreprise>) itemsSecteurDactiviteEntrepriseToDelete);
					if (itemsToDelete == null || itemsToDelete.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!datasSecteurDactiviteEntreprise.isEmpty()) {

					Request<SecteurDactiviteEntrepriseDto> req = new Request<>();
					Response<SecteurDactiviteEntrepriseDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasSecteurDactiviteEntreprise);

					res = secteurDactiviteEntrepriseBusiness.create(req, locale);

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
					
					// maj de appelDoffresRestreint
					
					
					for (Entreprise entreprise : itemsSaved) {
						
						// recuperer tous les users de l'entreprise

						List<SecteurDactiviteEntreprise> secteurDactiviteEntreprises = secteurDactiviteEntrepriseRepository.findByEntrepriseId(entreprise.getId(), false);

						// formation de la request 
						Request<SecteurDactiviteAppelDoffresDto> reqLocal = new Request<>();
						List<AppelDoffres> listAppelDoffres = new ArrayList<>();
						List<AppelDoffres> listAppelDoffresDeTypeRestreint = new ArrayList<>();
						List<SecteurDactiviteAppelDoffresDto> datas = new ArrayList<>();


						if (Utilities.isNotEmpty(secteurDactiviteEntreprises)) {
							for (SecteurDactiviteEntreprise secteurDactiviteEntreprise : secteurDactiviteEntreprises) {

								SecteurDactiviteAppelDoffresDto data = new SecteurDactiviteAppelDoffresDto();
								
								data.setIsAttribuer(false); // non attribués
								data.setEtatCode(EtatEnum.ENTRANT); // visible par les forunisseurs
								data.setSecteurDactiviteId(secteurDactiviteEntreprise.getSecteurDactivite().getId());
								data.setTypeAppelDoffresCode(TypeAppelDoffresEnum.GENERAL); // AO general
								datas.add(data);
							}
							reqLocal.setData(datas.get(0));
							datas.remove(0); // important pour eviter des recupereantion en doublon
							reqLocal.setDatas(datas);
							//req.setIsAnd(false);
							
							// on veut la liste des appelDoffres generaux non attribués qui concernent les secteurs d'activités de ce user
							listAppelDoffres = secteurDactiviteAppelDoffresRepository.getByCriteriaCustom(reqLocal, em, locale);
							
							
							
							// recuperation des AO de type restreint
							reqLocal.getData().setTypeAppelDoffresCode(TypeAppelDoffresEnum.RESTREINT);
							if (Utilities.isNotEmpty(reqLocal.getDatas())) {
								List<SecteurDactiviteAppelDoffresDto> datasLoc = new ArrayList<SecteurDactiviteAppelDoffresDto>() ;
								for (SecteurDactiviteAppelDoffresDto data : reqLocal.getDatas()) {
									data.setTypeAppelDoffresCode(TypeAppelDoffresEnum.RESTREINT);
									datasLoc.add(data);
								}
								reqLocal.setDatas(datasLoc) ;
							}
							
							listAppelDoffresDeTypeRestreint = secteurDactiviteAppelDoffresRepository.getByCriteriaCustom(reqLocal, em, locale);
						}

												
						List<User> usersEntreprise = userRepository.findByEntrepriseId(entreprise.getId(), false);
						if (Utilities.isNotEmpty(usersEntreprise)) {
							for (User user : usersEntreprise) {
								List<AppelDoffresRestreint> appelDoffresRestreintUserToDelete = new ArrayList<AppelDoffresRestreint>() ;
								List<AppelDoffresRestreint> appelDoffresRestreintUserRestant = new ArrayList<AppelDoffresRestreint>() ;
								List<AppelDoffresRestreint> datasAppelDoffresRestreint = new ArrayList<>();

								// pour chaque user de l'entreprise recuperer ses aor ou les AO sont general
								//List<AppelDoffresRestreint> appelDoffresRestreintUser = appelDoffresRestreintRepository.findByUserFournisseurId(user.getId(), false);
								List<AppelDoffresRestreint> appelDoffresRestreintUser = appelDoffresRestreintRepository.findByUserFournisseurIdAndTypeAppelDoffreCode(user.getId(), TypeAppelDoffresEnum.GENERAL, false);
								if (Utilities.isNotEmpty(appelDoffresRestreintUser)) {// sinon bloucler sur la liste des aor et conserver celle ou il na pas encore soumissionner
									for (AppelDoffresRestreint data : appelDoffresRestreintUser) {
										if (data.getIsSoumissionnerByFournisseur() == null || !data.getIsSoumissionnerByFournisseur()) {
											data.setIsDeleted(true);
											data.setDeletedBy(request.getUser());
											data.setDeletedAt(Utilities.getCurrentDate());
											appelDoffresRestreintUserToDelete.add(data) ;
										}else {
											appelDoffresRestreintUserRestant.add(data) ;
										}
									}
								}
								
								List<AppelDoffresRestreint> appelDoffresRestreintUserTpyeAORestreint = appelDoffresRestreintRepository.findByUserFournisseurIdAndTypeAppelDoffreCode(user.getId(), TypeAppelDoffresEnum.RESTREINT, false);
								if (Utilities.isNotEmpty(appelDoffresRestreintUserTpyeAORestreint)) {
									for (AppelDoffresRestreint data : appelDoffresRestreintUserTpyeAORestreint) {
										
										if (Utilities.isNotEmpty(listAppelDoffresDeTypeRestreint)) {
											
											if (listAppelDoffresDeTypeRestreint.stream().anyMatch(a->a.getId().equals(data.getAppelDoffres().getId()))) {
												appelDoffresRestreintUserRestant.add(data) ;
											}else { // c'est la que tous les problemes
												if ((data.getIsSoumissionnerByFournisseur() != null && data.getIsSoumissionnerByFournisseur()) ) {
													appelDoffresRestreintUserRestant.add(data) ;
												}else {
													data.setIsDeleted(true);
													data.setDeletedBy(request.getUser());
													data.setDeletedAt(Utilities.getCurrentDate());
													appelDoffresRestreintUserToDelete.add(data) ;
												}
											}
										}else {
											// on blame car cela veut dire que tout les AO de type restreint sont attribue
											data.setIsDeleted(true);
											data.setDeletedBy(request.getUser());
											data.setDeletedAt(Utilities.getCurrentDate());
											appelDoffresRestreintUserToDelete.add(data) ;
										}
									}
								}
								
								
								else {// si pas de aor creer basique des aor
									
								}
								
								
								// CREATION DES AOR
								// recuperation de toute les offres lies à ses secteurs puis creation dans appelDoffresRestreint

								if (Utilities.isNotEmpty(listAppelDoffres)) {
									
	 								for (AppelDoffres data : listAppelDoffres) {
	 									
	 									if (!Utilities.isNotEmpty(appelDoffresRestreintUserRestant)
	 										|| !appelDoffresRestreintUserRestant.stream().anyMatch(a->a.getAppelDoffres().getId().equals(data.getId())) ) {
	 										AppelDoffresRestreint appelDoffresRestreint = new AppelDoffresRestreint();
											
											appelDoffresRestreint.setAppelDoffres(data);
											appelDoffresRestreint.setUser(user);
											appelDoffresRestreint.setIsAppelRestreint(false); // pour dire que se sont des elements pour visualtisations
											appelDoffresRestreint.setIsSoumissionnerByFournisseur(false);
											appelDoffresRestreint.setIsDesactiver(false);
											appelDoffresRestreint.setIsVisualiserContenuByFournisseur(false);
											appelDoffresRestreint.setIsVisualiserNotifByFournisseur(false);
											//appelDoffresRestreint.setIsUpdateAppelDoffres(data.getIsUpdate()); // MAJ mis en evidence ne le concerne pas
											appelDoffresRestreint.setIsUpdateAppelDoffres(false);
											appelDoffresRestreint.setIsVisibleByFournisseur(true); // car il ne l'a pas encore supprimer logiquement
											appelDoffresRestreint.setCreatedAt(Utilities.getCurrentDate());
											appelDoffresRestreint.setCreatedBy(user.getId());
											appelDoffresRestreint.setIsSelectedByClient(false);
											appelDoffresRestreint.setIsDeleted(false);
											datasAppelDoffresRestreint.add(appelDoffresRestreint);
	 										
										}								
										
									}
								}
								
								if (Utilities.isNotEmpty(datasAppelDoffresRestreint)) {
									appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) datasAppelDoffresRestreint);
								}
								if (Utilities.isNotEmpty(appelDoffresRestreintUserToDelete)) {
									appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) appelDoffresRestreintUserToDelete);
								}
							}
						}
						
						// puis delete cette liste, puis faire un creer en ce basant sur la liste des domaines envoyes

						// si cette liste ne concerne pas ses domaines on delete
						// sinon 
						
						// bloucler sur la liste et recuperer ce qui 
						
					}
					
				}

				List<EntrepriseDto> itemsDto = new ArrayList<EntrepriseDto>();
				for (Entreprise entity : itemsSaved) {
					EntrepriseDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end updateSecteurDactivite Entreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete Entreprise by using EntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<EntrepriseDto> forceDelete(Request<EntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete Entreprise-----");

		response = new Response<EntrepriseDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Entreprise> items = new ArrayList<Entreprise>();

			for (EntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la entreprise existe
				Entreprise existingEntity = null;
				existingEntity = entrepriseRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("entreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// partenaireFournisseur
				List<PartenaireFournisseur> listOfPartenaireFournisseur = partenaireFournisseurRepository.findByEntrepriseId(existingEntity.getId(), false);
				if (listOfPartenaireFournisseur != null && !listOfPartenaireFournisseur.isEmpty()){
					Request<PartenaireFournisseurDto> deleteRequest = new Request<PartenaireFournisseurDto>();
					deleteRequest.setDatas(PartenaireFournisseurTransformer.INSTANCE.toDtos(listOfPartenaireFournisseur));
					deleteRequest.setUser(request.getUser());
					Response<PartenaireFournisseurDto> deleteResponse = partenaireFournisseurBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// secteurDactiviteEntreprise
				List<SecteurDactiviteEntreprise> listOfSecteurDactiviteEntreprise = secteurDactiviteEntrepriseRepository.findByEntrepriseId(existingEntity.getId(), false);
				if (listOfSecteurDactiviteEntreprise != null && !listOfSecteurDactiviteEntreprise.isEmpty()){
					Request<SecteurDactiviteEntrepriseDto> deleteRequest = new Request<SecteurDactiviteEntrepriseDto>();
					deleteRequest.setDatas(SecteurDactiviteEntrepriseTransformer.INSTANCE.toDtos(listOfSecteurDactiviteEntreprise));
					deleteRequest.setUser(request.getUser());
					Response<SecteurDactiviteEntrepriseDto> deleteResponse = secteurDactiviteEntrepriseBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// user
				List<User> listOfUser = userRepository.findByEntrepriseId(existingEntity.getId(), false);
				if (listOfUser != null && !listOfUser.isEmpty()){
					Request<UserDto> deleteRequest = new Request<UserDto>();
					deleteRequest.setDatas(UserTransformer.INSTANCE.toDtos(listOfUser));
					deleteRequest.setUser(request.getUser());
					Response<UserDto> deleteResponse = userBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setIsDeleted(true);
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				entrepriseRepository.save((Iterable<Entreprise>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete Entreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * locked Entreprise by using EntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<EntrepriseDto> locked(Request<EntrepriseDto> request, Locale locale)  {
		slf4jLogger.info("----begin locked Entreprise-----");

		response = new Response<EntrepriseDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			if (!utilisateur.getRole().getCode().equals(TypeUserEnum.USER_ADMIN)) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
				response.setHasError(true);
				return response;
			}


			List<Entreprise> items = new ArrayList<Entreprise>();
			List<User> itemsUser = new ArrayList<User>();

			for (EntrepriseDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}



				// Verifier si la entreprise existe
				Entreprise entityToSave = null;
				entityToSave = entrepriseRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("entreprise -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();


				entityToSave.setIsLocked(dto.getIsLocked());

				List<User> usersEntreprise = userRepository.findByEntrepriseId(entityToSaveId, false);

				if (Utilities.isNotEmpty(usersEntreprise)) {
					for (User user : usersEntreprise) {
						user.setIsLocked(true);
					}
					itemsUser.addAll(usersEntreprise);

				}
				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Entreprise> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = entrepriseRepository.save((Iterable<Entreprise>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("entreprise", locale));
					response.setHasError(true);
					return response;
				}
				if (!itemsUser.isEmpty()) {
					List<User> itemsUserSaved = null;
					// maj les donnees en base
					itemsUserSaved = userRepository.save((Iterable<User>) itemsUser);
					if (itemsUserSaved == null || itemsUserSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("user", locale));
						response.setHasError(true);
					}
				}


				List<EntrepriseDto> itemsDto = new ArrayList<EntrepriseDto>();
				for (Entreprise entity : itemsSaved) {
					EntrepriseDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end locked Entreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}





	/**
	 * get Entreprise by using EntrepriseDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<EntrepriseDto> getByCriteria(Request<EntrepriseDto> request, Locale locale) {
		slf4jLogger.info("----begin get Entreprise-----");

		response = new Response<EntrepriseDto>();

		try {
			List<Entreprise> items = null;
			items = entrepriseRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<EntrepriseDto> itemsDto = new ArrayList<EntrepriseDto>();
				for (Entreprise entity : items) {
					EntrepriseDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(entrepriseRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("entreprise", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get Entreprise-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full EntrepriseDto by using Entreprise as object.
	 *
	 * @param entity, locale
	 * @return EntrepriseDto
	 *
	 */
	private EntrepriseDto getFullInfos(Entreprise entity, Integer size, Locale locale) throws Exception {
		EntrepriseDto dto = EntrepriseTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		dto.setPassword("");

		if (!dto.getTypeEntrepriseCode().equals(TypeUserEnum.ENTREPRISE_FOURNISSEUR)) {
			List<User> users = userRepository.findByEntrepriseIdAndTypeEntrepriseCode(dto.getId(), dto.getTypeEntrepriseCode(), false);
			dto.setNombreUser(Utilities.isNotEmpty(users) ? users.size() : 0);
		}
		List<SecteurDactiviteEntreprise> datas = secteurDactiviteEntrepriseRepository.findByEntrepriseId(dto.getId(), false);

		if (Utilities.isNotEmpty(datas)) {
			List<SecteurDactivite> secteurDactivites = new ArrayList<>();
			for (SecteurDactiviteEntreprise data : datas) {
				secteurDactivites.add(data.getSecteurDactivite());
			}
			dto.setDatasSecteurDactivite(SecteurDactiviteTransformer.INSTANCE.toDtos(secteurDactivites));
		}
		
		List<DomaineEntreprise> datasDomaineEntreprise = domaineEntrepriseRepository.findByEntrepriseId(dto.getId(), false);

		if (Utilities.isNotEmpty(datasDomaineEntreprise)) {
			List<Domaine> domaines = new ArrayList<>();
			for (DomaineEntreprise data : datasDomaineEntreprise) {
				domaines.add(data.getDomaine());
			}
			dto.setDatasDomaine(DomaineTransformer.INSTANCE.toDtos(domaines));
		}
		
		if (Utilities.notBlank(dto.getUrlLogo())) {
			String fileNameByExtension = Utilities.addExtensionInFileName( dto.getUrlLogo() , dto.getExtensionLogo()) ;

			dto.setCheminFichier(Utilities.getSuitableFileUrl(fileNameByExtension, paramsUtils));
			
			// formatter le name du fichier
			dto.setUrlLogo(Utilities.getBasicNameFile(dto.getUrlLogo()));
			
			//dto.setCheminFichier(Utilities.getSuitableFileUrl(dto.getUrlLogo(), paramsUtils));
			//dto.setCheminFichier(Utilities.getSuitableFileUrlCustom(dto.getUrlLogo(), GlobalEnum.profils, paramsUtils));
		}
		if (size > 1) {
			return dto;
		}

		// TODO : Renvoyr l'url et non le nom du fichier
		List<Fichier> datasFichierDescription = new ArrayList<>();
		//datasFichier = fichierRepository.findByOffreId(dto.getId(), false);
		datasFichierDescription = fichierRepository.findBySourceIdAndSource(dto.getId(), GlobalEnum.fichiers_ins_description, false);

		if (Utilities.isNotEmpty(datasFichierDescription)) {
			dto.setDatasFichierDescription(FichierTransformer.INSTANCE.toDtos(datasFichierDescription));
			for (FichierDto data : dto.getDatasFichierDescription()) {
				if (Utilities.notBlank(data.getName())) {
					// Repertoire où je depose mon fichier
					String filesDirectory = Utilities.getSuitableFileDirectory(data.getExtension(), paramsUtils);
					Utilities.createDirectory(filesDirectory);
					if (!filesDirectory.endsWith("/")) {
						filesDirectory += "/";
					}
					//filesDirectory=filesDirectory+data.getName();
					String fileNameByExtension = Utilities.addExtensionInFileName( data.getName() , data.getExtension()) ;
					filesDirectory=filesDirectory + fileNameByExtension;
					//data.setFichierBase64(Utilities.convertFileToBase64(filesDirectory));


					//data.setUrlFichier(filesDirectory);
					// formatter le name du fichier
					data.setName(Utilities.getBasicNameFile(data.getName()));
					
					data.setUrlFichier(Utilities.getSuitableFileUrl(fileNameByExtension, paramsUtils));
					//data.setUrlFichier(Utilities.getSuitableFileUrl(data.getName(), paramsUtils));
					//data.setUrlFichier(Utilities.getSuitableFileUrlCustom(data.getName(), GlobalEnum.offres, paramsUtils));

				}
			}
		}

		// TODO : Renvoyr l'url et non le nom du fichier
		List<Fichier> datasFichierPartenaire = new ArrayList<>();
		//datasFichier = fichierRepository.findByOffreId(dto.getId(), false);
		datasFichierPartenaire = fichierRepository.findBySourceIdAndSource(dto.getId(), GlobalEnum.fichiers_ins_partenaire, false);

		if (Utilities.isNotEmpty(datasFichierPartenaire)) {
			dto.setDatasFichierPartenaire(FichierTransformer.INSTANCE.toDtos(datasFichierPartenaire));
			for (FichierDto data : dto.getDatasFichierPartenaire()) {
				if (Utilities.notBlank(data.getName())) {
					// Repertoire où je depose mon fichier
					String filesDirectory = Utilities.getSuitableFileDirectory(data.getExtension(), paramsUtils);
					Utilities.createDirectory(filesDirectory);
					if (!filesDirectory.endsWith("/")) {
						filesDirectory += "/";
					}

					String fileNameByExtension = Utilities.addExtensionInFileName( data.getName() , data.getExtension()) ;
					filesDirectory=filesDirectory + fileNameByExtension;
					//filesDirectory=filesDirectory+data.getName();
					//data.setFichierBase64(Utilities.convertFileToBase64(filesDirectory));

					data.setName(Utilities.getBasicNameFile(data.getName()));
					//data.setUrlFichier(filesDirectory);
					data.setUrlFichier(Utilities.getSuitableFileUrl(fileNameByExtension, paramsUtils));
					//data.setUrlFichier(Utilities.getSuitableFileUrl(data.getName(), paramsUtils));
					//data.setUrlFichier(Utilities.getSuitableFileUrlCustom(data.getName(), GlobalEnum.offres, paramsUtils));

				}
			}
		}


		return dto;
	}
}
