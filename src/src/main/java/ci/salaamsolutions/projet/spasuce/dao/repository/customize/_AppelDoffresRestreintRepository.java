package ci.salaamsolutions.projet.spasuce.dao.repository.customize;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;

/**
 * Repository customize : AppelDoffresRestreint.
 */
@Repository
public interface _AppelDoffresRestreintRepository {
	default List<String> _generateCriteria(AppelDoffresRestreintDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		// des champs des appels d'offres
		if (Utilities.notBlank(dto.getTypeAppelDoffresCode())) {
			listOfQuery.add(CriteriaUtils.generateCriteria("typeAppelDoffresCode", dto.getTypeAppelDoffresCode(), "e.appelDoffres.typeAppelDoffres.code", "String", dto.getTypeAppelDoffresCodeParam(), param, index, locale));
		}
		if (Utilities.notBlank(dto.getEtatCode())) {
			listOfQuery.add(CriteriaUtils.generateCriteria("etatCode", dto.getEtatCode(), "e.appelDoffres.etat.code", "String", dto.getEtatCodeParam(), param, index, locale));
		}
		if (dto.getIsLocked()!= null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isLocked", dto.getIsLocked(), "e.appelDoffres.isLocked", "Boolean", dto.getIsLockedParam(), param, index, locale));
		}
		if (dto.getIsAttribuer()!= null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isAttribuer", dto.getIsAttribuer(), "e.appelDoffres.isAttribuer", "Boolean", dto.getIsAttribuerParam(), param, index, locale));
		}
		if (dto.getIsModifiable()!= null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isModifiable", dto.getIsModifiable(), "e.appelDoffres.isModifiable", "Boolean", dto.getIsModifiableParam(), param, index, locale));
		}
		if (dto.getIsRetirer()!= null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isRetirer", dto.getIsRetirer(), "e.appelDoffres.isRetirer", "Boolean", dto.getIsRetirerParam(), param, index, locale));
		}
		if (dto.getIsVisibleByClient()!= null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isVisibleByClient", dto.getIsVisibleByClient(), "e.appelDoffres.isVisibleByClient", "Boolean", dto.getIsVisibleByClientParam(), param, index, locale));
		}
		if (dto.getIsUpdate()!= null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isUpdate", dto.getIsUpdate(), "e.appelDoffres.isUpdate", "Boolean", dto.getIsUpdateParam(), param, index, locale));
		}

		// pour ne pas renvoyer les appels d'offres deja supprimés
		listOfQuery.add(CriteriaUtils.generateCriteria("isDeletedAppelDoffres", dto.getIsDeleted(), "e.appelDoffres.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));



		return listOfQuery;
	}

	/**
	 * Finds AppelDoffresRestreint by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object AppelDoffresRestreint whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select distinct e.user.entreprise from AppelDoffresRestreint e where e.appelDoffres.id = :appelDoffresId and e.isDeleted = :isDeleted and e.user.isLocked = :isDeleted and e.user.entreprise.isLocked = :isDeleted")
	List<Entreprise> findDistinctEntrepriseByAppelDoffresId(@Param("appelDoffresId")Integer appelDoffresId, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds AppelDoffresRestreint by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object AppelDoffresRestreint whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.appelDoffres.id = :appelDoffresId and e.isVisualiserContenuByFournisseur = :isVisualiserContenuByFournisseur and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByAppelDoffresIdAndIsVisualiserContenuByFournisseur(@Param("appelDoffresId")Integer appelDoffresId, @Param("isVisualiserContenuByFournisseur")Boolean isVisualiserContenuByFournisseur, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AppelDoffresRestreint by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.user.id = :userFournisseurId and e.isDesactiver = :isDesactiver and e.isVisibleByFournisseur = :isVisibleByFournisseur and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByUserFournisseurIdAndIsVisibleByFournisseurAndIsDesactiverx(@Param("userFournisseurId")Integer userFournisseurId, @Param("isVisibleByFournisseur")Boolean isVisibleByFournisseur, @Param("isDesactiver")Boolean isDesactiver, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AppelDoffresRestreint by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.user.id = :userFournisseurId and e.isDesactiver = :isDesactiver and e.appelDoffres.isAttribuer = :isAttribuerOffre and e.isVisibleByFournisseur = :isVisibleByFournisseur and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByUserFournisseurIdAndIsAttribuerOffreAndIsVisibleByFournisseurAndIsDesactiver(@Param("userFournisseurId")Integer userFournisseurId, @Param("isAttribuerOffre")Boolean isAttribuerOffre, @Param("isVisibleByFournisseur")Boolean isVisibleByFournisseur, @Param("isDesactiver")Boolean isDesactiver, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AppelDoffresRestreint by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.user.id = :userFournisseurId and e.isDesactiver = :isDesactiver and e.isVisibleByFournisseur = :isVisibleByFournisseur and e.isAppelRestreint = :isAppelRestreint and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByUserFournisseurIdAndIsVisibleByFournisseurAndIsDesactiverAndIsAppelRestreint(@Param("userFournisseurId")Integer userFournisseurId, @Param("isVisibleByFournisseur")Boolean isVisibleByFournisseur, @Param("isDesactiver")Boolean isDesactiver, @Param("isAppelRestreint")Boolean isAppelRestreint, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AppelDoffresRestreint by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.user.id = :userFournisseurId and e.appelDoffres.id = :appelDoffresId and e.isDesactiver = :isDesactiver and e.isVisibleByFournisseur = :isVisibleByFournisseur and e.isAppelRestreint = :isAppelRestreint and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByUserFournisseurIdAndAppelDoffresIdAndIsVisibleByFournisseurAndIsDesactiverAndIsAppelRestreintx(@Param("userFournisseurId")Integer userFournisseurId, @Param("appelDoffresId")Integer appelDoffresId, @Param("isVisibleByFournisseur")Boolean isVisibleByFournisseur, @Param("isDesactiver")Boolean isDesactiver, @Param("isAppelRestreint")Boolean isAppelRestreint, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds AppelDoffresRestreint by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.user.id = :userFournisseurId and e.appelDoffres.id = :appelDoffresId and e.isVisualiserNotifByFournisseur = :isVisualiserNotifByFournisseur and e.isDesactiver = :isDesactiver and e.isVisibleByFournisseur = :isVisibleByFournisseur and e.isAppelRestreint = :isAppelRestreint and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByUserFournisseurIdAndAppelDoffresIdAndIsVisualiserNotifByFournisseurAndIsAppelRestreintAndIsVisibleByFournisseurAndIsDesactiver(@Param("userFournisseurId")Integer userFournisseurId, @Param("appelDoffresId")Integer appelDoffresId, @Param("isVisualiserNotifByFournisseur")Boolean isVisualiserNotifByFournisseur, @Param("isAppelRestreint")Boolean isAppelRestreint, @Param("isVisibleByFournisseur")Boolean isVisibleByFournisseur, @Param("isDesactiver")Boolean isDesactiver, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AppelDoffresRestreint by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.user.id = :userFournisseurId and e.appelDoffres.isAttribuer = :isAttribuer and e.appelDoffres.isLocked = :isLocked and e.appelDoffres.id = :appelDoffresId and e.appelDoffres.id = :appelDoffresId and e.appelDoffres.isRetirer = :isRetirer and e.isVisualiserNotifByFournisseur = :isVisualiserNotifByFournisseur and e.isDesactiver = :isDesactiver and e.isAppelRestreint = :isAppelRestreint and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> 
	findByUserFournisseurIdAndAppelDoffresIdAndIsVisualiserNotifByFournisseurAndIsAppelRestreintAndIsDesactiverAndIsAttribuerAndIsLockedAndIsRetirerAndIsVisibleByClient
	(@Param("userFournisseurId")Integer userFournisseurId, @Param("appelDoffresId")Integer appelDoffresId, @Param("isVisualiserNotifByFournisseur")Boolean isVisualiserNotifByFournisseur, @Param("isAppelRestreint")Boolean isAppelRestreint, @Param("isDesactiver")Boolean isDesactiver, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds AppelDoffresRestreint by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.user.id = :userFournisseurId and e.appelDoffres.id = :appelDoffresId and e.isVisualiserNotifByFournisseur = :isVisualiserNotifByFournisseur and e.isDesactiver = :isDesactiver and e.isAppelRestreint = :isAppelRestreint and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByUserFournisseurIdAndAppelDoffresIdAndIsVisualiserNotifByFournisseurAndIsAppelRestreintAndIsDesactiver(@Param("userFournisseurId")Integer userFournisseurId, @Param("appelDoffresId")Integer appelDoffresId, @Param("isVisualiserNotifByFournisseur")Boolean isVisualiserNotifByFournisseur, @Param("isAppelRestreint")Boolean isAppelRestreint, @Param("isDesactiver")Boolean isDesactiver, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds AppelDoffresRestreint by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.appelDoffres.id = :appelDoffresId and e.isVisualiserContenuByFournisseur = :isVisualiserContenuByFournisseur and e.isDesactiver = :isDesactiver and e.isAppelRestreint = :isAppelRestreint and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByAppelDoffresIdAndIsVisualiserContenuByFournisseurAndIsAppelRestreintAndIsDesactiver(@Param("appelDoffresId")Integer appelDoffresId, @Param("isVisualiserContenuByFournisseur")Boolean isVisualiserContenuByFournisseur, @Param("isAppelRestreint")Boolean isAppelRestreint, @Param("isDesactiver")Boolean isDesactiver, @Param("isDeleted")Boolean isDeleted);



	/**
	 * Finds AppelDoffresRestreint by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.user.id = :userFournisseurId and e.appelDoffres.id = :appelDoffresId and e.isDesactiver = :isDesactiver and e.isVisibleByFournisseur = :isVisibleByFournisseur and e.isAppelRestreint = :isAppelRestreint and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByUserFournisseurIdAndAppelDoffresIdAndIsVisibleByFournisseurAndIsAppelRestreint(@Param("userFournisseurId")Integer userFournisseurId, @Param("appelDoffresId")Integer appelDoffresId, @Param("isVisibleByFournisseur")Boolean isVisibleByFournisseur, @Param("isDesactiver")Boolean isDesactiver, @Param("isAppelRestreint")Boolean isAppelRestreint, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AppelDoffresRestreint by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.user.id = :userFournisseurId and e.appelDoffres.id = :appelDoffresId and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByUserFournisseurIdAndAppelDoffresId(@Param("userFournisseurId")Integer userFournisseurId, @Param("appelDoffresId")Integer appelDoffresId, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AppelDoffresRestreint by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.user.id = :userFournisseurId and e.appelDoffres.id = :appelDoffresId and e.isDeleted = :isDeleted")
	AppelDoffresRestreint findAppelDoffresRestreintByUserFournisseurIdAndAppelDoffresId(@Param("userFournisseurId")Integer userFournisseurId, @Param("appelDoffresId")Integer appelDoffresId, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds AppelDoffresRestreint by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object AppelDoffresRestreint whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.appelDoffres.id = :appelDoffresId and e.isAppelRestreint = :isAppelRestreint and  e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByAppelDoffresIdAndIsAppelRestreint(@Param("appelDoffresId")Integer appelDoffresId, @Param("isAppelRestreint")Boolean isAppelRestreint, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AppelDoffresRestreint by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.user.id = :userFournisseurId and e.appelDoffres.typeAppelDoffres.code = :typeAppelDoffresCode and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByUserFournisseurIdAndTypeAppelDoffreCode(@Param("userFournisseurId")Integer userFournisseurId, @Param("typeAppelDoffresCode")String typeAppelDoffresCode, @Param("isDeleted")Boolean isDeleted);


}
