package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._SousCritereNoteRepository;

/**
 * Repository : SousCritereNote.
 */
@Repository
public interface SousCritereNoteRepository extends JpaRepository<SousCritereNote, Integer>, _SousCritereNoteRepository {
	/**
	 * Finds SousCritereNote by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object SousCritereNote whose id is equals to the given id. If
	 *         no SousCritereNote is found, this method returns null.
	 */
	@Query("select e from SousCritereNote e where e.id = :id and e.isDeleted = :isDeleted")
	SousCritereNote findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SousCritereNote by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object SousCritereNote whose libelle is equals to the given libelle. If
	 *         no SousCritereNote is found, this method returns null.
	 */
	@Query("select e from SousCritereNote e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	SousCritereNote findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SousCritereNote by using coefficient as a search criteria.
	 *
	 * @param coefficient
	 * @return An Object SousCritereNote whose coefficient is equals to the given coefficient. If
	 *         no SousCritereNote is found, this method returns null.
	 */
	@Query("select e from SousCritereNote e where e.coefficient = :coefficient and e.isDeleted = :isDeleted")
	List<SousCritereNote> findByCoefficient(@Param("coefficient")Integer coefficient, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SousCritereNote by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object SousCritereNote whose createdAt is equals to the given createdAt. If
	 *         no SousCritereNote is found, this method returns null.
	 */
	@Query("select e from SousCritereNote e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<SousCritereNote> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SousCritereNote by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object SousCritereNote whose createdBy is equals to the given createdBy. If
	 *         no SousCritereNote is found, this method returns null.
	 */
	@Query("select e from SousCritereNote e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<SousCritereNote> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SousCritereNote by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object SousCritereNote whose updatedAt is equals to the given updatedAt. If
	 *         no SousCritereNote is found, this method returns null.
	 */
	@Query("select e from SousCritereNote e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<SousCritereNote> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SousCritereNote by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object SousCritereNote whose updatedBy is equals to the given updatedBy. If
	 *         no SousCritereNote is found, this method returns null.
	 */
	@Query("select e from SousCritereNote e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<SousCritereNote> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SousCritereNote by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object SousCritereNote whose deletedAt is equals to the given deletedAt. If
	 *         no SousCritereNote is found, this method returns null.
	 */
	@Query("select e from SousCritereNote e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<SousCritereNote> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SousCritereNote by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object SousCritereNote whose deletedBy is equals to the given deletedBy. If
	 *         no SousCritereNote is found, this method returns null.
	 */
	@Query("select e from SousCritereNote e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<SousCritereNote> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SousCritereNote by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object SousCritereNote whose isDeleted is equals to the given isDeleted. If
	 *         no SousCritereNote is found, this method returns null.
	 */
	@Query("select e from SousCritereNote e where e.isDeleted = :isDeleted")
	List<SousCritereNote> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SousCritereNote by using critereNoteId as a search criteria.
	 *
	 * @param critereNoteId
	 * @return A list of Object SousCritereNote whose critereNoteId is equals to the given critereNoteId. If
	 *         no SousCritereNote is found, this method returns null.
	 */
	@Query("select e from SousCritereNote e where e.critereNote.id = :critereNoteId and e.isDeleted = :isDeleted")
	List<SousCritereNote> findByCritereNoteId(@Param("critereNoteId")Integer critereNoteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one SousCritereNote by using critereNoteId as a search criteria.
   *
   * @param critereNoteId
   * @return An Object SousCritereNote whose critereNoteId is equals to the given critereNoteId. If
   *         no SousCritereNote is found, this method returns null.
   */
  @Query("select e from SousCritereNote e where e.critereNote.id = :critereNoteId and e.isDeleted = :isDeleted")
  SousCritereNote findSousCritereNoteByCritereNoteId(@Param("critereNoteId")Integer critereNoteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of SousCritereNote by using sousCritereNoteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of SousCritereNote
	 * @throws DataAccessException,ParseException
	 */
	public default List<SousCritereNote> getByCriteria(Request<SousCritereNoteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from SousCritereNote e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<SousCritereNote> query = em.createQuery(req, SousCritereNote.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of SousCritereNote by using sousCritereNoteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of SousCritereNote
	 *
	 */
	public default Long count(Request<SousCritereNoteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from SousCritereNote e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<SousCritereNoteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		SousCritereNoteDto dto = request.getData() != null ? request.getData() : new SousCritereNoteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (SousCritereNoteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(SousCritereNoteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (dto.getCoefficient()!= null && dto.getCoefficient() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("coefficient", dto.getCoefficient(), "e.coefficient", "Integer", dto.getCoefficientParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCritereNoteId()!= null && dto.getCritereNoteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("critereNoteId", dto.getCritereNoteId(), "e.critereNote.id", "Integer", dto.getCritereNoteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCritereNoteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("critereNoteLibelle", dto.getCritereNoteLibelle(), "e.critereNote.libelle", "String", dto.getCritereNoteLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
