package ci.salaamsolutions.projet.spasuce.dao.repository.customize;

import java.io.Console;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffres;
import ci.salaamsolutions.projet.spasuce.dao.entity.SecteurDactivite;
import ci.salaamsolutions.projet.spasuce.dao.entity.SecteurDactiviteAppelDoffres;
import ci.salaamsolutions.projet.spasuce.helper.CriteriaUtils;
import ci.salaamsolutions.projet.spasuce.helper.Utilities;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.SearchParam;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteAppelDoffresDto;
import ci.salaamsolutions.projet.spasuce.helper.enums.OperatorEnum;

/**
 * Repository customize : SecteurDactiviteAppelDoffres.
 */
@Repository
public interface _SecteurDactiviteAppelDoffresRepository {
	default List<String> _generateCriteria(SecteurDactiviteAppelDoffresDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE


		// des champs des appels d'offres

		if (Utilities.notBlank(dto.getTypeAppelDoffresCode())) {
			listOfQuery.add(CriteriaUtils.generateCriteria("typeAppelDoffresCode", dto.getTypeAppelDoffresCode(), "e.appelDoffres.typeAppelDoffres.code", "String", dto.getTypeAppelDoffresCodeParam(), param, index, locale));
		}
		if (Utilities.notBlank(dto.getEtatCode())) {
			listOfQuery.add(CriteriaUtils.generateCriteria("etatCode", dto.getEtatCode(), "e.appelDoffres.etat.code", "String", dto.getEtatCodeParam(), param, index, locale));
		}
		if (dto.getIsLocked()!= null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isLocked", dto.getIsLocked(), "e.appelDoffres.isLocked", "Boolean", dto.getIsLockedParam(), param, index, locale));
		}
		if (dto.getIsAttribuer()!= null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isAttribuer", dto.getIsAttribuer(), "e.appelDoffres.isAttribuer", "Boolean", dto.getIsAttribuerParam(), param, index, locale));
		}
		if (dto.getIsModifiable()!= null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isModifiable", dto.getIsModifiable(), "e.appelDoffres.isModifiable", "Boolean", dto.getIsModifiableParam(), param, index, locale));
		}
		if (dto.getIsRetirer()!= null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isRetirer", dto.getIsRetirer(), "e.appelDoffres.isRetirer", "Boolean", dto.getIsRetirerParam(), param, index, locale));
		}
		if (dto.getIsVisibleByClient()!= null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isVisibleByClient", dto.getIsVisibleByClient(), "e.appelDoffres.isVisibleByClient", "Boolean", dto.getIsVisibleByClientParam(), param, index, locale));
		}
		if (dto.getIsUpdate()!= null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isUpdate", dto.getIsUpdate(), "e.appelDoffres.isUpdate", "Boolean", dto.getIsUpdateParam(), param, index, locale));
		}

		if (Utilities.notBlank(dto.getDateFin()) && Utilities.notBlank(dto.getDateDebut())) {

			SearchParam<String>   createdAtAppelDoffresParam = new SearchParam<String>() ;
			createdAtAppelDoffresParam.setOperator(OperatorEnum.BETWEEN);
			createdAtAppelDoffresParam.setStart(dto.getDateDebut());
			createdAtAppelDoffresParam.setEnd(dto.getDateFin());
			dto.setCreatedAtAppelDoffresParam(createdAtAppelDoffresParam);
			//dto.setCreatedAt(dto.getDateDebut()) ;
			listOfQuery.add(CriteriaUtils.generateCriteria("dateCreation", dto.getCreatedAt(), "e.appelDoffres.dateCreation", "Date", dto.getCreatedAtAppelDoffresParam(), param, index, locale));
		}
		// pour ne pas renvoyer les appels d'offres deja supprimés
		listOfQuery.add(CriteriaUtils.generateCriteria("isDeletedAppelDoffres", dto.getIsDeleted(), "e.appelDoffres.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));

		// CriteriaUtils.generateCriteria("isDeletedAppelDoffres", dto.getIsDeleted(), "e.appelDoffres.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale);

		return listOfQuery;
	}


	/**
	 * Finds SecteurDactiviteAppelDoffres by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object SecteurDactiviteAppelDoffres whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no SecteurDactiviteAppelDoffres is found, this method returns null.
	 */
	@Query("select e.secteurDactivite from SecteurDactiviteAppelDoffres e where e.appelDoffres.id = :appelDoffresId and e.isDeleted = :isDeleted")
	List<SecteurDactivite> findSecteurDactivitesByAppelDoffresId(@Param("appelDoffresId")Integer appelDoffresId, @Param("isDeleted")Boolean isDeleted);



	/**
	 * Finds count of SecteurDactiviteAppelDoffres by using secteurDactiviteAppelDoffresDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of SecteurDactiviteAppelDoffres
	 *
	 */
	public default Long countCustom(Request<SecteurDactiviteAppelDoffresDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(DISTINCT e.appelDoffres.id) from SecteurDactiviteAppelDoffres e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		//req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}


	/**
	 * Finds List of SecteurDactiviteAppelDoffres by using secteurDactiviteAppelDoffresDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of SecteurDactiviteAppelDoffres
	 * @throws DataAccessException,ParseException
	 */
	public default List<AppelDoffres> getByCriteriaCustom(Request<SecteurDactiviteAppelDoffresDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select DISTINCT e.appelDoffres from SecteurDactiviteAppelDoffres e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		//req += " order by e.appelDoffres.id desc";
		TypedQuery<AppelDoffres> query = em.createQuery(req, AppelDoffres.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<SecteurDactiviteAppelDoffresDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		SecteurDactiviteAppelDoffresDto dto = request.getData() != null ? request.getData() : new SecteurDactiviteAppelDoffresDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (SecteurDactiviteAppelDoffresDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(SecteurDactiviteAppelDoffresDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getSecteurDactiviteId()!= null && dto.getSecteurDactiviteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("secteurDactiviteId", dto.getSecteurDactiviteId(), "e.secteurDactivite.id", "Integer", dto.getSecteurDactiviteIdParam(), param, index, locale));
			}
			if (dto.getAppelDoffresId()!= null && dto.getAppelDoffresId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("appelDoffresId", dto.getAppelDoffresId(), "e.appelDoffres.id", "Integer", dto.getAppelDoffresIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSecteurDactiviteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("secteurDactiviteCode", dto.getSecteurDactiviteCode(), "e.secteurDactivite.code", "String", dto.getSecteurDactiviteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSecteurDactiviteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("secteurDactiviteLibelle", dto.getSecteurDactiviteLibelle(), "e.secteurDactivite.libelle", "String", dto.getSecteurDactiviteLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getAppelDoffresLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("appelDoffresLibelle", dto.getAppelDoffresLibelle(), "e.appelDoffres.libelle", "String", dto.getAppelDoffresLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
