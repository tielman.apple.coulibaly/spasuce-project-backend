


/*
 * Java transformer for entity table appel_doffres
 * Created on 2020-01-03 ( Time 08:37:03 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffres;
import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffresRestreint;
import ci.salaamsolutions.projet.spasuce.dao.entity.Entreprise;
import ci.salaamsolutions.projet.spasuce.dao.entity.Etat;
import ci.salaamsolutions.projet.spasuce.dao.entity.Fichier;
import ci.salaamsolutions.projet.spasuce.dao.entity.Offre;
import ci.salaamsolutions.projet.spasuce.dao.entity.SecteurDactivite;
import ci.salaamsolutions.projet.spasuce.dao.entity.SecteurDactiviteAppelDoffres;
import ci.salaamsolutions.projet.spasuce.dao.entity.TypeAppelDoffres;
import ci.salaamsolutions.projet.spasuce.dao.entity.TypeDuree;
import ci.salaamsolutions.projet.spasuce.dao.entity.User;
import ci.salaamsolutions.projet.spasuce.dao.entity.ValeurCritereAppelDoffres;
import ci.salaamsolutions.projet.spasuce.dao.entity.Ville;
import ci.salaamsolutions.projet.spasuce.dao.repository.AppelDoffresRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.AppelDoffresRestreintRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.EtatRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.FichierRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.OffreRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.SecteurDactiviteAppelDoffresRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.SecteurDactiviteEntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.TypeAppelDoffresRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.TypeDureeRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.UserRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.ValeurCritereAppelDoffresRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.VilleRepository;
import ci.salaamsolutions.projet.spasuce.helper.ExceptionUtils;
import ci.salaamsolutions.projet.spasuce.helper.FunctionalError;
import ci.salaamsolutions.projet.spasuce.helper.ParamsUtils;
import ci.salaamsolutions.projet.spasuce.helper.TechnicalError;
import ci.salaamsolutions.projet.spasuce.helper.Utilities;
import ci.salaamsolutions.projet.spasuce.helper.Validate;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.contract.SearchParam;
import ci.salaamsolutions.projet.spasuce.helper.dto.AppelDoffresDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.AppelDoffresRestreintDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.EntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.FichierDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.OffreDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteAppelDoffresDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteEntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.UserDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.ValeurCritereAppelDoffresDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.AppelDoffresRestreintTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.AppelDoffresTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.EntrepriseTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.FichierTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.OffreTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.SecteurDactiviteAppelDoffresTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.ValeurCritereAppelDoffresTransformer;
import ci.salaamsolutions.projet.spasuce.helper.enums.EtatEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.GlobalEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeAppelDoffresEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeUserEnum;

/**
BUSINESS for table "appel_doffres"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class AppelDoffresBusiness implements IBasicBusiness<Request<AppelDoffresDto>, Response<AppelDoffresDto>> {

	private Response<AppelDoffresDto> response;
	@Autowired
	private AppelDoffresRepository appelDoffresRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private VilleRepository villeRepository;
	@Autowired
	private ValeurCritereAppelDoffresRepository valeurCritereAppelDoffresRepository;
	@Autowired
	private EtatRepository etatRepository;
	@Autowired
	private TypeAppelDoffresRepository typeAppelDoffresRepository;
	@Autowired
	private TypeDureeRepository typeDureeRepository;
	@Autowired
	private FichierRepository fichierRepository;
	@Autowired
	private SecteurDactiviteAppelDoffresRepository secteurDactiviteAppelDoffresRepository;
	@Autowired
	private OffreRepository offreRepository;
	@Autowired
	private AppelDoffresRestreintRepository appelDoffresRestreintRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private ValeurCritereAppelDoffresBusiness valeurCritereAppelDoffresBusiness;


	@Autowired
	private FichierBusiness fichierBusiness;


	@Autowired
	private SecteurDactiviteAppelDoffresBusiness secteurDactiviteAppelDoffresBusiness;


	@Autowired
	private OffreBusiness offreBusiness;


	@Autowired
	private AppelDoffresRestreintBusiness appelDoffresRestreintBusiness;
	
	@Autowired
	private  SecteurDactiviteEntrepriseRepository secteurDactiviteEntrepriseRepository ;
	
	@Autowired
	private SecteurDactiviteEntrepriseBusiness secteurDactiviteEntrepriseBusiness ;
	
	
	@Autowired
	private ParamsUtils paramsUtils;



	public AppelDoffresBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create AppelDoffres by using AppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<AppelDoffresDto> create(Request<AppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin create AppelDoffres-----");

		response = new Response<AppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<AppelDoffres> items = new ArrayList<AppelDoffres>();
			
			List<ValeurCritereAppelDoffresDto> datasValeurCritereAppelDoffres = new ArrayList<>();
			List<SecteurDactiviteAppelDoffresDto> datasSecteurDactiviteAppelDoffres = new ArrayList<>();
			List<FichierDto> datasFichier = new ArrayList<>();
			List<AppelDoffresRestreintDto> datasAppelDoffresRestreint = new ArrayList<>();

			for (AppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				//fieldsToVerify.put("description", dto.getDescription());
				//fieldsToVerify.put("dateLimiteDepot", dto.getDateLimiteDepot());
				//fieldsToVerify.put("dateCreation", dto.getDateCreation());
				//fieldsToVerify.put("dureRealisation", dto.getDureRealisation());
				//fieldsToVerify.put("isLocked", dto.getIsLocked());
				fieldsToVerify.put("villeId", dto.getVilleId());
				//fieldsToVerify.put("typeAppelDoffresId", dto.getTypeAppelDoffresId());
				fieldsToVerify.put("typeAppelDoffresLibelle", dto.getTypeAppelDoffresLibelle());
				fieldsToVerify.put("userClientId", dto.getUserClientId());
				//fieldsToVerify.put("etatId", dto.getEtatId());
				fieldsToVerify.put("etatCode", dto.getEtatCode());
				
				//
				fieldsToVerify.put("datasValeurCritereAppelDoffres", dto.getDatasValeurCritereAppelDoffres());
				fieldsToVerify.put("datasSecteurDactiviteAppelDoffres", dto.getDatasSecteurDactiviteAppelDoffres());
				fieldsToVerify.put("datasFichier", dto.getDatasFichier());
				
				
				//fieldsToVerify.put("datasOffre", dto.getDatasOffre());
				
				//fieldsToVerify.put("datasUser", dto.getDatasUser());
				// fieldsToVerify.put("datasAppelDoffresRestreint", dto.getDatasAppelDoffresRestreint());


				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}
				
				

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */
				

				// Verify if appelDoffres to insert do not exist
				AppelDoffres existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("appelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = appelDoffresRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("appelDoffres -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les appelDoffress", locale));
					response.setHasError(true);
					return response;
				}

				// Verify if ville exist
				Ville existingVille = villeRepository.findById(dto.getVilleId(), false);
				if (existingVille == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ville -> " + dto.getVilleId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if etat exist
				//Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
				Etat existingEtat = etatRepository.findByCode(dto.getEtatCode(), false);
				if (existingEtat == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatCode(), locale));
					response.setHasError(true);
					return response;
				}
				
				// Verify if user exist
				User existingUser = userRepository.findById(dto.getUserClientId(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserClientId(), locale));
					response.setHasError(true);
					return response;
				}
				
				// Verify if typeAppelDoffres exist
				//TypeAppelDoffres existingTypeAppelDoffres = typeAppelDoffresRepository.findById(dto.getTypeAppelDoffresId(), false);
				TypeAppelDoffres existingTypeAppelDoffres = typeAppelDoffresRepository.findByLibelle(dto.getTypeAppelDoffresLibelle(), false);
				if (existingTypeAppelDoffres == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeAppelDoffres -> " + dto.getTypeAppelDoffresLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				
				// Verify if typeAppelDoffres exist
				TypeDuree existingTypeDuree = null ;
				
				if (dto.getTypeDureeId() != null && dto.getTypeDureeId() > 0){
					existingTypeDuree = typeDureeRepository.findById(dto.getTypeDureeId(), false);
					if (existingTypeDuree == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("typeDuree -> " + dto.getTypeDureeId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				
				dto.setIsContenuNotificationDispo(false);
				dto.setIsLocked(false);
				dto.setIsModifiable(true);
				dto.setIsRetirer(false);
				dto.setIsUpdate(false);
				dto.setIsAttribuer(false);
				dto.setIsVisibleByClient(true);
				
				AppelDoffres entityToSave = null;
				entityToSave = AppelDoffresTransformer.INSTANCE.toEntity(dto, existingVille, existingEtat, existingTypeDuree, existingTypeAppelDoffres, existingUser);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				if (existingEtat.getCode().equals(EtatEnum.ENTRANT)) {
					// generation du code du contrat
					
					
					//List<AppelDoffres> appelDoffresSysteme = appelDoffresRepository.findByEtatCode(EtatEnum.ENTRANT, false);
					List<AppelDoffres> appelDoffresSysteme = appelDoffresRepository.findByEtatCodeAll(EtatEnum.ENTRANT);
					
					Integer nbre = (Utilities.isNotEmpty(appelDoffresSysteme) ? appelDoffresSysteme.size() : 0);
					
					
					String code = Utilities.getCodeByCritreria(GlobalEnum.code_ao, nbre) ;
					
					
					slf4jLogger.info("********code*********", code);
					
					entityToSave.setCode(code);
					entityToSave.setDateCreation(Utilities.getCurrentDate());
				}
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
				
				
				if (existingTypeAppelDoffres.getCode().equals(TypeAppelDoffresEnum.RESTREINT)) {
					Map<String, Object> fieldsToVerifyUsers = new HashMap<String, Object>();
					fieldsToVerifyUsers.put("datasEntreprise", dto.getDatasEntreprise());
					//fieldsToVerifyUsers.put("datasAppelDoffresRestreint", dto.getDatasAppelDoffresRestreint());
					if (!Validate.RequiredValue(fieldsToVerifyUsers).isGood()) {
						response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
						response.setHasError(true);
						return response;
					}
					
					List<User> users =  bienFormerDatasUser(dto.getDatasEntreprise()) ;

					// faire les traitement concernant cette partie
					for (User user :users) {
						
						AppelDoffresRestreintDto data = new AppelDoffresRestreintDto();
						//AppelDoffresRestreintDto data2 = new AppelDoffresRestreintDto();
						
						//data2.setIsAppelRestreint(false); // pour dire que l'appel est restreint
						//data2.setUserFournisseurId(userDto.getId());
						//data2.setEntityAppelDoffres(entityToSave);
						
						data.setIsAppelRestreint(true); // pour dire que l'appel est restreint
						data.setUserFournisseurId(user.getId());
						data.setEntityAppelDoffres(entityToSave);
						
						//datasAppelDoffresRestreint.add(data2);
						datasAppelDoffresRestreint.add(data);
						
					}
				}
				
				for (SecteurDactiviteAppelDoffresDto data : dto.getDatasSecteurDactiviteAppelDoffres()) {
					data.setEntityAppelDoffres(entityToSave);
				}
				datasSecteurDactiviteAppelDoffres.addAll(dto.getDatasSecteurDactiviteAppelDoffres());
				
				for (FichierDto data : dto.getDatasFichier()) {
					data.setEntityAppelDoffres(entityToSave);
					//data.setIsFichierOffre(true);
				}
				datasFichier.addAll(dto.getDatasFichier());
				
				for (ValeurCritereAppelDoffresDto data : dto.getDatasValeurCritereAppelDoffres()) {
					data.setEntityAppelDoffres(entityToSave);
				}
				datasValeurCritereAppelDoffres.addAll(dto.getDatasValeurCritereAppelDoffres());
			}

			if (!items.isEmpty()) {
				List<AppelDoffres> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = appelDoffresRepository.save((Iterable<AppelDoffres>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("appelDoffres", locale));
					response.setHasError(true);
					return response;
				}
				
				// appels des services de creation
				
				// creation des SecteurDactiviteAppelDoffres
				for (SecteurDactiviteAppelDoffresDto data : datasSecteurDactiviteAppelDoffres) {
					data.setAppelDoffresId(data.getEntityAppelDoffres().getId());
				}
				Request<SecteurDactiviteAppelDoffresDto> reqSecteurDactiviteAppelDoffres = new Request<>();
				Response<SecteurDactiviteAppelDoffresDto> resSecteurDactiviteAppelDoffres = new Response<>();
				reqSecteurDactiviteAppelDoffres.setUser(request.getUser());
				reqSecteurDactiviteAppelDoffres.setDatas(datasSecteurDactiviteAppelDoffres);

				resSecteurDactiviteAppelDoffres = secteurDactiviteAppelDoffresBusiness.create(reqSecteurDactiviteAppelDoffres, locale);

				if (resSecteurDactiviteAppelDoffres != null && resSecteurDactiviteAppelDoffres.isHasError()) {
					response.setStatus(resSecteurDactiviteAppelDoffres.getStatus());
					response.setHasError(true);
					return response;
				}

				// creation des ValeurCritereAppelDoffres
				for (ValeurCritereAppelDoffresDto data : datasValeurCritereAppelDoffres) {
					data.setAppelDoffresId(data.getEntityAppelDoffres().getId());
				}
				Request<ValeurCritereAppelDoffresDto> reqValeurCritereAppelDoffres = new Request<ValeurCritereAppelDoffresDto>();
				Response<ValeurCritereAppelDoffresDto> resValeurCritereAppelDoffres = new Response<>();
				reqValeurCritereAppelDoffres.setUser(request.getUser());
				reqValeurCritereAppelDoffres.setDatas(datasValeurCritereAppelDoffres);

				resValeurCritereAppelDoffres = valeurCritereAppelDoffresBusiness.create(reqValeurCritereAppelDoffres, locale);

				if (resValeurCritereAppelDoffres != null && resValeurCritereAppelDoffres.isHasError()) {
					response.setStatus(resValeurCritereAppelDoffres.getStatus());
					response.setHasError(true);
					return response;
				}

				// creation des AppelDoffresRestreint
				if (!datasAppelDoffresRestreint.isEmpty()) {
					for (AppelDoffresRestreintDto data : datasAppelDoffresRestreint) {
						data.setAppelDoffresId(data.getEntityAppelDoffres().getId());
					}
					Request<AppelDoffresRestreintDto> req = new Request<>();
					Response<AppelDoffresRestreintDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasAppelDoffresRestreint);
					
					res = appelDoffresRestreintBusiness.create(req, locale);
					
					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				
				// creation des Fichier 
				for (FichierDto data : datasFichier) {
					//data.setAppelDoffresId(data.getEntityAppelDoffres().getId());
					data.setSourceId(data.getEntityAppelDoffres().getId());
					data.setSource(GlobalEnum.fichiers_ao);
				}
				
				Request<FichierDto> reqFichier = new Request<>();
				Response<FichierDto> resFichier = new Response<>();
				reqFichier.setUser(request.getUser());
				reqFichier.setDatas(datasFichier);

				resFichier = fichierBusiness.create(reqFichier, locale);

				if (resFichier != null && resFichier.isHasError()) {
					response.setStatus(resFichier.getStatus());
					response.setHasError(true);
					return response;
				}
				
				
				
				//***************************************** Associé l'appelDoffres aux AORestreint des fournisseurs du meme secteurs d'activites *************************************************//
				
				// DEBUT 
				List<AppelDoffresRestreint> itemsAppelDoffresRestreint = new ArrayList<>();
				if (Utilities.isNotEmpty(itemsSaved)) {
					for (AppelDoffres appelDoffres : itemsSaved) {

						// MAJ de la table appelDoffresRestreint apres soumission d'un appelDoffres				
						if (appelDoffres.getTypeAppelDoffres().getCode().equals(TypeAppelDoffresEnum.GENERAL) && appelDoffres.getEtat().getCode().equals(EtatEnum.ENTRANT)) {

							Integer appelDoffresId = appelDoffres.getId();
							// listes des secteurs d'activites de l'appelDoffres
							List<SecteurDactivite> secteurDactivitesAO = secteurDactiviteAppelDoffresRepository.findSecteurDactivitesByAppelDoffresId(appelDoffresId, false) ;

							Request<SecteurDactiviteEntrepriseDto> req = new Request<>();
							Response<SecteurDactiviteEntrepriseDto> res = new Response<>();
							List<SecteurDactiviteEntrepriseDto> datas = new ArrayList<>();
							//List<SecteurDactiviteEntreprise> itemsSecteurDactiviteEntreprise = new ArrayList<>();
							List<Entreprise> itemsEntreprise = new ArrayList<>();

							if (Utilities.isNotEmpty(secteurDactivitesAO)) {

								//construire la body de la req pour recuperer les entreprises fournisseurs et mixte du meme secteurs d'activites
								for (SecteurDactivite entity : secteurDactivitesAO) {
									SecteurDactiviteEntrepriseDto data = new SecteurDactiviteEntrepriseDto();

									data.setSecteurDactiviteId(entity.getId());
									data.setTypeEntrepriseCode(TypeUserEnum.ENTREPRISE_CLIENTE);
									SearchParam<String> typeEntrepriseCodeParam = new SearchParam<String>();
									typeEntrepriseCodeParam.setOperator("<>");
									data.setTypeEntrepriseCodeParam(typeEntrepriseCodeParam);

									datas.add(data);
								}
								req.setData(datas.get(0));
								datas.remove(0); // important pour eviter des recupereantion en doublon
								req.setDatas(datas);

								itemsEntreprise = secteurDactiviteEntrepriseRepository.getByCriteriaCustom(req, em, locale); 

								if (Utilities.isNotEmpty(itemsEntreprise)) {

									// recuperation des users de chaque entreprises
									List<User> itemsUsersEntreprise =  new ArrayList<>() ;

									for (Entreprise entity : itemsEntreprise) {
										List<User> usersEntreprise = userRepository.findByEntrepriseId(entity.getId(), false);

										if (Utilities.isNotEmpty(usersEntreprise)) {
											itemsUsersEntreprise.addAll(usersEntreprise);
										}
									}

									if (Utilities.isNotEmpty(itemsUsersEntreprise)) {
										for (User entity : itemsUsersEntreprise) {
											AppelDoffresRestreint appelDoffresRestreint = new AppelDoffresRestreint();

											appelDoffresRestreint.setAppelDoffres(appelDoffres);
											appelDoffresRestreint.setUser(entity);
											appelDoffresRestreint.setIsAppelRestreint(false); // pour dire que se sont des elements pour visualtisations
											appelDoffresRestreint.setIsSoumissionnerByFournisseur(false);
											appelDoffresRestreint.setIsDesactiver(false);
											appelDoffresRestreint.setIsVisualiserContenuByFournisseur(false);
											appelDoffresRestreint.setIsVisualiserNotifByFournisseur(false);
											//appelDoffresRestreint.setIsUpdateAppelDoffres(data.getIsUpdate());
											appelDoffresRestreint.setIsUpdateAppelDoffres(false);
											appelDoffresRestreint.setIsVisibleByFournisseur(true); // car il ne l'a pas encore supprimer logiquement
											appelDoffresRestreint.setCreatedAt(Utilities.getCurrentDate());
											appelDoffresRestreint.setCreatedBy(request.getUser());
											appelDoffresRestreint.setIsSelectedByClient(false);
											appelDoffresRestreint.setIsDeleted(false);
											itemsAppelDoffresRestreint.add(appelDoffresRestreint);
										}
									}
								}
							}
						}
					}
				}

				
				slf4jLogger.info("----$$$$$$$$itemsAppelDoffresRestreint$$$$$$$$$$-----" + itemsAppelDoffresRestreint);

				if (Utilities.isNotEmpty(itemsAppelDoffresRestreint)) {
					slf4jLogger.info("----$$$$$$$$itemsAppelDoffresRestreint$$$$$$$$$$----- size :::" + itemsAppelDoffresRestreint.size());

					 appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) itemsAppelDoffresRestreint);
				}
				
				// FIN 

				
				List<AppelDoffresDto> itemsDto = new ArrayList<AppelDoffresDto>();
				for (AppelDoffres entity : itemsSaved) {
					AppelDoffresDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create AppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				// penser a supprimer les fichiers en cas d'erreurs
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update AppelDoffres by using AppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<AppelDoffresDto> update(Request<AppelDoffresDto> request, Locale locale) {
		slf4jLogger.info("----begin update AppelDoffres-----");

		response = new Response<AppelDoffresDto>();
		List<String> listOfOldFiles = new 	ArrayList<>();
		List<String> listOfFilesCreate = new 	ArrayList<>();


		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<AppelDoffres> items = new ArrayList<AppelDoffres>();
			List<AppelDoffresRestreint> itemsAppelDoffresRestreintToSave = new ArrayList<>();
			
			List<ValeurCritereAppelDoffres> itemsValeurCritereAppelDoffresToDelete = new ArrayList<>();
			List<SecteurDactiviteAppelDoffres> itemsSecteurDactiviteAppelDoffresToDelete = new ArrayList<>();
			List<Fichier> itemsFichierToDelete = new ArrayList<>();
			List<AppelDoffresRestreint> itemsAppelDoffresRestreintToDelete = new ArrayList<>();
			
			//List<AppelDoffresRestreint> itemsAppelDoffresRestreint = new ArrayList<>();

			List<ValeurCritereAppelDoffresDto> datasValeurCritereAppelDoffres = new ArrayList<>();
			List<SecteurDactiviteAppelDoffresDto> datasSecteurDactiviteAppelDoffres = new ArrayList<>();
			List<FichierDto> datasFichier = new ArrayList<>();
			List<AppelDoffresRestreintDto> datasAppelDoffresRestreint = new ArrayList<>();


			Map<String, Map<Integer, String>> fieldsToVerifyEtatCode = new HashMap<String, Map<Integer, String>>();

			
			for (AppelDoffresDto dto : request.getDatas()) {
				
				
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la appelDoffres existe
				AppelDoffres entityToSave = null;
				entityToSave = appelDoffresRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				
				

				Integer entityToSaveId = entityToSave.getId();
				String ancienEtatCode = entityToSave.getEtat().getCode() ;
				String nouveauEtatCode = "";
				

				// Verify if ville exist
				if (dto.getVilleId() != null && dto.getVilleId() > 0){
					Ville existingVille = villeRepository.findById(dto.getVilleId(), false);
					if (existingVille == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("ville -> " + dto.getVilleId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setVille(existingVille);
				}
				
				// l'appel d'offres n'est pas modifiable car des offres ont ete deja soumises
				if (!entityToSave.getIsModifiable()) {
					response.setStatus(functionalError.DISALLOWED_OPERATION("appelDoffres -> " + dto.getLibelle() + " n'est pas modifiable", locale));
					response.setHasError(true);
					return response;
				}
				
				// faire les updates pour ce qui ont deja consulté l'appel en cas de modifications
				if (entityToSave.getEtat().getCode().equals(EtatEnum.ENTRANT)) {
					entityToSave.setIsUpdate(true); // pour montrer que l'off'e a ete MAJ
					List<AppelDoffresRestreint> appelDoffresRestreints = appelDoffresRestreintRepository.findByAppelDoffresIdAndIsVisualiserContenuByFournisseur(entityToSaveId, true, false);
					
					if (Utilities.isNotEmpty(appelDoffresRestreints)) {
						for (AppelDoffresRestreint data : appelDoffresRestreints) {
							data.setIsUpdateAppelDoffres(true); // 
							data.setIsVisualiserContenuByFournisseur(false);
							data.setIsVisualiserNotifByFournisseur(false);
						}
						// TODO : on peut faire le save ici mais à revoir ce qui sera mieux
						itemsAppelDoffresRestreintToSave.addAll(appelDoffresRestreints);
					}
				}
				
				
				// Verify if etat exist
				//if (dto.getEtatId() != null && dto.getEtatId() > 0){
				if (Utilities.notBlank(dto.getEtatCode())) {
					//Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
					Etat existingEtat = etatRepository.findByCode(dto.getEtatCode(), false);
					if (existingEtat == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (!entityToSave.getEtat().getCode().equals(existingEtat.getCode()) 
							&& existingEtat.getCode().equals(EtatEnum.BROUILLON)) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("On ne peut pas passer de l'etat -> " + entityToSave.getEtat().getCode() + " à l'etat -> "+dto.getEtatCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (existingEtat.getCode().equals(EtatEnum.ENTRANT)
							&& entityToSave.getEtat().getCode().equals(EtatEnum.BROUILLON)) {
						
						
						//List<AppelDoffres> appelDoffresSysteme = appelDoffresRepository.findByEtatCode(EtatEnum.ENTRANT, false);
						List<AppelDoffres> appelDoffresSysteme = appelDoffresRepository.findByEtatCodeAll(EtatEnum.ENTRANT);
						
						Integer nbre = (Utilities.isNotEmpty(appelDoffresSysteme) ? appelDoffresSysteme.size() : 0);

						String code = Utilities.getCodeByCritreria(GlobalEnum.code_ao, nbre) ;

						slf4jLogger.info("********code*********", code);
						
						entityToSave.setCode(code);
						entityToSave.setDateCreation(Utilities.getCurrentDate());
					}
					nouveauEtatCode = existingEtat.getCode() ;
					entityToSave.setEtat(existingEtat);
				}
				Map<Integer, String> mapIdCode = new HashMap<Integer, String>();
				mapIdCode.put(entityToSaveId, nouveauEtatCode) ; 
				fieldsToVerifyEtatCode.put(ancienEtatCode, mapIdCode);
				
				
				// Verify if typeAppelDoffres exist
				if (dto.getTypeAppelDoffresId() != null && dto.getTypeAppelDoffresId() > 0){
					TypeAppelDoffres existingTypeAppelDoffres = typeAppelDoffresRepository.findById(dto.getTypeAppelDoffresId(), false);
					if (existingTypeAppelDoffres == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("typeAppelDoffres -> " + dto.getTypeAppelDoffresId(), locale));
						response.setHasError(true);
						return response;
					}
					// s'assurer du changement de type
					if (!existingTypeAppelDoffres.getCode().equals(entityToSave.getTypeAppelDoffres().getCode()) 
							&& existingTypeAppelDoffres.getCode().equals(TypeAppelDoffresEnum.RESTREINT) ) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("Impossible de passer d'un appel d'offres général à un appel restreint !!! ",  locale));
						response.setHasError(true);
						return response;
						
					}
					entityToSave.setTypeAppelDoffres(existingTypeAppelDoffres);
				}
				// Verify if user exist
				if (dto.getUserClientId() != null && dto.getUserClientId() > 0){
					User existingUser = userRepository.findById(dto.getUserClientId(), false);
					if (existingUser == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserClientId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUser(existingUser);
				}
				if (dto.getTypeDureeId() != null && dto.getTypeDureeId() > 0){
					TypeDuree existingTypeDuree = typeDureeRepository.findById(dto.getTypeDureeId(), false);
					if (existingTypeDuree == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("typeDuree -> " + dto.getTypeDureeId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				if (Utilities.notBlank(dto.getLibelle())) {
					AppelDoffres existingEntity = appelDoffresRepository.findByLibelle(dto.getLibelle(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("appelDoffres -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les appelDoffress", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
				if (Utilities.notBlank(dto.getDescription())) {
					entityToSave.setDescription(dto.getDescription());
				}
				
				if (dto.getPrixPrestation() != null && dto.getPrixPrestation() > 0){
					entityToSave.setPrixPrestation(dto.getPrixPrestation());
				}
				if (dto.getNombreTypeDuree() != null && dto.getNombreTypeDuree() > 0){
					entityToSave.setNombreTypeDuree(dto.getNombreTypeDuree());
				}
				if (Utilities.notBlank(dto.getDateLimiteDepot())) {
					entityToSave.setDateLimiteDepot(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateLimiteDepot()));
				}
//				if (Utilities.notBlank(dto.getDateCreation())) {
//					entityToSave.setDateCreation(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateCreation()));
//				}
//				if (Utilities.notBlank(dto.getDureRealisation())) {
//					entityToSave.setDureRealisation(dto.getDureRealisation());
//				}
				if (dto.getIsContenuNotificationDispo() != null) {
					entityToSave.setIsContenuNotificationDispo(dto.getIsContenuNotificationDispo());
				}
				if (dto.getIsLocked() != null) {
					entityToSave.setIsLocked(dto.getIsLocked());
				}
				if (dto.getIsAttribuer() != null) {
					entityToSave.setIsLocked(dto.getIsLocked());
				}
				if (dto.getIsModifiable() != null) {
					entityToSave.setIsAttribuer(dto.getIsAttribuer());
				}
				if (dto.getIsVisibleByClient() != null) {
					entityToSave.setIsVisibleByClient(dto.getIsVisibleByClient());
				}
				
//				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
//					entityToSave.setCreatedBy(dto.getCreatedBy());
//				}
//				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
//					entityToSave.setUpdatedBy(dto.getUpdatedBy());
//				}
//				if (Utilities.notBlank(dto.getDeletedAt())) {
//					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
//				}
//				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
//					entityToSave.setDeletedBy(dto.getDeletedBy());
//				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
				
				
				//ajouter des users pour le cas restreint
				if (entityToSave.getTypeAppelDoffres().getCode().equals(TypeAppelDoffresEnum.RESTREINT) && Utilities.isNotEmpty(dto.getDatasEntreprise())) {
									
					List<User> users =  bienFormerDatasUser(dto.getDatasEntreprise()) ;
					
					if (Utilities.isNotEmpty(users)) {
						List<AppelDoffresRestreint> appelDoffresRestreints = appelDoffresRestreintRepository.findByAppelDoffresId(entityToSaveId, false);
						List<AppelDoffresRestreint> appelDoffresRestreintsRestant = new ArrayList<>();
						if (Utilities.isNotEmpty(appelDoffresRestreints)) {
							for (AppelDoffresRestreint data : appelDoffresRestreints) {
								if (!users.stream().anyMatch(a->a.getId().equals(data.getUser().getId()))) {
									
									if (data.getIsSoumissionnerByFournisseur() != null && data.getIsSoumissionnerByFournisseur()) {
										response.setStatus(functionalError.DISALLOWED_OPERATION("Impossible de supprimer ce fournisseur de car il a déjà une offre poour cet appel d'offre !!! ",  locale));
										response.setHasError(true);
										return response;
									}
									data.setIsDesactiver(true); // pour lui dire qu'on l'a retirer
									data.setIsDeleted(true);
									data.setDeletedBy(request.getUser());
									data.setDeletedAt(Utilities.getCurrentDate());
								}else {
									appelDoffresRestreintsRestant.add(data);
								}
							}
							// enregistrer les modifs
							List<AppelDoffresRestreint> appelDoffresRestreintsSaved = null;
							// maj les donnees en base
							appelDoffresRestreintsSaved = appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) appelDoffresRestreints);
							if (appelDoffresRestreintsSaved == null || appelDoffresRestreintsSaved.isEmpty()) {
								response.setStatus(functionalError.SAVE_FAIL("appelDoffresRestreint", locale));
								response.setHasError(true);
								return response;
							}
							//itemsAppelDoffresRestreint.addAll(appelDoffresRestreints);
						}
						
						// retirer les anciens et creer les nouveaux
						//if (!appelDoffresRestreintsRestant.isEmpty()) {
							for (User data : users) {
							//for (UserDto data : dto.getDatasUser()) {								
								
								if ( appelDoffresRestreintsRestant.isEmpty() || !appelDoffresRestreintsRestant.stream().anyMatch(a->a.getUser().getId().equals(data.getId()))) {
									AppelDoffresRestreintDto appelDoffresRestreintDto = new AppelDoffresRestreintDto() ;
									//AppelDoffresRestreintDto appelDoffresRestreintDto1 = new AppelDoffresRestreintDto() ;
									
									appelDoffresRestreintDto.setAppelDoffresId(entityToSaveId);
									appelDoffresRestreintDto.setUserFournisseurId(data.getId());
									appelDoffresRestreintDto.setIsAppelRestreint(true); // pour dire que l'appel est restreint

									//appelDoffresRestreintDto1.setAppelDoffresId(entityToSaveId);
									//appelDoffresRestreintDto1.setUserFournisseurId(data.getId());
									//appelDoffresRestreintDto1.setIsAppelRestreint(false); // pour dire que l'appel est restreint

									datasAppelDoffresRestreint.add(appelDoffresRestreintDto);
								}
							}
						//}
					}

					
					
				}
				
				if (Utilities.isNotEmpty(dto.getDatasSecteurDactiviteAppelDoffres())) {

					// recuperation des anciens pour suppression
					List<SecteurDactiviteAppelDoffres> secteurDactiviteAppelDoffres = secteurDactiviteAppelDoffresRepository.findByAppelDoffresId(entityToSaveId, false);
					if (Utilities.isNotEmpty(secteurDactiviteAppelDoffres)) {
						itemsSecteurDactiviteAppelDoffresToDelete.addAll(secteurDactiviteAppelDoffres);
					}
					for (SecteurDactiviteAppelDoffresDto data : dto.getDatasSecteurDactiviteAppelDoffres()) {
						data.setAppelDoffresId(entityToSaveId);
					}
					datasSecteurDactiviteAppelDoffres.addAll(dto.getDatasSecteurDactiviteAppelDoffres());
				}
				
				if (Utilities.isNotEmpty(dto.getDatasFichier())) {
					// recuperation des anciens pour suppression
					//List<Fichier> fichiers = fichierRepository.findByAppelDoffresId(entityToSaveId, false);
					List<Fichier> fichiers = fichierRepository.findBySourceIdAndSource(entityToSaveId, GlobalEnum.fichiers_ao, false);
					if (Utilities.isNotEmpty(fichiers)) {
						itemsFichierToDelete.addAll(fichiers);
					}
					
					for (FichierDto data : dto.getDatasFichier()) {
						
						data.setSourceId(entityToSaveId);
						data.setSource(GlobalEnum.fichiers_ao);
						//data.setAppelDoffresId(entityToSaveId);
						//data.setIsFichierOffre(true);
					}
					datasFichier.addAll(dto.getDatasFichier());
				}
				if (Utilities.isNotEmpty(dto.getDatasValeurCritereAppelDoffres())) {
					// recuperation des anciens pour suppression
					List<ValeurCritereAppelDoffres> valeurCritereAppelDoffres = valeurCritereAppelDoffresRepository.findByAppelDoffresId(entityToSaveId, false);
					if (Utilities.isNotEmpty(valeurCritereAppelDoffres)) {
						itemsValeurCritereAppelDoffresToDelete.addAll(valeurCritereAppelDoffres);
					}
					for (ValeurCritereAppelDoffresDto data : dto.getDatasValeurCritereAppelDoffres()) {
						data.setAppelDoffresId(entityToSaveId);
					}
					datasValeurCritereAppelDoffres.addAll(dto.getDatasValeurCritereAppelDoffres());
				}
				

				
				
			}

			if (!items.isEmpty()) {
				List<AppelDoffres> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = appelDoffresRepository.save((Iterable<AppelDoffres>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("appelDoffres", locale));
					response.setHasError(true);
					return response;
				}
				
//				if (!itemsAppelDoffresRestreintToSave.isEmpty()) {
//					List<AppelDoffresRestreint> itemsAppelDoffresRestreintToSaveSaved = null;
//					// maj les donnees en base
//					itemsAppelDoffresRestreintToSaveSaved = appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) itemsAppelDoffresRestreintToSave);
//					if (itemsAppelDoffresRestreintToSaveSaved == null || itemsAppelDoffresRestreintToSaveSaved.isEmpty()) {
//						response.setStatus(functionalError.SAVE_FAIL("appelDoffresRestreint", locale));
//						response.setHasError(true);
//						return response;
//					}
//				}
				
				
				// appels des services de creation

				
				// create datasAppelDoffresRestreint 
				if (!datasAppelDoffresRestreint.isEmpty()) {
					
					Request<AppelDoffresRestreintDto> req = new Request<>();
					Response<AppelDoffresRestreintDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasAppelDoffresRestreint);
					
					res = appelDoffresRestreintBusiness.create(req, locale);
					
					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				
				
				
				// creation des SecteurDactiviteAppelDoffres
				if (!itemsSecteurDactiviteAppelDoffresToDelete.isEmpty()) {
					
					for (SecteurDactiviteAppelDoffres data : itemsSecteurDactiviteAppelDoffresToDelete) {
						data.setDeletedAt(Utilities.getCurrentDate());
						data.setDeletedBy(request.getUser());
						data.setIsDeleted(true);
					}
					List<SecteurDactiviteAppelDoffres> itemsToDelete = null;
					// maj les donnees en base
					itemsToDelete = secteurDactiviteAppelDoffresRepository.save((Iterable<SecteurDactiviteAppelDoffres>) itemsSecteurDactiviteAppelDoffresToDelete);
					if (itemsToDelete == null || itemsToDelete.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!datasSecteurDactiviteAppelDoffres.isEmpty()) {
					
					Request<SecteurDactiviteAppelDoffresDto> req = new Request<>();
					Response<SecteurDactiviteAppelDoffresDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasSecteurDactiviteAppelDoffres);

					res = secteurDactiviteAppelDoffresBusiness.create(req, locale);

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}

				}


				// creation des ValeurCritereAppelDoffres
				if (!itemsValeurCritereAppelDoffresToDelete.isEmpty()) {
					for (ValeurCritereAppelDoffres data : itemsValeurCritereAppelDoffresToDelete) {
						data.setDeletedAt(Utilities.getCurrentDate());
						data.setDeletedBy(request.getUser());
						data.setIsDeleted(true);
					}
					List<ValeurCritereAppelDoffres> itemsToDelete = null;
					// maj les donnees en base
					itemsToDelete = valeurCritereAppelDoffresRepository.save((Iterable<ValeurCritereAppelDoffres>) itemsValeurCritereAppelDoffresToDelete);
					if (itemsToDelete == null || itemsToDelete.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("valeurCritereAppelDoffres", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!datasValeurCritereAppelDoffres.isEmpty()) {
					
					Request<ValeurCritereAppelDoffresDto> req = new Request<ValeurCritereAppelDoffresDto>();
					Response<ValeurCritereAppelDoffresDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasValeurCritereAppelDoffres);

					res = valeurCritereAppelDoffresBusiness.create(req, locale);

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}

				}
				
				
				// creation des Fichier 
				if (!itemsFichierToDelete.isEmpty()) {
					for (Fichier data : itemsFichierToDelete) {
						data.setDeletedAt(Utilities.getCurrentDate());
						data.setDeletedBy(request.getUser());
						data.setIsDeleted(true);
						//listOfOldFiles.add(data.getName());
						String fileNameByExtension = Utilities.addExtensionInFileName( data.getName() , data.getExtension()) ;

						listOfOldFiles.add(fileNameByExtension);
					}
					List<Fichier> itemsToDelete = null;
					// maj les donnees en base
					itemsToDelete = fichierRepository.save((Iterable<Fichier>) itemsFichierToDelete);
					if (itemsToDelete == null || itemsToDelete.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!datasFichier.isEmpty()) {


					Request<FichierDto> req = new Request<>();
					Response<FichierDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasFichier);

					res = fichierBusiness.create(req, locale);

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				
				
				
				
				//***************************************** Associé l'appelDoffres aux AORestreint des fournisseurs du meme secteurs d'activites *************************************************//

				// DEBUT 
				
				if (fieldsToVerifyEtatCode.size() > 0) {
					
					List<AppelDoffresRestreint> itemsAppelDoffresRestreintToCreate = new ArrayList<>();
					List<AppelDoffresRestreint> appelDoffresRestreintToDelete = new ArrayList<>();

					fieldsToVerifyEtatCode.forEach((ancienCode, mapIdCode)->{
						mapIdCode.forEach((idEntity, nouveauCode)->{

							if ((Utilities.notBlank(nouveauCode) && ancienCode.equals(EtatEnum.BROUILLON) && nouveauCode.equals(EtatEnum.ENTRANT))
									|| (ancienCode.equals(EtatEnum.ENTRANT))) {

								AppelDoffres appelDoffres = appelDoffresRepository.findById(idEntity, false);

								if (appelDoffres != null ) {
									// MAJ de la table appelDoffresRestreint apres soumission d'un appelDoffres				
									if (appelDoffres.getTypeAppelDoffres().getCode().equals(TypeAppelDoffresEnum.GENERAL) && appelDoffres.getEtat().getCode().equals(EtatEnum.ENTRANT)) {

										Integer appelDoffresId = appelDoffres.getId();
										// listes des secteurs d'activites de l'appelDoffres
										List<SecteurDactivite> secteurDactivitesAO = secteurDactiviteAppelDoffresRepository.findSecteurDactivitesByAppelDoffresId(appelDoffresId, false) ;

										Request<SecteurDactiviteEntrepriseDto> req = new Request<>();
										Response<SecteurDactiviteEntrepriseDto> res = new Response<>();
										List<SecteurDactiviteEntrepriseDto> datas = new ArrayList<>();
										//List<SecteurDactiviteEntreprise> itemsSecteurDactiviteEntreprise = new ArrayList<>();
										List<Entreprise> itemsEntreprise = new ArrayList<>();

										if (Utilities.isNotEmpty(secteurDactivitesAO)) {

											//construire la body de la req pour recuperer les entreprises fournisseurs et mixte du meme secteurs d'activites
											for (SecteurDactivite entity : secteurDactivitesAO) {
												SecteurDactiviteEntrepriseDto data = new SecteurDactiviteEntrepriseDto();

												data.setSecteurDactiviteId(entity.getId());
												data.setTypeEntrepriseCode(TypeUserEnum.ENTREPRISE_CLIENTE);
												SearchParam<String> typeEntrepriseCodeParam = new SearchParam<String>();
												typeEntrepriseCodeParam.setOperator("<>");
												data.setTypeEntrepriseCodeParam(typeEntrepriseCodeParam);

												datas.add(data);
											}
											req.setData(datas.get(0));
											datas.remove(0); // important pour eviter des recupereantion en doublon
											req.setDatas(datas);

											try {
												itemsEntreprise = secteurDactiviteEntrepriseRepository.getByCriteriaCustom(req, em, locale);
											} catch (DataAccessException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											} catch (Exception e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											} 

											if (Utilities.isNotEmpty(itemsEntreprise)) {

												// recuperation des users de chaque entreprises
												List<User> itemsUsersEntreprise =  new ArrayList<>() ;

												for (Entreprise entity : itemsEntreprise) {
													List<User> usersEntreprise = userRepository.findByEntrepriseId(entity.getId(), false);

													if (Utilities.isNotEmpty(usersEntreprise)) {
														itemsUsersEntreprise.addAll(usersEntreprise);
													}
												}

												if (Utilities.isNotEmpty(itemsUsersEntreprise)) {
													
													for (User entity : itemsUsersEntreprise) {
														
														AppelDoffresRestreint appelDoffresRestreint = new AppelDoffresRestreint() ;;

														if (ancienCode.equals(EtatEnum.ENTRANT)) {
															
															AppelDoffresRestreint existingAppelDoffresRestreint = appelDoffresRestreintRepository.findAppelDoffresRestreintByUserFournisseurIdAndAppelDoffresId(entity.getId(), appelDoffresId, false);
															if (existingAppelDoffresRestreint != null ) { 
																
																appelDoffresRestreint.setAppelDoffres(appelDoffres);
																appelDoffresRestreint.setUser(entity);
																appelDoffresRestreint.setIsAppelRestreint(existingAppelDoffresRestreint.getIsAppelRestreint()); // pour dire que se sont des elements pour visualtisations
																appelDoffresRestreint.setIsSoumissionnerByFournisseur(existingAppelDoffresRestreint.getIsSoumissionnerByFournisseur());
																appelDoffresRestreint.setIsDesactiver(existingAppelDoffresRestreint.getIsDesactiver());
																appelDoffresRestreint.setIsVisualiserContenuByFournisseur(false);
																appelDoffresRestreint.setIsVisualiserNotifByFournisseur(false);
																//appelDoffresRestreint.setIsVisualiserContenuByFournisseur(existingAppelDoffresRestreint.getIsVisualiserContenuByFournisseur());
																//appelDoffresRestreint.setIsVisualiserNotifByFournisseur(existingAppelDoffresRestreint.getIsVisualiserNotifByFournisseur());
																//appelDoffresRestreint.setIsUpdateAppelDoffres(data.getIsUpdate());
																appelDoffresRestreint.setIsUpdateAppelDoffres((existingAppelDoffresRestreint.getIsVisualiserContenuByFournisseur())); // c'est la condition pour le notifier d'un update
																appelDoffresRestreint.setIsVisibleByFournisseur(existingAppelDoffresRestreint.getIsVisibleByFournisseur()); // car il ne l'a pas encore supprimer logiquement
																appelDoffresRestreint.setCreatedAt(Utilities.getCurrentDate());
																appelDoffresRestreint.setCreatedBy(request.getUser());
																appelDoffresRestreint.setIsSelectedByClient(existingAppelDoffresRestreint.getIsSelectedByClient());
																appelDoffresRestreint.setIsDeleted(false);
																
															} else {
																
																appelDoffresRestreint.setAppelDoffres(appelDoffres);
																appelDoffresRestreint.setUser(entity);
																appelDoffresRestreint.setIsAppelRestreint(false); // pour dire que se sont des elements pour visualtisations
																appelDoffresRestreint.setIsSoumissionnerByFournisseur(false);
																appelDoffresRestreint.setIsDesactiver(false);
																appelDoffresRestreint.setIsVisualiserContenuByFournisseur(false);
																appelDoffresRestreint.setIsVisualiserNotifByFournisseur(false);
																//appelDoffresRestreint.setIsUpdateAppelDoffres(data.getIsUpdate());
																appelDoffresRestreint.setIsUpdateAppelDoffres(false);
																appelDoffresRestreint.setIsVisibleByFournisseur(true); // car il ne l'a pas encore supprimer logiquement
																appelDoffresRestreint.setCreatedAt(Utilities.getCurrentDate());
																appelDoffresRestreint.setCreatedBy(request.getUser());
																appelDoffresRestreint.setIsSelectedByClient(false);
																appelDoffresRestreint.setIsDeleted(false);
															}
															
														}else {
															appelDoffresRestreint = new AppelDoffresRestreint() ;
															appelDoffresRestreint.setAppelDoffres(appelDoffres);
															appelDoffresRestreint.setUser(entity);
															appelDoffresRestreint.setIsAppelRestreint(false); // pour dire que se sont des elements pour visualtisations
															appelDoffresRestreint.setIsSoumissionnerByFournisseur(false);
															appelDoffresRestreint.setIsDesactiver(false);
															appelDoffresRestreint.setIsVisualiserContenuByFournisseur(false);
															appelDoffresRestreint.setIsVisualiserNotifByFournisseur(false);
															//appelDoffresRestreint.setIsUpdateAppelDoffres(data.getIsUpdate());
															appelDoffresRestreint.setIsUpdateAppelDoffres(false);
															appelDoffresRestreint.setIsVisibleByFournisseur(true); // car il ne l'a pas encore supprimer logiquement
															appelDoffresRestreint.setCreatedAt(Utilities.getCurrentDate());
															appelDoffresRestreint.setCreatedBy(request.getUser());
															appelDoffresRestreint.setIsSelectedByClient(false);
															appelDoffresRestreint.setIsDeleted(false);
														}
														itemsAppelDoffresRestreintToCreate.add(appelDoffresRestreint);
													}
												}
												
												// supprimer les anciens elements si le ancienCode = ENTRANT
												List<AppelDoffresRestreint> listAppelDoffresRestreint = appelDoffresRestreintRepository.findByAppelDoffresId(appelDoffresId, false);
												if (Utilities.isNotEmpty(listAppelDoffresRestreint)) {
													for (AppelDoffresRestreint data : listAppelDoffresRestreint) {
														
														data.setDeletedAt(Utilities.getCurrentDate());
														data.setDeletedBy(request.getUser());
														data.setIsDeleted(true);
													}
													appelDoffresRestreintToDelete.addAll(listAppelDoffresRestreint) ;
												}

											}
										}
									}
								}
							}
						});
					});
					
					if (Utilities.isNotEmpty(appelDoffresRestreintToDelete)) {
						appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) appelDoffresRestreintToDelete);
					}
					if (Utilities.isNotEmpty(itemsAppelDoffresRestreintToCreate)) {
						appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) itemsAppelDoffresRestreintToCreate);
					}
				}
				
				// FIN 
				
				
				

				List<AppelDoffresDto> itemsDto = new ArrayList<AppelDoffresDto>();
				for (AppelDoffres entity : itemsSaved) {
					AppelDoffresDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update AppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				// TODO : a revoir comment supprimer
				//Utilities.deleteFileOnSever(listOfFilesCreate, paramsUtils);
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}else {
				// suppression des anciens fichiers quand le processus de update c'est bien dérouler
				slf4jLogger.info("listOfOldFiles ... " + listOfOldFiles);
				slf4jLogger.info("listOfOldFiles.toString() ... " + listOfOldFiles.toString());

				Utilities.deleteFileOnSever(listOfOldFiles, paramsUtils);
			}
		}
		return response;
	}

	
	/**
	 * update AppelDoffres by using AppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<AppelDoffresDto> updateContenuNotificationDispo(Request<AppelDoffresDto> request, Locale locale) {
		slf4jLogger.info("----begin updateContenuNotificationDispo AppelDoffres-----");

		response = new Response<AppelDoffresDto>();
		
		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<AppelDoffres> items = new ArrayList<AppelDoffres>();
			
			for (AppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la appelDoffres existe
				AppelDoffres entityToSave = null;
				entityToSave = appelDoffresRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				Integer entityToSaveId = entityToSave.getId();
			
				if (dto.getIsContenuNotificationDispo() != null) {
					entityToSave.setIsContenuNotificationDispo(dto.getIsContenuNotificationDispo());
				}
				
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<AppelDoffres> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = appelDoffresRepository.save((Iterable<AppelDoffres>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("appelDoffres", locale));
					response.setHasError(true);
					return response;
				}
				

				List<AppelDoffresDto> itemsDto = new ArrayList<AppelDoffresDto>();
				for (AppelDoffres entity : itemsSaved) {
					AppelDoffresDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end updateContenuNotificationDispo AppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				// TODO : a revoir comment supprimer
				//Utilities.deleteFileOnSever(listOfFilesCreate, paramsUtils);
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * changeTypeAppelDoffre AppelDoffres by using AppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<AppelDoffresDto> changeTypeAppelDoffre(Request<AppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin changeTypeAppelDoffre AppelDoffres-----");

		response = new Response<AppelDoffresDto>();


		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<AppelDoffres> items = new ArrayList<AppelDoffres>();
			List<AppelDoffresRestreintDto> datasAppelDoffresRestreint = new ArrayList<>();
			List<AppelDoffresRestreint> itemsAppelDoffresRestreintx = new ArrayList<>();

			
			for (AppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				fieldsToVerify.put("typeAppelDoffresId", dto.getTypeAppelDoffresId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la appelDoffres existe
				AppelDoffres entityToSave = null;
				entityToSave = appelDoffresRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				
				
				Integer entityToSaveId = entityToSave.getId();

				TypeAppelDoffres existingTypeAppelDoffres = typeAppelDoffresRepository.findById(dto.getTypeAppelDoffresId(), false);
				if (existingTypeAppelDoffres == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeAppelDoffres -> " + dto.getTypeAppelDoffresId(), locale));
					response.setHasError(true);
					return response;
				}
				// s'assurer du changement de type
				if (!existingTypeAppelDoffres.getCode().equals(entityToSave.getTypeAppelDoffres().getCode()) 
						&& existingTypeAppelDoffres.getCode().equals(TypeAppelDoffresEnum.RESTREINT) ) {
					response.setStatus(functionalError.DISALLOWED_OPERATION("Impossible de passer d'un appel d'offres général à un appel restreint !!! ",  locale));
					response.setHasError(true);
					return response;
					
				}
				entityToSave.setTypeAppelDoffres(existingTypeAppelDoffres);

				//ajouter des users pour le cas restreint
				if (existingTypeAppelDoffres.getCode().equals(TypeAppelDoffresEnum.RESTREINT) && Utilities.isNotEmpty(dto.getDatasEntreprise())) {
					
					List<User> users =  bienFormerDatasUser(dto.getDatasEntreprise()) ;
					
					if (Utilities.isNotEmpty(users)) {
						List<AppelDoffresRestreint> appelDoffresRestreints = appelDoffresRestreintRepository.findByAppelDoffresId(entityToSaveId, false);
						List<AppelDoffresRestreint> appelDoffresRestreintsRestant = new ArrayList<>();
						if (Utilities.isNotEmpty(appelDoffresRestreints)) {
							for (AppelDoffresRestreint data : appelDoffresRestreints) {
								if (!users.stream().anyMatch(a->a.getId().equals(data.getUser().getId()))) {
									
									if (data.getIsSoumissionnerByFournisseur() != null && data.getIsSoumissionnerByFournisseur()) {
										response.setStatus(functionalError.DISALLOWED_OPERATION("Impossible de supprimer ce fournisseur de car il a déjà une offre poour cet appel d'offre !!! ",  locale));
										response.setHasError(true);
										return response;
									}
									data.setIsDesactiver(true); // pour lui dire qu'on l'a retirer
									data.setIsDeleted(false);
									data.setDeletedBy(request.getUser());
									data.setDeletedAt(Utilities.getCurrentDate());
								}else {
									appelDoffresRestreintsRestant.add(data);
								}
							}
							// enregistrer les modifs
							//itemsAppelDoffresRestreint.addAll(appelDoffresRestreints);
							List<AppelDoffresRestreint> itemsAppelDoffresRestreintSaved = null;
							// maj les donnees en base
							itemsAppelDoffresRestreintSaved = appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) appelDoffresRestreints);
							if (itemsAppelDoffresRestreintSaved == null || itemsAppelDoffresRestreintSaved.isEmpty()) {
								response.setStatus(functionalError.SAVE_FAIL("appelDoffresRestreint", locale));
								response.setHasError(true);
								return response;
							}
							//appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) appelDoffresRestreints);
							
						}
						
						// retirer les anciens et creer les nouveaux
						//if (!appelDoffresRestreintsRestant.isEmpty()) {
							for (User data : users) {
							//for (UserDto data : dto.getDatasUser()) {
								if (!Utilities.isNotEmpty(appelDoffresRestreintsRestant) || !appelDoffresRestreintsRestant.stream().anyMatch(a->a.getUser().getId().equals(data.getId()))) {
									AppelDoffresRestreintDto appelDoffresRestreintDto = new AppelDoffresRestreintDto() ;
									//AppelDoffresRestreintDto appelDoffresRestreintDto1 = new AppelDoffresRestreintDto() ;
									
									appelDoffresRestreintDto.setAppelDoffresId(entityToSaveId);
									appelDoffresRestreintDto.setUserFournisseurId(data.getId());
									appelDoffresRestreintDto.setIsAppelRestreint(true); // pour dire que l'appel est restreint

									//appelDoffresRestreintDto1.setAppelDoffresId(entityToSaveId);
									//appelDoffresRestreintDto1.setUserFournisseurId(data.getId());
									//appelDoffresRestreintDto1.setIsAppelRestreint(false); // pour dire que l'appel est restreint

									datasAppelDoffresRestreint.add(appelDoffresRestreintDto);
								}
							}
						//}
					}
										
					
				}
				
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<AppelDoffres> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = appelDoffresRepository.save((Iterable<AppelDoffres>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("appelDoffres", locale));
					response.setHasError(true);
					return response;
				}
				
//				if (!itemsAppelDoffresRestreint.isEmpty()) {
//					List<AppelDoffresRestreint> itemsAppelDoffresRestreintSaved = null;
//					// maj les donnees en base
//					itemsAppelDoffresRestreintSaved = appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) itemsAppelDoffresRestreint);
//					if (itemsAppelDoffresRestreintSaved == null || itemsAppelDoffresRestreintSaved.isEmpty()) {
//						response.setStatus(functionalError.SAVE_FAIL("appelDoffresRestreint", locale));
//						response.setHasError(true);
//						return response;
//					}
//				}
					
				
				// create datasAppelDoffresRestreint 
				if (!datasAppelDoffresRestreint.isEmpty()) {
					
					Request<AppelDoffresRestreintDto> req = new Request<>();
					Response<AppelDoffresRestreintDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasAppelDoffresRestreint);
					
					res = appelDoffresRestreintBusiness.create(req, locale);
					
					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				List<AppelDoffresDto> itemsDto = new ArrayList<AppelDoffresDto>();
				for (AppelDoffres entity : itemsSaved) {
					AppelDoffresDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end changeTypeAppelDoffre AppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * changeTypeAppelDoffre AppelDoffres by using AppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<AppelDoffresDto> noterOffre(Request<AppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin noterOffre AppelDoffres-----");

		response = new Response<AppelDoffresDto>();


		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<AppelDoffres> items = new ArrayList<AppelDoffres>();
			List<Offre> itemsOffre = new ArrayList<>();

			
			for (AppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la appelDoffres existe
				AppelDoffres entityToSave = null;
				entityToSave = appelDoffresRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				
				
				Integer entityToSaveId = entityToSave.getId();

				List<Offre> offres = offreRepository.findByAppelDoffresIdAndIsSelectedByClient(entityToSaveId, true, false);
				
				if (Utilities.isNotEmpty(offres)) {
					
					for (Offre offre : itemsOffre) {
						
						// mettre les message
						
						
					}
				}
				
				
				
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<AppelDoffres> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = appelDoffresRepository.save((Iterable<AppelDoffres>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("appelDoffres", locale));
					response.setHasError(true);
					return response;
				}
				
				
				List<AppelDoffresDto> itemsDto = new ArrayList<AppelDoffresDto>();
				for (AppelDoffres entity : itemsSaved) {
					AppelDoffresDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end noterOffre AppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete AppelDoffres by using AppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<AppelDoffresDto> delete(Request<AppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete AppelDoffres-----");

		response = new Response<AppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<AppelDoffres> items = new ArrayList<AppelDoffres>();

			for (AppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la appelDoffres existe
				AppelDoffres existingEntity = null;
				existingEntity = appelDoffresRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				
				
				
				// offre
				List<Offre> listOfOffre = offreRepository.findByAppelDoffresId(existingEntity.getId(), false);
				if (listOfOffre == null || listOfOffre.isEmpty()){
					forceDelete(request, locale);
				}
				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				existingEntity.setIsVisibleByClient(false);

				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				appelDoffresRepository.save((Iterable<AppelDoffres>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete AppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete AppelDoffres by using AppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<AppelDoffresDto> forceDelete(Request<AppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete AppelDoffres-----");

		response = new Response<AppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<AppelDoffres> items = new ArrayList<AppelDoffres>();

			for (AppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la appelDoffres existe
				AppelDoffres existingEntity = null;
				existingEntity = appelDoffresRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// valeurCritereAppelDoffres
				List<ValeurCritereAppelDoffres> listOfValeurCritereAppelDoffres = valeurCritereAppelDoffresRepository.findByAppelDoffresId(existingEntity.getId(), false);
				if (listOfValeurCritereAppelDoffres != null && !listOfValeurCritereAppelDoffres.isEmpty()){
					Request<ValeurCritereAppelDoffresDto> deleteRequest = new Request<ValeurCritereAppelDoffresDto>();
					deleteRequest.setDatas(ValeurCritereAppelDoffresTransformer.INSTANCE.toDtos(listOfValeurCritereAppelDoffres));
					deleteRequest.setUser(request.getUser());
					Response<ValeurCritereAppelDoffresDto> deleteResponse = valeurCritereAppelDoffresBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				
				// secteurDactiviteAppelDoffres
				List<SecteurDactiviteAppelDoffres> listOfSecteurDactiviteAppelDoffres = secteurDactiviteAppelDoffresRepository.findByAppelDoffresId(existingEntity.getId(), false);
				if (listOfSecteurDactiviteAppelDoffres != null && !listOfSecteurDactiviteAppelDoffres.isEmpty()){
					Request<SecteurDactiviteAppelDoffresDto> deleteRequest = new Request<SecteurDactiviteAppelDoffresDto>();
					deleteRequest.setDatas(SecteurDactiviteAppelDoffresTransformer.INSTANCE.toDtos(listOfSecteurDactiviteAppelDoffres));
					deleteRequest.setUser(request.getUser());
					Response<SecteurDactiviteAppelDoffresDto> deleteResponse = secteurDactiviteAppelDoffresBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// offre
				List<Offre> listOfOffre = offreRepository.findByAppelDoffresId(existingEntity.getId(), false);
				if (listOfOffre != null && !listOfOffre.isEmpty()){
					Request<OffreDto> deleteRequest = new Request<OffreDto>();
					deleteRequest.setDatas(OffreTransformer.INSTANCE.toDtos(listOfOffre));
					deleteRequest.setUser(request.getUser());
					Response<OffreDto> deleteResponse = offreBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// appelDoffresRestreint
				List<AppelDoffresRestreint> listOfAppelDoffresRestreint = appelDoffresRestreintRepository.findByAppelDoffresId(existingEntity.getId(), false);
				if (listOfAppelDoffresRestreint != null && !listOfAppelDoffresRestreint.isEmpty()){
					Request<AppelDoffresRestreintDto> deleteRequest = new Request<AppelDoffresRestreintDto>();
					deleteRequest.setDatas(AppelDoffresRestreintTransformer.INSTANCE.toDtos(listOfAppelDoffresRestreint));
					deleteRequest.setUser(request.getUser());
					Response<AppelDoffresRestreintDto> deleteResponse = appelDoffresRestreintBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}

				// fichier
				//List<Fichier> listOfFichier = fichierRepository.findByAppelDoffresId(existingEntity.getId(), false);
				List<Fichier> listOfFichier = fichierRepository.findBySourceIdAndSource(existingEntity.getId(), GlobalEnum.fichiers_ao, false);

				if (listOfFichier != null && !listOfFichier.isEmpty()){
					Request<FichierDto> deleteRequest = new Request<FichierDto>();
					deleteRequest.setDatas(FichierTransformer.INSTANCE.toDtos(listOfFichier));
					deleteRequest.setUser(request.getUser());
					Response<FichierDto> deleteResponse = fichierBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				appelDoffresRepository.save((Iterable<AppelDoffres>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete AppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	
	/**
	 * getListesActivites AppelDoffres by using AppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	public Response<AppelDoffresDto> getListesActivites(Request<AppelDoffresDto> request, Locale locale) {
		slf4jLogger.info("----begin getListesActivites AppelDoffres-----");

		response = new Response<AppelDoffresDto>();

		try {
			
			response = getByCriteria(request, locale);
			if (!response.isHasError()) {
				if (Utilities.isNotEmpty(response.getItems())) {
					for (AppelDoffresDto dto : response.getItems()) {
						
						List<Offre> offres = offreRepository.findByAppelDoffresIdAndIsSelectedByClient(dto.getId(), true, false);
												
						if (Utilities.isNotEmpty(offres)) {
							dto.setDataOffre(OffreTransformer.INSTANCE.toDto(offres.get(0))) ;
						}
						
						// methode delaisser
						
						
						
					}

				}

				
				
			}
			
			List<AppelDoffres> items = null;
			items = appelDoffresRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<AppelDoffresDto> itemsDto = new ArrayList<AppelDoffresDto>();
				for (AppelDoffres entity : items) {
					AppelDoffresDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(appelDoffresRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("appelDoffres", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end getListesActivites AppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
	
	// bien former le datasUser
	private List<User> bienFormerDatasUser(List<EntrepriseDto> datasEntreprise){
		List<User> users = new ArrayList<User>() ;
		
		if (Utilities.isNotEmpty(datasEntreprise)) {
			for (EntrepriseDto data : datasEntreprise) {
				List<User> usersLocal = userRepository.findByEntrepriseId(data.getId(), false) ;
				if (Utilities.isNotEmpty(usersLocal)) {
					users.addAll(usersLocal) ;
				}
			}
		}
		return users ;
	}


	/**
	 * get AppelDoffres by using AppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<AppelDoffresDto> getByCriteria(Request<AppelDoffresDto> request, Locale locale) {
		slf4jLogger.info("----begin get AppelDoffres-----");

		response = new Response<AppelDoffresDto>();

		try {
			List<AppelDoffres> items = null;
			items = appelDoffresRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<AppelDoffresDto> itemsDto = new ArrayList<AppelDoffresDto>();
				for (AppelDoffres entity : items) {
					AppelDoffresDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(appelDoffresRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("appelDoffres", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get AppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full AppelDoffresDto by using AppelDoffres as object.
	 *
	 * @param entity, locale
	 * @return AppelDoffresDto
	 *
	 */
	private AppelDoffresDto getFullInfos(AppelDoffres entity, Integer size, Locale locale) throws Exception {
		AppelDoffresDto dto = AppelDoffresTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		if(entity.getVille() != null && entity.getVille().getPays() != null){
			dto.setPaysId(entity.getVille().getPays().getId()) ;
			dto.setPaysCode(entity.getVille().getPays().getCode()) ;
			dto.setPaysLibelle(entity.getVille().getPays().getLibelle()) ;
		}
		
		//List<Offre> offres = offreRepository.findByAppelDoffresId(dto.getId(), false);
		List<Offre> offres = offreRepository.findByAppelDoffresIdAndEtatCodeAndIsRetirer(dto.getId(), EtatEnum.ENTRANT, false, false);

		dto.setNombreOffre(Utilities.isNotEmpty(offres) ? ( offres.size() == 1 ?  offres.size() + " offre" : offres.size() + " offres") : "Aucune offre");
		
		
		
		// TODO : RENVOYER LE NOMBRE DE FOURNISSEUR QUI ONT DEJA VU L'APPEL D'OFFRES
		List<AppelDoffresRestreint> appelDoffresRestreints = appelDoffresRestreintRepository.findByAppelDoffresIdAndIsVisualiserContenuByFournisseurAndIsAppelRestreintAndIsDesactiver(dto.getId(), true, false, false, false) ;
		dto.setNombreVisualisation(Utilities.isNotEmpty(appelDoffresRestreints) ? ( appelDoffresRestreints.size() == 1 ?  appelDoffresRestreints.size() + " vu" : appelDoffresRestreints.size() + " vus") : "Aucune vu");

		
		
		// recuperer l'offre selectionnée
		List<Offre> offresSelectionner = offreRepository.findByAppelDoffresIdAndIsSelectedByClient(dto.getId(), true, false);
		if (Utilities.isNotEmpty(offresSelectionner)) {
			dto.setDataOffre(OffreTransformer.INSTANCE.toDto(offresSelectionner.get(0))) ;
		}
 		// pas besoin de setter le isMOdifiable car il sera setter apres sooumission d'une offre
		if (size > 1) {
			return dto;
		}

		Integer appelDoffresId = dto.getId() ;
		
		// renvoi des offres 
		if (Utilities.isNotEmpty(offres)) {
			dto.setDatasOffre(OffreTransformer.INSTANCE.toDtos(offres));
		}
		
		// renvoi des datas associé
		List<SecteurDactiviteAppelDoffres> datasSecteurDactiviteAppelDoffres = new ArrayList<>();
		datasSecteurDactiviteAppelDoffres = secteurDactiviteAppelDoffresRepository.findByAppelDoffresId(appelDoffresId, false);
		if (!datasSecteurDactiviteAppelDoffres.isEmpty()) {
			dto.setDatasSecteurDactiviteAppelDoffres(SecteurDactiviteAppelDoffresTransformer.INSTANCE.toDtos(datasSecteurDactiviteAppelDoffres));
		}

		List<ValeurCritereAppelDoffres> datasValeurCritereAppelDoffres = new ArrayList<>();
		datasValeurCritereAppelDoffres = valeurCritereAppelDoffresRepository.findByAppelDoffresId(appelDoffresId, false);
		if (!datasValeurCritereAppelDoffres.isEmpty()) {
			dto.setDatasValeurCritereAppelDoffres(ValeurCritereAppelDoffresTransformer.INSTANCE.toDtos(datasValeurCritereAppelDoffres));
		}

		// TODO : Renvoyr l'url et non le nom du fichier
		List<Fichier> datasFichier = new ArrayList<>();
		//datasFichier = fichierRepository.findByAppelDoffresId(appelDoffresId, false);
		datasFichier = fichierRepository.findBySourceIdAndSource(appelDoffresId, GlobalEnum.fichiers_ao, false);

		if (!datasFichier.isEmpty()) {
			dto.setDatasFichier(FichierTransformer.INSTANCE.toDtos(datasFichier));
			for (FichierDto data : dto.getDatasFichier()) {
				if (Utilities.notBlank(data.getName())) {
					// Repertoire où je depose mon fichier
					String filesDirectory = Utilities.getSuitableFileDirectory(data.getExtension(), paramsUtils);
					Utilities.createDirectory(filesDirectory);
					if (!filesDirectory.endsWith("/")) {
						filesDirectory += "/";
					}
					String fileNameByExtension = Utilities.addExtensionInFileName( data.getName() , data.getExtension()) ;
					filesDirectory=filesDirectory+fileNameByExtension;
					//filesDirectory=filesDirectory+data.getName();
					//data.setFichierBase64(Utilities.convertFileToBase64(filesDirectory));
					
					//data.setUrlFichier(filesDirectory);
					
					// formatter le name du fichier
					data.setName(Utilities.getBasicNameFile(data.getName()));
					
					data.setUrlFichier(Utilities.getSuitableFileUrl(fileNameByExtension, paramsUtils));
					//data.setUrlFichier(Utilities.getSuitableFileUrl(data.getName(), paramsUtils));
					//data.setUrlFichier(Utilities.getSuitableFileUrlCustom(data.getName(), GlobalEnum.appel_doffres, paramsUtils));
					
				}
			}
		}
		
//		if (dto.getTypeAppelDoffresCode().equals(TypeAppelDoffresEnum.RESTREINT)) {
//
//			List<AppelDoffresRestreint> datasAppelDoffresRestreint = new ArrayList<>();
//			datasAppelDoffresRestreint = appelDoffresRestreintRepository.findByAppelDoffresId(appelDoffresId, false);
//			if (!datasAppelDoffresRestreint.isEmpty()) {
//				List<User> datasUser = new ArrayList<>();
//				for (AppelDoffresRestreint data : datasAppelDoffresRestreint) {
//					datasUser.add(data.getUser());
//				}
//				dto.setDatasUser(UserTransformer.INSTANCE.toDtos(datasUser));
//			}
//		}
		
		if (dto.getTypeAppelDoffresCode().equals(TypeAppelDoffresEnum.RESTREINT)) {
			List<Entreprise> datasEntreprise = new ArrayList<Entreprise>() ;
			datasEntreprise = appelDoffresRestreintRepository.findDistinctEntrepriseByAppelDoffresId(appelDoffresId, false);

			//List<AppelDoffresRestreint> datasAppelDoffresRestreint = new ArrayList<>();
			//datasAppelDoffresRestreint = appelDoffresRestreintRepository.findByAppelDoffresId(appelDoffresId, false);
			if (!datasEntreprise.isEmpty()) {
				dto.setDatasEntreprise(EntrepriseTransformer.INSTANCE.toDtos(datasEntreprise));
			}
		}

			
		


		return dto;
	}
}
