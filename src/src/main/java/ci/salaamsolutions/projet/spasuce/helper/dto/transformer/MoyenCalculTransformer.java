

/*
 * Java transformer for entity table moyen_calcul 
 * Created on 2020-01-02 ( Time 17:59:59 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "moyen_calcul"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface MoyenCalculTransformer {

	MoyenCalculTransformer INSTANCE = Mappers.getMapper(MoyenCalculTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.sousCritereNote.id", target="sousCritereNoteId"),
				@Mapping(source="entity.sousCritereNote.libelle", target="sousCritereNoteLibelle"),
	})
	MoyenCalculDto toDto(MoyenCalcul entity) throws ParseException;

    List<MoyenCalculDto> toDtos(List<MoyenCalcul> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.valeur", target="valeur"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="sousCritereNote", target="sousCritereNote"),
	})
    MoyenCalcul toEntity(MoyenCalculDto dto, SousCritereNote sousCritereNote) throws ParseException;

    //List<MoyenCalcul> toEntities(List<MoyenCalculDto> dtos) throws ParseException;

}
