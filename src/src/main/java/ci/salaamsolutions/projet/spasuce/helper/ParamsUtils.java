package ci.salaamsolutions.projet.spasuce.helper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ParamsUtils {
	// -----------------------------
	// ATTRIBUTES
	// -----------------------------   


	@Value("${smtp.mail.host}")
	private String	smtpHost;

	@Value("${smtp.mail.port}")
	private Integer	smtpPort;

	@Value("${smtp.mail.username}")
	private String	smtpMailUsername;

	@Value("${smtp.mail.password}")
	private String	smtpPassword;
	
	@Value("${smtp.mail.adresse}")
	private String smtpMailAdresse;
	
	@Value("${smtp.mail.user}")
	private String smtpMailUser;
	
	@Value("${template.newsletter}")
	private String templateNewsletter;
	
	
	@Value("${newsletter.subject}")
	private String newsletterSubject;
	
	@Value("${template.succes.change.password}")
	private String templateSuccesChangePassword;
	
	@Value("${template.creation.compte.par.admin}")
	private String templateCreationCompteParAdmin;
	
	
	@Value("${template.creation.compte.par.user}")
	private String templateCreationCompteParUser;
	
	
	@Value("${template.selection.offre.par.client}")
	private String templateSelectionOffreParClient;
	
	
	

	
	@Value("${template.creation.compte.par.user.with.token}")
	private String templateCreationCompteParUserWithToken;
	
	
	@Value("${template.nous.contacter.retour.par.defaut}")
	private String templateNousContacterRetourParDefaut;
	
	
	
	@Value("${template.reset.user.password}")
	private String templateResetUserPassword;
	
	
	@Value("${url.root.admin}")
	private String urlRootAdmin;
	
	@Value("${url.root.user}")
	private String urlRootUser;
	
	
	
	
	
	
	// ATTRIBUTES
		// -----------------------------

		@Value("${path.root.files}")
		private String	rootPathFiles;
		
		@Value("${gal.video.directory}")
		private String	galerieVideoFilesDirectory;

		@Value("${gal.image.directory}")
		private String	galerieImageFilesDirectory;
		
		@Value("${gal.text.directory}")
		private String	galerieTextFilesDirectory;
		
		@Value("${other.directory}")
		private String	otherFilesDirectory;

		
		@Value("${gal.offres}")
		private String	galerieOffres;

		@Value("${gal.appels_doffres}")
		private String	galerieAppelsDoffres;
		
		@Value("${gal.profils}")
		private String	galerieProfils;
		
		@Value("${gal.autres}")
		private String	galerieAutres;

	
		
	
	public String getTemplateSelectionOffreParClient() {
			return templateSelectionOffreParClient;
		}

	public String getTemplateCreationCompteParUserWithToken() {
			return templateCreationCompteParUserWithToken;
		}

	public String getTemplateNousContacterRetourParDefaut() {
			return templateNousContacterRetourParDefaut;
		}

	public String getSmtpHost() {
		return smtpHost;
	}

	/**
	 * @return the smtpPort
	 */

	public Integer getSmtpPort() {
		return smtpPort;
	}

	/**
	 * @return the smtpLogin
	 */
	public String getSmtpMailUser() {
		return smtpMailUser;
	}

	/**
	 * @return the smtpPassword
	 */
	public String getSmtpMailAdresse() {
		return smtpMailAdresse;
	}

	public String getSmtpMailUsername() {
		return smtpMailUsername;
	}

	public String getSmtpPassword() {
		return smtpPassword;
	}

	public String getTemplateNewsletter() {
		return templateNewsletter;
	}

	public String getNewsletterSubject() {
		return newsletterSubject;
	}

	// ATTRIBUTES
	// -----------------------------

	@Value("${root.url.file}")
	private String	rootUrlFiles;
	
	public String getTemplateSuccesChangePassword() {
		return templateSuccesChangePassword;
	}

	public String getTemplateResetUserPassword() {
		return templateResetUserPassword;
	}

	/**
	 * @return the rootUrlFiles
	 */
	public String getRootUrlFiles() {
		return rootUrlFiles;
	}

	public String getUrlRootAdmin() {
		return urlRootAdmin;
	}

	public String getUrlRootUser() {
		return urlRootUser;
	}


	public String getTemplateCreationCompteParAdmin() {
		return templateCreationCompteParAdmin;
	}

	public String getTemplateCreationCompteParUser() {
		return templateCreationCompteParUser;
	}

	public String getRootPathFiles() {
		return rootPathFiles;
	}

	public String getGalerieVideoFilesDirectory() {
		return galerieVideoFilesDirectory;
	}

	public String getGalerieImageFilesDirectory() {
		return galerieImageFilesDirectory;
	}

	public String getGalerieTextFilesDirectory() {
		return galerieTextFilesDirectory;
	}

	public String getOtherFilesDirectory() {
		return otherFilesDirectory;
	}

	public String getGalerieOffres() {
		return galerieOffres;
	}

	public String getGalerieAppelsDoffres() {
		return galerieAppelsDoffres;
	}

	public String getGalerieProfils() {
		return galerieProfils;
	}

	public String getGalerieAutres() {
		return galerieAutres;
	}
	
	
	
	
	

	// -----------------------------

}