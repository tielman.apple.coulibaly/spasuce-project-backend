package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._CritereAppelDoffresRepository;

/**
 * Repository : CritereAppelDoffres.
 */
@Repository
public interface CritereAppelDoffresRepository extends JpaRepository<CritereAppelDoffres, Integer>, _CritereAppelDoffresRepository {
	/**
	 * Finds CritereAppelDoffres by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object CritereAppelDoffres whose id is equals to the given id. If
	 *         no CritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from CritereAppelDoffres e where e.id = :id and e.isDeleted = :isDeleted")
	CritereAppelDoffres findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds CritereAppelDoffres by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object CritereAppelDoffres whose libelle is equals to the given libelle. If
	 *         no CritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from CritereAppelDoffres e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	CritereAppelDoffres findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereAppelDoffres by using code as a search criteria.
	 *
	 * @param code
	 * @return An Object CritereAppelDoffres whose code is equals to the given code. If
	 *         no CritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from CritereAppelDoffres e where e.code = :code and e.isDeleted = :isDeleted")
	CritereAppelDoffres findByCode(@Param("code")String code, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereAppelDoffres by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object CritereAppelDoffres whose createdAt is equals to the given createdAt. If
	 *         no CritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from CritereAppelDoffres e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<CritereAppelDoffres> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereAppelDoffres by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object CritereAppelDoffres whose createdBy is equals to the given createdBy. If
	 *         no CritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from CritereAppelDoffres e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<CritereAppelDoffres> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereAppelDoffres by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object CritereAppelDoffres whose updatedAt is equals to the given updatedAt. If
	 *         no CritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from CritereAppelDoffres e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<CritereAppelDoffres> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereAppelDoffres by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object CritereAppelDoffres whose updatedBy is equals to the given updatedBy. If
	 *         no CritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from CritereAppelDoffres e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<CritereAppelDoffres> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereAppelDoffres by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object CritereAppelDoffres whose deletedAt is equals to the given deletedAt. If
	 *         no CritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from CritereAppelDoffres e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<CritereAppelDoffres> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereAppelDoffres by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object CritereAppelDoffres whose deletedBy is equals to the given deletedBy. If
	 *         no CritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from CritereAppelDoffres e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<CritereAppelDoffres> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereAppelDoffres by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object CritereAppelDoffres whose isDeleted is equals to the given isDeleted. If
	 *         no CritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from CritereAppelDoffres e where e.isDeleted = :isDeleted")
	List<CritereAppelDoffres> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds CritereAppelDoffres by using typeCritereAppelDoffresId as a search criteria.
	 *
	 * @param typeCritereAppelDoffresId
	 * @return A list of Object CritereAppelDoffres whose typeCritereAppelDoffresId is equals to the given typeCritereAppelDoffresId. If
	 *         no CritereAppelDoffres is found, this method returns null.
	 */
	@Query("select e from CritereAppelDoffres e where e.typeCritereAppelDoffres.id = :typeCritereAppelDoffresId and e.isDeleted = :isDeleted")
	List<CritereAppelDoffres> findByTypeCritereAppelDoffresId(@Param("typeCritereAppelDoffresId")Integer typeCritereAppelDoffresId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one CritereAppelDoffres by using typeCritereAppelDoffresId as a search criteria.
   *
   * @param typeCritereAppelDoffresId
   * @return An Object CritereAppelDoffres whose typeCritereAppelDoffresId is equals to the given typeCritereAppelDoffresId. If
   *         no CritereAppelDoffres is found, this method returns null.
   */
  @Query("select e from CritereAppelDoffres e where e.typeCritereAppelDoffres.id = :typeCritereAppelDoffresId and e.isDeleted = :isDeleted")
  CritereAppelDoffres findCritereAppelDoffresByTypeCritereAppelDoffresId(@Param("typeCritereAppelDoffresId")Integer typeCritereAppelDoffresId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of CritereAppelDoffres by using critereAppelDoffresDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of CritereAppelDoffres
	 * @throws DataAccessException,ParseException
	 */
	public default List<CritereAppelDoffres> getByCriteria(Request<CritereAppelDoffresDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from CritereAppelDoffres e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<CritereAppelDoffres> query = em.createQuery(req, CritereAppelDoffres.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of CritereAppelDoffres by using critereAppelDoffresDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of CritereAppelDoffres
	 *
	 */
	public default Long count(Request<CritereAppelDoffresDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from CritereAppelDoffres e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<CritereAppelDoffresDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		CritereAppelDoffresDto dto = request.getData() != null ? request.getData() : new CritereAppelDoffresDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (CritereAppelDoffresDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(CritereAppelDoffresDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("code", dto.getCode(), "e.code", "String", dto.getCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getTypeCritereAppelDoffresId()!= null && dto.getTypeCritereAppelDoffresId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeCritereAppelDoffresId", dto.getTypeCritereAppelDoffresId(), "e.typeCritereAppelDoffres.id", "Integer", dto.getTypeCritereAppelDoffresIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeCritereAppelDoffresCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeCritereAppelDoffresCode", dto.getTypeCritereAppelDoffresCode(), "e.typeCritereAppelDoffres.code", "String", dto.getTypeCritereAppelDoffresCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeCritereAppelDoffresLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeCritereAppelDoffresLibelle", dto.getTypeCritereAppelDoffresLibelle(), "e.typeCritereAppelDoffres.libelle", "String", dto.getTypeCritereAppelDoffresLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
