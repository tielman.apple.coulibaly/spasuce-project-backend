
/*
 * Java dto for entity table critere_note_offre
 * Created on 2020-01-20 ( Time 17:38:35 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.customize._CritereNoteOffreDto;

import lombok.*;
/**
 * DTO for table "critere_note_offre"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class CritereNoteOffreDto extends _CritereNoteOffreDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Float      valeurPartielle      ;
	/*
	 * 
	 */
    private Integer    offreId              ;
	/*
	 * 
	 */
    private Integer    critereNoteId        ;
	/*
	 * 
	 */
    private Integer    sousCritereNoteId    ;
	/*
	 * 
	 */
    private Integer    moyenCalculId        ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String sousCritereNoteLibelle;
	private String moyenCalculLibelle;
	private String critereNoteLibelle;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Float>    valeurPartielleParam  ;                     
	private SearchParam<Integer>  offreIdParam          ;                     
	private SearchParam<Integer>  critereNoteIdParam    ;                     
	private SearchParam<Integer>  sousCritereNoteIdParam;                     
	private SearchParam<Integer>  moyenCalculIdParam    ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   sousCritereNoteLibelleParam;                     
	private SearchParam<String>   moyenCalculLibelleParam;                     
	private SearchParam<String>   critereNoteLibelleParam;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		CritereNoteOffreDto other = (CritereNoteOffreDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
