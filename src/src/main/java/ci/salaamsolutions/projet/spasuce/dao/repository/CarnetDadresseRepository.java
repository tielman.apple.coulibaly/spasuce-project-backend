package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._CarnetDadresseRepository;

/**
 * Repository : CarnetDadresse.
 */
@Repository
public interface CarnetDadresseRepository extends JpaRepository<CarnetDadresse, Integer>, _CarnetDadresseRepository {
	/**
	 * Finds CarnetDadresse by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object CarnetDadresse whose id is equals to the given id. If
	 *         no CarnetDadresse is found, this method returns null.
	 */
	@Query("select e from CarnetDadresse e where e.id = :id and e.isDeleted = :isDeleted")
	CarnetDadresse findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds CarnetDadresse by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object CarnetDadresse whose libelle is equals to the given libelle. If
	 *         no CarnetDadresse is found, this method returns null.
	 */
	@Query("select e from CarnetDadresse e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	CarnetDadresse findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CarnetDadresse by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object CarnetDadresse whose createdAt is equals to the given createdAt. If
	 *         no CarnetDadresse is found, this method returns null.
	 */
	@Query("select e from CarnetDadresse e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<CarnetDadresse> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CarnetDadresse by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object CarnetDadresse whose createdBy is equals to the given createdBy. If
	 *         no CarnetDadresse is found, this method returns null.
	 */
	@Query("select e from CarnetDadresse e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<CarnetDadresse> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CarnetDadresse by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object CarnetDadresse whose updatedAt is equals to the given updatedAt. If
	 *         no CarnetDadresse is found, this method returns null.
	 */
	@Query("select e from CarnetDadresse e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<CarnetDadresse> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CarnetDadresse by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object CarnetDadresse whose updatedBy is equals to the given updatedBy. If
	 *         no CarnetDadresse is found, this method returns null.
	 */
	@Query("select e from CarnetDadresse e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<CarnetDadresse> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CarnetDadresse by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object CarnetDadresse whose deletedAt is equals to the given deletedAt. If
	 *         no CarnetDadresse is found, this method returns null.
	 */
	@Query("select e from CarnetDadresse e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<CarnetDadresse> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CarnetDadresse by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object CarnetDadresse whose deletedBy is equals to the given deletedBy. If
	 *         no CarnetDadresse is found, this method returns null.
	 */
	@Query("select e from CarnetDadresse e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<CarnetDadresse> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CarnetDadresse by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object CarnetDadresse whose isDeleted is equals to the given isDeleted. If
	 *         no CarnetDadresse is found, this method returns null.
	 */
	@Query("select e from CarnetDadresse e where e.isDeleted = :isDeleted")
	List<CarnetDadresse> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds CarnetDadresse by using userClientId as a search criteria.
	 *
	 * @param userClientId
	 * @return A list of Object CarnetDadresse whose userClientId is equals to the given userClientId. If
	 *         no CarnetDadresse is found, this method returns null.
	 */
	@Query("select e from CarnetDadresse e where e.user.id = :userClientId and e.isDeleted = :isDeleted")
	List<CarnetDadresse> findByUserClientId(@Param("userClientId")Integer userClientId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one CarnetDadresse by using userClientId as a search criteria.
   *
   * @param userClientId
   * @return An Object CarnetDadresse whose userClientId is equals to the given userClientId. If
   *         no CarnetDadresse is found, this method returns null.
   */
  @Query("select e from CarnetDadresse e where e.user.id = :userClientId and e.isDeleted = :isDeleted")
  CarnetDadresse findCarnetDadresseByUserClientId(@Param("userClientId")Integer userClientId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of CarnetDadresse by using carnetDadresseDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of CarnetDadresse
	 * @throws DataAccessException,ParseException
	 */
	public default List<CarnetDadresse> getByCriteria(Request<CarnetDadresseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from CarnetDadresse e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<CarnetDadresse> query = em.createQuery(req, CarnetDadresse.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of CarnetDadresse by using carnetDadresseDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of CarnetDadresse
	 *
	 */
	public default Long count(Request<CarnetDadresseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from CarnetDadresse e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<CarnetDadresseDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		CarnetDadresseDto dto = request.getData() != null ? request.getData() : new CarnetDadresseDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (CarnetDadresseDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(CarnetDadresseDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getUserClientId()!= null && dto.getUserClientId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userClientId", dto.getUserClientId(), "e.user.id", "Integer", dto.getUserClientIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserPrenoms())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenoms", dto.getUserPrenoms(), "e.user.prenoms", "String", dto.getUserPrenomsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
