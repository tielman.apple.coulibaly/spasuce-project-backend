package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._MoyenCalculRepository;

/**
 * Repository : MoyenCalcul.
 */
@Repository
public interface MoyenCalculRepository extends JpaRepository<MoyenCalcul, Integer>, _MoyenCalculRepository {
	/**
	 * Finds MoyenCalcul by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object MoyenCalcul whose id is equals to the given id. If
	 *         no MoyenCalcul is found, this method returns null.
	 */
	@Query("select e from MoyenCalcul e where e.id = :id and e.isDeleted = :isDeleted")
	MoyenCalcul findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds MoyenCalcul by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object MoyenCalcul whose libelle is equals to the given libelle. If
	 *         no MoyenCalcul is found, this method returns null.
	 */
	@Query("select e from MoyenCalcul e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	MoyenCalcul findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds MoyenCalcul by using valeur as a search criteria.
	 *
	 * @param valeur
	 * @return An Object MoyenCalcul whose valeur is equals to the given valeur. If
	 *         no MoyenCalcul is found, this method returns null.
	 */
	@Query("select e from MoyenCalcul e where e.valeur = :valeur and e.isDeleted = :isDeleted")
	List<MoyenCalcul> findByValeur(@Param("valeur")Integer valeur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds MoyenCalcul by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object MoyenCalcul whose createdAt is equals to the given createdAt. If
	 *         no MoyenCalcul is found, this method returns null.
	 */
	@Query("select e from MoyenCalcul e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<MoyenCalcul> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds MoyenCalcul by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object MoyenCalcul whose createdBy is equals to the given createdBy. If
	 *         no MoyenCalcul is found, this method returns null.
	 */
	@Query("select e from MoyenCalcul e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<MoyenCalcul> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds MoyenCalcul by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object MoyenCalcul whose updatedAt is equals to the given updatedAt. If
	 *         no MoyenCalcul is found, this method returns null.
	 */
	@Query("select e from MoyenCalcul e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<MoyenCalcul> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds MoyenCalcul by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object MoyenCalcul whose updatedBy is equals to the given updatedBy. If
	 *         no MoyenCalcul is found, this method returns null.
	 */
	@Query("select e from MoyenCalcul e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<MoyenCalcul> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds MoyenCalcul by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object MoyenCalcul whose deletedAt is equals to the given deletedAt. If
	 *         no MoyenCalcul is found, this method returns null.
	 */
	@Query("select e from MoyenCalcul e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<MoyenCalcul> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds MoyenCalcul by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object MoyenCalcul whose deletedBy is equals to the given deletedBy. If
	 *         no MoyenCalcul is found, this method returns null.
	 */
	@Query("select e from MoyenCalcul e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<MoyenCalcul> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds MoyenCalcul by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object MoyenCalcul whose isDeleted is equals to the given isDeleted. If
	 *         no MoyenCalcul is found, this method returns null.
	 */
	@Query("select e from MoyenCalcul e where e.isDeleted = :isDeleted")
	List<MoyenCalcul> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds MoyenCalcul by using sousCritereNoteId as a search criteria.
	 *
	 * @param sousCritereNoteId
	 * @return A list of Object MoyenCalcul whose sousCritereNoteId is equals to the given sousCritereNoteId. If
	 *         no MoyenCalcul is found, this method returns null.
	 */
	@Query("select e from MoyenCalcul e where e.sousCritereNote.id = :sousCritereNoteId and e.isDeleted = :isDeleted")
	List<MoyenCalcul> findBySousCritereNoteId(@Param("sousCritereNoteId")Integer sousCritereNoteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one MoyenCalcul by using sousCritereNoteId as a search criteria.
   *
   * @param sousCritereNoteId
   * @return An Object MoyenCalcul whose sousCritereNoteId is equals to the given sousCritereNoteId. If
   *         no MoyenCalcul is found, this method returns null.
   */
  @Query("select e from MoyenCalcul e where e.sousCritereNote.id = :sousCritereNoteId and e.isDeleted = :isDeleted")
  MoyenCalcul findMoyenCalculBySousCritereNoteId(@Param("sousCritereNoteId")Integer sousCritereNoteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of MoyenCalcul by using moyenCalculDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of MoyenCalcul
	 * @throws DataAccessException,ParseException
	 */
	public default List<MoyenCalcul> getByCriteria(Request<MoyenCalculDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from MoyenCalcul e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<MoyenCalcul> query = em.createQuery(req, MoyenCalcul.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of MoyenCalcul by using moyenCalculDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of MoyenCalcul
	 *
	 */
	public default Long count(Request<MoyenCalculDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from MoyenCalcul e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<MoyenCalculDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		MoyenCalculDto dto = request.getData() != null ? request.getData() : new MoyenCalculDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (MoyenCalculDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(MoyenCalculDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (dto.getValeur()!= null && dto.getValeur() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("valeur", dto.getValeur(), "e.valeur", "Integer", dto.getValeurParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getSousCritereNoteId()!= null && dto.getSousCritereNoteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("sousCritereNoteId", dto.getSousCritereNoteId(), "e.sousCritereNote.id", "Integer", dto.getSousCritereNoteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSousCritereNoteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("sousCritereNoteLibelle", dto.getSousCritereNoteLibelle(), "e.sousCritereNote.libelle", "String", dto.getSousCritereNoteLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
