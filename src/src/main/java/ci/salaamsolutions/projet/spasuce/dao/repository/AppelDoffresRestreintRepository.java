package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._AppelDoffresRestreintRepository;

/**
 * Repository : AppelDoffresRestreint.
 */
@Repository
public interface AppelDoffresRestreintRepository extends JpaRepository<AppelDoffresRestreint, Integer>, _AppelDoffresRestreintRepository {
	/**
	 * Finds AppelDoffresRestreint by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object AppelDoffresRestreint whose id is equals to the given id. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.id = :id and e.isDeleted = :isDeleted")
	AppelDoffresRestreint findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AppelDoffresRestreint by using isDesactiver as a search criteria.
	 *
	 * @param isDesactiver
	 * @return An Object AppelDoffresRestreint whose isDesactiver is equals to the given isDesactiver. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.isDesactiver = :isDesactiver and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByIsDesactiver(@Param("isDesactiver")Boolean isDesactiver, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using isSelectedByClient as a search criteria.
	 *
	 * @param isSelectedByClient
	 * @return An Object AppelDoffresRestreint whose isSelectedByClient is equals to the given isSelectedByClient. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.isSelectedByClient = :isSelectedByClient and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByIsSelectedByClient(@Param("isSelectedByClient")Boolean isSelectedByClient, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using isVisibleByFournisseur as a search criteria.
	 *
	 * @param isVisibleByFournisseur
	 * @return An Object AppelDoffresRestreint whose isVisibleByFournisseur is equals to the given isVisibleByFournisseur. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.isVisibleByFournisseur = :isVisibleByFournisseur and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByIsVisibleByFournisseur(@Param("isVisibleByFournisseur")Boolean isVisibleByFournisseur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using isVisualiserNotifByFournisseur as a search criteria.
	 *
	 * @param isVisualiserNotifByFournisseur
	 * @return An Object AppelDoffresRestreint whose isVisualiserNotifByFournisseur is equals to the given isVisualiserNotifByFournisseur. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.isVisualiserNotifByFournisseur = :isVisualiserNotifByFournisseur and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByIsVisualiserNotifByFournisseur(@Param("isVisualiserNotifByFournisseur")Boolean isVisualiserNotifByFournisseur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using isVisualiserContenuByFournisseur as a search criteria.
	 *
	 * @param isVisualiserContenuByFournisseur
	 * @return An Object AppelDoffresRestreint whose isVisualiserContenuByFournisseur is equals to the given isVisualiserContenuByFournisseur. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.isVisualiserContenuByFournisseur = :isVisualiserContenuByFournisseur and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByIsVisualiserContenuByFournisseur(@Param("isVisualiserContenuByFournisseur")Boolean isVisualiserContenuByFournisseur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using isUpdateAppelDoffres as a search criteria.
	 *
	 * @param isUpdateAppelDoffres
	 * @return An Object AppelDoffresRestreint whose isUpdateAppelDoffres is equals to the given isUpdateAppelDoffres. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.isUpdateAppelDoffres = :isUpdateAppelDoffres and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByIsUpdateAppelDoffres(@Param("isUpdateAppelDoffres")Boolean isUpdateAppelDoffres, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using isSoumissionnerByFournisseur as a search criteria.
	 *
	 * @param isSoumissionnerByFournisseur
	 * @return An Object AppelDoffresRestreint whose isSoumissionnerByFournisseur is equals to the given isSoumissionnerByFournisseur. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.isSoumissionnerByFournisseur = :isSoumissionnerByFournisseur and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByIsSoumissionnerByFournisseur(@Param("isSoumissionnerByFournisseur")Boolean isSoumissionnerByFournisseur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using isAppelRestreint as a search criteria.
	 *
	 * @param isAppelRestreint
	 * @return An Object AppelDoffresRestreint whose isAppelRestreint is equals to the given isAppelRestreint. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.isAppelRestreint = :isAppelRestreint and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByIsAppelRestreint(@Param("isAppelRestreint")Boolean isAppelRestreint, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object AppelDoffresRestreint whose createdAt is equals to the given createdAt. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object AppelDoffresRestreint whose createdBy is equals to the given createdBy. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object AppelDoffresRestreint whose updatedAt is equals to the given updatedAt. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object AppelDoffresRestreint whose updatedBy is equals to the given updatedBy. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object AppelDoffresRestreint whose deletedAt is equals to the given deletedAt. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object AppelDoffresRestreint whose deletedBy is equals to the given deletedBy. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffresRestreint by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object AppelDoffresRestreint whose isDeleted is equals to the given isDeleted. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AppelDoffresRestreint by using userFournisseurId as a search criteria.
	 *
	 * @param userFournisseurId
	 * @return A list of Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.user.id = :userFournisseurId and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByUserFournisseurId(@Param("userFournisseurId")Integer userFournisseurId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one AppelDoffresRestreint by using userFournisseurId as a search criteria.
   *
   * @param userFournisseurId
   * @return An Object AppelDoffresRestreint whose userFournisseurId is equals to the given userFournisseurId. If
   *         no AppelDoffresRestreint is found, this method returns null.
   */
  @Query("select e from AppelDoffresRestreint e where e.user.id = :userFournisseurId and e.isDeleted = :isDeleted")
  AppelDoffresRestreint findAppelDoffresRestreintByUserFournisseurId(@Param("userFournisseurId")Integer userFournisseurId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds AppelDoffresRestreint by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object AppelDoffresRestreint whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no AppelDoffresRestreint is found, this method returns null.
	 */
	@Query("select e from AppelDoffresRestreint e where e.appelDoffres.id = :appelDoffresId and e.isDeleted = :isDeleted")
	List<AppelDoffresRestreint> findByAppelDoffresId(@Param("appelDoffresId")Integer appelDoffresId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one AppelDoffresRestreint by using appelDoffresId as a search criteria.
   *
   * @param appelDoffresId
   * @return An Object AppelDoffresRestreint whose appelDoffresId is equals to the given appelDoffresId. If
   *         no AppelDoffresRestreint is found, this method returns null.
   */
  @Query("select e from AppelDoffresRestreint e where e.appelDoffres.id = :appelDoffresId and e.isDeleted = :isDeleted")
  AppelDoffresRestreint findAppelDoffresRestreintByAppelDoffresId(@Param("appelDoffresId")Integer appelDoffresId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of AppelDoffresRestreint by using appelDoffresRestreintDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of AppelDoffresRestreint
	 * @throws DataAccessException,ParseException
	 */
	public default List<AppelDoffresRestreint> getByCriteria(Request<AppelDoffresRestreintDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from AppelDoffresRestreint e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<AppelDoffresRestreint> query = em.createQuery(req, AppelDoffresRestreint.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of AppelDoffresRestreint by using appelDoffresRestreintDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of AppelDoffresRestreint
	 *
	 */
	public default Long count(Request<AppelDoffresRestreintDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from AppelDoffresRestreint e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<AppelDoffresRestreintDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		AppelDoffresRestreintDto dto = request.getData() != null ? request.getData() : new AppelDoffresRestreintDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (AppelDoffresRestreintDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(AppelDoffresRestreintDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getIsDesactiver()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDesactiver", dto.getIsDesactiver(), "e.isDesactiver", "Boolean", dto.getIsDesactiverParam(), param, index, locale));
			}
			if (dto.getIsSelectedByClient()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isSelectedByClient", dto.getIsSelectedByClient(), "e.isSelectedByClient", "Boolean", dto.getIsSelectedByClientParam(), param, index, locale));
			}
			if (dto.getIsVisibleByFournisseur()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isVisibleByFournisseur", dto.getIsVisibleByFournisseur(), "e.isVisibleByFournisseur", "Boolean", dto.getIsVisibleByFournisseurParam(), param, index, locale));
			}
			if (dto.getIsVisualiserNotifByFournisseur()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isVisualiserNotifByFournisseur", dto.getIsVisualiserNotifByFournisseur(), "e.isVisualiserNotifByFournisseur", "Boolean", dto.getIsVisualiserNotifByFournisseurParam(), param, index, locale));
			}
			if (dto.getIsVisualiserContenuByFournisseur()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isVisualiserContenuByFournisseur", dto.getIsVisualiserContenuByFournisseur(), "e.isVisualiserContenuByFournisseur", "Boolean", dto.getIsVisualiserContenuByFournisseurParam(), param, index, locale));
			}
			if (dto.getIsUpdateAppelDoffres()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isUpdateAppelDoffres", dto.getIsUpdateAppelDoffres(), "e.isUpdateAppelDoffres", "Boolean", dto.getIsUpdateAppelDoffresParam(), param, index, locale));
			}
			if (dto.getIsSoumissionnerByFournisseur()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isSoumissionnerByFournisseur", dto.getIsSoumissionnerByFournisseur(), "e.isSoumissionnerByFournisseur", "Boolean", dto.getIsSoumissionnerByFournisseurParam(), param, index, locale));
			}
			if (dto.getIsAppelRestreint()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isAppelRestreint", dto.getIsAppelRestreint(), "e.isAppelRestreint", "Boolean", dto.getIsAppelRestreintParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getUserFournisseurId()!= null && dto.getUserFournisseurId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userFournisseurId", dto.getUserFournisseurId(), "e.user.id", "Integer", dto.getUserFournisseurIdParam(), param, index, locale));
			}
			if (dto.getAppelDoffresId()!= null && dto.getAppelDoffresId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("appelDoffresId", dto.getAppelDoffresId(), "e.appelDoffres.id", "Integer", dto.getAppelDoffresIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserPrenoms())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenoms", dto.getUserPrenoms(), "e.user.prenoms", "String", dto.getUserPrenomsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getAppelDoffresLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("appelDoffresLibelle", dto.getAppelDoffresLibelle(), "e.appelDoffres.libelle", "String", dto.getAppelDoffresLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getAppelDoffresCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("appelDoffresCode", dto.getAppelDoffresCode(), "e.appelDoffres.code", "String", dto.getAppelDoffresCodeParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
