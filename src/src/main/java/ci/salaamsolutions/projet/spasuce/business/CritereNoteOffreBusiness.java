


                                                                                            /*
 * Java transformer for entity table critere_note_offre
 * Created on 2020-01-02 ( Time 17:59:56 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "critere_note_offre"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class CritereNoteOffreBusiness implements IBasicBusiness<Request<CritereNoteOffreDto>, Response<CritereNoteOffreDto>> {

  private Response<CritereNoteOffreDto> response;
  @Autowired
  private CritereNoteOffreRepository critereNoteOffreRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private SousCritereNoteRepository sousCritereNoteRepository;
    @Autowired
  private OffreRepository offreRepository;
    @Autowired
  private MoyenCalculRepository moyenCalculRepository;
    @Autowired
  private CritereNoteRepository critereNoteRepository;
  
  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

                  

  public CritereNoteOffreBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create CritereNoteOffre by using CritereNoteOffreDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<CritereNoteOffreDto> create(Request<CritereNoteOffreDto> request, Locale locale)  {
    slf4jLogger.info("----begin create CritereNoteOffre-----");

    response = new Response<CritereNoteOffreDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<CritereNoteOffre> items = new ArrayList<CritereNoteOffre>();

      for (CritereNoteOffreDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("valeurPartielle", dto.getValeurPartielle());
        fieldsToVerify.put("offreId", dto.getOffreId());
        fieldsToVerify.put("critereNoteId", dto.getCritereNoteId());
        fieldsToVerify.put("sousCritereNoteId", dto.getSousCritereNoteId());
        fieldsToVerify.put("moyenCalculId", dto.getMoyenCalculId());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if critereNoteOffre to insert do not exist
        CritereNoteOffre existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("critereNoteOffre -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if sousCritereNote exist
                SousCritereNote existingSousCritereNote = sousCritereNoteRepository.findById(dto.getSousCritereNoteId(), false);
                if (existingSousCritereNote == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("sousCritereNote -> " + dto.getSousCritereNoteId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if offre exist
                Offre existingOffre = offreRepository.findById(dto.getOffreId(), false);
                if (existingOffre == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("offre -> " + dto.getOffreId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if moyenCalcul exist
                MoyenCalcul existingMoyenCalcul = moyenCalculRepository.findById(dto.getMoyenCalculId(), false);
                if (existingMoyenCalcul == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("moyenCalcul -> " + dto.getMoyenCalculId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if critereNote exist
                CritereNote existingCritereNote = critereNoteRepository.findById(dto.getCritereNoteId(), false);
                if (existingCritereNote == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("critereNote -> " + dto.getCritereNoteId(), locale));
          response.setHasError(true);
          return response;
        }
        CritereNoteOffre entityToSave = null;
        entityToSave = CritereNoteOffreTransformer.INSTANCE.toEntity(dto, existingSousCritereNote, existingOffre, existingMoyenCalcul, existingCritereNote);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        entityToSave.setIsDeleted(false);
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<CritereNoteOffre> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = critereNoteOffreRepository.save((Iterable<CritereNoteOffre>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("critereNoteOffre", locale));
          response.setHasError(true);
          return response;
        }
        List<CritereNoteOffreDto> itemsDto = new ArrayList<CritereNoteOffreDto>();
        for (CritereNoteOffre entity : itemsSaved) {
          CritereNoteOffreDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create CritereNoteOffre-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update CritereNoteOffre by using CritereNoteOffreDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<CritereNoteOffreDto> update(Request<CritereNoteOffreDto> request, Locale locale)  {
    slf4jLogger.info("----begin update CritereNoteOffre-----");

    response = new Response<CritereNoteOffreDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<CritereNoteOffre> items = new ArrayList<CritereNoteOffre>();

      for (CritereNoteOffreDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la critereNoteOffre existe
        CritereNoteOffre entityToSave = null;
        entityToSave = critereNoteOffreRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("critereNoteOffre -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if sousCritereNote exist
        if (dto.getSousCritereNoteId() != null && dto.getSousCritereNoteId() > 0){
                    SousCritereNote existingSousCritereNote = sousCritereNoteRepository.findById(dto.getSousCritereNoteId(), false);
          if (existingSousCritereNote == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("sousCritereNote -> " + dto.getSousCritereNoteId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setSousCritereNote(existingSousCritereNote);
        }
        // Verify if offre exist
        if (dto.getOffreId() != null && dto.getOffreId() > 0){
                    Offre existingOffre = offreRepository.findById(dto.getOffreId(), false);
          if (existingOffre == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("offre -> " + dto.getOffreId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setOffre(existingOffre);
        }
        // Verify if moyenCalcul exist
        if (dto.getMoyenCalculId() != null && dto.getMoyenCalculId() > 0){
                    MoyenCalcul existingMoyenCalcul = moyenCalculRepository.findById(dto.getMoyenCalculId(), false);
          if (existingMoyenCalcul == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("moyenCalcul -> " + dto.getMoyenCalculId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setMoyenCalcul(existingMoyenCalcul);
        }
        // Verify if critereNote exist
        if (dto.getCritereNoteId() != null && dto.getCritereNoteId() > 0){
                    CritereNote existingCritereNote = critereNoteRepository.findById(dto.getCritereNoteId(), false);
          if (existingCritereNote == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("critereNote -> " + dto.getCritereNoteId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setCritereNote(existingCritereNote);
        }
        if (dto.getValeurPartielle() != null && dto.getValeurPartielle() > 0) {
          entityToSave.setValeurPartielle(dto.getValeurPartielle());
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<CritereNoteOffre> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = critereNoteOffreRepository.save((Iterable<CritereNoteOffre>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("critereNoteOffre", locale));
          response.setHasError(true);
          return response;
        }
        List<CritereNoteOffreDto> itemsDto = new ArrayList<CritereNoteOffreDto>();
        for (CritereNoteOffre entity : itemsSaved) {
          CritereNoteOffreDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update CritereNoteOffre-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete CritereNoteOffre by using CritereNoteOffreDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<CritereNoteOffreDto> delete(Request<CritereNoteOffreDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete CritereNoteOffre-----");

    response = new Response<CritereNoteOffreDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<CritereNoteOffre> items = new ArrayList<CritereNoteOffre>();

      for (CritereNoteOffreDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la critereNoteOffre existe
        CritereNoteOffre existingEntity = null;
        existingEntity = critereNoteOffreRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("critereNoteOffre -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        existingEntity.setIsDeleted(true);
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        critereNoteOffreRepository.save((Iterable<CritereNoteOffre>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete CritereNoteOffre-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete CritereNoteOffre by using CritereNoteOffreDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<CritereNoteOffreDto> forceDelete(Request<CritereNoteOffreDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete CritereNoteOffre-----");

    response = new Response<CritereNoteOffreDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<CritereNoteOffre> items = new ArrayList<CritereNoteOffre>();

      for (CritereNoteOffreDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la critereNoteOffre existe
        CritereNoteOffre existingEntity = null;
          existingEntity = critereNoteOffreRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("critereNoteOffre -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

                  

                                                  existingEntity.setDeletedAt(Utilities.getCurrentDate());
                    existingEntity.setDeletedBy(request.getUser());
              existingEntity.setIsDeleted(true);
              items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          critereNoteOffreRepository.save((Iterable<CritereNoteOffre>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete CritereNoteOffre-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get CritereNoteOffre by using CritereNoteOffreDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<CritereNoteOffreDto> getByCriteria(Request<CritereNoteOffreDto> request, Locale locale) {
    slf4jLogger.info("----begin get CritereNoteOffre-----");

    response = new Response<CritereNoteOffreDto>();

    try {
      List<CritereNoteOffre> items = null;
      items = critereNoteOffreRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<CritereNoteOffreDto> itemsDto = new ArrayList<CritereNoteOffreDto>();
        for (CritereNoteOffre entity : items) {
          CritereNoteOffreDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(critereNoteOffreRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("critereNoteOffre", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get CritereNoteOffre-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full CritereNoteOffreDto by using CritereNoteOffre as object.
   *
   * @param entity, locale
   * @return CritereNoteOffreDto
   *
   */
  private CritereNoteOffreDto getFullInfos(CritereNoteOffre entity, Integer size, Locale locale) throws Exception {
    CritereNoteOffreDto dto = CritereNoteOffreTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
