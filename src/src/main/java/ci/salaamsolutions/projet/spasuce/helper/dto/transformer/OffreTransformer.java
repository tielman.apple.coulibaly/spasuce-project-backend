

/*
 * Java transformer for entity table offre 
 * Created on 2020-01-08 ( Time 09:47:27 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "offre"
 * 
 * @author Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface OffreTransformer {

	OffreTransformer INSTANCE = Mappers.getMapper(OffreTransformer.class);

	@Mappings({

		@Mapping(source="entity.dateSoumission", dateFormat="dd/MM/yyyy",target="dateSoumission"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.user.id", target="userFournisseurId"),
		@Mapping(source="entity.user.nom", target="userNom"),
		@Mapping(source="entity.user.prenoms", target="userPrenoms"),
		@Mapping(source="entity.user.login", target="userLogin"),
		@Mapping(source="entity.user.entreprise.nom", target="entrepriseNom"), // Ajouter
		@Mapping(source="entity.appelDoffres.isAttribuer", target="isAttribuerAppelDoffres"), // Ajouter
		@Mapping(source="entity.appelDoffres.id", target="appelDoffresId"),
		@Mapping(source="entity.appelDoffres.libelle", target="appelDoffresLibelle"),
		@Mapping(source="entity.appelDoffres.code", target="appelDoffresCode"),
		@Mapping(source="entity.etat.id", target="etatId"),
		@Mapping(source="entity.etat.code", target="etatCode"),
		@Mapping(source="entity.etat.libelle", target="etatLibelle"),
		@Mapping(source="entity.classeNote.id", target="classeNoteId"),
		@Mapping(source="entity.classeNote.code", target="classeNoteCode"),
		@Mapping(source="entity.classeNote.libelle", target="classeNoteLibelle"),
	})
	OffreDto toDto(Offre entity) throws ParseException;

	List<OffreDto> toDtos(List<Offre> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.codeContrat", target="codeContrat"),
		@Mapping(source="dto.messageClient", target="messageClient"),
		@Mapping(source="dto.isSelectedByClient", target="isSelectedByClient"),
		@Mapping(source="dto.commentaire", target="commentaire"),
		@Mapping(source="dto.dateSoumission", dateFormat="dd/MM/yyyy",target="dateSoumission"),
		@Mapping(source="dto.isPayer", target="isPayer"),
		@Mapping(source="dto.isNoterByClient", target="isNoterByClient"),
		@Mapping(source="dto.isVisualiserNoteByFournisseur", target="isVisualiserNoteByFournisseur"),
		@Mapping(source="dto.isVisualiserNotifByClient", target="isVisualiserNotifByClient"),
		@Mapping(source="dto.isVisualiserContenuByClient", target="isVisualiserContenuByClient"),
		@Mapping(source="dto.isModifiable", target="isModifiable"),
		@Mapping(source="dto.isUpdate", target="isUpdate"),
		@Mapping(source="dto.isRetirer", target="isRetirer"),
		@Mapping(source="dto.prix", target="prix"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="user", target="user"),
		@Mapping(source="appelDoffres", target="appelDoffres"),
		@Mapping(source="etat", target="etat"),
		@Mapping(source="classeNote", target="classeNote"),
	})
	Offre toEntity(OffreDto dto, User user, AppelDoffres appelDoffres, ClasseNote classeNote, Etat etat) throws ParseException;

	//List<Offre> toEntities(List<OffreDto> dtos) throws ParseException;

}
