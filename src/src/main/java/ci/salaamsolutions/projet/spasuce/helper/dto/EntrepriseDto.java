
/*
 * Java dto for entity table entreprise
 * Created on 2020-02-07 ( Time 09:01:54 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.customize._EntrepriseDto;

import lombok.*;
/**
 * DTO for table "entreprise"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class EntrepriseDto extends _EntrepriseDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private String     nom                  ;
	/*
	 * 
	 */
    private String     email                ;
	/*
	 * 
	 */
    private String     numeroImmatriculation ;
	/*
	 * 
	 */
    private String     numeroDuns           ;
	/*
	 * 
	 */
    private String     password             ;
	/*
	 * 
	 */
    private String     numeroFix            ;
	/*
	 * 
	 */
    private String     numeroFax            ;
	/*
	 * 
	 */
    private String     telephone            ;
	/*
	 * 
	 */
    private String     login                ;
	/*
	 * 
	 */
    private String     urlLogo              ;
	/*
	 * 
	 */
    private String     extensionLogo        ;
	/*
	 * 
	 */
    private String     description          ;
	/*
	 * elle correspond à la moyenne de toute les notes obtenus
	 */
    private Float      note                 ;
	/*
	 * 
	 */
    private Boolean    isLocked             ;
	/*
	 * 
	 */
    private Integer    villeId              ;
	/*
	 * 
	 */
    private Integer    typeEntrepriseId     ;
	/*
	 * 
	 */
    private Integer    statutJuridiqueId    ;
	/*
	 * 
	 */
    private Integer    classeNoteId         ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String villeCode;
	private String villeLibelle;
	private String statutJuridiqueCode;
	private String statutJuridiqueLibelle;
	private String classeNoteCode;
	private String classeNoteLibelle;
	private String typeEntrepriseCode;
	private String typeEntrepriseLibelle;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   nomParam              ;                     
	private SearchParam<String>   emailParam            ;                     
	private SearchParam<String>   numeroImmatriculationParam;                     
	private SearchParam<String>   numeroDunsParam       ;                     
	private SearchParam<String>   passwordParam         ;                     
	private SearchParam<String>   numeroFixParam        ;                     
	private SearchParam<String>   numeroFaxParam        ;                     
	private SearchParam<String>   telephoneParam        ;                     
	private SearchParam<String>   loginParam            ;                     
	private SearchParam<String>   urlLogoParam          ;                     
	private SearchParam<String>   extensionLogoParam    ;                     
	private SearchParam<String>   descriptionParam      ;                     
	private SearchParam<Float>    noteParam             ;                     
	private SearchParam<Boolean>  isLockedParam         ;                     
	private SearchParam<Integer>  villeIdParam          ;                     
	private SearchParam<Integer>  typeEntrepriseIdParam ;                     
	private SearchParam<Integer>  statutJuridiqueIdParam;                     
	private SearchParam<Integer>  classeNoteIdParam     ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<String>   villeCodeParam        ;                     
	private SearchParam<String>   villeLibelleParam     ;                     
	private SearchParam<String>   statutJuridiqueCodeParam;                     
	private SearchParam<String>   statutJuridiqueLibelleParam;                     
	private SearchParam<String>   classeNoteCodeParam   ;                     
	private SearchParam<String>   classeNoteLibelleParam;                     
	private SearchParam<String>   typeEntrepriseCodeParam;                     
	private SearchParam<String>   typeEntrepriseLibelleParam;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		EntrepriseDto other = (EntrepriseDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
