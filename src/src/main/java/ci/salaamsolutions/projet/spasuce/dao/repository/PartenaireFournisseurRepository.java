package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._PartenaireFournisseurRepository;

/**
 * Repository : PartenaireFournisseur.
 */
@Repository
public interface PartenaireFournisseurRepository extends JpaRepository<PartenaireFournisseur, Integer>, _PartenaireFournisseurRepository {
	/**
	 * Finds PartenaireFournisseur by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object PartenaireFournisseur whose id is equals to the given id. If
	 *         no PartenaireFournisseur is found, this method returns null.
	 */
	@Query("select e from PartenaireFournisseur e where e.id = :id and e.isDeleted = :isDeleted")
	PartenaireFournisseur findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds PartenaireFournisseur by using nom as a search criteria.
	 *
	 * @param nom
	 * @return An Object PartenaireFournisseur whose nom is equals to the given nom. If
	 *         no PartenaireFournisseur is found, this method returns null.
	 */
	@Query("select e from PartenaireFournisseur e where e.nom = :nom and e.isDeleted = :isDeleted")
	List<PartenaireFournisseur> findByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PartenaireFournisseur by using urlLogo as a search criteria.
	 *
	 * @param urlLogo
	 * @return An Object PartenaireFournisseur whose urlLogo is equals to the given urlLogo. If
	 *         no PartenaireFournisseur is found, this method returns null.
	 */
	@Query("select e from PartenaireFournisseur e where e.urlLogo = :urlLogo and e.isDeleted = :isDeleted")
	List<PartenaireFournisseur> findByUrlLogo(@Param("urlLogo")String urlLogo, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PartenaireFournisseur by using extension as a search criteria.
	 *
	 * @param extension
	 * @return An Object PartenaireFournisseur whose extension is equals to the given extension. If
	 *         no PartenaireFournisseur is found, this method returns null.
	 */
	@Query("select e from PartenaireFournisseur e where e.extension = :extension and e.isDeleted = :isDeleted")
	List<PartenaireFournisseur> findByExtension(@Param("extension")String extension, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PartenaireFournisseur by using description as a search criteria.
	 *
	 * @param description
	 * @return An Object PartenaireFournisseur whose description is equals to the given description. If
	 *         no PartenaireFournisseur is found, this method returns null.
	 */
	@Query("select e from PartenaireFournisseur e where e.description = :description and e.isDeleted = :isDeleted")
	List<PartenaireFournisseur> findByDescription(@Param("description")String description, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PartenaireFournisseur by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object PartenaireFournisseur whose createdAt is equals to the given createdAt. If
	 *         no PartenaireFournisseur is found, this method returns null.
	 */
	@Query("select e from PartenaireFournisseur e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<PartenaireFournisseur> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PartenaireFournisseur by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object PartenaireFournisseur whose createdBy is equals to the given createdBy. If
	 *         no PartenaireFournisseur is found, this method returns null.
	 */
	@Query("select e from PartenaireFournisseur e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<PartenaireFournisseur> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PartenaireFournisseur by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object PartenaireFournisseur whose updatedAt is equals to the given updatedAt. If
	 *         no PartenaireFournisseur is found, this method returns null.
	 */
	@Query("select e from PartenaireFournisseur e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<PartenaireFournisseur> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PartenaireFournisseur by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object PartenaireFournisseur whose updatedBy is equals to the given updatedBy. If
	 *         no PartenaireFournisseur is found, this method returns null.
	 */
	@Query("select e from PartenaireFournisseur e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<PartenaireFournisseur> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PartenaireFournisseur by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object PartenaireFournisseur whose deletedAt is equals to the given deletedAt. If
	 *         no PartenaireFournisseur is found, this method returns null.
	 */
	@Query("select e from PartenaireFournisseur e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<PartenaireFournisseur> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PartenaireFournisseur by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object PartenaireFournisseur whose deletedBy is equals to the given deletedBy. If
	 *         no PartenaireFournisseur is found, this method returns null.
	 */
	@Query("select e from PartenaireFournisseur e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<PartenaireFournisseur> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PartenaireFournisseur by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object PartenaireFournisseur whose isDeleted is equals to the given isDeleted. If
	 *         no PartenaireFournisseur is found, this method returns null.
	 */
	@Query("select e from PartenaireFournisseur e where e.isDeleted = :isDeleted")
	List<PartenaireFournisseur> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds PartenaireFournisseur by using entrepriseId as a search criteria.
	 *
	 * @param entrepriseId
	 * @return A list of Object PartenaireFournisseur whose entrepriseId is equals to the given entrepriseId. If
	 *         no PartenaireFournisseur is found, this method returns null.
	 */
	@Query("select e from PartenaireFournisseur e where e.entreprise.id = :entrepriseId and e.isDeleted = :isDeleted")
	List<PartenaireFournisseur> findByEntrepriseId(@Param("entrepriseId")Integer entrepriseId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PartenaireFournisseur by using entrepriseId as a search criteria.
   *
   * @param entrepriseId
   * @return An Object PartenaireFournisseur whose entrepriseId is equals to the given entrepriseId. If
   *         no PartenaireFournisseur is found, this method returns null.
   */
  @Query("select e from PartenaireFournisseur e where e.entreprise.id = :entrepriseId and e.isDeleted = :isDeleted")
  PartenaireFournisseur findPartenaireFournisseurByEntrepriseId(@Param("entrepriseId")Integer entrepriseId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of PartenaireFournisseur by using partenaireFournisseurDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of PartenaireFournisseur
	 * @throws DataAccessException,ParseException
	 */
	public default List<PartenaireFournisseur> getByCriteria(Request<PartenaireFournisseurDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from PartenaireFournisseur e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<PartenaireFournisseur> query = em.createQuery(req, PartenaireFournisseur.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of PartenaireFournisseur by using partenaireFournisseurDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of PartenaireFournisseur
	 *
	 */
	public default Long count(Request<PartenaireFournisseurDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from PartenaireFournisseur e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<PartenaireFournisseurDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		PartenaireFournisseurDto dto = request.getData() != null ? request.getData() : new PartenaireFournisseurDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (PartenaireFournisseurDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(PartenaireFournisseurDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nom", dto.getNom(), "e.nom", "String", dto.getNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUrlLogo())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("urlLogo", dto.getUrlLogo(), "e.urlLogo", "String", dto.getUrlLogoParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getExtension())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("extension", dto.getExtension(), "e.extension", "String", dto.getExtensionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDescription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getEntrepriseId()!= null && dto.getEntrepriseId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseId", dto.getEntrepriseId(), "e.entreprise.id", "Integer", dto.getEntrepriseIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEntrepriseNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseNom", dto.getEntrepriseNom(), "e.entreprise.nom", "String", dto.getEntrepriseNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEntrepriseLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseLogin", dto.getEntrepriseLogin(), "e.entreprise.login", "String", dto.getEntrepriseLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
