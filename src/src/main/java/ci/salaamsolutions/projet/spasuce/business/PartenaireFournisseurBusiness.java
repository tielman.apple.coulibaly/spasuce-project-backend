


                                                                                            /*
 * Java transformer for entity table partenaire_fournisseur
 * Created on 2020-01-02 ( Time 18:00:01 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "partenaire_fournisseur"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class PartenaireFournisseurBusiness implements IBasicBusiness<Request<PartenaireFournisseurDto>, Response<PartenaireFournisseurDto>> {

  private Response<PartenaireFournisseurDto> response;
  @Autowired
  private PartenaireFournisseurRepository partenaireFournisseurRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private EntrepriseRepository entrepriseRepository;
  
  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

      

  public PartenaireFournisseurBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create PartenaireFournisseur by using PartenaireFournisseurDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PartenaireFournisseurDto> create(Request<PartenaireFournisseurDto> request, Locale locale)  {
    slf4jLogger.info("----begin create PartenaireFournisseur-----");

    response = new Response<PartenaireFournisseurDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PartenaireFournisseur> items = new ArrayList<PartenaireFournisseur>();

      for (PartenaireFournisseurDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("nom", dto.getNom());
        fieldsToVerify.put("urlLogo", dto.getUrlLogo());
        fieldsToVerify.put("extension", dto.getExtension());
        fieldsToVerify.put("description", dto.getDescription());
        fieldsToVerify.put("entrepriseId", dto.getEntrepriseId());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if partenaireFournisseur to insert do not exist
        PartenaireFournisseur existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("partenaireFournisseur -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if entreprise exist
                Entreprise existingEntreprise = entrepriseRepository.findById(dto.getEntrepriseId(), false);
                if (existingEntreprise == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("entreprise -> " + dto.getEntrepriseId(), locale));
          response.setHasError(true);
          return response;
        }
        PartenaireFournisseur entityToSave = null;
        entityToSave = PartenaireFournisseurTransformer.INSTANCE.toEntity(dto, existingEntreprise);
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setCreatedBy(request.getUser());
        entityToSave.setIsDeleted(false);
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<PartenaireFournisseur> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = partenaireFournisseurRepository.save((Iterable<PartenaireFournisseur>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("partenaireFournisseur", locale));
          response.setHasError(true);
          return response;
        }
        List<PartenaireFournisseurDto> itemsDto = new ArrayList<PartenaireFournisseurDto>();
        for (PartenaireFournisseur entity : itemsSaved) {
          PartenaireFournisseurDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create PartenaireFournisseur-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update PartenaireFournisseur by using PartenaireFournisseurDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PartenaireFournisseurDto> update(Request<PartenaireFournisseurDto> request, Locale locale)  {
    slf4jLogger.info("----begin update PartenaireFournisseur-----");

    response = new Response<PartenaireFournisseurDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PartenaireFournisseur> items = new ArrayList<PartenaireFournisseur>();

      for (PartenaireFournisseurDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la partenaireFournisseur existe
        PartenaireFournisseur entityToSave = null;
        entityToSave = partenaireFournisseurRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("partenaireFournisseur -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if entreprise exist
        if (dto.getEntrepriseId() != null && dto.getEntrepriseId() > 0){
                    Entreprise existingEntreprise = entrepriseRepository.findById(dto.getEntrepriseId(), false);
          if (existingEntreprise == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("entreprise -> " + dto.getEntrepriseId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setEntreprise(existingEntreprise);
        }
        if (Utilities.notBlank(dto.getNom())) {
                  entityToSave.setNom(dto.getNom());
        }
        if (Utilities.notBlank(dto.getUrlLogo())) {
                  entityToSave.setUrlLogo(dto.getUrlLogo());
        }
        if (Utilities.notBlank(dto.getExtension())) {
                  entityToSave.setExtension(dto.getExtension());
        }
        if (Utilities.notBlank(dto.getDescription())) {
                  entityToSave.setDescription(dto.getDescription());
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        entityToSave.setUpdatedBy(request.getUser());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<PartenaireFournisseur> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = partenaireFournisseurRepository.save((Iterable<PartenaireFournisseur>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("partenaireFournisseur", locale));
          response.setHasError(true);
          return response;
        }
        List<PartenaireFournisseurDto> itemsDto = new ArrayList<PartenaireFournisseurDto>();
        for (PartenaireFournisseur entity : itemsSaved) {
          PartenaireFournisseurDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update PartenaireFournisseur-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete PartenaireFournisseur by using PartenaireFournisseurDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PartenaireFournisseurDto> delete(Request<PartenaireFournisseurDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete PartenaireFournisseur-----");

    response = new Response<PartenaireFournisseurDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PartenaireFournisseur> items = new ArrayList<PartenaireFournisseur>();

      for (PartenaireFournisseurDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la partenaireFournisseur existe
        PartenaireFournisseur existingEntity = null;
        existingEntity = partenaireFournisseurRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("partenaireFournisseur -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setDeletedBy(request.getUser());
        existingEntity.setIsDeleted(true);
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        partenaireFournisseurRepository.save((Iterable<PartenaireFournisseur>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete PartenaireFournisseur-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete PartenaireFournisseur by using PartenaireFournisseurDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<PartenaireFournisseurDto> forceDelete(Request<PartenaireFournisseurDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete PartenaireFournisseur-----");

    response = new Response<PartenaireFournisseurDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PartenaireFournisseur> items = new ArrayList<PartenaireFournisseur>();

      for (PartenaireFournisseurDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la partenaireFournisseur existe
        PartenaireFournisseur existingEntity = null;
          existingEntity = partenaireFournisseurRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("partenaireFournisseur -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

      

                                                                          existingEntity.setDeletedAt(Utilities.getCurrentDate());
                    existingEntity.setDeletedBy(request.getUser());
              existingEntity.setIsDeleted(true);
              items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          partenaireFournisseurRepository.save((Iterable<PartenaireFournisseur>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete PartenaireFournisseur-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get PartenaireFournisseur by using PartenaireFournisseurDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<PartenaireFournisseurDto> getByCriteria(Request<PartenaireFournisseurDto> request, Locale locale) {
    slf4jLogger.info("----begin get PartenaireFournisseur-----");

    response = new Response<PartenaireFournisseurDto>();

    try {
      List<PartenaireFournisseur> items = null;
      items = partenaireFournisseurRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<PartenaireFournisseurDto> itemsDto = new ArrayList<PartenaireFournisseurDto>();
        for (PartenaireFournisseur entity : items) {
          PartenaireFournisseurDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(partenaireFournisseurRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("partenaireFournisseur", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get PartenaireFournisseur-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full PartenaireFournisseurDto by using PartenaireFournisseur as object.
   *
   * @param entity, locale
   * @return PartenaireFournisseurDto
   *
   */
  private PartenaireFournisseurDto getFullInfos(PartenaireFournisseur entity, Integer size, Locale locale) throws Exception {
    PartenaireFournisseurDto dto = PartenaireFournisseurTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
