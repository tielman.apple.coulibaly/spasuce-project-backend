

/*
 * Java transformer for entity table objet_nous_contacter 
 * Created on 2020-01-03 ( Time 10:54:34 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "objet_nous_contacter"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface ObjetNousContacterTransformer {

	ObjetNousContacterTransformer INSTANCE = Mappers.getMapper(ObjetNousContacterTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
	})
	ObjetNousContacterDto toDto(ObjetNousContacter entity) throws ParseException;

    List<ObjetNousContacterDto> toDtos(List<ObjetNousContacter> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
	})
    ObjetNousContacter toEntity(ObjetNousContacterDto dto) throws ParseException;

    //List<ObjetNousContacter> toEntities(List<ObjetNousContacterDto> dtos) throws ParseException;

}
