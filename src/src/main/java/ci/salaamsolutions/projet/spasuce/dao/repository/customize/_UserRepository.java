package ci.salaamsolutions.projet.spasuce.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.dao.entity.User;
import ci.salaamsolutions.projet.spasuce.helper.CriteriaUtils;
import ci.salaamsolutions.projet.spasuce.helper.Utilities;
import ci.salaamsolutions.projet.spasuce.helper.dto.UserDto;

/**
 * Repository customize : User.
 */
@Repository
public interface _UserRepository {
	default List<String> _generateCriteria(UserDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE
		
		
		// pour prendre en compte le type de l'entreprise dans le get
		if (Utilities.notBlank(dto.getTypeEntrepriseCode())) {
			listOfQuery.add(CriteriaUtils.generateCriteria("typeEntrepriseCode", dto.getTypeEntrepriseCode(), "e.entreprise.typeEntreprise.code", "String", dto.getTypeEntrepriseCodeParam(), param, index, locale));
		}

		return listOfQuery;
	}
	
	/**
	 * Finds User by using roleCode as a search criteria.
	 *
	 * @param roleCode
	 * @return A list of Object User whose roleCode is equals to the given roleCode. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.role.code = :roleCode and e.isDeleted = :isDeleted")
	List<User> findByRoleCode(@Param("roleCode")String roleCode, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds User by using entrepriseId as a search criteria.
	 *
	 * @param entrepriseId
	 * @return A list of Object User whose entrepriseId is equals to the given entrepriseId. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.entreprise.typeEntreprise.code <> :typeEntrepriseCode and e.entreprise.isDeleted = :isDeleted and e.isDeleted = :isDeleted")
	List<User> findByTypeEntrepriseCodeDifferent(@Param("typeEntrepriseCode")String typeEntrepriseCode, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds User by using login as a search criteria.
	 *
	 * @param login, password
	 * @return An Object User whose login is equals to the given login. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.login = :login and e.password = :password and e.isDeleted = :isDeleted")
	User findByLoginAndPassword(@Param("login")String login, @Param("password")String password, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds User by using login as a search criteria.
	 *
	 * @param login, password
	 * @return An Object User whose login is equals to the given login. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.login = :login and e.isLocked = :isLocked and e.password = :password and e.isDeleted = :isDeleted")
	User findByLoginAndPasswordAndIsLocked(@Param("login")String login, @Param("password")String password, @Param("isLocked")Boolean isLocked, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds User by using login as a search criteria.
	 *
	 * @param token, email
	 * @return An Object User whose login is equals to the given login. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.token = :token and e.email = :email and e.isDeleted = :isDeleted")
	User findUserByTokenAndEmail(@Param("token")String token, @Param("email")String email, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using email as a search criteria.
	 *
	 * @param email
	 * @return An Object User whose email is equals to the given email. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.email = :email and e.entreprise.id <> :entrepriseId and e.isDeleted = :isDeleted")
	List<User> findByEmailAndEntrepriseIdDifferent(@Param("email")String email, @Param("entrepriseId")Integer entrepriseId, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds User by using entrepriseId as a search criteria.
	 *
	 * @param entrepriseId
	 * @return A list of Object User whose entrepriseId is equals to the given entrepriseId. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.entreprise.id = :entrepriseId and e.entreprise.typeEntreprise.code = :typeEntrepriseCode and e.isDeleted = :isDeleted")
	List<User> findByEntrepriseIdAndTypeEntrepriseCode(@Param("entrepriseId")Integer entrepriseId, @Param("typeEntrepriseCode")String typeEntrepriseCode, @Param("isDeleted")Boolean isDeleted);

  
}
