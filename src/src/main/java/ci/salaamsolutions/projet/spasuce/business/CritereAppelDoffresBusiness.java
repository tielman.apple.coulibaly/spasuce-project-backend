


/*
 * Java transformer for entity table critere_appel_doffres
 * Created on 2020-01-02 ( Time 17:59:55 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "critere_appel_doffres"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class CritereAppelDoffresBusiness implements IBasicBusiness<Request<CritereAppelDoffresDto>, Response<CritereAppelDoffresDto>> {

	private Response<CritereAppelDoffresDto> response;
	@Autowired
	private CritereAppelDoffresRepository critereAppelDoffresRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ValeurCritereAppelDoffresRepository valeurCritereAppelDoffresRepository;
	@Autowired
	private ValeurCritereOffreRepository valeurCritereOffreRepository;
	@Autowired
	private TypeCritereAppelDoffresRepository typeCritereAppelDoffresRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private ValeurCritereAppelDoffresBusiness valeurCritereAppelDoffresBusiness;


	@Autowired
	private ValeurCritereOffreBusiness valeurCritereOffreBusiness;



	public CritereAppelDoffresBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create CritereAppelDoffres by using CritereAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<CritereAppelDoffresDto> create(Request<CritereAppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin create CritereAppelDoffres-----");

		response = new Response<CritereAppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<CritereAppelDoffres> items = new ArrayList<CritereAppelDoffres>();

			for (CritereAppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				fieldsToVerify.put("code", dto.getCode());
				fieldsToVerify.put("typeCritereAppelDoffresId", dto.getTypeCritereAppelDoffresId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if critereAppelDoffres to insert do not exist
				CritereAppelDoffres existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("critereAppelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = critereAppelDoffresRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("critereAppelDoffres -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les critereAppelDoffress", locale));
					response.setHasError(true);
					return response;
				}
				
				existingEntity = critereAppelDoffresRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("critereAppelDoffres -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les critereAppelDoffress", locale));
					response.setHasError(true);
					return response;
				}

				// Verify if typeCritereAppelDoffres exist
				TypeCritereAppelDoffres existingTypeCritereAppelDoffres = typeCritereAppelDoffresRepository.findById(dto.getTypeCritereAppelDoffresId(), false);
				if (existingTypeCritereAppelDoffres == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeCritereAppelDoffres -> " + dto.getTypeCritereAppelDoffresId(), locale));
					response.setHasError(true);
					return response;
				}
				CritereAppelDoffres entityToSave = null;
				entityToSave = CritereAppelDoffresTransformer.INSTANCE.toEntity(dto, existingTypeCritereAppelDoffres);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<CritereAppelDoffres> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = critereAppelDoffresRepository.save((Iterable<CritereAppelDoffres>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("critereAppelDoffres", locale));
					response.setHasError(true);
					return response;
				}
				List<CritereAppelDoffresDto> itemsDto = new ArrayList<CritereAppelDoffresDto>();
				for (CritereAppelDoffres entity : itemsSaved) {
					CritereAppelDoffresDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create CritereAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update CritereAppelDoffres by using CritereAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<CritereAppelDoffresDto> update(Request<CritereAppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin update CritereAppelDoffres-----");

		response = new Response<CritereAppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<CritereAppelDoffres> items = new ArrayList<CritereAppelDoffres>();

			for (CritereAppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la critereAppelDoffres existe
				CritereAppelDoffres entityToSave = null;
				entityToSave = critereAppelDoffresRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("critereAppelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if typeCritereAppelDoffres exist
				if (dto.getTypeCritereAppelDoffresId() != null && dto.getTypeCritereAppelDoffresId() > 0){
					TypeCritereAppelDoffres existingTypeCritereAppelDoffres = typeCritereAppelDoffresRepository.findById(dto.getTypeCritereAppelDoffresId(), false);
					if (existingTypeCritereAppelDoffres == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("typeCritereAppelDoffres -> " + dto.getTypeCritereAppelDoffresId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setTypeCritereAppelDoffres(existingTypeCritereAppelDoffres);
				}
				if (Utilities.notBlank(dto.getLibelle())) {
					CritereAppelDoffres existingEntity = critereAppelDoffresRepository.findByLibelle(dto.getLibelle(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("critereAppelDoffres -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les critereAppelDoffress", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
				if (Utilities.notBlank(dto.getCode())) {
					CritereAppelDoffres existingEntity = critereAppelDoffresRepository.findByCode(dto.getCode(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("critereAppelDoffres -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les critereAppelDoffress", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCode(dto.getCode());
				}

				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<CritereAppelDoffres> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = critereAppelDoffresRepository.save((Iterable<CritereAppelDoffres>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("critereAppelDoffres", locale));
					response.setHasError(true);
					return response;
				}
				List<CritereAppelDoffresDto> itemsDto = new ArrayList<CritereAppelDoffresDto>();
				for (CritereAppelDoffres entity : itemsSaved) {
					CritereAppelDoffresDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update CritereAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete CritereAppelDoffres by using CritereAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<CritereAppelDoffresDto> delete(Request<CritereAppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete CritereAppelDoffres-----");

		response = new Response<CritereAppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<CritereAppelDoffres> items = new ArrayList<CritereAppelDoffres>();

			for (CritereAppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la critereAppelDoffres existe
				CritereAppelDoffres existingEntity = null;
				existingEntity = critereAppelDoffresRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("critereAppelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// valeurCritereAppelDoffres
				List<ValeurCritereAppelDoffres> listOfValeurCritereAppelDoffres = valeurCritereAppelDoffresRepository.findByCritereId(existingEntity.getId(), false);
				if (listOfValeurCritereAppelDoffres != null && !listOfValeurCritereAppelDoffres.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfValeurCritereAppelDoffres.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// valeurCritereOffre
				List<ValeurCritereOffre> listOfValeurCritereOffre = valeurCritereOffreRepository.findByCritereId(existingEntity.getId(), false);
				if (listOfValeurCritereOffre != null && !listOfValeurCritereOffre.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfValeurCritereOffre.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				critereAppelDoffresRepository.save((Iterable<CritereAppelDoffres>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete CritereAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete CritereAppelDoffres by using CritereAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<CritereAppelDoffresDto> forceDelete(Request<CritereAppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete CritereAppelDoffres-----");

		response = new Response<CritereAppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<CritereAppelDoffres> items = new ArrayList<CritereAppelDoffres>();

			for (CritereAppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la critereAppelDoffres existe
				CritereAppelDoffres existingEntity = null;
				existingEntity = critereAppelDoffresRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("critereAppelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// valeurCritereAppelDoffres
				List<ValeurCritereAppelDoffres> listOfValeurCritereAppelDoffres = valeurCritereAppelDoffresRepository.findByCritereId(existingEntity.getId(), false);
				if (listOfValeurCritereAppelDoffres != null && !listOfValeurCritereAppelDoffres.isEmpty()){
					Request<ValeurCritereAppelDoffresDto> deleteRequest = new Request<ValeurCritereAppelDoffresDto>();
					deleteRequest.setDatas(ValeurCritereAppelDoffresTransformer.INSTANCE.toDtos(listOfValeurCritereAppelDoffres));
					deleteRequest.setUser(request.getUser());
					Response<ValeurCritereAppelDoffresDto> deleteResponse = valeurCritereAppelDoffresBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// valeurCritereOffre
				List<ValeurCritereOffre> listOfValeurCritereOffre = valeurCritereOffreRepository.findByCritereId(existingEntity.getId(), false);
				if (listOfValeurCritereOffre != null && !listOfValeurCritereOffre.isEmpty()){
					Request<ValeurCritereOffreDto> deleteRequest = new Request<ValeurCritereOffreDto>();
					deleteRequest.setDatas(ValeurCritereOffreTransformer.INSTANCE.toDtos(listOfValeurCritereOffre));
					deleteRequest.setUser(request.getUser());
					Response<ValeurCritereOffreDto> deleteResponse = valeurCritereOffreBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				critereAppelDoffresRepository.save((Iterable<CritereAppelDoffres>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete CritereAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get CritereAppelDoffres by using CritereAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<CritereAppelDoffresDto> getByCriteria(Request<CritereAppelDoffresDto> request, Locale locale) {
		slf4jLogger.info("----begin get CritereAppelDoffres-----");

		response = new Response<CritereAppelDoffresDto>();

		try {
			List<CritereAppelDoffres> items = null;
			items = critereAppelDoffresRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<CritereAppelDoffresDto> itemsDto = new ArrayList<CritereAppelDoffresDto>();
				for (CritereAppelDoffres entity : items) {
					CritereAppelDoffresDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(critereAppelDoffresRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("critereAppelDoffres", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get CritereAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full CritereAppelDoffresDto by using CritereAppelDoffres as object.
	 *
	 * @param entity, locale
	 * @return CritereAppelDoffresDto
	 *
	 */
	private CritereAppelDoffresDto getFullInfos(CritereAppelDoffres entity, Integer size, Locale locale) throws Exception {
		CritereAppelDoffresDto dto = CritereAppelDoffresTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
