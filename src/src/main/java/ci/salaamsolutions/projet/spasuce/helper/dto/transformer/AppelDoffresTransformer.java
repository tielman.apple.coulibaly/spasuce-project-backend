

/*
 * Java transformer for entity table appel_doffres 
 * Created on 2020-02-08 ( Time 09:42:16 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "appel_doffres"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface AppelDoffresTransformer {

	AppelDoffresTransformer INSTANCE = Mappers.getMapper(AppelDoffresTransformer.class);

	@Mappings({

		@Mapping(source="entity.dateLimiteDepot", dateFormat="dd/MM/yyyy",target="dateLimiteDepot"),

		@Mapping(source="entity.dateCreation", dateFormat="dd/MM/yyyy",target="dateCreation"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.ville.id", target="villeId"),
				@Mapping(source="entity.ville.code", target="villeCode"),
				@Mapping(source="entity.ville.libelle", target="villeLibelle"),
		@Mapping(source="entity.etat.id", target="etatId"),
				@Mapping(source="entity.etat.code", target="etatCode"),
				@Mapping(source="entity.etat.libelle", target="etatLibelle"),
		@Mapping(source="entity.typeDuree.id", target="typeDureeId"),
				@Mapping(source="entity.typeDuree.libelle", target="typeDureeLibelle"),
				@Mapping(source="entity.typeDuree.code", target="typeDureeCode"),
		@Mapping(source="entity.typeAppelDoffres.id", target="typeAppelDoffresId"),
				@Mapping(source="entity.typeAppelDoffres.code", target="typeAppelDoffresCode"),
				@Mapping(source="entity.typeAppelDoffres.libelle", target="typeAppelDoffresLibelle"),
		@Mapping(source="entity.user.id", target="userClientId"),
				@Mapping(source="entity.user.nom", target="userNom"),
				@Mapping(source="entity.user.prenoms", target="userPrenoms"),
				@Mapping(source="entity.user.login", target="userLogin"),
	})
	AppelDoffresDto toDto(AppelDoffres entity) throws ParseException;

    List<AppelDoffresDto> toDtos(List<AppelDoffres> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.prixPrestation", target="prixPrestation"),
		@Mapping(source="dto.nombreTypeDuree", target="nombreTypeDuree"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.dateLimiteDepot", dateFormat="dd/MM/yyyy",target="dateLimiteDepot"),
		@Mapping(source="dto.dateCreation", dateFormat="dd/MM/yyyy",target="dateCreation"),
		@Mapping(source="dto.dureRealisation", target="dureRealisation"),
		@Mapping(source="dto.isLocked", target="isLocked"),
		@Mapping(source="dto.isContenuNotificationDispo", target="isContenuNotificationDispo"),
		@Mapping(source="dto.isAttribuer", target="isAttribuer"),
		@Mapping(source="dto.isModifiable", target="isModifiable"),
		@Mapping(source="dto.isRetirer", target="isRetirer"),
		@Mapping(source="dto.isVisibleByClient", target="isVisibleByClient"),
		@Mapping(source="dto.isUpdate", target="isUpdate"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="ville", target="ville"),
		@Mapping(source="etat", target="etat"),
		@Mapping(source="typeDuree", target="typeDuree"),
		@Mapping(source="typeAppelDoffres", target="typeAppelDoffres"),
		@Mapping(source="user", target="user"),
	})
    AppelDoffres toEntity(AppelDoffresDto dto, Ville ville, Etat etat, TypeDuree typeDuree, TypeAppelDoffres typeAppelDoffres, User user) throws ParseException;

    //List<AppelDoffres> toEntities(List<AppelDoffresDto> dtos) throws ParseException;

}
