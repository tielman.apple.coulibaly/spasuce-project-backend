package ci.salaamsolutions.projet.spasuce.helper.enums;

public class TypeCritereAppelDoffresEnum {
	public static final String	DATE				= "DATE";
	public static final String	DATETIME			= "DATETIME";
	public static final String	STRING				= "STRING";
	public static final String	NOMBRE				= "NOMBRE";
	public static final String	ENTIER				= "ENTIER";
	public static final String	NUMBER				= "NUMBER";
	public static final String	BOOLEAN				= "BOOLEAN";
}

