package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._ValeurCritereOffreRepository;

/**
 * Repository : ValeurCritereOffre.
 */
@Repository
public interface ValeurCritereOffreRepository extends JpaRepository<ValeurCritereOffre, Integer>, _ValeurCritereOffreRepository {
	/**
	 * Finds ValeurCritereOffre by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object ValeurCritereOffre whose id is equals to the given id. If
	 *         no ValeurCritereOffre is found, this method returns null.
	 */
	@Query("select e from ValeurCritereOffre e where e.id = :id and e.isDeleted = :isDeleted")
	ValeurCritereOffre findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ValeurCritereOffre by using valeur as a search criteria.
	 *
	 * @param valeur
	 * @return An Object ValeurCritereOffre whose valeur is equals to the given valeur. If
	 *         no ValeurCritereOffre is found, this method returns null.
	 */
	@Query("select e from ValeurCritereOffre e where e.valeur = :valeur and e.isDeleted = :isDeleted")
	List<ValeurCritereOffre> findByValeur(@Param("valeur")String valeur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereOffre by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object ValeurCritereOffre whose createdAt is equals to the given createdAt. If
	 *         no ValeurCritereOffre is found, this method returns null.
	 */
	@Query("select e from ValeurCritereOffre e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<ValeurCritereOffre> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereOffre by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object ValeurCritereOffre whose createdBy is equals to the given createdBy. If
	 *         no ValeurCritereOffre is found, this method returns null.
	 */
	@Query("select e from ValeurCritereOffre e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<ValeurCritereOffre> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereOffre by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object ValeurCritereOffre whose updatedAt is equals to the given updatedAt. If
	 *         no ValeurCritereOffre is found, this method returns null.
	 */
	@Query("select e from ValeurCritereOffre e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<ValeurCritereOffre> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereOffre by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object ValeurCritereOffre whose updatedBy is equals to the given updatedBy. If
	 *         no ValeurCritereOffre is found, this method returns null.
	 */
	@Query("select e from ValeurCritereOffre e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<ValeurCritereOffre> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereOffre by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object ValeurCritereOffre whose deletedAt is equals to the given deletedAt. If
	 *         no ValeurCritereOffre is found, this method returns null.
	 */
	@Query("select e from ValeurCritereOffre e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<ValeurCritereOffre> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereOffre by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object ValeurCritereOffre whose deletedBy is equals to the given deletedBy. If
	 *         no ValeurCritereOffre is found, this method returns null.
	 */
	@Query("select e from ValeurCritereOffre e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<ValeurCritereOffre> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ValeurCritereOffre by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object ValeurCritereOffre whose isDeleted is equals to the given isDeleted. If
	 *         no ValeurCritereOffre is found, this method returns null.
	 */
	@Query("select e from ValeurCritereOffre e where e.isDeleted = :isDeleted")
	List<ValeurCritereOffre> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ValeurCritereOffre by using offreId as a search criteria.
	 *
	 * @param offreId
	 * @return A list of Object ValeurCritereOffre whose offreId is equals to the given offreId. If
	 *         no ValeurCritereOffre is found, this method returns null.
	 */
	@Query("select e from ValeurCritereOffre e where e.offre.id = :offreId and e.isDeleted = :isDeleted")
	List<ValeurCritereOffre> findByOffreId(@Param("offreId")Integer offreId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one ValeurCritereOffre by using offreId as a search criteria.
   *
   * @param offreId
   * @return An Object ValeurCritereOffre whose offreId is equals to the given offreId. If
   *         no ValeurCritereOffre is found, this method returns null.
   */
  @Query("select e from ValeurCritereOffre e where e.offre.id = :offreId and e.isDeleted = :isDeleted")
  ValeurCritereOffre findValeurCritereOffreByOffreId(@Param("offreId")Integer offreId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds ValeurCritereOffre by using critereId as a search criteria.
	 *
	 * @param critereId
	 * @return A list of Object ValeurCritereOffre whose critereId is equals to the given critereId. If
	 *         no ValeurCritereOffre is found, this method returns null.
	 */
	@Query("select e from ValeurCritereOffre e where e.critereAppelDoffres.id = :critereId and e.isDeleted = :isDeleted")
	List<ValeurCritereOffre> findByCritereId(@Param("critereId")Integer critereId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one ValeurCritereOffre by using critereId as a search criteria.
   *
   * @param critereId
   * @return An Object ValeurCritereOffre whose critereId is equals to the given critereId. If
   *         no ValeurCritereOffre is found, this method returns null.
   */
  @Query("select e from ValeurCritereOffre e where e.critereAppelDoffres.id = :critereId and e.isDeleted = :isDeleted")
  ValeurCritereOffre findValeurCritereOffreByCritereId(@Param("critereId")Integer critereId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of ValeurCritereOffre by using valeurCritereOffreDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of ValeurCritereOffre
	 * @throws DataAccessException,ParseException
	 */
	public default List<ValeurCritereOffre> getByCriteria(Request<ValeurCritereOffreDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from ValeurCritereOffre e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<ValeurCritereOffre> query = em.createQuery(req, ValeurCritereOffre.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of ValeurCritereOffre by using valeurCritereOffreDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of ValeurCritereOffre
	 *
	 */
	public default Long count(Request<ValeurCritereOffreDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from ValeurCritereOffre e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<ValeurCritereOffreDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		ValeurCritereOffreDto dto = request.getData() != null ? request.getData() : new ValeurCritereOffreDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (ValeurCritereOffreDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(ValeurCritereOffreDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getValeur())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("valeur", dto.getValeur(), "e.valeur", "String", dto.getValeurParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getOffreId()!= null && dto.getOffreId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("offreId", dto.getOffreId(), "e.offre.id", "Integer", dto.getOffreIdParam(), param, index, locale));
			}
			if (dto.getCritereId()!= null && dto.getCritereId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("critereId", dto.getCritereId(), "e.critereAppelDoffres.id", "Integer", dto.getCritereIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCritereAppelDoffresLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("critereAppelDoffresLibelle", dto.getCritereAppelDoffresLibelle(), "e.critereAppelDoffres.libelle", "String", dto.getCritereAppelDoffresLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
