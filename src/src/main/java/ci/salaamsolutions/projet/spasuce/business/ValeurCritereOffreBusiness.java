


/*
 * Java transformer for entity table valeur_critere_offre
 * Created on 2020-01-02 ( Time 18:00:06 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.enums.GlobalEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeUserEnum;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "valeur_critere_offre"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class ValeurCritereOffreBusiness implements IBasicBusiness<Request<ValeurCritereOffreDto>, Response<ValeurCritereOffreDto>> {

	private Response<ValeurCritereOffreDto> response;
	@Autowired
	private ValeurCritereOffreRepository valeurCritereOffreRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private OffreRepository offreRepository;
	@Autowired
	private CritereAppelDoffresRepository critereAppelDoffresRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public ValeurCritereOffreBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create ValeurCritereOffre by using ValeurCritereOffreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ValeurCritereOffreDto> create(Request<ValeurCritereOffreDto> request, Locale locale)  {
		slf4jLogger.info("----begin create ValeurCritereOffre-----");

		response = new Response<ValeurCritereOffreDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			// type de user autorisé
			if (utilisateur.getEntreprise().getTypeEntreprise().getCode().equals(TypeUserEnum.ENTREPRISE_CLIENTE)) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un fournisseur" , locale));
				response.setHasError(true);
				return response;
			}
			List<ValeurCritereOffre> items = new ArrayList<ValeurCritereOffre>();

			for (ValeurCritereOffreDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("valeur", dto.getValeur());
				fieldsToVerify.put("critereId", dto.getCritereId());
				fieldsToVerify.put("offreId", dto.getOffreId());

				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if valeurCritereOffre to insert do not exist
				ValeurCritereOffre existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("valeurCritereOffre -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if offre exist
				Offre existingOffre = offreRepository.findById(dto.getOffreId(), false);
				if (existingOffre == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("offre -> " + dto.getOffreId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if critereAppelDoffres exist
				CritereAppelDoffres existingCritereAppelDoffres = critereAppelDoffresRepository.findById(dto.getCritereId(), false);
				if (existingCritereAppelDoffres == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("critereAppelDoffres -> " + dto.getCritereId(), locale));
					response.setHasError(true);
					return response;
				}

				// verifier le contenu de la valeuur selon le type du critere

				String valeur = dto.getValeur();
				String typeCritere = existingCritereAppelDoffres.getTypeCritereAppelDoffres().getCode();



				response = Utilities.verifierSiContenuValeurCorrespondAuType(response, valeur, typeCritere, locale, functionalError);

				if (response.isHasError()) {
					return response;
				}


				//				switch (existingCritereAppelDoffres.getTypeCritereAppelDoffres().getCode()) {
				//				case TypeCritereAppelDoffresEnum.DATE:
				//					if (!Utilities.isDateValid(valeur)) {
				//						response.setStatus(functionalError.INVALID_DATA("la valeur -> " + dto.getValeur() + " saisie n'est pas une date", locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//
				//					break;
				//
				//				case TypeCritereAppelDoffresEnum.DATETIME:
				//					try {
				//						Date dateTime = dateFormat.parse(dto.getValeur());
				//					} catch (Exception e) {
				//						// TODO: handle exception
				//						response.setStatus(functionalError.INVALID_DATA("la valeur -> " + dto.getValeur() + " saisie n'est pas une date avec heure", locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//
				//					break;
				//
				//				case TypeCritereAppelDoffresEnum.BOOLEAN:
				//					if (!Utilities.isBoolean(valeur)) {
				//						response.setStatus(functionalError.INVALID_DATA("la valeur -> " + dto.getValeur() + " saisie n'est pas correcte", locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					break;
				//
				//				case TypeCritereAppelDoffresEnum.ENTIER:
				//
				//					if (!Utilities.isInteger(valeur)) {
				//						response.setStatus(functionalError.INVALID_DATA("la valeur -> " + dto.getValeur() + " saisie n'est pas un entier", locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					break;
				//
				//				case TypeCritereAppelDoffresEnum.STRING:
				//					// rien a faire ici
				//					break;
				//
				//				case TypeCritereAppelDoffresEnum.NOMBRE:
				//					if (!Utilities.isNumeric(valeur)) {
				//						response.setStatus(functionalError.INVALID_DATA("la valeur -> " + dto.getValeur() + " saisie n'est pas nombre", locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					break;
				//
				//				default:
				//					break;
				//				}

				// doublons de critere pour les appelDoffres
				if (!items.isEmpty()) {
					if (items.stream().anyMatch(a->a.getCritereAppelDoffres().getId().equals(dto.getCritereId()) && a.getOffre().getId().equals(dto.getOffreId()) )) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du critere '" + dto.getCritereId()+"' pour les offres", locale));
						response.setHasError(true);
						return response;
					}
				}

				ValeurCritereOffre entityToSave = null;
				entityToSave = ValeurCritereOffreTransformer.INSTANCE.toEntity(dto, existingOffre, existingCritereAppelDoffres);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ValeurCritereOffre> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = valeurCritereOffreRepository.save((Iterable<ValeurCritereOffre>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("valeurCritereOffre", locale));
					response.setHasError(true);
					return response;
				}
				List<ValeurCritereOffreDto> itemsDto = new ArrayList<ValeurCritereOffreDto>();
				for (ValeurCritereOffre entity : itemsSaved) {
					ValeurCritereOffreDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create ValeurCritereOffre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update ValeurCritereOffre by using ValeurCritereOffreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ValeurCritereOffreDto> update(Request<ValeurCritereOffreDto> request, Locale locale)  {
		slf4jLogger.info("----begin update ValeurCritereOffre-----");

		response = new Response<ValeurCritereOffreDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ValeurCritereOffre> items = new ArrayList<ValeurCritereOffre>();

			for (ValeurCritereOffreDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la valeurCritereOffre existe
				ValeurCritereOffre entityToSave = null;
				entityToSave = valeurCritereOffreRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("valeurCritereOffre -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if offre exist
				if (dto.getOffreId() != null && dto.getOffreId() > 0){
					Offre existingOffre = offreRepository.findById(dto.getOffreId(), false);
					if (existingOffre == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("offre -> " + dto.getOffreId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setOffre(existingOffre);
				}
				// Verify if critereAppelDoffres exist
				if (dto.getCritereId() != null && dto.getCritereId() > 0){
					CritereAppelDoffres existingCritereAppelDoffres = critereAppelDoffresRepository.findById(dto.getCritereId(), false);
					if (existingCritereAppelDoffres == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("critereAppelDoffres -> " + dto.getCritereId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCritereAppelDoffres(existingCritereAppelDoffres);
				}
				if (Utilities.notBlank(dto.getValeur())) {
					entityToSave.setValeur(dto.getValeur());
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				if (Utilities.notBlank(dto.getDeletedAt())) {
					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
				}
				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
					entityToSave.setDeletedBy(dto.getDeletedBy());
				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ValeurCritereOffre> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = valeurCritereOffreRepository.save((Iterable<ValeurCritereOffre>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("valeurCritereOffre", locale));
					response.setHasError(true);
					return response;
				}
				List<ValeurCritereOffreDto> itemsDto = new ArrayList<ValeurCritereOffreDto>();
				for (ValeurCritereOffre entity : itemsSaved) {
					ValeurCritereOffreDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update ValeurCritereOffre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete ValeurCritereOffre by using ValeurCritereOffreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ValeurCritereOffreDto> delete(Request<ValeurCritereOffreDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete ValeurCritereOffre-----");

		response = new Response<ValeurCritereOffreDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ValeurCritereOffre> items = new ArrayList<ValeurCritereOffre>();

			for (ValeurCritereOffreDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la valeurCritereOffre existe
				ValeurCritereOffre existingEntity = null;
				existingEntity = valeurCritereOffreRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("valeurCritereOffre -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				valeurCritereOffreRepository.save((Iterable<ValeurCritereOffre>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete ValeurCritereOffre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete ValeurCritereOffre by using ValeurCritereOffreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<ValeurCritereOffreDto> forceDelete(Request<ValeurCritereOffreDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete ValeurCritereOffre-----");

		response = new Response<ValeurCritereOffreDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ValeurCritereOffre> items = new ArrayList<ValeurCritereOffre>();

			for (ValeurCritereOffreDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la valeurCritereOffre existe
				ValeurCritereOffre existingEntity = null;
				existingEntity = valeurCritereOffreRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("valeurCritereOffre -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				valeurCritereOffreRepository.save((Iterable<ValeurCritereOffre>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete ValeurCritereOffre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get ValeurCritereOffre by using ValeurCritereOffreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<ValeurCritereOffreDto> getByCriteria(Request<ValeurCritereOffreDto> request, Locale locale) {
		slf4jLogger.info("----begin get ValeurCritereOffre-----");

		response = new Response<ValeurCritereOffreDto>();

		try {
			List<ValeurCritereOffre> items = null;
			items = valeurCritereOffreRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<ValeurCritereOffreDto> itemsDto = new ArrayList<ValeurCritereOffreDto>();
				for (ValeurCritereOffre entity : items) {
					ValeurCritereOffreDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(valeurCritereOffreRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("valeurCritereOffre", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get ValeurCritereOffre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * getByCriteriaCustom ValeurCritereOffre by using ValeurCritereOffreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	public Response<OffreDto> getByCriteriaCustom(Request<ValeurCritereOffreDto> request, Locale locale) {
		slf4jLogger.info("----begin getByCriteriaCustom ValeurCritereOffre-----");

		Response<OffreDto> res = new Response<OffreDto>();

		try {
			List<ValeurCritereOffre> items = null;
			items = valeurCritereOffreRepository.getByCriteriaCustom(request, em, locale);
			if (items != null && !items.isEmpty()) {


				// tripar rapport a la valeur du critere er son type
				if (request.getData() != null && Utilities.notBlank(request.getData().getCode()) ) {
					switch (request.getData().getCode()) {
					case GlobalEnum.prix:
						Collections.sort(items, new Comparator<ValeurCritereOffre>() {
							@Override
							public int compare(ValeurCritereOffre o1, ValeurCritereOffre o2) {
								// TODO Auto-generated method stub
								//return o1.getId().compareTo(o2.getId())* - 1;
								//return o2.getId().compareTo(o1.getId());

								return o1.getValeur().compareTo(o2.getValeur());
							}
						}); // trier la liste


						slf4jLogger.info("----items apres tri par_date_limite -----" + items);


						break;

					case GlobalEnum.delai_dexecution:
						Collections.sort(items, new Comparator<ValeurCritereOffre>() {
							@Override
							public int compare(ValeurCritereOffre o1, ValeurCritereOffre o2) {
								// TODO Auto-generated method stub
								//return o1.getId().compareTo(o2.getId())* - 1;
								//return o2.getId().compareTo(o1.getId());

								//return dateFormat.parse(o1.getValeur()).compareTo(dateFormat.parse(o2.getValeur()));
								return new Date(o1.getValeur()).compareTo(new Date(o2.getValeur()));

								//return o1.getCreatedAt().compareTo(o2.getCreatedAt());
							}
						}); // trier la liste
						slf4jLogger.info("----items apres tri plus_ancien -----" + items);

						break;

					case GlobalEnum.dure_de_mise_a_disponibilite:
						Collections.sort(items, new Comparator<ValeurCritereOffre>() {
							@Override
							public int compare(ValeurCritereOffre o1, ValeurCritereOffre o2) {
								// TODO Auto-generated method stub
								//return o1.getId().compareTo(o2.getId())* - 1;
								//return o2.getId().compareTo(o1.getId());
								
								//return dateFormat.parse(o1.getValeur()).compareTo(dateFormat.parse(o2.getValeur()));
								return new Date(o1.getValeur()).compareTo(new Date(o2.getValeur()));

								//return o2.getCreatedAt().compareTo(o1.getCreatedAt());
							}
						}); // trier la liste
						slf4jLogger.info("----items apres tri plus_recent -----" + items);

						break;

					default:
						break;
					}
				}
				if (Utilities.isNotEmpty(request.getDatas())) {
					
					
					for (ValeurCritereOffreDto data  : request.getDatas()) {

						slf4jLogger.info("----begin items-----" + items);

						switch (data.getCode()) {
						case GlobalEnum.prix:
							Collections.sort(items, new Comparator<ValeurCritereOffre>() {
								@Override
								public int compare(ValeurCritereOffre o1, ValeurCritereOffre o2) {
									// TODO Auto-generated method stub
									//return o1.getId().compareTo(o2.getId())* - 1;
									//return o2.getId().compareTo(o1.getId());

									return o1.getValeur().compareTo(o2.getValeur());
								}
							}); // trier la liste


							slf4jLogger.info("----items apres tri par_date_limite -----" + items);


							break;

						case GlobalEnum.delai_dexecution:
							Collections.sort(items, new Comparator<ValeurCritereOffre>() {
								@Override
								public int compare(ValeurCritereOffre o1, ValeurCritereOffre o2) {
									// TODO Auto-generated method stub
									//return o1.getId().compareTo(o2.getId())* - 1;
									//return o2.getId().compareTo(o1.getId());

									//return dateFormat.parse(o1.getValeur()).compareTo(dateFormat.parse(o2.getValeur()));
									return new Date(o1.getValeur()).compareTo(new Date(o2.getValeur()));

									//return o1.getCreatedAt().compareTo(o2.getCreatedAt());
								}
							}); // trier la liste
							slf4jLogger.info("----items apres tri plus_ancien -----" + items);

							break;

						case GlobalEnum.dure_de_mise_a_disponibilite:
							Collections.sort(items, new Comparator<ValeurCritereOffre>() {
								@Override
								public int compare(ValeurCritereOffre o1, ValeurCritereOffre o2) {
									// TODO Auto-generated method stub
									//return o1.getId().compareTo(o2.getId())* - 1;
									//return o2.getId().compareTo(o1.getId());
									
									//return dateFormat.parse(o1.getValeur()).compareTo(dateFormat.parse(o2.getValeur()));
									return new Date(o1.getValeur()).compareTo(new Date(o2.getValeur()));

									//return o2.getCreatedAt().compareTo(o1.getCreatedAt());
								}
							}); // trier la liste
							slf4jLogger.info("----items apres tri plus_recent -----" + items);

							break;

						default:
							break;
						}
					}
				}

				
				// faire les tris selons les dates
				if (Utilities.isNotEmpty(request.getCritereFiltreDtos())) {
					List<ValeurCritereOffre> itemsRetenu = new ArrayList<>();
					for (CritereFiltreDto pays  : request.getCritereFiltreDtos()) {


						for (ValeurCritereOffre data : items) {

							if (data.getOffre().getUser().getEntreprise().getVille().getPays().getCode().equals(pays.getCode())) {
								itemsRetenu.add(data);
							}
						}
					}

					//					if (Utilities.isNotEmpty(itemsRetenu)) {
					//						items = itemsRetenu ;
					//					}
					items = itemsRetenu ;
				}

				List<OffreDto> itemsDto = new ArrayList<OffreDto>();
				for (ValeurCritereOffre entity : items) {

					OffreDto dto = getFullInfosCustom(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				res.setItems(itemsDto);
				res.setCount(valeurCritereOffreRepository.count(request, em, locale));
				res.setHasError(false);
			} else {
				res.setStatus(functionalError.DATA_EMPTY("valeurCritereOffre", locale));
				res.setHasError(false);
				return res;
			}

			slf4jLogger.info("----end getByCriteriaCustom ValeurCritereOffre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(res, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(res, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(res, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(res, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(res, locale, e);
		} finally {
			if (response.isHasError() && res.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", res.getStatus().getCode(), res.getStatus().getMessage());
				throw new RuntimeException(res.getStatus().getCode() + ";" + res.getStatus().getMessage());
			}
		}
		return res;
	}

	/**
	 * get full ValeurCritereOffreDto by using ValeurCritereOffre as object.
	 *
	 * @param entity, locale
	 * @return ValeurCritereOffreDto
	 *
	 */
	private ValeurCritereOffreDto getFullInfos(ValeurCritereOffre entity, Integer size, Locale locale) throws Exception {
		ValeurCritereOffreDto dto = ValeurCritereOffreTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}

	/**
	 * get full ValeurCritereOffreDto by using ValeurCritereOffre as object.
	 *
	 * @param entity, locale
	 * @return ValeurCritereOffreDto
	 *
	 */
	private OffreDto getFullInfosCustom(ValeurCritereOffre entity, Integer size, Locale locale) throws Exception {
		OffreDto dto = OffreTransformer.INSTANCE.toDto(entity.getOffre());
		if (dto == null){
			return null;
		}
		ValeurCritereOffre valeurCritereOffre = valeurCritereOffreRepository.findValeurCritereOffreByOffreIdAndCritereAppelDoffresCode(dto.getId(), GlobalEnum.prix, false) ;

		if (valeurCritereOffre != null) {
			dto.setPrix(Integer.valueOf(valeurCritereOffre.getValeur()));
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
