package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._UserRepository;

/**
 * Repository : User.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer>, _UserRepository {
	/**
	 * Finds User by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object User whose id is equals to the given id. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.id = :id and e.isDeleted = :isDeleted")
	User findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds User by using nom as a search criteria.
	 *
	 * @param nom
	 * @return An Object User whose nom is equals to the given nom. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.nom = :nom and e.isDeleted = :isDeleted")
	List<User> findByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using prenoms as a search criteria.
	 *
	 * @param prenoms
	 * @return An Object User whose prenoms is equals to the given prenoms. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.prenoms = :prenoms and e.isDeleted = :isDeleted")
	List<User> findByPrenoms(@Param("prenoms")String prenoms, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using login as a search criteria.
	 *
	 * @param login
	 * @return An Object User whose login is equals to the given login. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.login = :login and e.isDeleted = :isDeleted")
	User findByLogin(@Param("login")String login, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using password as a search criteria.
	 *
	 * @param password
	 * @return An Object User whose password is equals to the given password. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.password = :password and e.isDeleted = :isDeleted")
	List<User> findByPassword(@Param("password")String password, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using telephone as a search criteria.
	 *
	 * @param telephone
	 * @return An Object User whose telephone is equals to the given telephone. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.telephone = :telephone and e.isDeleted = :isDeleted")
	User findByTelephone(@Param("telephone")String telephone, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using email as a search criteria.
	 *
	 * @param email
	 * @return An Object User whose email is equals to the given email. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.email = :email and e.isDeleted = :isDeleted")
	User findByEmail(@Param("email")String email, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isLocked as a search criteria.
	 *
	 * @param isLocked
	 * @return An Object User whose isLocked is equals to the given isLocked. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isLocked = :isLocked and e.isDeleted = :isDeleted")
	List<User> findByIsLocked(@Param("isLocked")Boolean isLocked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isSuUser as a search criteria.
	 *
	 * @param isSuUser
	 * @return An Object User whose isSuUser is equals to the given isSuUser. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isSuUser = :isSuUser and e.isDeleted = :isDeleted")
	List<User> findByIsSuUser(@Param("isSuUser")Boolean isSuUser, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isValider as a search criteria.
	 *
	 * @param isValider
	 * @return An Object User whose isValider is equals to the given isValider. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isValider = :isValider and e.isDeleted = :isDeleted")
	List<User> findByIsValider(@Param("isValider")Boolean isValider, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using token as a search criteria.
	 *
	 * @param token
	 * @return An Object User whose token is equals to the given token. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.token = :token and e.isDeleted = :isDeleted")
	List<User> findByToken(@Param("token")String token, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using dateExpirationToken as a search criteria.
	 *
	 * @param dateExpirationToken
	 * @return An Object User whose dateExpirationToken is equals to the given dateExpirationToken. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.dateExpirationToken = :dateExpirationToken and e.isDeleted = :isDeleted")
	List<User> findByDateExpirationToken(@Param("dateExpirationToken")Date dateExpirationToken, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isTokenValide as a search criteria.
	 *
	 * @param isTokenValide
	 * @return An Object User whose isTokenValide is equals to the given isTokenValide. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isTokenValide = :isTokenValide and e.isDeleted = :isDeleted")
	List<User> findByIsTokenValide(@Param("isTokenValide")Boolean isTokenValide, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object User whose createdBy is equals to the given createdBy. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<User> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object User whose createdAt is equals to the given createdAt. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<User> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object User whose updatedAt is equals to the given updatedAt. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<User> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object User whose updatedBy is equals to the given updatedBy. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<User> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object User whose deletedAt is equals to the given deletedAt. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<User> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object User whose deletedBy is equals to the given deletedBy. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<User> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object User whose isDeleted is equals to the given isDeleted. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isDeleted = :isDeleted")
	List<User> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds User by using roleId as a search criteria.
	 *
	 * @param roleId
	 * @return A list of Object User whose roleId is equals to the given roleId. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.role.id = :roleId and e.isDeleted = :isDeleted")
	List<User> findByRoleId(@Param("roleId")Integer roleId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one User by using roleId as a search criteria.
   *
   * @param roleId
   * @return An Object User whose roleId is equals to the given roleId. If
   *         no User is found, this method returns null.
   */
  @Query("select e from User e where e.role.id = :roleId and e.isDeleted = :isDeleted")
  User findUserByRoleId(@Param("roleId")Integer roleId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds User by using entrepriseId as a search criteria.
	 *
	 * @param entrepriseId
	 * @return A list of Object User whose entrepriseId is equals to the given entrepriseId. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.entreprise.id = :entrepriseId and e.isDeleted = :isDeleted")
	List<User> findByEntrepriseId(@Param("entrepriseId")Integer entrepriseId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one User by using entrepriseId as a search criteria.
   *
   * @param entrepriseId
   * @return An Object User whose entrepriseId is equals to the given entrepriseId. If
   *         no User is found, this method returns null.
   */
  @Query("select e from User e where e.entreprise.id = :entrepriseId and e.isDeleted = :isDeleted")
  User findUserByEntrepriseId(@Param("entrepriseId")Integer entrepriseId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of User by using userDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of User
	 * @throws DataAccessException,ParseException
	 */
	public default List<User> getByCriteria(Request<UserDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from User e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<User> query = em.createQuery(req, User.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of User by using userDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of User
	 *
	 */
	public default Long count(Request<UserDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from User e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<UserDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		UserDto dto = request.getData() != null ? request.getData() : new UserDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (UserDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(UserDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nom", dto.getNom(), "e.nom", "String", dto.getNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPrenoms())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prenoms", dto.getPrenoms(), "e.prenoms", "String", dto.getPrenomsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("login", dto.getLogin(), "e.login", "String", dto.getLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPassword())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("password", dto.getPassword(), "e.password", "String", dto.getPasswordParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTelephone())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephone", dto.getTelephone(), "e.telephone", "String", dto.getTelephoneParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEmail())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("email", dto.getEmail(), "e.email", "String", dto.getEmailParam(), param, index, locale));
			}
			if (dto.getIsLocked()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isLocked", dto.getIsLocked(), "e.isLocked", "Boolean", dto.getIsLockedParam(), param, index, locale));
			}
			if (dto.getIsSuUser()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isSuUser", dto.getIsSuUser(), "e.isSuUser", "Boolean", dto.getIsSuUserParam(), param, index, locale));
			}
			if (dto.getIsValider()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValider", dto.getIsValider(), "e.isValider", "Boolean", dto.getIsValiderParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getToken())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("token", dto.getToken(), "e.token", "String", dto.getTokenParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateExpirationToken())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateExpirationToken", dto.getDateExpirationToken(), "e.dateExpirationToken", "Date", dto.getDateExpirationTokenParam(), param, index, locale));
			}
			if (dto.getIsTokenValide()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isTokenValide", dto.getIsTokenValide(), "e.isTokenValide", "Boolean", dto.getIsTokenValideParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getRoleId()!= null && dto.getRoleId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("roleId", dto.getRoleId(), "e.role.id", "Integer", dto.getRoleIdParam(), param, index, locale));
			}
			if (dto.getEntrepriseId()!= null && dto.getEntrepriseId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseId", dto.getEntrepriseId(), "e.entreprise.id", "Integer", dto.getEntrepriseIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getRoleCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("roleCode", dto.getRoleCode(), "e.role.code", "String", dto.getRoleCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getRoleLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("roleLibelle", dto.getRoleLibelle(), "e.role.libelle", "String", dto.getRoleLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEntrepriseNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseNom", dto.getEntrepriseNom(), "e.entreprise.nom", "String", dto.getEntrepriseNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEntrepriseLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseLogin", dto.getEntrepriseLogin(), "e.entreprise.login", "String", dto.getEntrepriseLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
