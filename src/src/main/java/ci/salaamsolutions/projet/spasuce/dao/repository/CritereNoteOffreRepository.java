package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._CritereNoteOffreRepository;

/**
 * Repository : CritereNoteOffre.
 */
@Repository
public interface CritereNoteOffreRepository extends JpaRepository<CritereNoteOffre, Integer>, _CritereNoteOffreRepository {
	/**
	 * Finds CritereNoteOffre by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object CritereNoteOffre whose id is equals to the given id. If
	 *         no CritereNoteOffre is found, this method returns null.
	 */
	@Query("select e from CritereNoteOffre e where e.id = :id and e.isDeleted = :isDeleted")
	CritereNoteOffre findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds CritereNoteOffre by using valeurPartielle as a search criteria.
	 *
	 * @param valeurPartielle
	 * @return An Object CritereNoteOffre whose valeurPartielle is equals to the given valeurPartielle. If
	 *         no CritereNoteOffre is found, this method returns null.
	 */
	@Query("select e from CritereNoteOffre e where e.valeurPartielle = :valeurPartielle and e.isDeleted = :isDeleted")
	List<CritereNoteOffre> findByValeurPartielle(@Param("valeurPartielle")Float valeurPartielle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereNoteOffre by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object CritereNoteOffre whose createdAt is equals to the given createdAt. If
	 *         no CritereNoteOffre is found, this method returns null.
	 */
	@Query("select e from CritereNoteOffre e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<CritereNoteOffre> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereNoteOffre by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object CritereNoteOffre whose createdBy is equals to the given createdBy. If
	 *         no CritereNoteOffre is found, this method returns null.
	 */
	@Query("select e from CritereNoteOffre e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<CritereNoteOffre> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereNoteOffre by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object CritereNoteOffre whose updatedAt is equals to the given updatedAt. If
	 *         no CritereNoteOffre is found, this method returns null.
	 */
	@Query("select e from CritereNoteOffre e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<CritereNoteOffre> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereNoteOffre by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object CritereNoteOffre whose updatedBy is equals to the given updatedBy. If
	 *         no CritereNoteOffre is found, this method returns null.
	 */
	@Query("select e from CritereNoteOffre e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<CritereNoteOffre> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereNoteOffre by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object CritereNoteOffre whose deletedAt is equals to the given deletedAt. If
	 *         no CritereNoteOffre is found, this method returns null.
	 */
	@Query("select e from CritereNoteOffre e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<CritereNoteOffre> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereNoteOffre by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object CritereNoteOffre whose deletedBy is equals to the given deletedBy. If
	 *         no CritereNoteOffre is found, this method returns null.
	 */
	@Query("select e from CritereNoteOffre e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<CritereNoteOffre> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereNoteOffre by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object CritereNoteOffre whose isDeleted is equals to the given isDeleted. If
	 *         no CritereNoteOffre is found, this method returns null.
	 */
	@Query("select e from CritereNoteOffre e where e.isDeleted = :isDeleted")
	List<CritereNoteOffre> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds CritereNoteOffre by using sousCritereNoteId as a search criteria.
	 *
	 * @param sousCritereNoteId
	 * @return A list of Object CritereNoteOffre whose sousCritereNoteId is equals to the given sousCritereNoteId. If
	 *         no CritereNoteOffre is found, this method returns null.
	 */
	@Query("select e from CritereNoteOffre e where e.sousCritereNote.id = :sousCritereNoteId and e.isDeleted = :isDeleted")
	List<CritereNoteOffre> findBySousCritereNoteId(@Param("sousCritereNoteId")Integer sousCritereNoteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one CritereNoteOffre by using sousCritereNoteId as a search criteria.
   *
   * @param sousCritereNoteId
   * @return An Object CritereNoteOffre whose sousCritereNoteId is equals to the given sousCritereNoteId. If
   *         no CritereNoteOffre is found, this method returns null.
   */
  @Query("select e from CritereNoteOffre e where e.sousCritereNote.id = :sousCritereNoteId and e.isDeleted = :isDeleted")
  CritereNoteOffre findCritereNoteOffreBySousCritereNoteId(@Param("sousCritereNoteId")Integer sousCritereNoteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds CritereNoteOffre by using offreId as a search criteria.
	 *
	 * @param offreId
	 * @return A list of Object CritereNoteOffre whose offreId is equals to the given offreId. If
	 *         no CritereNoteOffre is found, this method returns null.
	 */
	@Query("select e from CritereNoteOffre e where e.offre.id = :offreId and e.isDeleted = :isDeleted")
	List<CritereNoteOffre> findByOffreId(@Param("offreId")Integer offreId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one CritereNoteOffre by using offreId as a search criteria.
   *
   * @param offreId
   * @return An Object CritereNoteOffre whose offreId is equals to the given offreId. If
   *         no CritereNoteOffre is found, this method returns null.
   */
  @Query("select e from CritereNoteOffre e where e.offre.id = :offreId and e.isDeleted = :isDeleted")
  CritereNoteOffre findCritereNoteOffreByOffreId(@Param("offreId")Integer offreId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds CritereNoteOffre by using moyenCalculId as a search criteria.
	 *
	 * @param moyenCalculId
	 * @return A list of Object CritereNoteOffre whose moyenCalculId is equals to the given moyenCalculId. If
	 *         no CritereNoteOffre is found, this method returns null.
	 */
	@Query("select e from CritereNoteOffre e where e.moyenCalcul.id = :moyenCalculId and e.isDeleted = :isDeleted")
	List<CritereNoteOffre> findByMoyenCalculId(@Param("moyenCalculId")Integer moyenCalculId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one CritereNoteOffre by using moyenCalculId as a search criteria.
   *
   * @param moyenCalculId
   * @return An Object CritereNoteOffre whose moyenCalculId is equals to the given moyenCalculId. If
   *         no CritereNoteOffre is found, this method returns null.
   */
  @Query("select e from CritereNoteOffre e where e.moyenCalcul.id = :moyenCalculId and e.isDeleted = :isDeleted")
  CritereNoteOffre findCritereNoteOffreByMoyenCalculId(@Param("moyenCalculId")Integer moyenCalculId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds CritereNoteOffre by using critereNoteId as a search criteria.
	 *
	 * @param critereNoteId
	 * @return A list of Object CritereNoteOffre whose critereNoteId is equals to the given critereNoteId. If
	 *         no CritereNoteOffre is found, this method returns null.
	 */
	@Query("select e from CritereNoteOffre e where e.critereNote.id = :critereNoteId and e.isDeleted = :isDeleted")
	List<CritereNoteOffre> findByCritereNoteId(@Param("critereNoteId")Integer critereNoteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one CritereNoteOffre by using critereNoteId as a search criteria.
   *
   * @param critereNoteId
   * @return An Object CritereNoteOffre whose critereNoteId is equals to the given critereNoteId. If
   *         no CritereNoteOffre is found, this method returns null.
   */
  @Query("select e from CritereNoteOffre e where e.critereNote.id = :critereNoteId and e.isDeleted = :isDeleted")
  CritereNoteOffre findCritereNoteOffreByCritereNoteId(@Param("critereNoteId")Integer critereNoteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of CritereNoteOffre by using critereNoteOffreDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of CritereNoteOffre
	 * @throws DataAccessException,ParseException
	 */
	public default List<CritereNoteOffre> getByCriteria(Request<CritereNoteOffreDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from CritereNoteOffre e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<CritereNoteOffre> query = em.createQuery(req, CritereNoteOffre.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of CritereNoteOffre by using critereNoteOffreDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of CritereNoteOffre
	 *
	 */
	public default Long count(Request<CritereNoteOffreDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from CritereNoteOffre e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<CritereNoteOffreDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		CritereNoteOffreDto dto = request.getData() != null ? request.getData() : new CritereNoteOffreDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (CritereNoteOffreDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(CritereNoteOffreDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getValeurPartielle()!= null && dto.getValeurPartielle() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("valeurPartielle", dto.getValeurPartielle(), "e.valeurPartielle", "Float", dto.getValeurPartielleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getSousCritereNoteId()!= null && dto.getSousCritereNoteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("sousCritereNoteId", dto.getSousCritereNoteId(), "e.sousCritereNote.id", "Integer", dto.getSousCritereNoteIdParam(), param, index, locale));
			}
			if (dto.getOffreId()!= null && dto.getOffreId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("offreId", dto.getOffreId(), "e.offre.id", "Integer", dto.getOffreIdParam(), param, index, locale));
			}
			if (dto.getMoyenCalculId()!= null && dto.getMoyenCalculId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("moyenCalculId", dto.getMoyenCalculId(), "e.moyenCalcul.id", "Integer", dto.getMoyenCalculIdParam(), param, index, locale));
			}
			if (dto.getCritereNoteId()!= null && dto.getCritereNoteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("critereNoteId", dto.getCritereNoteId(), "e.critereNote.id", "Integer", dto.getCritereNoteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSousCritereNoteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("sousCritereNoteLibelle", dto.getSousCritereNoteLibelle(), "e.sousCritereNote.libelle", "String", dto.getSousCritereNoteLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getMoyenCalculLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("moyenCalculLibelle", dto.getMoyenCalculLibelle(), "e.moyenCalcul.libelle", "String", dto.getMoyenCalculLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCritereNoteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("critereNoteLibelle", dto.getCritereNoteLibelle(), "e.critereNote.libelle", "String", dto.getCritereNoteLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
