package ci.salaamsolutions.projet.spasuce.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.CriteriaUtils;
import ci.salaamsolutions.projet.spasuce.helper.Utilities;
import ci.salaamsolutions.projet.spasuce.helper.contract.SearchParam;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteEntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.enums.OperatorEnum;

/**
 * Repository customize : SecteurDactiviteEntreprise.
 */
@Repository
public interface _SecteurDactiviteEntrepriseRepository {
	default List<String> _generateCriteria(SecteurDactiviteEntrepriseDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE
		
		if (Utilities.notBlank(dto.getTypeEntrepriseCode())) {
			listOfQuery.add(CriteriaUtils.generateCriteria("typeEntrepriseCode", dto.getTypeEntrepriseCode(), "e.entreprise.typeEntreprise.code", "String", dto.getTypeEntrepriseCodeParam(), param, index, locale));
		}
		
		if (Utilities.notBlank(dto.getDateFin()) && Utilities.notBlank(dto.getDateDebut())) {
			
			SearchParam<String>   createdAtEntrepriseParam = new SearchParam<String>() ;
			createdAtEntrepriseParam.setOperator(OperatorEnum.BETWEEN);
			createdAtEntrepriseParam.setStart(dto.getDateDebut());
			createdAtEntrepriseParam.setEnd(dto.getDateFin());
			dto.setCreatedAtEntrepriseParam(createdAtEntrepriseParam);
			//dto.setCreatedAt(dto.getDateDebut()) ;
			System.out.print("je suis *****************************");

			listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.entreprise.createdAt", "Date", dto.getCreatedAtEntrepriseParam(), param, index, locale));
		}
		
//		if (Utilities.notBlank(dto.getPaysEntrepriseLibelle())) {
//			listOfQuery.add(CriteriaUtils.generateCriteria("typeEntrepriseCode", dto.getTypeEntrepriseCode(), "e.entreprise.typeEntreprise.code", "String", dto.getTypeEntrepriseCodeParam(), param, index, locale));
//		}
		
		listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseIsDeleted", dto.getIsDeleted(), "e.entreprise.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
		listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseIsLocked", dto.getIsDeleted(), "e.entreprise.isLocked", "Boolean", dto.getIsDeletedParam(), param, index, locale));


		return listOfQuery;
	}
}
