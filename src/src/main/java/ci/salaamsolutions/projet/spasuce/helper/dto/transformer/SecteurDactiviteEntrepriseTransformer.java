

/*
 * Java transformer for entity table secteur_dactivite_entreprise 
 * Created on 2020-01-02 ( Time 18:00:03 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "secteur_dactivite_entreprise"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface SecteurDactiviteEntrepriseTransformer {

	SecteurDactiviteEntrepriseTransformer INSTANCE = Mappers.getMapper(SecteurDactiviteEntrepriseTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.secteurDactivite.id", target="secteurDactiviteId"),
				@Mapping(source="entity.secteurDactivite.code", target="secteurDactiviteCode"),
				@Mapping(source="entity.secteurDactivite.libelle", target="secteurDactiviteLibelle"),
		@Mapping(source="entity.entreprise.id", target="entrepriseId"),
				@Mapping(source="entity.entreprise.nom", target="entrepriseNom"),
				@Mapping(source="entity.entreprise.login", target="entrepriseLogin"),
	})
	SecteurDactiviteEntrepriseDto toDto(SecteurDactiviteEntreprise entity) throws ParseException;

    List<SecteurDactiviteEntrepriseDto> toDtos(List<SecteurDactiviteEntreprise> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.valeurAutre", target="valeurAutre"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="secteurDactivite", target="secteurDactivite"),
		@Mapping(source="entreprise", target="entreprise"),
	})
    SecteurDactiviteEntreprise toEntity(SecteurDactiviteEntrepriseDto dto, SecteurDactivite secteurDactivite, Entreprise entreprise) throws ParseException;

    //List<SecteurDactiviteEntreprise> toEntities(List<SecteurDactiviteEntrepriseDto> dtos) throws ParseException;

}
