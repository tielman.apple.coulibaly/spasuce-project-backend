

/*
 * Java transformer for entity table contenu_statique 
 * Created on 2020-03-23 ( Time 12:05:53 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "contenu_statique"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface ContenuStatiqueTransformer {

	ContenuStatiqueTransformer INSTANCE = Mappers.getMapper(ContenuStatiqueTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.typeContenuStatique.id", target="typeContenuStatiqueId"),
				@Mapping(source="entity.typeContenuStatique.code", target="typeContenuStatiqueCode"),
				@Mapping(source="entity.typeContenuStatique.libelle", target="typeContenuStatiqueLibelle"),
	})
	ContenuStatiqueDto toDto(ContenuStatique entity) throws ParseException;

    List<ContenuStatiqueDto> toDtos(List<ContenuStatique> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.contenu", target="contenu"),
		@Mapping(source="dto.titre", target="titre"),
		@Mapping(source="dto.ordre", target="ordre"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="typeContenuStatique", target="typeContenuStatique"),
	})
    ContenuStatique toEntity(ContenuStatiqueDto dto, TypeContenuStatique typeContenuStatique) throws ParseException;

    //List<ContenuStatique> toEntities(List<ContenuStatiqueDto> dtos) throws ParseException;

}
