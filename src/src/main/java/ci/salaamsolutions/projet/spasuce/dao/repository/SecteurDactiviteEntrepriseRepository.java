package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._SecteurDactiviteEntrepriseRepository;

/**
 * Repository : SecteurDactiviteEntreprise.
 */
@Repository
public interface SecteurDactiviteEntrepriseRepository extends JpaRepository<SecteurDactiviteEntreprise, Integer>, _SecteurDactiviteEntrepriseRepository {
	/**
	 * Finds SecteurDactiviteEntreprise by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object SecteurDactiviteEntreprise whose id is equals to the given id. If
	 *         no SecteurDactiviteEntreprise is found, this method returns null.
	 */
	@Query("select e from SecteurDactiviteEntreprise e where e.id = :id and e.isDeleted = :isDeleted")
	SecteurDactiviteEntreprise findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SecteurDactiviteEntreprise by using valeurAutre as a search criteria.
	 *
	 * @param valeurAutre
	 * @return An Object SecteurDactiviteEntreprise whose valeurAutre is equals to the given valeurAutre. If
	 *         no SecteurDactiviteEntreprise is found, this method returns null.
	 */
	@Query("select e from SecteurDactiviteEntreprise e where e.valeurAutre = :valeurAutre and e.isDeleted = :isDeleted")
	List<SecteurDactiviteEntreprise> findByValeurAutre(@Param("valeurAutre")String valeurAutre, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SecteurDactiviteEntreprise by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object SecteurDactiviteEntreprise whose createdAt is equals to the given createdAt. If
	 *         no SecteurDactiviteEntreprise is found, this method returns null.
	 */
	@Query("select e from SecteurDactiviteEntreprise e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<SecteurDactiviteEntreprise> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SecteurDactiviteEntreprise by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object SecteurDactiviteEntreprise whose createdBy is equals to the given createdBy. If
	 *         no SecteurDactiviteEntreprise is found, this method returns null.
	 */
	@Query("select e from SecteurDactiviteEntreprise e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<SecteurDactiviteEntreprise> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SecteurDactiviteEntreprise by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object SecteurDactiviteEntreprise whose updatedAt is equals to the given updatedAt. If
	 *         no SecteurDactiviteEntreprise is found, this method returns null.
	 */
	@Query("select e from SecteurDactiviteEntreprise e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<SecteurDactiviteEntreprise> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SecteurDactiviteEntreprise by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object SecteurDactiviteEntreprise whose updatedBy is equals to the given updatedBy. If
	 *         no SecteurDactiviteEntreprise is found, this method returns null.
	 */
	@Query("select e from SecteurDactiviteEntreprise e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<SecteurDactiviteEntreprise> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SecteurDactiviteEntreprise by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object SecteurDactiviteEntreprise whose deletedAt is equals to the given deletedAt. If
	 *         no SecteurDactiviteEntreprise is found, this method returns null.
	 */
	@Query("select e from SecteurDactiviteEntreprise e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<SecteurDactiviteEntreprise> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SecteurDactiviteEntreprise by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object SecteurDactiviteEntreprise whose deletedBy is equals to the given deletedBy. If
	 *         no SecteurDactiviteEntreprise is found, this method returns null.
	 */
	@Query("select e from SecteurDactiviteEntreprise e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<SecteurDactiviteEntreprise> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SecteurDactiviteEntreprise by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object SecteurDactiviteEntreprise whose isDeleted is equals to the given isDeleted. If
	 *         no SecteurDactiviteEntreprise is found, this method returns null.
	 */
	@Query("select e from SecteurDactiviteEntreprise e where e.isDeleted = :isDeleted")
	List<SecteurDactiviteEntreprise> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SecteurDactiviteEntreprise by using secteurDactiviteId as a search criteria.
	 *
	 * @param secteurDactiviteId
	 * @return A list of Object SecteurDactiviteEntreprise whose secteurDactiviteId is equals to the given secteurDactiviteId. If
	 *         no SecteurDactiviteEntreprise is found, this method returns null.
	 */
	@Query("select e from SecteurDactiviteEntreprise e where e.secteurDactivite.id = :secteurDactiviteId and e.isDeleted = :isDeleted")
	List<SecteurDactiviteEntreprise> findBySecteurDactiviteId(@Param("secteurDactiviteId")Integer secteurDactiviteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one SecteurDactiviteEntreprise by using secteurDactiviteId as a search criteria.
   *
   * @param secteurDactiviteId
   * @return An Object SecteurDactiviteEntreprise whose secteurDactiviteId is equals to the given secteurDactiviteId. If
   *         no SecteurDactiviteEntreprise is found, this method returns null.
   */
  @Query("select e from SecteurDactiviteEntreprise e where e.secteurDactivite.id = :secteurDactiviteId and e.isDeleted = :isDeleted")
  SecteurDactiviteEntreprise findSecteurDactiviteEntrepriseBySecteurDactiviteId(@Param("secteurDactiviteId")Integer secteurDactiviteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds SecteurDactiviteEntreprise by using entrepriseId as a search criteria.
	 *
	 * @param entrepriseId
	 * @return A list of Object SecteurDactiviteEntreprise whose entrepriseId is equals to the given entrepriseId. If
	 *         no SecteurDactiviteEntreprise is found, this method returns null.
	 */
	@Query("select e from SecteurDactiviteEntreprise e where e.entreprise.id = :entrepriseId and e.isDeleted = :isDeleted")
	List<SecteurDactiviteEntreprise> findByEntrepriseId(@Param("entrepriseId")Integer entrepriseId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one SecteurDactiviteEntreprise by using entrepriseId as a search criteria.
   *
   * @param entrepriseId
   * @return An Object SecteurDactiviteEntreprise whose entrepriseId is equals to the given entrepriseId. If
   *         no SecteurDactiviteEntreprise is found, this method returns null.
   */
  @Query("select e from SecteurDactiviteEntreprise e where e.entreprise.id = :entrepriseId and e.isDeleted = :isDeleted")
  SecteurDactiviteEntreprise findSecteurDactiviteEntrepriseByEntrepriseId(@Param("entrepriseId")Integer entrepriseId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of SecteurDactiviteEntreprise by using secteurDactiviteEntrepriseDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of SecteurDactiviteEntreprise
	 * @throws DataAccessException,ParseException
	 */
	public default List<SecteurDactiviteEntreprise> getByCriteria(Request<SecteurDactiviteEntrepriseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from SecteurDactiviteEntreprise e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<SecteurDactiviteEntreprise> query = em.createQuery(req, SecteurDactiviteEntreprise.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of SecteurDactiviteEntreprise by using secteurDactiviteEntrepriseDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of SecteurDactiviteEntreprise
	 *
	 */
	public default Long count(Request<SecteurDactiviteEntrepriseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from SecteurDactiviteEntreprise e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<SecteurDactiviteEntrepriseDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		SecteurDactiviteEntrepriseDto dto = request.getData() != null ? request.getData() : new SecteurDactiviteEntrepriseDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (SecteurDactiviteEntrepriseDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(SecteurDactiviteEntrepriseDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getValeurAutre())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("valeurAutre", dto.getValeurAutre(), "e.valeurAutre", "String", dto.getValeurAutreParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getSecteurDactiviteId()!= null && dto.getSecteurDactiviteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("secteurDactiviteId", dto.getSecteurDactiviteId(), "e.secteurDactivite.id", "Integer", dto.getSecteurDactiviteIdParam(), param, index, locale));
			}
			if (dto.getEntrepriseId()!= null && dto.getEntrepriseId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseId", dto.getEntrepriseId(), "e.entreprise.id", "Integer", dto.getEntrepriseIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSecteurDactiviteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("secteurDactiviteCode", dto.getSecteurDactiviteCode(), "e.secteurDactivite.code", "String", dto.getSecteurDactiviteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSecteurDactiviteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("secteurDactiviteLibelle", dto.getSecteurDactiviteLibelle(), "e.secteurDactivite.libelle", "String", dto.getSecteurDactiviteLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEntrepriseNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseNom", dto.getEntrepriseNom(), "e.entreprise.nom", "String", dto.getEntrepriseNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEntrepriseLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseLogin", dto.getEntrepriseLogin(), "e.entreprise.login", "String", dto.getEntrepriseLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
	
	
	
	/**
	 * Finds List of SecteurDactiviteEntreprise by using secteurDactiviteEntrepriseDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of SecteurDactiviteEntreprise
	 * @throws DataAccessException,ParseException
	 */
	public default List<Entreprise> getByCriteriaCustom(Request<SecteurDactiviteEntrepriseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select distinct e.entreprise from SecteurDactiviteEntreprise e where e.entreprise IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		//req += " order by u.id desc";
		TypedQuery<Entreprise> query = em.createQuery(req, Entreprise.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}
}
