

/*
 * Java transformer for entity table critere_filtre 
 * Created on 2020-01-02 ( Time 17:59:55 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "critere_filtre"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface CritereFiltreTransformer {

	CritereFiltreTransformer INSTANCE = Mappers.getMapper(CritereFiltreTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.filtre.id", target="filtreId"),
				@Mapping(source="entity.filtre.code", target="filtreCode"),
				@Mapping(source="entity.filtre.libelle", target="filtreLibelle"),
		@Mapping(source="entity.typeCritereAppelDoffres.id", target="typeCritereFiltreId"),
				@Mapping(source="entity.typeCritereAppelDoffres.code", target="typeCritereAppelDoffresCode"),
				@Mapping(source="entity.typeCritereAppelDoffres.libelle", target="typeCritereAppelDoffresLibelle"),
	})
	CritereFiltreDto toDto(CritereFiltre entity) throws ParseException;

    List<CritereFiltreDto> toDtos(List<CritereFiltre> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="filtre", target="filtre"),
		@Mapping(source="typeCritereAppelDoffres", target="typeCritereAppelDoffres"),
	})
    CritereFiltre toEntity(CritereFiltreDto dto, Filtre filtre, TypeCritereAppelDoffres typeCritereAppelDoffres) throws ParseException;

    //List<CritereFiltre> toEntities(List<CritereFiltreDto> dtos) throws ParseException;

}
