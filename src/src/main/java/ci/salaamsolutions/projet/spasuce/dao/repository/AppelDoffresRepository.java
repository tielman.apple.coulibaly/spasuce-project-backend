package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._AppelDoffresRepository;

/**
 * Repository : AppelDoffres.
 */
@Repository
public interface AppelDoffresRepository extends JpaRepository<AppelDoffres, Integer>, _AppelDoffresRepository {
	/**
	 * Finds AppelDoffres by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object AppelDoffres whose id is equals to the given id. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.id = :id and e.isDeleted = :isDeleted")
	AppelDoffres findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AppelDoffres by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object AppelDoffres whose libelle is equals to the given libelle. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	AppelDoffres findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds AppelDoffres by using code as a search criteria.
	 *
	 * @param code
	 * @return An Object AppelDoffres whose code is equals to the given code. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.code = :code and e.isDeleted = :isDeleted")
	AppelDoffres findByCode(@Param("code")String code, @Param("isDeleted")Boolean isDeleted);
	

	/**
	 * Finds AppelDoffres by using prixPrestation as a search criteria.
	 *
	 * @param prixPrestation
	 * @return An Object AppelDoffres whose prixPrestation is equals to the given prixPrestation. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.prixPrestation = :prixPrestation and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByPrixPrestation(@Param("prixPrestation")Integer prixPrestation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using nombreTypeDuree as a search criteria.
	 *
	 * @param nombreTypeDuree
	 * @return An Object AppelDoffres whose nombreTypeDuree is equals to the given nombreTypeDuree. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.nombreTypeDuree = :nombreTypeDuree and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByNombreTypeDuree(@Param("nombreTypeDuree")Integer nombreTypeDuree, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using description as a search criteria.
	 *
	 * @param description
	 * @return An Object AppelDoffres whose description is equals to the given description. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.description = :description and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByDescription(@Param("description")String description, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using dateLimiteDepot as a search criteria.
	 *
	 * @param dateLimiteDepot
	 * @return An Object AppelDoffres whose dateLimiteDepot is equals to the given dateLimiteDepot. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.dateLimiteDepot = :dateLimiteDepot and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByDateLimiteDepot(@Param("dateLimiteDepot")Date dateLimiteDepot, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using dateCreation as a search criteria.
	 *
	 * @param dateCreation
	 * @return An Object AppelDoffres whose dateCreation is equals to the given dateCreation. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.dateCreation = :dateCreation and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByDateCreation(@Param("dateCreation")Date dateCreation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using dureRealisation as a search criteria.
	 *
	 * @param dureRealisation
	 * @return An Object AppelDoffres whose dureRealisation is equals to the given dureRealisation. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.dureRealisation = :dureRealisation and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByDureRealisation(@Param("dureRealisation")String dureRealisation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using isLocked as a search criteria.
	 *
	 * @param isLocked
	 * @return An Object AppelDoffres whose isLocked is equals to the given isLocked. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.isLocked = :isLocked and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByIsLocked(@Param("isLocked")Boolean isLocked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using isContenuNotificationDispo as a search criteria.
	 *
	 * @param isContenuNotificationDispo
	 * @return An Object AppelDoffres whose isContenuNotificationDispo is equals to the given isContenuNotificationDispo. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.isContenuNotificationDispo = :isContenuNotificationDispo and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByIsContenuNotificationDispo(@Param("isContenuNotificationDispo")Boolean isContenuNotificationDispo, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using isAttribuer as a search criteria.
	 *
	 * @param isAttribuer
	 * @return An Object AppelDoffres whose isAttribuer is equals to the given isAttribuer. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.isAttribuer = :isAttribuer and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByIsAttribuer(@Param("isAttribuer")Boolean isAttribuer, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using isModifiable as a search criteria.
	 *
	 * @param isModifiable
	 * @return An Object AppelDoffres whose isModifiable is equals to the given isModifiable. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.isModifiable = :isModifiable and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByIsModifiable(@Param("isModifiable")Boolean isModifiable, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using isRetirer as a search criteria.
	 *
	 * @param isRetirer
	 * @return An Object AppelDoffres whose isRetirer is equals to the given isRetirer. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.isRetirer = :isRetirer and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByIsRetirer(@Param("isRetirer")Boolean isRetirer, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using isVisibleByClient as a search criteria.
	 *
	 * @param isVisibleByClient
	 * @return An Object AppelDoffres whose isVisibleByClient is equals to the given isVisibleByClient. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.isVisibleByClient = :isVisibleByClient and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByIsVisibleByClient(@Param("isVisibleByClient")Boolean isVisibleByClient, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using isUpdate as a search criteria.
	 *
	 * @param isUpdate
	 * @return An Object AppelDoffres whose isUpdate is equals to the given isUpdate. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.isUpdate = :isUpdate and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByIsUpdate(@Param("isUpdate")Boolean isUpdate, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object AppelDoffres whose createdAt is equals to the given createdAt. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object AppelDoffres whose createdBy is equals to the given createdBy. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object AppelDoffres whose updatedAt is equals to the given updatedAt. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object AppelDoffres whose updatedBy is equals to the given updatedBy. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object AppelDoffres whose deletedAt is equals to the given deletedAt. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object AppelDoffres whose deletedBy is equals to the given deletedBy. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AppelDoffres by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object AppelDoffres whose isDeleted is equals to the given isDeleted. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.isDeleted = :isDeleted")
	List<AppelDoffres> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AppelDoffres by using villeId as a search criteria.
	 *
	 * @param villeId
	 * @return A list of Object AppelDoffres whose villeId is equals to the given villeId. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.ville.id = :villeId and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByVilleId(@Param("villeId")Integer villeId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one AppelDoffres by using villeId as a search criteria.
   *
   * @param villeId
   * @return An Object AppelDoffres whose villeId is equals to the given villeId. If
   *         no AppelDoffres is found, this method returns null.
   */
  @Query("select e from AppelDoffres e where e.ville.id = :villeId and e.isDeleted = :isDeleted")
  AppelDoffres findAppelDoffresByVilleId(@Param("villeId")Integer villeId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds AppelDoffres by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object AppelDoffres whose etatId is equals to the given etatId. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one AppelDoffres by using etatId as a search criteria.
   *
   * @param etatId
   * @return An Object AppelDoffres whose etatId is equals to the given etatId. If
   *         no AppelDoffres is found, this method returns null.
   */
  @Query("select e from AppelDoffres e where e.etat.id = :etatId and e.isDeleted = :isDeleted")
  AppelDoffres findAppelDoffresByEtatId(@Param("etatId")Integer etatId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds AppelDoffres by using typeDureeId as a search criteria.
	 *
	 * @param typeDureeId
	 * @return A list of Object AppelDoffres whose typeDureeId is equals to the given typeDureeId. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.typeDuree.id = :typeDureeId and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByTypeDureeId(@Param("typeDureeId")Integer typeDureeId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one AppelDoffres by using typeDureeId as a search criteria.
   *
   * @param typeDureeId
   * @return An Object AppelDoffres whose typeDureeId is equals to the given typeDureeId. If
   *         no AppelDoffres is found, this method returns null.
   */
  @Query("select e from AppelDoffres e where e.typeDuree.id = :typeDureeId and e.isDeleted = :isDeleted")
  AppelDoffres findAppelDoffresByTypeDureeId(@Param("typeDureeId")Integer typeDureeId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds AppelDoffres by using typeAppelDoffresId as a search criteria.
	 *
	 * @param typeAppelDoffresId
	 * @return A list of Object AppelDoffres whose typeAppelDoffresId is equals to the given typeAppelDoffresId. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.typeAppelDoffres.id = :typeAppelDoffresId and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByTypeAppelDoffresId(@Param("typeAppelDoffresId")Integer typeAppelDoffresId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one AppelDoffres by using typeAppelDoffresId as a search criteria.
   *
   * @param typeAppelDoffresId
   * @return An Object AppelDoffres whose typeAppelDoffresId is equals to the given typeAppelDoffresId. If
   *         no AppelDoffres is found, this method returns null.
   */
  @Query("select e from AppelDoffres e where e.typeAppelDoffres.id = :typeAppelDoffresId and e.isDeleted = :isDeleted")
  AppelDoffres findAppelDoffresByTypeAppelDoffresId(@Param("typeAppelDoffresId")Integer typeAppelDoffresId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds AppelDoffres by using userClientId as a search criteria.
	 *
	 * @param userClientId
	 * @return A list of Object AppelDoffres whose userClientId is equals to the given userClientId. If
	 *         no AppelDoffres is found, this method returns null.
	 */
	@Query("select e from AppelDoffres e where e.user.id = :userClientId and e.isDeleted = :isDeleted")
	List<AppelDoffres> findByUserClientId(@Param("userClientId")Integer userClientId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one AppelDoffres by using userClientId as a search criteria.
   *
   * @param userClientId
   * @return An Object AppelDoffres whose userClientId is equals to the given userClientId. If
   *         no AppelDoffres is found, this method returns null.
   */
  @Query("select e from AppelDoffres e where e.user.id = :userClientId and e.isDeleted = :isDeleted")
  AppelDoffres findAppelDoffresByUserClientId(@Param("userClientId")Integer userClientId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of AppelDoffres by using appelDoffresDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of AppelDoffres
	 * @throws DataAccessException,ParseException
	 */
	public default List<AppelDoffres> getByCriteria(Request<AppelDoffresDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from AppelDoffres e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<AppelDoffres> query = em.createQuery(req, AppelDoffres.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of AppelDoffres by using appelDoffresDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of AppelDoffres
	 *
	 */
	public default Long count(Request<AppelDoffresDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from AppelDoffres e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<AppelDoffresDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		AppelDoffresDto dto = request.getData() != null ? request.getData() : new AppelDoffresDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (AppelDoffresDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(AppelDoffresDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("code", dto.getCode(), "e.code", "String", dto.getCodeParam(), param, index, locale));
			}
			if (dto.getPrixPrestation()!= null && dto.getPrixPrestation() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prixPrestation", dto.getPrixPrestation(), "e.prixPrestation", "Integer", dto.getPrixPrestationParam(), param, index, locale));
			}
			if (dto.getNombreTypeDuree()!= null && dto.getNombreTypeDuree() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nombreTypeDuree", dto.getNombreTypeDuree(), "e.nombreTypeDuree", "Integer", dto.getNombreTypeDureeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDescription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateLimiteDepot())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateLimiteDepot", dto.getDateLimiteDepot(), "e.dateLimiteDepot", "Date", dto.getDateLimiteDepotParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateCreation())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateCreation", dto.getDateCreation(), "e.dateCreation", "Date", dto.getDateCreationParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDureRealisation())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dureRealisation", dto.getDureRealisation(), "e.dureRealisation", "String", dto.getDureRealisationParam(), param, index, locale));
			}
			if (dto.getIsLocked()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isLocked", dto.getIsLocked(), "e.isLocked", "Boolean", dto.getIsLockedParam(), param, index, locale));
			}
			if (dto.getIsContenuNotificationDispo()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isContenuNotificationDispo", dto.getIsContenuNotificationDispo(), "e.isContenuNotificationDispo", "Boolean", dto.getIsContenuNotificationDispoParam(), param, index, locale));
			}
			if (dto.getIsAttribuer()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isAttribuer", dto.getIsAttribuer(), "e.isAttribuer", "Boolean", dto.getIsAttribuerParam(), param, index, locale));
			}
			if (dto.getIsModifiable()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isModifiable", dto.getIsModifiable(), "e.isModifiable", "Boolean", dto.getIsModifiableParam(), param, index, locale));
			}
			if (dto.getIsRetirer()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isRetirer", dto.getIsRetirer(), "e.isRetirer", "Boolean", dto.getIsRetirerParam(), param, index, locale));
			}
			if (dto.getIsVisibleByClient()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isVisibleByClient", dto.getIsVisibleByClient(), "e.isVisibleByClient", "Boolean", dto.getIsVisibleByClientParam(), param, index, locale));
			}
			if (dto.getIsUpdate()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isUpdate", dto.getIsUpdate(), "e.isUpdate", "Boolean", dto.getIsUpdateParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getVilleId()!= null && dto.getVilleId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("villeId", dto.getVilleId(), "e.ville.id", "Integer", dto.getVilleIdParam(), param, index, locale));
			}
			if (dto.getEtatId()!= null && dto.getEtatId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatId", dto.getEtatId(), "e.etat.id", "Integer", dto.getEtatIdParam(), param, index, locale));
			}
			if (dto.getTypeDureeId()!= null && dto.getTypeDureeId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeDureeId", dto.getTypeDureeId(), "e.typeDuree.id", "Integer", dto.getTypeDureeIdParam(), param, index, locale));
			}
			if (dto.getTypeAppelDoffresId()!= null && dto.getTypeAppelDoffresId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeAppelDoffresId", dto.getTypeAppelDoffresId(), "e.typeAppelDoffres.id", "Integer", dto.getTypeAppelDoffresIdParam(), param, index, locale));
			}
			if (dto.getUserClientId()!= null && dto.getUserClientId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userClientId", dto.getUserClientId(), "e.user.id", "Integer", dto.getUserClientIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getVilleCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("villeCode", dto.getVilleCode(), "e.ville.code", "String", dto.getVilleCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getVilleLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("villeLibelle", dto.getVilleLibelle(), "e.ville.libelle", "String", dto.getVilleLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatCode", dto.getEtatCode(), "e.etat.code", "String", dto.getEtatCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatLibelle", dto.getEtatLibelle(), "e.etat.libelle", "String", dto.getEtatLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeDureeLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeDureeLibelle", dto.getTypeDureeLibelle(), "e.typeDuree.libelle", "String", dto.getTypeDureeLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeDureeCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeDureeCode", dto.getTypeDureeCode(), "e.typeDuree.code", "String", dto.getTypeDureeCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeAppelDoffresCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeAppelDoffresCode", dto.getTypeAppelDoffresCode(), "e.typeAppelDoffres.code", "String", dto.getTypeAppelDoffresCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeAppelDoffresLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeAppelDoffresLibelle", dto.getTypeAppelDoffresLibelle(), "e.typeAppelDoffres.libelle", "String", dto.getTypeAppelDoffresLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserPrenoms())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenoms", dto.getUserPrenoms(), "e.user.prenoms", "String", dto.getUserPrenomsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
