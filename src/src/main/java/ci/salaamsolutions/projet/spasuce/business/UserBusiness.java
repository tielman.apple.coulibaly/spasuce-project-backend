


/*
 * Java transformer for entity table user
 * Created on 2020-01-02 ( Time 18:00:05 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffres;
import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffresRestreint;
import ci.salaamsolutions.projet.spasuce.dao.entity.CarnetDadresse;
import ci.salaamsolutions.projet.spasuce.dao.entity.Domaine;
import ci.salaamsolutions.projet.spasuce.dao.entity.DomaineEntreprise;
import ci.salaamsolutions.projet.spasuce.dao.entity.Entreprise;
import ci.salaamsolutions.projet.spasuce.dao.entity.Fonctionnalite;
import ci.salaamsolutions.projet.spasuce.dao.entity.Offre;
import ci.salaamsolutions.projet.spasuce.dao.entity.Realisation;
import ci.salaamsolutions.projet.spasuce.dao.entity.Role;
import ci.salaamsolutions.projet.spasuce.dao.entity.RoleFonctionnalite;
import ci.salaamsolutions.projet.spasuce.dao.entity.SecteurDactivite;
import ci.salaamsolutions.projet.spasuce.dao.entity.SecteurDactiviteEntreprise;
import ci.salaamsolutions.projet.spasuce.dao.entity.User;
import ci.salaamsolutions.projet.spasuce.dao.repository.AppelDoffresRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.AppelDoffresRestreintRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.CarnetDadresseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.DomaineEntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.EntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.OffreRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.RealisationRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.RoleFonctionnaliteRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.RoleRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.SecteurDactiviteAppelDoffresRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.SecteurDactiviteEntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.UserRepository;
import ci.salaamsolutions.projet.spasuce.helper.ExceptionUtils;
import ci.salaamsolutions.projet.spasuce.helper.FunctionalError;
import ci.salaamsolutions.projet.spasuce.helper.HostingUtils;
import ci.salaamsolutions.projet.spasuce.helper.ParamsUtils;
import ci.salaamsolutions.projet.spasuce.helper.TechnicalError;
import ci.salaamsolutions.projet.spasuce.helper.Utilities;
import ci.salaamsolutions.projet.spasuce.helper.Validate;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.dto.AppelDoffresDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.AppelDoffresRestreintDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.CarnetDadresseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.EntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.OffreDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.RealisationDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteAppelDoffresDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.UserDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.AppelDoffresRestreintTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.AppelDoffresTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.CarnetDadresseTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.DomaineTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.EntrepriseTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.FonctionnaliteTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.OffreTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.RealisationTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.SecteurDactiviteTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.UserTransformer;
import ci.salaamsolutions.projet.spasuce.helper.enums.EmailEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.GlobalEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeUserEnum;

/**
BUSINESS for table "user"
 *
 * @author SFL Back-End developper
 *
 */
@Component
@EnableAsync
public class UserBusiness implements IBasicBusiness<Request<UserDto>, Response<UserDto>> {

	private Response<UserDto> response;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private OffreRepository offreRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private RealisationRepository realisationRepository;
	@Autowired
	private AppelDoffresRestreintRepository appelDoffresRestreintRepository;
	@Autowired
	private AppelDoffresRepository appelDoffresRepository;
	@Autowired
	private CarnetDadresseRepository carnetDadresseRepository;
	@Autowired
	private EntrepriseRepository entrepriseRepository;
	@Autowired
	private RoleFonctionnaliteRepository roleFonctionnaliteRepository;
	
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private OffreBusiness offreBusiness;


	@Autowired
	private RealisationBusiness realisationBusiness;


	@Autowired
	private AppelDoffresRestreintBusiness appelDoffresRestreintBusiness;


	@Autowired
	private AppelDoffresBusiness appelDoffresBusiness;


	@Autowired
	private CarnetDadresseBusiness carnetDadresseBusiness;
	
	@Autowired
	private EntrepriseBusiness entrepriseBusiness;
	
	@Autowired
	private SecteurDactiviteEntrepriseRepository secteurDactiviteEntrepriseRepository;
	@Autowired
	private DomaineEntrepriseRepository domaineEntrepriseRepository;
	
	@Autowired
	private SecteurDactiviteAppelDoffresRepository secteurDactiviteAppelDoffresRepository ;
	
	
	@Autowired
	private SecteurDactiviteAppelDoffresBusiness secteurDactiviteAppelDoffresBusiness ;
	
	 
	@Autowired
	private ParamsUtils paramsUtils;

	private Context context;

	@Autowired
	private HostingUtils hostingUtils;


	public UserBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<UserDto> create(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin create User-----");

		response = new Response<UserDto>();

		try {
//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}
//
//			User utilisateur = userRepository.findById(request.getUser(), false);
//			if (utilisateur == null) {
//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
//				response.setHasError(true);
//				return response;
//			}
//			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
//				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
//				response.setHasError(true);
//				return response;
//			}

			List<User> items = new ArrayList<User>();
			List<Entreprise> itemsEntreprise = new ArrayList<>();
			boolean isUpdateEntreprise = false ;

			for (UserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("nom", dto.getNom());
				//fieldsToVerify.put("prenoms", dto.getPrenoms());
				fieldsToVerify.put("login", dto.getLogin());
				fieldsToVerify.put("password", dto.getPassword());
				//fieldsToVerify.put("telephone", dto.getTelephone());
				fieldsToVerify.put("email", dto.getEmail());
				fieldsToVerify.put("dataEntreprise", dto.getDataEntreprise());


				//fieldsToVerify.put("isLocked", dto.getIsLocked());
				//fieldsToVerify.put("isSuUser", dto.getIsSuUser());
				//fieldsToVerify.put("isValider", dto.getIsValider());
				//fieldsToVerify.put("token", dto.getToken());
				//fieldsToVerify.put("dateExpirationToken", dto.getDateExpirationToken());
				//fieldsToVerify.put("isTokenValide", dto.getIsTokenValide());
				fieldsToVerify.put("roleId", dto.getRoleId());
				//fieldsToVerify.put("roleCode", dto.getRoleCode());
				//fieldsToVerify.put("entrepriseId", dto.getEntrepriseId());
				//				fieldsToVerify.put("createdBy", dto.getCreatedBy());
				//				fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
				//				fieldsToVerify.put("deletedAt", dto.getDeletedAt());
				//				fieldsToVerify.put("deletedBy", dto.getDeletedBy());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if user to insert do not exist
				User existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = userRepository.findByLogin(dto.getLogin(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getLogin(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getLogin()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getLogin()+"' pour les users", locale));
					response.setHasError(true);
					return response;
				}

				if (Utilities.notBlank(dto.getTelephone())) {

					// TODO : à revoir pour les autres pays
					if (!Utilities.isValidateIvorianPhoneNumber(dto.getTelephone())){
						response.setStatus(functionalError.INVALID_DATA("user -> " + dto.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}
					existingEntity = userRepository.findByTelephone(dto.getTelephone(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les users", locale));
						response.setHasError(true);
						return response;
					}
				}

				// Verify if role exist
				//Role existingRole = roleRepository.findByCode(dto.getRoleCode(), false);
				Role existingRole = roleRepository.findById(dto.getRoleId(), false);
				if (existingRole == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getRoleId(), locale));
					response.setHasError(true);
					return response;
				}
				Entreprise existingEntreprise = null ;
				// Verify if entreprise exist
				//if (dto.getDataEntreprise() != null) {
				if (dto.getDataEntreprise().getId() != null && dto.getDataEntreprise().getId() > 0) {
					
					/*
					dto.setEntrepriseId(dto.getDataEntreprise().getId());
					// update entreprise
					Request<EntrepriseDto> req = new Request<EntrepriseDto>();
					Response<EntrepriseDto> res = new Response<EntrepriseDto>();
					
					req.setDatas(Arrays.asList(dto.getDataEntreprise()));

					res = entrepriseBusiness.update(req, locale);
					if (res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
					
					
					if (Utilities.isNotEmpty(res.getItems())) {
						dto.setEntrepriseId(res.getItems().get(0).getId());
					}
					//*/
					isUpdateEntreprise = true ;
					
					existingEntreprise =  entrepriseRepository.findById(dto.getDataEntreprise().getId(), false);
					if (existingEntreprise == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("entreprise -> " + dto.getDataEntreprise().getId(), locale));
						response.setHasError(true);
						return response;
					}
					dto.setEntrepriseId(dto.getDataEntreprise().getId());
					
					//existingEntreprise = entrepriseRepository.findById(dto.getDataEntreprise().getId(), false);
				}else {
					// create entreprise
					Request<EntrepriseDto> req = new Request<EntrepriseDto>();
					Response<EntrepriseDto> res = new Response<EntrepriseDto>();

					req.setDatas(Arrays.asList(dto.getDataEntreprise()));

					res = entrepriseBusiness.create(req, locale);
					if (res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
					if (Utilities.isNotEmpty(res.getItems())) {
						dto.setEntrepriseId(res.getItems().get(0).getId());
					}
				}

				//}
				existingEntreprise = entrepriseRepository.findById(dto.getEntrepriseId(), false);
				
				
				// verification du nbre de user par entreprise et // verfier la non duplication des roles pour une meme entreprise
				List<User> users = userRepository.findByEntrepriseIdAndTypeEntrepriseCode(existingEntreprise.getId(), existingEntreprise.getTypeEntreprise().getCode(), false);
				if (Utilities.isNotEmpty(users)) {
					
					switch (existingEntreprise.getTypeEntreprise().getCode()) {
					case TypeUserEnum.ENTREPRISE_FOURNISSEUR:
						response.setStatus(functionalError.DISALLOWED_OPERATION("L'entreprise ->"+ existingEntreprise.getNom() +"  a déjà "+users.size()+" utilisateur.", locale));
						response.setHasError(true);
						return response;
						//break;
					case TypeUserEnum.ENTREPRISE_MIXTE:
						if (Utilities.isNotEmpty(users) && users.size() >= GlobalEnum.NBRE_MAXI_USER_PAR_ENTREPRISE_MIXTE) {
							response.setStatus(functionalError.DISALLOWED_OPERATION("L'entreprise ->"+ existingEntreprise.getNom() +"  a déjà "+users.size()+" utilisateur(s).", locale));
							response.setHasError(true);
							return response;
						}
						//break;
					case TypeUserEnum.ENTREPRISE_CLIENTE:
						if (Utilities.isNotEmpty(users) && users.size() >= GlobalEnum.NBRE_MAXI_USER_PAR_ENTREPRISE_CLIENTE) {
							response.setStatus(functionalError.DISALLOWED_OPERATION("L'entreprise ->"+ existingEntreprise.getNom() +"  a déjà "+users.size()+" utilisateur(s).", locale));
							response.setHasError(true);
							return response;
						}
						//break;

					default:
						break;
					}
					
//					if (existingEntreprise.getTypeEntreprise().getCode().equals(TypeUserEnum.ENTREPRISE_FOURNISSEUR)) {
//						response.setStatus(functionalError.DISALLOWED_OPERATION("L'entreprise ->"+ existingEntreprise.getNom() +"  a déjà "+users.size()+" utilisateur.", locale));
//						response.setHasError(true);
//						return response;
//					}else {
//						if (Utilities.isNotEmpty(users) && users.size() >= GlobalEnum.NBRE_MAXI_USER_PAR_ENTREPRISE_CLIENTE) {
//							response.setStatus(functionalError.DISALLOWED_OPERATION("L'entreprise ->"+ existingEntreprise.getNom() +"  a déjà "+users.size()+" utilisateur(s).", locale));
//							response.setHasError(true);
//							return response;
//						}
//						
//						// le cas des entreprise mxite
//					}
//					
					for (User data : users) {						
						if (data.getRole().getId().equals(dto.getRoleId())) {
							response.setStatus(functionalError.DISALLOWED_OPERATION("Un utilisateur de l'entreprise ->"+ existingEntreprise.getNom() +"  a déjà le role ->" + data.getRole().getLibelle(), locale));
							response.setHasError(true);
							return response;
						}
					}
				}
				
				
				
				//existingEntity = userRepository.findByEmail(dto.getEmail(), false);
				List<User> listExistingEntity = userRepository.findByEmailAndEntrepriseIdDifferent(dto.getEmail(), dto.getEntrepriseId(), false);
				if (Utilities.isNotEmpty(listExistingEntity)) {
					response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getEmail(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les users", locale));
					response.setHasError(true);
					return response;
				}

				itemsEntreprise.add(existingEntreprise);

				dto.setIsLocked(true); // il sera valider a l'inscription

//				if (dto.getIsLocked() == null) {
//					dto.setIsLocked(false);
//				}
				if (dto.getIsValider() == null) {
					dto.setIsValider(true);
				}
				// HASHAGE du password avant de le mettre dans la BD
				dto.setPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getPassword()));

				User entityToSave = null;
				entityToSave = UserTransformer.INSTANCE.toEntity(dto, existingRole, existingEntreprise);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				
				// infos sur token
				// setter token, isValidToken et tokenCreatedAt
				entityToSave.setToken(Utilities.randomString(50));
				GregorianCalendar calendar =new GregorianCalendar();

				calendar.setTime(Utilities.getCurrentDate());
				calendar.add(Calendar.HOUR, 24);
				//entityToSave.setDateExpirationToken(calendar.getTime());
				entityToSave.setIsTokenValide(true);
				
				
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<User> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = userRepository.save((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}
				
				if (!itemsEntreprise.isEmpty()) {
					for (Entreprise data : itemsEntreprise) {
						data.setCreatedBy(itemsSaved.get(0).getId());
					}
					if (isUpdateEntreprise) {
						for (Entreprise data : itemsEntreprise) {
							data.setUpdatedBy(itemsSaved.get(0).getId());
						}
					}
					entrepriseRepository.save((Iterable<Entreprise>)itemsEntreprise);
				}
				List<UserDto> itemsDto = new ArrayList<UserDto>();
				for (User entity : itemsSaved) {
					UserDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
				
				
				// mise à jour du user (fournisseur ou mixte) par rapport aux appels d'offres du système
				/*
				if (Utilities.isNotEmpty(itemsDto)) {
					List<AppelDoffresRestreint> datasAppelDoffresRestreint = new ArrayList<>();

					for (UserDto userDto : itemsDto) {
						
						if (userDto.getTypeEntrepriseCode().equals(TypeUserEnum.ENTREPRISE_MIXTE)
								|| userDto.getTypeEntrepriseCode().equals(TypeUserEnum.ENTREPRISE_FOURNISSEUR)) {
							
							User user = userRepository.findById(userDto.getId(), false) ;

							// recuperation de toute les offres lies à ses secteurs puis creation dans appelDoffresRestreint

							// formation de la request 
							Request<SecteurDactiviteAppelDoffresDto> req = new Request<>();
							List<AppelDoffres> listAppelDoffres = new ArrayList<>();
							List<SecteurDactiviteAppelDoffresDto> datas = new ArrayList<>();


							List<SecteurDactiviteEntreprise> secteurDactiviteEntreprises = secteurDactiviteEntrepriseRepository.findByEntrepriseId(userDto.getEntrepriseId(), false);
							if (Utilities.isNotEmpty(secteurDactiviteEntreprises)) {
								for (SecteurDactiviteEntreprise secteurDactiviteEntreprise : secteurDactiviteEntreprises) {

									SecteurDactiviteAppelDoffresDto data = new SecteurDactiviteAppelDoffresDto();
									
									data.setIsAttribuer(false); // non attribués
									data.setEtatCode(EtatEnum.ENTRANT); // visible par les forunisseurs
									data.setSecteurDactiviteId(secteurDactiviteEntreprise.getSecteurDactivite().getId());
									data.setTypeAppelDoffresCode(TypeAppelDoffresEnum.GENERAL); // AO general
									datas.add(data);
								}
								req.setData(datas.get(0));
								datas.remove(0); // important pour eviter des recupereantion en doublon
								req.setDatas(datas);
								//req.setIsAnd(false);
							}

							slf4jLogger.info("-------request-------"+ request);
							slf4jLogger.info("-------request-------"+ request.toString());
							slf4jLogger.info("-------request-------"+ request.getData());
							slf4jLogger.info("-------request-------"+ request.getDatas());

							// on veut la liste des appelDoffres generaux non attribués qui concernent les secteurs d'activités de ce user
							listAppelDoffres = secteurDactiviteAppelDoffresRepository.getByCriteriaCustom(req, em, locale);
							
							if (Utilities.isNotEmpty(listAppelDoffres)) {
								
 								for (AppelDoffres data : listAppelDoffres) {
									
									AppelDoffresRestreint appelDoffresRestreint = new AppelDoffresRestreint();
									
									appelDoffresRestreint.setAppelDoffres(data);
									appelDoffresRestreint.setUser(user);
									appelDoffresRestreint.setIsAppelRestreint(false); // pour dire que se sont des elements pour visualtisations
									appelDoffresRestreint.setIsSoumissionnerByFournisseur(false);
									appelDoffresRestreint.setIsDesactiver(false);
									appelDoffresRestreint.setIsVisualiserContenuByFournisseur(false);
									appelDoffresRestreint.setIsVisualiserNotifByFournisseur(false);
									//appelDoffresRestreint.setIsUpdateAppelDoffres(data.getIsUpdate()); // MAJ mis en evidence ne le concerne pas
									appelDoffresRestreint.setIsUpdateAppelDoffres(false);
									appelDoffresRestreint.setIsVisibleByFournisseur(true); // car il ne l'a pas encore supprimer logiquement
									appelDoffresRestreint.setCreatedAt(Utilities.getCurrentDate());
									appelDoffresRestreint.setCreatedBy(user.getId());
									appelDoffresRestreint.setIsSelectedByClient(false);
									appelDoffresRestreint.setIsDeleted(false);
									datasAppelDoffresRestreint.add(appelDoffresRestreint);
								}
							}
						}
					}
					if (Utilities.isNotEmpty(datasAppelDoffresRestreint)) {
						appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) datasAppelDoffresRestreint);
					}
				}
				
				//*/
				
				// envoi de mail 
				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user" ,  paramsUtils.getSmtpMailUser());

					// choisir la vraie url
					String appLink = "";
					//			if (entityToSave.getAdmin() != null ) {
					//				appLink = paramsUtils.getUrlRootAdmin();
					//			}else {
					//				appLink = paramsUtils.getUrlRootUser();
					//			}

					appLink = paramsUtils.getUrlRootUser();

					for (User user : itemsSaved) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_CREATION_COMPTE;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						//String template = paramsUtils.getTemplateCreationCompteParUser();
						String template = paramsUtils.getTemplateCreationCompteParUserWithToken();
						
						context.setVariable("email", user.getEmail());
						//context.setVariable("login", user.getLogin());
						context.setVariable("token", user.getToken());
						//context.setVariable("expiration", dateFormat.format(user.getDateExpirationToken()));
						
						
						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable(EmailEnum.nomEntreprise, user.getEntreprise().getNom());
						context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,template, locale);
						// si jamais une exception c'est produite pour un mail

					}
				}
			}

			slf4jLogger.info("----end create User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<UserDto> update(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin update User-----");

		response = new Response<UserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<User> items = new ArrayList<User>();

			for (UserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la user existe
				User entityToSave = null;
				entityToSave = userRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				
				
				Integer entityToSaveId = entityToSave.getId();

				
				if (!utilisateur.getId().equals(entityToSaveId)) {
					response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" est different de celui qu'il veut modifier : " +entityToSave.getLogin(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if role exist
				if (dto.getRoleId() != null && dto.getRoleId() > 0){
					Role existingRole = roleRepository.findById(dto.getRoleId(), false);
					if (existingRole == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getRoleId(), locale));
						response.setHasError(true);
						return response;
					}
					
					
					
					// verfier la non duplication des roles pour une meme entreprise
					List<User> users = userRepository.findByEntrepriseId(entityToSave.getEntreprise().getId(), false);
					if (Utilities.isNotEmpty(users)) {
						
						// TODO : PRENDRE EN COMPTE LE CHANGEMENT DE TYPE POUR UNE ENTREPRISE
						
//						switch (existingEntreprise.getTypeEntreprise().getCode()) {
//						case TypeUserEnum.ENTREPRISE_FOURNISSEUR:
//							response.setStatus(functionalError.DISALLOWED_OPERATION("L'entreprise ->"+ existingEntreprise.getNom() +"  a déjà "+users.size()+" utilisateur.", locale));
//							response.setHasError(true);
//							return response;
//							//break;
//						case TypeUserEnum.ENTREPRISE_MIXTE:
//							if (Utilities.isNotEmpty(users) && users.size() >= GlobalEnum.NBRE_MAXI_USER_PAR_ENTREPRISE_MIXTE) {
//								response.setStatus(functionalError.DISALLOWED_OPERATION("L'entreprise ->"+ existingEntreprise.getNom() +"  a déjà "+users.size()+" utilisateur(s).", locale));
//								response.setHasError(true);
//								return response;
//							}
//							//break;
//						case TypeUserEnum.ENTREPRISE_CLIENTE:
//							if (Utilities.isNotEmpty(users) && users.size() >= GlobalEnum.NBRE_MAXI_USER_PAR_ENTREPRISE_CLIENTE) {
//								response.setStatus(functionalError.DISALLOWED_OPERATION("L'entreprise ->"+ existingEntreprise.getNom() +"  a déjà "+users.size()+" utilisateur(s).", locale));
//								response.setHasError(true);
//								return response;
//							}
//							//break;
//
//						default:
//							break;
//						}
						
//						if (existingEntreprise.getTypeEntreprise().getCode().equals(TypeUserEnum.ENTREPRISE_FOURNISSEUR)) {
//							response.setStatus(functionalError.DISALLOWED_OPERATION("L'entreprise ->"+ existingEntreprise.getNom() +"  a déjà "+users.size()+" utilisateur.", locale));
//							response.setHasError(true);
//							return response;
//						}else {
//							if (Utilities.isNotEmpty(users) && users.size() >= GlobalEnum.NBRE_MAXI_USER_PAR_ENTREPRISE_CLIENTE) {
//								response.setStatus(functionalError.DISALLOWED_OPERATION("L'entreprise ->"+ existingEntreprise.getNom() +"  a déjà "+users.size()+" utilisateur(s).", locale));
//								response.setHasError(true);
//								return response;
//							}
//							
//							// le cas des entreprise mxite
//						}
//						
						for (User data : users) {						
							if (data.getRole().getId().equals(dto.getRoleId()) && !data.getId().equals(entityToSaveId) ) {
								response.setStatus(functionalError.DISALLOWED_OPERATION("Un utilisateur de l'entreprise ->"+ entityToSave.getEntreprise().getNom() +"  a déjà le role ->" + data.getRole().getLibelle(), locale));
								response.setHasError(true);
								return response;
							}
						}
					}
					
					
					
					entityToSave.setRole(existingRole);
				}
				// Verify if entreprise exist
				// TODO : PRENDRE EN COMPTE LE CHANGEMENT DE TYPE POUR UNE ENTREPRISE

				if (dto.getEntrepriseId() != null && dto.getEntrepriseId() > 0){
					Entreprise existingEntreprise = entrepriseRepository.findById(dto.getEntrepriseId(), false);
					if (existingEntreprise == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("entreprise -> " + dto.getEntrepriseId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEntreprise(existingEntreprise);
				}
				if (Utilities.notBlank(dto.getNom())) {
					entityToSave.setNom(dto.getNom());
				}
				if (Utilities.notBlank(dto.getPrenoms())) {
					entityToSave.setPrenoms(dto.getPrenoms());
				}
				if(dto.getDataEntreprise() != null && dto.getDataEntreprise().getId() != null) {
				//if(dto.getDataEntreprise() != null && dto.getDataEntreprise().getCreatedBy().equals(entityToSaveId)) {
					// create entreprise
					Request<EntrepriseDto> req = new Request<EntrepriseDto>();
					Response<EntrepriseDto> res = new Response<EntrepriseDto>();
					Entreprise entreprise = null;
					entreprise = entrepriseRepository.findById(dto.getId(), false);

					if(entreprise != null && entreprise.getCreatedBy().equals(entityToSaveId)){
						req.setDatas(Arrays.asList(dto.getDataEntreprise()));
						req.setUser(entityToSaveId); 
						res = entrepriseBusiness.update(req, locale);
						if (res.isHasError()) {
							response.setStatus(res.getStatus());
							response.setHasError(true);
							return response;
						}
						if (Utilities.isNotEmpty(res.getItems())) {
							dto.setEntrepriseId(res.getItems().get(0).getId());
						}
					}
				}
//				if (Utilities.notBlank(dto.getLogin())) {
//					User existingEntity = userRepository.findByLogin(dto.getLogin(), false);
//					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
//						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getLogin(), locale));
//						response.setHasError(true);
//						return response;
//					}
//					if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getLogin()) && !a.getId().equals(entityToSaveId))) {
//						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getLogin()+"' pour les users", locale));
//						response.setHasError(true);
//						return response;
//					}
//					entityToSave.setLogin(dto.getLogin());
//				}
//				if (Utilities.notBlank(dto.getPassword())) {
//					entityToSave.setPassword(dto.getPassword());
//				}
				if (Utilities.notBlank(dto.getTelephone())) {
					
					// TODO : à revoir pour les autres pays
					if (!Utilities.isValidateIvorianPhoneNumber(dto.getTelephone())){
						response.setStatus(functionalError.INVALID_DATA("user -> " + dto.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}
					
					User existingEntity = userRepository.findByTelephone(dto.getTelephone(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les users", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setTelephone(dto.getTelephone());
				}
				if (Utilities.notBlank(dto.getEmail())) {
					//User existingEntity = userRepository.findByEmail(dto.getEmail(), false);
					List<User> listExistingEntity = userRepository.findByEmailAndEntrepriseIdDifferent(dto.getEmail(), entityToSave.getEntreprise().getId(), false);
//					boolean isExist = false;
//					if (Utilities.isNotEmpty(listExistingEntity)) {
//						for (User user : listExistingEntity) {
//							if (!user.getId().equals(entityToSave.getId())) {
//								isExist = true ;
//								break ;
//							}
//						}
//						
//					}
					if (Utilities.isNotEmpty(listExistingEntity)) {
						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getEmail(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les users", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEmail(dto.getEmail());
				}
				if (dto.getIsLocked() != null) {
					entityToSave.setIsLocked(dto.getIsLocked());
				}
				if (dto.getIsSuUser() != null) {
					entityToSave.setIsSuUser(dto.getIsSuUser());
				}
				if (dto.getIsValider() != null) {
					entityToSave.setIsValider(dto.getIsValider());
				}
				if (Utilities.notBlank(dto.getToken())) {
					entityToSave.setToken(dto.getToken());
				}
				if (Utilities.notBlank(dto.getDateExpirationToken())) {
					entityToSave.setDateExpirationToken(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateExpirationToken()));
				}
				if (dto.getIsTokenValide() != null) {
					entityToSave.setIsTokenValide(dto.getIsTokenValide());
				}
//				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
//					entityToSave.setCreatedBy(dto.getCreatedBy());
//				}
//				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
//					entityToSave.setUpdatedBy(dto.getUpdatedBy());
//				}
//				if (Utilities.notBlank(dto.getDeletedAt())) {
//					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
//				}
//				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
//					entityToSave.setDeletedBy(dto.getDeletedBy());
//				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
				
				// pas de MAJ pendant la MAJ du profil
				
//				if (dto.getDataEntreprise() != null) {
//					if (dto.getDataEntreprise().getId() != null && dto.getDataEntreprise().getId() > 0) {
//						dto.setEntrepriseId(dto.getDataEntreprise().getId());
//						// update entreprise
//						Request<EntrepriseDto> req = new Request<EntrepriseDto>();
//						Response<EntrepriseDto> res = new Response<EntrepriseDto>();
//
//						req.setDatas(Arrays.asList(dto.getDataEntreprise()));
//
//						res = entrepriseBusiness.update(req, locale);
//						if (res.isHasError()) {
//							response.setStatus(res.getStatus());
//							response.setHasError(true);
//							return response;
//						}
//						if (Utilities.isNotEmpty(res.getItems())) {
//							dto.setEntrepriseId(res.getItems().get(0).getId());
//						}
//						//isUpdateEntreprise = true ;
//
//						//existingEntreprise = entrepriseRepository.findById(dto.getDataEntreprise().getId(), false);
//					}else {
//						// create entreprise
//						Request<EntrepriseDto> req = new Request<EntrepriseDto>();
//						Response<EntrepriseDto> res = new Response<EntrepriseDto>();
//
//						req.setDatas(Arrays.asList(dto.getDataEntreprise()));
//
//						res = entrepriseBusiness.create(req, locale);
//						if (res.isHasError()) {
//							response.setStatus(res.getStatus());
//							response.setHasError(true);
//							return response;
//						}
//						if (Utilities.isNotEmpty(res.getItems())) {
//							dto.setEntrepriseId(res.getItems().get(0).getId());
//						}
//					}
//				}
				
				
			}

			if (!items.isEmpty()) {
				List<User> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = userRepository.save((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}
				List<UserDto> itemsDto = new ArrayList<UserDto>();
				for (User entity : itemsSaved) {
					UserDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<UserDto> delete(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete User-----");

		response = new Response<UserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<User> items = new ArrayList<User>();

			for (UserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la user existe
				User existingEntity = null;
				existingEntity = userRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// offre
				List<Offre> listOfOffre = offreRepository.findByUserFournisseurId(existingEntity.getId(), false);
				if (listOfOffre != null && !listOfOffre.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfOffre.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// realisation
				List<Realisation> listOfRealisation = realisationRepository.findByUserId(existingEntity.getId(), false);
				if (listOfRealisation != null && !listOfRealisation.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfRealisation.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// appelDoffresRestreint
				List<AppelDoffresRestreint> listOfAppelDoffresRestreint = appelDoffresRestreintRepository.findByUserFournisseurId(existingEntity.getId(), false);
				if (listOfAppelDoffresRestreint != null && !listOfAppelDoffresRestreint.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfAppelDoffresRestreint.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// appelDoffres
				List<AppelDoffres> listOfAppelDoffres = appelDoffresRepository.findByUserClientId(existingEntity.getId(), false);
				if (listOfAppelDoffres != null && !listOfAppelDoffres.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfAppelDoffres.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// carnetDadresse
				List<CarnetDadresse> listOfCarnetDadresse = carnetDadresseRepository.findByUserClientId(existingEntity.getId(), false);
				if (listOfCarnetDadresse != null && !listOfCarnetDadresse.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfCarnetDadresse.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				userRepository.save((Iterable<User>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<UserDto> forceDelete(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete User-----");

		response = new Response<UserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<User> items = new ArrayList<User>();

			for (UserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la user existe
				User existingEntity = null;
				existingEntity = userRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// offre
				List<Offre> listOfOffre = offreRepository.findByUserFournisseurId(existingEntity.getId(), false);
				if (listOfOffre != null && !listOfOffre.isEmpty()){
					Request<OffreDto> deleteRequest = new Request<OffreDto>();
					deleteRequest.setDatas(OffreTransformer.INSTANCE.toDtos(listOfOffre));
					deleteRequest.setUser(request.getUser());
					Response<OffreDto> deleteResponse = offreBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// realisation
				List<Realisation> listOfRealisation = realisationRepository.findByUserId(existingEntity.getId(), false);
				if (listOfRealisation != null && !listOfRealisation.isEmpty()){
					Request<RealisationDto> deleteRequest = new Request<RealisationDto>();
					deleteRequest.setDatas(RealisationTransformer.INSTANCE.toDtos(listOfRealisation));
					deleteRequest.setUser(request.getUser());
					Response<RealisationDto> deleteResponse = realisationBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// appelDoffresRestreint
				List<AppelDoffresRestreint> listOfAppelDoffresRestreint = appelDoffresRestreintRepository.findByUserFournisseurId(existingEntity.getId(), false);
				if (listOfAppelDoffresRestreint != null && !listOfAppelDoffresRestreint.isEmpty()){
					Request<AppelDoffresRestreintDto> deleteRequest = new Request<AppelDoffresRestreintDto>();
					deleteRequest.setDatas(AppelDoffresRestreintTransformer.INSTANCE.toDtos(listOfAppelDoffresRestreint));
					deleteRequest.setUser(request.getUser());
					Response<AppelDoffresRestreintDto> deleteResponse = appelDoffresRestreintBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// appelDoffres
				List<AppelDoffres> listOfAppelDoffres = appelDoffresRepository.findByUserClientId(existingEntity.getId(), false);
				if (listOfAppelDoffres != null && !listOfAppelDoffres.isEmpty()){
					Request<AppelDoffresDto> deleteRequest = new Request<AppelDoffresDto>();
					deleteRequest.setDatas(AppelDoffresTransformer.INSTANCE.toDtos(listOfAppelDoffres));
					deleteRequest.setUser(request.getUser());
					Response<AppelDoffresDto> deleteResponse = appelDoffresBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// carnetDadresse
				List<CarnetDadresse> listOfCarnetDadresse = carnetDadresseRepository.findByUserClientId(existingEntity.getId(), false);
				if (listOfCarnetDadresse != null && !listOfCarnetDadresse.isEmpty()){
					Request<CarnetDadresseDto> deleteRequest = new Request<CarnetDadresseDto>();
					deleteRequest.setDatas(CarnetDadresseTransformer.INSTANCE.toDtos(listOfCarnetDadresse));
					deleteRequest.setUser(request.getUser());
					Response<CarnetDadresseDto> deleteResponse = carnetDadresseBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				userRepository.save((Iterable<User>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	
	
	
	
	
	
	
	
	/**
	 * connexion User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> connexion(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin connexion User-----");

		response = new Response<UserDto>();

		try {

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("login", dto.getLogin());
			fieldsToVerify.put("password", dto.getPassword());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}


			// Verify if user to insert do not exist
			User existingEntity = null;
			String oldPassword = dto.getPassword() ;

			// cryptage du password avant de rechercher dans la BD
			dto.setPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getPassword()));

			existingEntity = userRepository.findByLoginAndPassword(dto.getLogin(), dto.getPassword(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user avec le login-> " + dto.getLogin() + " et le password -> " + oldPassword + "n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}
//			if (existingEntity.getIsLocked() != null && existingEntity.getIsLocked()) {
//				response.setStatus(functionalError.DATA_NOT_EXIST("Le compte -> " + dto.getLogin() + " est verrouillé. Merci de le déverrouiller !!!" , locale));
//				response.setHasError(true);
//				return response;
//			}
//			
//			if (existingEntity.getEntreprise()!= null && existingEntity.getEntreprise().getIsLocked() != null && existingEntity.getEntreprise().getIsLocked()) {
//				response.setStatus(functionalError.DATA_NOT_EXIST("L'entreprise -> " + existingEntity.getEntreprise().getNom() + " est verrouillée. Merci de la déverrouiller !!!" , locale));
//				response.setHasError(true);
//				return response;
//			}


			if (existingEntity.getIsLocked() != null && existingEntity.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+existingEntity.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			
			if (existingEntity.getEntreprise()!= null && existingEntity.getEntreprise().getIsLocked() != null && existingEntity.getEntreprise().getIsLocked()) {

			//if (existingEntity.getEntreprise().getIsLocked() != null && existingEntity.getEntreprise().getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+existingEntity.getLogin()+" "+" est verouille(e) car son entreprise " + existingEntity.getEntreprise().getNom()+ " est verouillée " , locale));
				response.setHasError(true);
				return response;
			}
			UserDto userDto = getFullInfos(existingEntity, 1, locale);

			response.setItems(Arrays.asList(userDto));
			response.setHasError(false);

			slf4jLogger.info("----end connexion User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
	/**
	 * finaliserInscription User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> finaliserInscription(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin finaliserInscription User-----");

		response = new Response<UserDto>();

		try {

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("email", dto.getEmail());
			fieldsToVerify.put("login", dto.getLogin());
			fieldsToVerify.put("password", dto.getPassword());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}


			// Verify if user to insert do not exist
			User entityToSave = null;
			
			entityToSave = userRepository.findByEmail(dto.getEmail(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user avec le login-> " + dto.getLogin() + " et le password n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}
			if (Utilities.notBlank(entityToSave.getLogin()) || Utilities.notBlank(entityToSave.getPassword())) {
				response.setStatus(functionalError.DATA_EXIST("cet user login-> " + dto.getLogin() + " a déjà finaliser son inscription." , locale));
				response.setHasError(true);
				return response;
			}
			
			User existingEntity = null;
			existingEntity = userRepository.findByLogin(dto.getLogin(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("user avec le login-> " + dto.getLogin() + " existe déjà." , locale));
				response.setHasError(true);
				return response;
			}
			

			// cryptage du password avant de rechercher dans la BD
			dto.setPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getPassword()));

			entityToSave.setLogin(dto.getLogin());
			entityToSave.setPassword(dto.getPassword());
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(entityToSave.getId());
			
			userRepository.save(entityToSave);
			UserDto userDto = getFullInfos(entityToSave, 1, locale);


			response.setItems(Arrays.asList(userDto));
			response.setHasError(false);

			slf4jLogger.info("----end finaliserInscription User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	
	
	

	/**
	 * changerPassword User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> changerPassword(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin changerPassword User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();
			List<User> itemsAvant = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("login", dto.getLogin());
			fieldsToVerify.put("newPassword", dto.getNewPassword());
			fieldsToVerify.put("password", dto.getPassword());
			fieldsToVerify.put("userId", request.getUser());

			//			 fieldsToVerify.put("newPassword", dto.getNewPassword());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if utilisateur exist
			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser() +" n'existe pas.", locale));
				response.setHasError(true);
				return response;
			}

			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			// Verify if user to insert do not exist
			User entityToSave = null;

			// cryptage du password avant de rechercher dans la BD
			String passwordSaisie = dto.getPassword() ;
			dto.setPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getPassword()));
			entityToSave = userRepository.findByLoginAndPassword(dto.getLogin(), dto.getPassword(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("le login-> " + dto.getLogin() + " avec ce password "+ passwordSaisie +" n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}

			// choisir la vraie url
			String appLink = "";
			//			if (entityToSave.getAdmin() != null ) {
			//				appLink = paramsUtils.getUrlRootAdmin();
			//			}else {
			//				appLink = paramsUtils.getUrlRootUser();
			//			}

			appLink = paramsUtils.getUrlRootUser();

			// verifier que le user connecter correspond à celui qui veut changer le password
			//if (!entityToSave.getId().equals(utilisateur.getId()) && !entityToSave.getIsSuperUser()) {
			if (!entityToSave.getId().equals(utilisateur.getId())) {
				response.setStatus(functionalError.AUTH_FAIL("Utilisateur -> " + utilisateur.getId() +" n'a pas ce droit.", locale));
				response.setHasError(true);
				return response;
			}

			//			User entityToSaveOld = null;
			//			// pour historisation
			//			entityToSaveOld = (User) entityToSave.clone();
			//			itemsAvant.add(entityToSaveOld);

			Integer entityToSaveId = entityToSave.getId();

			//			 // renseigner les valeurs a MAJ
			//			 if (Utilities.notBlank(dto.getNewPassword())) {
			//				 entityToSave.setPassword(dto.getNewPassword());
			//			 }
			//			 if (Utilities.notBlank(dto.getNewLogin())) {
			//				 User existingEntity = userRepository.findByLogin(dto.getNewLogin(), false);
			//				 if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
			//					 response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getNewLogin() +" existe deja.", locale));
			//					 response.setHasError(true);
			//					 return response;
			//				 }
			//				 if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getNewLogin()) && !a.getId().equals(entityToSaveId))) {
			//					 response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getNewLogin()+"' pour les users", locale));
			//					 response.setHasError(true);
			//					 return response;
			//				 }
			//				 entityToSave.setLogin(dto.getNewLogin());
			//			 }


			// imposer que le newPassword >= 8 caracteres
			//			if (dto.getNewPassword().length() < EmailEnum.MIN_CARACTERE_PASSWORD) {
			//				response.setStatus(functionalError.INVALID_FORMAT("Votre nouveau password doit etre au moins de 8 caractères. Merci de renseigner un password d'au moins 8 caractères.", locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			// cryptage du NewPassword avant de l'inserer dans la BD
			dto.setNewPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getNewPassword()));

			entityToSave.setPassword(dto.getNewPassword());
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);

			if (!items.isEmpty()) {
				List<User> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = userRepository.save((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				//				Response<HistoriqueActionUserDto> responseHistoriqueActionUser = new Response<>();
				//
				//				responseHistoriqueActionUser =  historiqueActionUserBusiness.callHistorisation(itemsAvant, itemsSaved, request.getUrlService(), request.getUser(), locale);
				//
				//				if (responseHistoriqueActionUser.isHasError()) {
				//					response.setHasError(true);
				//					response.setStatus(responseHistoriqueActionUser.getStatus());
				//					return response;
				//				}

				response.setStatus(functionalError.SUCCESS("Votre login et/ou password à bien été modifié.", locale));
				response.setHasError(false);

				// envoi de mail 

				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user", paramsUtils.getSmtpMailUser());

					for (User user : itemsSaved) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_PASSWORD_RESET;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateSuccesChangePassword();
						//		        	context.setVariable("email", user.getEmail());
						//					context.setVariable("login", user.getLogin());
						//					context.setVariable("token", user.getToken());
						//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
						context.setVariable("appLink",appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable(EmailEnum.nomEntreprise, EmailEnum.NOM_ENTREPRISE);
						context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
						
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail

					}

				}
			}

			slf4jLogger.info("----end changerPassword User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * passwordOublier User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> passwordOublier(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin passwordOublier User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			//			fieldsToVerify.put("login", dto.getLogin());
			fieldsToVerify.put("email", dto.getEmail());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}




			// Verify if user to insert do not exist
			User entityToSave = null;


			entityToSave = userRepository.findByEmail(dto.getEmail(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user-> " + dto.getEmail()  +" n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}
			// choisir la vraie url
			String appLink = "";
			//			if (entityToSave.getAdmin() != null ) {
			//				appLink = paramsUtils.getUrlRootAdmin();
			//			}else {
			//				appLink = paramsUtils.getUrlRootUser();
			//			}

			appLink = paramsUtils.getUrlRootUser();


			//			// verifier que le user connecter correspond à celui qui a oublié le password
			//			if (!entityToSave.getId().equals(utilisateur.getId())) {
			//				response.setStatus(functionalError.AUTH_FAIL("Utilisateur -> " + utilisateur.getId() +" n'a pas ce droit.", locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			// setter token, isValidToken et tokenCreatedAt
			entityToSave.setToken(Utilities.randomString(50));
			GregorianCalendar calendar =new GregorianCalendar();

			calendar.setTime(Utilities.getCurrentDate());
			calendar.add(Calendar.HOUR, 24);
			entityToSave.setDateExpirationToken(calendar.getTime());
			entityToSave.setIsTokenValide(true);
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(entityToSave.getId());


			items.add(entityToSave);

			if (!items.isEmpty()) {

				// sauvegarde du user avec le champ token
				List<User> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = userRepository.save((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				// envoi de mail avec le lien comportant le token et email

				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user", paramsUtils.getSmtpMailUser());
					for (User user : itemsSaved) {


						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//					for (User user : items) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//					recipient.put("user", user.getNom());
						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//			        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_FORGET_PASSWORD;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateResetUserPassword();

						context.setVariable("email", user.getEmail());
						context.setVariable("nom", user.getNom());
						context.setVariable("token", user.getToken());
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate()));
						context.setVariable("expiration", dateFormat.format(user.getDateExpirationToken()));
						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						
						context.setVariable(EmailEnum.nomEntreprise, EmailEnum.NOM_ENTREPRISE);
						context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
						
						// //app.link=http://ims-pp-webbo.ovh.swan.smile.ci
						//					context.setVariable("date", dateFormat.format(new Date()));


						//					context.setVariable("userName", entityToSave.getNom());
						//					context.setVariable("userName", entityToSave.getEmail());
						//					context.setVariable("url", paramsUtils.getUrlRootToken() + entityToSave.getEmail()+ "/"+entityToSave.getToken() );
						//					context.setVariable("expiration", dateFormat.format(entityToSave.getTokenCreatedAt()));
						//					context.setVariable("date", dateFormat.format(new Date()));
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail
					}

				}
				response.setStatus(functionalError.SUCCESS("Veuillez consulter votre mail pour saisir votre password.", locale));
				response.setHasError(false);
			}
			slf4jLogger.info("----end passwordOublier User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * changerPasswordOublier User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> changerPasswordOublier(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin changerPasswordOublier User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();
			List<User> itemsAvant = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("email", dto.getEmail());
			fieldsToVerify.put("newPassword", dto.getNewPassword());
			//			fieldsToVerify.put("password", dto.getPassword());
			//			fieldsToVerify.put("user", request.getUser());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if utilisateur exist
			//			User utilisateur = userRepository.findById(request.getUser(), false);
			//			if (utilisateur == null) {
			//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser() +" n'existe pas.", locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			// Verify if user to insert do not exist
			User entityToSave = null;

			entityToSave = userRepository.findByEmail(dto.getEmail(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user-> " + dto.getEmail()  +" n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}

			// choisir la vraie url
			String appLink = "";
			//			if (entityToSave.getAdmin() != null ) {
			//				appLink = paramsUtils.getUrlRootAdmin();
			//			}else {
			//				appLink = paramsUtils.getUrlRootUser();
			//			}

			appLink = paramsUtils.getUrlRootUser();


			User entityToSaveOld = null;
			// pour historisation
			entityToSaveOld = (User) entityToSave.clone();
			itemsAvant.add(entityToSaveOld);

			// cryptage du password avant de le setter dans la BD
			entityToSave.setPassword(Utilities.encryptPasswordBy_(entityToSave.getLogin(), dto.getNewPassword()));

			Integer entityToSaveId = entityToSave.getId();

			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(entityToSaveId);
			items.add(entityToSave);

			if (!items.isEmpty()) {
				List<User> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = userRepository.save((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				//				Response<HistoriqueActionUserDto> responseHistoriqueActionUser = new Response<>();
				//
				//				responseHistoriqueActionUser =  historiqueActionUserBusiness.callHistorisation(itemsAvant, itemsSaved, request.getUrlService(), entityToSave.getId(), locale);
				//
				//				if (responseHistoriqueActionUser.isHasError()) {
				//					response.setHasError(true);
				//					response.setStatus(responseHistoriqueActionUser.getStatus());
				//					return response;
				//				}

				response.setStatus(functionalError.SUCCESS("Votre login et/ou password à bien été modifié.", locale));
				response.setHasError(false);

				//				// envoi de mail password changer avec succes
				//				if (Utilities.isNotEmpty(itemsSaved)) {
				//					
				//					//set mail to user
				//					Map<String, String> from=new HashMap<>();
				//		        	from.put("email", paramsUtils.getSmtpMailUsername());
				//		        	from.put("user", paramsUtils.getSmtpMailUser());
				//		        	//recipients
				//		        	List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
				////		        	for (User user : itemsSaved) {
				//		        	User user = itemsSaved.get(0);
				//		        	Map<String, String> recipient = new HashMap<String, String>();
				//		        	recipient = new HashMap<String, String>();
				//		        	recipient.put("email", user.getEmail());
				//		        	recipient.put("user", user.getLogin());
				////		        	recipient.put("user", user.getNom());
				//
				//		        	toRecipients.add(recipient); 
				////					}
				//		        	
				//		        	//subject
				//		        	// ajout du champ objet a la newsletter
				////		        	String subject = "NEWSLETTER DU VOLONTARIAT";
				//		        	String subject = EmailEnum.SUBJECT_RESET_PASSWORD_OK;
				//		        	String body = "";
				//		        	context = new Context();
				//		        	String templateChangerPassword = paramsUtils.getTemplateSuccesChangePassword();
				////		        	context.setVariable("email", user.getEmail());
				////					context.setVariable("login", user.getLogin());
				////					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
				//					context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
				//					
				//		        	response = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
				//		        	// si jamais une exception c'est produite pour un mail
				//
				//				}
				
				// envoi de mail 

				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user", paramsUtils.getSmtpMailUser());

					for (User user : itemsSaved) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_PASSWORD_RESET;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateSuccesChangePassword();
						//		        	context.setVariable("email", user.getEmail());
						//					context.setVariable("login", user.getLogin());
						//					context.setVariable("token", user.getToken());
						//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
						context.setVariable("appLink",appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}

						context.setVariable(EmailEnum.nomEntreprise, EmailEnum.NOM_ENTREPRISE);
						context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
						
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail

					}

				}
			}

			slf4jLogger.info("----end changerPasswordOublier User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * checkEmailWithTokenIsValid User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> checkEmailWithTokenIsValid(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin checkEmailWithTokenIsValid User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("token", dto.getToken());
			fieldsToVerify.put("email", dto.getEmail());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}
			// Verify if user to insert do not exist
			User entityToSave = null;

			entityToSave = userRepository.findUserByTokenAndEmail(dto.getToken(), dto.getEmail(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user-> " + dto.getEmail()  + " avec le token "+ dto.getToken()+" n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}else{
				if (entityToSave.getIsTokenValide() != null) {
					//if (!entityToSave.getIsTokenValide() || Utilities.getCurrentDate().after(entityToSave.getDateExpirationToken())  ) {
					if (!entityToSave.getIsTokenValide()) {
						response.setStatus(functionalError.DATA_NOT_EXIST("ce token a deja été utilisé ou a expiré", locale));
						response.setHasError(true);
						return response;
					}
				}else {
					response.setStatus(functionalError.AUTH_FAIL("Veuillez signifier d'abord que vous avez oublié votre password.", locale));
					response.setHasError(true);
					return response;
				}

				// setter ces valeurs a null car elles n'ont plus de sens
				entityToSave.setIsLocked(false);
				entityToSave.setToken(null);
				entityToSave.setIsTokenValide(false);
				//entityToSave.setDateExpirationToken(null);
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(entityToSave.getId());
				items.add(entityToSave);

				// maj les donnees en base
				items = userRepository.save((Iterable<User>) items);
				if (!Utilities.isNotEmpty(items)) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				response.setItems(Arrays.asList(getFullInfos(items.get(0), items.size(), locale))); // transformer en liste 
				response.setHasError(false);
				
				// envoi de mail 
				if (Utilities.isNotEmpty(items)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user" ,  paramsUtils.getSmtpMailUser());

					// choisir la vraie url
					String appLink = "";
					//			if (entityToSave.getAdmin() != null ) {
					//				appLink = paramsUtils.getUrlRootAdmin();
					//			}else {
					//				appLink = paramsUtils.getUrlRootUser();
					//			}

					appLink = paramsUtils.getUrlRootUser();

					for (User user : items) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_CREATION_COMPTE_VALIDER;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String template = paramsUtils.getTemplateCreationCompteParUser();
						//String template = paramsUtils.getTemplateCreationCompteParUserWithToken();
						
						//context.setVariable("email", user.getEmail());
						//context.setVariable("login", user.getLogin());
						//context.setVariable("token", user.getToken());
						//context.setVariable("expiration", dateFormat.format(user.getDateExpirationToken()));
						
						
						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable(EmailEnum.nomEntreprise, user.getEntreprise().getNom());
						context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,template, locale);
						// si jamais une exception c'est produite pour un mail
					}
				}
				
				
				if (Utilities.isNotEmpty(items)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user" ,  paramsUtils.getSmtpMailUser());

					// choisir la vraie url
					String appLink = "";
					//			if (entityToSave.getAdmin() != null ) {
					//				appLink = paramsUtils.getUrlRootAdmin();
					//			}else {
					//				appLink = paramsUtils.getUrlRootUser();
					//			}

					appLink = paramsUtils.getUrlRootUser();

					for (User user : items) {
						
						Entreprise data = user.getEntreprise() ;
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", data.getEmail());
						recipient.put("user", data.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_CREATION_COMPTE;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateCreationCompteParUser();
						//		        	context.setVariable("email", user.getEmail());
						//					context.setVariable("login", user.getLogin());
						//					context.setVariable("token", user.getToken());
						//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable(EmailEnum.nomEntreprise, data.getNom());
						context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail

					}
				}
				
				//***********************************************
			}

			slf4jLogger.info("----end checkEmailWithTokenIsValid User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
	
	/**
	 * checkEmailWithTokenIsValid User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> checkEmailWithTokenIsValidOld(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin checkEmailWithTokenIsValid User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("token", dto.getToken());
			fieldsToVerify.put("email", dto.getEmail());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}
			// Verify if user to insert do not exist
			User entityToSave = null;

			entityToSave = userRepository.findUserByTokenAndEmail(dto.getToken(), dto.getEmail(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user-> " + dto.getEmail()  + " avec le token "+ dto.getToken()+" n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}else{
				if (entityToSave.getIsTokenValide() != null && entityToSave.getDateExpirationToken() != null) {
					if (!entityToSave.getIsTokenValide() || Utilities.getCurrentDate().after(entityToSave.getDateExpirationToken())  ) {
						response.setStatus(functionalError.DATA_NOT_EXIST("ce token a deja été utilisé ou est expiré", locale));
						response.setHasError(true);
						return response;
					}
				}else {
					response.setStatus(functionalError.AUTH_FAIL("Veuillez signifier d'abord que vous avez oublié votre password.", locale));
					response.setHasError(true);
					return response;
				}

				// setter ces valeurs a null car elles n'ont plus de sens
				entityToSave.setIsLocked(false);
				entityToSave.setToken(null);
				entityToSave.setIsTokenValide(false);
				entityToSave.setDateExpirationToken(null);
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(entityToSave.getId());
				items.add(entityToSave);

				// maj les donnees en base
				items = userRepository.save((Iterable<User>) items);
				if (!Utilities.isNotEmpty(items)) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				response.setItems(Arrays.asList(getFullInfos(items.get(0), items.size(), locale))); // transformer en liste 
				response.setHasError(false);
				
				// envoi de mail 
				if (Utilities.isNotEmpty(items)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user" ,  paramsUtils.getSmtpMailUser());

					// choisir la vraie url
					String appLink = "";
					//			if (entityToSave.getAdmin() != null ) {
					//				appLink = paramsUtils.getUrlRootAdmin();
					//			}else {
					//				appLink = paramsUtils.getUrlRootUser();
					//			}

					appLink = paramsUtils.getUrlRootUser();

					for (User user : items) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_CREATION_COMPTE_VALIDER;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String template = paramsUtils.getTemplateCreationCompteParUser();
						//String template = paramsUtils.getTemplateCreationCompteParUserWithToken();
						
						//context.setVariable("email", user.getEmail());
						//context.setVariable("login", user.getLogin());
						//context.setVariable("token", user.getToken());
						//context.setVariable("expiration", dateFormat.format(user.getDateExpirationToken()));
						
						
						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable(EmailEnum.nomEntreprise, EmailEnum.NOM_ENTREPRISE);
						context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,template, locale);
						// si jamais une exception c'est produite pour un mail
					}
				}
				
				
				if (Utilities.isNotEmpty(items)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user" ,  paramsUtils.getSmtpMailUser());

					// choisir la vraie url
					String appLink = "";
					//			if (entityToSave.getAdmin() != null ) {
					//				appLink = paramsUtils.getUrlRootAdmin();
					//			}else {
					//				appLink = paramsUtils.getUrlRootUser();
					//			}

					appLink = paramsUtils.getUrlRootUser();

					for (User user : items) {
						
						Entreprise data = user.getEntreprise() ;
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", data.getEmail());
						recipient.put("user", data.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_CREATION_COMPTE;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateCreationCompteParUser();
						//		        	context.setVariable("email", user.getEmail());
						//					context.setVariable("login", user.getLogin());
						//					context.setVariable("token", user.getToken());
						//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable(EmailEnum.nomEntreprise, EmailEnum.NOM_ENTREPRISE);
						context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail

					}
				}
				
				//***********************************************
			}

			slf4jLogger.info("----end checkEmailWithTokenIsValid User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
	
	/**
	 * checkEmailIsValid User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> checkEmailIsValid(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin checkEmailIsValid User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("email", dto.getEmail());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}
			// Verify if user to insert do not exist
			User entityToSave = null;

			entityToSave = userRepository.findByEmail(dto.getEmail(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user-> " + dto.getEmail()  + " n'appartient pas au système." , locale));
				response.setHasError(true);
				return response;
			}
			if (Utilities.notBlank(entityToSave.getLogin()) || Utilities.notBlank(entityToSave.getPassword())) {
				response.setStatus(functionalError.DATA_NOT_EXIST("cet user email-> " + dto.getEmail() + " a déjà finaliser son inscription." , locale));
				response.setHasError(true);
				return response;
			}
			
			response.setHasError(false);


			slf4jLogger.info("----end checkEmailIsValid User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	
	
	/**
	 * connexion User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> getNotifications(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin getNotifications User-----");

		response = new Response<UserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			
			int compteurAppelDoffresNew = 0 ;
			int compteurAppelDoffresSelect = 0 ;
			int compteurAppelDoffresUpdate = 0 ;
			
			int compteurOffreNew = 0 ;
		    int compteurOffreUpdate = 0 ;
			
			
			String typeEntrepriseCode = utilisateur.getEntreprise().getTypeEntreprise().getCode();
			Integer entrepriseId = utilisateur.getEntreprise().getId();
			Integer userId = utilisateur.getId() ;
			
			//List<SecteurDactiviteEntreprise> secteurDactiviteEntreprises = new ArrayList<>();
			//secteurDactiviteEntreprises = secteurDactiviteEntrepriseRepository.findByEntrepriseId(entrepriseId, false);
			
			List<AppelDoffresDto> appelDoffresUser = new ArrayList<>();
			List<AppelDoffres> appelDoffres = new ArrayList<>();
			//List<OffreDto> offresUser = new ArrayList<>();
			List<Offre> offresUser = new ArrayList<>();
					
			
			
			

			//Request<SecteurDactiviteAppelDoffresDto> req = new Request<>() ; 
			Request<AppelDoffresRestreintDto> req = new Request<>() ; 
			Response<AppelDoffresDto> res = new Response<>() ;
			AppelDoffresRestreintDto data = new AppelDoffresRestreintDto() ;
			//SecteurDactiviteAppelDoffresDto data = new SecteurDactiviteAppelDoffresDto() ;

			
			/*
			data.setIsVisibleByClient(true);
		    data.setIsRetirer(false);
		    data.setIsAttribuer(false);
		    data.setIsLocked(false);
		    
			req.setUser(userId);
			req.setData(data);
		    
			res = secteurDactiviteAppelDoffresBusiness.getByCriteriaCustom(req, locale) ;
			
			if (res.isHasError()) {
				response.setStatus(res.getStatus());
				response.setHasError(res.isHasError());
				return response ;
			}
			
			// entreprises fournisseurs
			// recuperer la liste des appels d'offres
			appelDoffresUser = res.getItems() ;
			if (Utilities.isNotEmpty(appelDoffresUser)) {
				
				int cptAppelDoffresNew = 0 ;
				int cptAppelDoffresSelect = 0 ;
				int cptAppelDoffresUpdate = 0 ;				
				List<AppelDoffresRestreint> appelDoffresRestreints = new ArrayList<>() ; 
				List<Offre> offres = new ArrayList<>() ; 
				
				for (AppelDoffresDto dto : appelDoffresUser) {
					if (dto.getIsUpdate() != null) {
						if(dto.getIsUpdate()) {
							appelDoffresRestreints = appelDoffresRestreintRepository.findByUserFournisseurIdAndAppelDoffresIdAndIsVisualiserNotifByFournisseurAndIsAppelRestreintAndIsVisibleByFournisseurAndIsDesactiver(userId, dto.getId(), true, false, true, false, false);
						}else {
							appelDoffresRestreints = appelDoffresRestreintRepository.findByUserFournisseurIdAndAppelDoffresIdAndIsVisualiserNotifByFournisseurAndIsAppelRestreintAndIsDesactiver(userId, dto.getId(), true, false, false, false);
						}
					}

					if (Utilities.isNotEmpty(appelDoffresRestreints)) { // => ancien appel d'offres
						if(dto.getIsUpdate() != null && dto.getIsUpdate()) {
							appelDoffresRestreints.get(0).setIsUpdateAppelDoffres(true);
							appelDoffresRestreints.get(0).setIsVisualiserNotifByFournisseur(true);
							appelDoffresRestreints.get(0).setIsVisualiserContenuByFournisseur(true);
							//appelDoffresRestreints.get(0).setIsUpdateAppelDoffres(true);
							cptAppelDoffresUpdate ++ ;
							
							appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>)appelDoffresRestreints);
						}
						//else{}
					}else { // => nouvel appel d'offres
						cptAppelDoffresNew ++ ;
					}
					//isSelectedByClient
					offres = offreRepository.findByAppelDoffresIdAndIsSelectedByClient(dto.getId(), true, false) ;
					if (Utilities.isNotEmpty(offres)) {
						cptAppelDoffresSelect ++ ;
					}
				}
				
				compteurAppelDoffresNew = cptAppelDoffresNew ;
				compteurAppelDoffresUpdate = cptAppelDoffresUpdate ;
				compteurAppelDoffresSelect = cptAppelDoffresSelect ;
				
			}
			//*/

			
//			Request<OffreDto> reqOffre = new Request<>() ; 
//			Response<OffreDto> resOffre = new Response<>() ; 
//			OffreDto dataOffre = new OffreDto() ;
//			
//			dataOffre.setUserClientId(userId);
//			reqOffre.setData(dataOffre);
//			resOffre = offreBusiness.getByCriteria(reqOffre, locale);
//			if (!resOffre.isHasError()) {
//				// recuperer la liste des offres
//				offresUser = resOffre.getItems();
//			}
			
			
			// entreprises clientes
			
			
			/*
			offresUser = offreRepository.findByUserCliendId(userId, false);
			if (Utilities.isNotEmpty(offresUser)) {
				int cptOffreNew = 0 ;
				int cptOffreUpdate = 0 ;
				for (Offre entity : offresUser) {
					if (entity.getIsVisualiserNotifByClient()) {
						if (entity.getIsUpdate() != null && entity.getIsUpdate()) {
							entity.setIsVisualiserNotifByClient(false);
							entity.setIsVisualiserContenuByClient(false);
							cptOffreUpdate ++ ;
						}
					}else {
						cptOffreNew ++ ;
					}
				}
				offreRepository.save((Iterable<Offre>)offresUser);
				compteurOffreNew = cptOffreNew;
				compteurOffreUpdate = cptOffreUpdate;
			}
			//*/
			
			
			
			
			// administrateurs
			
			List<AppelDoffresRestreint> nouveauAppelDoffresRestreints = new ArrayList<>();

			List<AppelDoffresRestreint> appelDoffresRestreintsMAJ = new ArrayList<>();

			List<AppelDoffresRestreint> appelDoffresRestreintsSelect = new ArrayList<>();

			
			switch (typeEntrepriseCode) {
			case TypeUserEnum.ENTREPRISE_CLIENTE:
				// creation 
				
				
				// entreprises clientes
				offresUser = offreRepository.findByUserCliendId(userId, false);
				if (Utilities.isNotEmpty(offresUser)) {
					int cptOffreNew = 0 ;
					int cptOffreUpdate = 0 ;
					for (Offre entity : offresUser) {
						
						// TODO : le cas ou il n'avait pas vu avant la modif. Resolu la MAJ est gerer ici
						
						if (entity.getIsVisualiserNotifByClient() != null && !entity.getIsVisualiserNotifByClient()) {
							if (entity.getIsUpdate() != null && entity.getIsUpdate()){
								cptOffreUpdate ++ ;
							} else {
								cptOffreNew ++ ;
							}
						}
						
						/*
						if (entity.getIsVisualiserNotifByClient() != null && !entity.getIsVisualiserNotifByClient()) {
							if (entity.getIsUpdate() != null && entity.getIsUpdate()) {
								cptOffreUpdate ++ ;
								entity.setIsVisualiserNotifByClient(false);
								entity.setIsVisualiserContenuByClient(false);
							} else {
								cptOffreNew ++ ;
							}
						}
						//*/
						
						
//						if (entity.getIsVisualiserNotifByClient()) {
//							if (entity.getIsUpdate() != null && entity.getIsUpdate()) {
//								//entity.setIsVisualiserNotifByClient(false);
//								//entity.setIsVisualiserContenuByClient(false);
//								cptOffreUpdate ++ ;
//							}
//						}else {
//							cptOffreNew ++ ;
//						}
					}
					offreRepository.save((Iterable<Offre>)offresUser);
					compteurOffreNew = cptOffreNew;
					compteurOffreUpdate = cptOffreUpdate;
				}
				
				
				// maj aprs visualisation 
				// selection 

				break;

			case TypeUserEnum.ENTREPRISE_FOURNISSEUR:
				// creation 
				// maj aprs visualisation 
				// selection 
				data.setIsVisibleByClient(true);
			    data.setIsRetirer(false);
			    data.setIsLocked(false);
			    data.setUserFournisseurId(userId);
			    data.setIsAttribuer(false);
			    data.setIsVisualiserNotifByFournisseur(false);


				// recuperer tous les appelDoffres de sont domaines qui n'ont encore ete concluse
				// faire la difference entre les nouvelles, maj, et ou il a ete selectionné
				
			    // appelDoffres selectionner de ses secteurs d'activites (cad offre selctionner pour l'appelDoffres et conclu)
				data.setIsSelectedByClient(true);
			    //data.setIsVisualiserNotifByFournisseur(true);

				req.setData(data);
				appelDoffresRestreintsSelect = appelDoffresRestreintRepository.getByCriteria(req, em, locale) ;
				/*// MAJ
				if (Utilities.isNotEmpty(appelDoffresRestreintsSelect)) {
					for (AppelDoffresRestreint entity : appelDoffresRestreintsSelect) {
						entity.setIsVisualiserContenuByFournisseur(false);
						entity.setIsVisualiserNotifByFournisseur(false);
					}
					appelDoffresRestreintRepository.save(appelDoffresRestreintsSelect) ;
				}
				//*/
				
				// appelDoffres MAJ de ses secteurs d'activites (cad appelDoffres vu mais MAJ et non conclu)
				//data.setIsVisualiserNotifByFournisseur(true);
				//data.setIsVisualiserNotifByFournisseur(true);
				data.setIsUpdate(true);
				data.setIsUpdateAppelDoffres(true);
				data.setIsSelectedByClient(false);

			    
				req.setData(data);
				appelDoffresRestreintsMAJ = appelDoffresRestreintRepository.getByCriteria(req, em, locale) ;	
				/*// MAJ
				if (Utilities.isNotEmpty(appelDoffresRestreintsMAJ)) {
					for (AppelDoffresRestreint entity : appelDoffresRestreintsMAJ) {
						entity.setIsVisualiserContenuByFournisseur(false);
						entity.setIsVisualiserNotifByFournisseur(false);
					}
					appelDoffresRestreintRepository.save(appelDoffresRestreintsMAJ) ;
				}
				//*/
				
				
				// nouveaux appelDoffres de ses secteurs d'activites (cad appelDoffres non vu et non conclu)
			    //data.setIsVisualiserNotifByFournisseur(false);
				//data.setIsSelectedByClient(false);
				data.setIsUpdate(false);
				data.setIsUpdateAppelDoffres(false);
				

				req.setData(data);
				nouveauAppelDoffresRestreints = appelDoffresRestreintRepository.getByCriteria(req, em, locale) ;
				
				
				compteurAppelDoffresNew = (Utilities.isNotEmpty(nouveauAppelDoffresRestreints) ? nouveauAppelDoffresRestreints.size() : 0) ;
				compteurAppelDoffresUpdate = (Utilities.isNotEmpty(appelDoffresRestreintsMAJ) ? appelDoffresRestreintsMAJ.size() : 0) ;
				compteurAppelDoffresSelect = (Utilities.isNotEmpty(appelDoffresRestreintsSelect) ? appelDoffresRestreintsSelect.size() : 0) ;
				
				/*
				res = secteurDactiviteAppelDoffresBusiness.getByCriteriaCustom(req, locale) ;
				
				if (res.isHasError()) {
					response.setStatus(res.getStatus());
					response.setHasError(res.isHasError());
					return response ;
				}
				
				// entreprises fournisseurs
				// recuperer la liste des appels d'offres
				appelDoffresUser = res.getItems() ;
				if (Utilities.isNotEmpty(appelDoffresUser)) {
					
					int cptAppelDoffresNew = 0 ;
					int cptAppelDoffresSelect = 0 ;
					int cptAppelDoffresUpdate = 0 ;				
					List<AppelDoffresRestreint> appelDoffresRestreints = new ArrayList<>() ; 
					List<Offre> offres = new ArrayList<>() ; 
					
					for (AppelDoffresDto dto : appelDoffresUser) {
						if (dto.getIsUpdate() != null) {
							if(dto.getIsUpdate()) {										 
								appelDoffresRestreints = appelDoffresRestreintRepository.findByUserFournisseurIdAndAppelDoffresIdAndIsVisualiserNotifByFournisseurAndIsAppelRestreintAndIsVisibleByFournisseurAndIsDesactiver(userId, dto.getId(), true, false, false, false, false);
							}else {
								appelDoffresRestreints = appelDoffresRestreintRepository.findByUserFournisseurIdAndAppelDoffresIdAndIsVisualiserNotifByFournisseurAndIsAppelRestreintAndIsDesactiver(userId, dto.getId(), false, false, false, false);
							}
						}

						if (Utilities.isNotEmpty(appelDoffresRestreints)) { // => ancien ou nouveau appel d'offres
							if(dto.getIsUpdate() != null && dto.getIsUpdate()) {
								appelDoffresRestreints.get(0).setIsUpdateAppelDoffres(true);
								appelDoffresRestreints.get(0).setIsVisualiserNotifByFournisseur(false);
								appelDoffresRestreints.get(0).setIsVisualiserContenuByFournisseur(false);
								//appelDoffresRestreints.get(0).setIsUpdateAppelDoffres(true);
								//appelDoffresRestreints.get(0).setIsVisualiserNotifByFournisseur(true);
								//appelDoffresRestreints.get(0).setIsVisualiserContenuByFournisseur(true);
								//appelDoffresRestreints.get(0).setIsUpdateAppelDoffres(true);
								cptAppelDoffresUpdate ++ ;
								
								appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>)appelDoffresRestreints);
							}else{ // => nouvel appel d'offres
								cptAppelDoffresNew ++ ;
							}
						}else {
						}
						//isSelectedByClient  findByAppelDoffresIdAnduserFournisseurIdAndIsSelectedByClient
						//offres = offreRepository.findByAppelDoffresIdAndIsSelectedByClient(dto.getId(), true, false) ;
						offres = offreRepository.findByAppelDoffresIdAnduserFournisseurIdAndIsSelectedByClient(dto.getId(), userId, true, false) ;
						if (Utilities.isNotEmpty(offres)) {
							cptAppelDoffresSelect ++ ;
						}
					}
					
					compteurAppelDoffresNew = cptAppelDoffresNew ;
					compteurAppelDoffresUpdate = cptAppelDoffresUpdate ;
					compteurAppelDoffresSelect = cptAppelDoffresSelect ;
					
					
					
				}
				//*/
				
				break;

			case TypeUserEnum.ENTREPRISE_MIXTE:
				// creation 
				
				
				//---------------- PARTIE CLIENT

				// entreprises clientes
				offresUser = offreRepository.findByUserCliendId(userId, false);
				if (Utilities.isNotEmpty(offresUser)) {
					int cptOffreNew = 0 ;
					int cptOffreUpdate = 0 ;
					for (Offre entity : offresUser) {
						
						// TODO : le cas ou il n'avait pas vu avant la modif. Resolu la MAJ est gerer ici
						
						if (entity.getIsVisualiserNotifByClient() != null && !entity.getIsVisualiserNotifByClient()) {
							if (entity.getIsUpdate() != null && entity.getIsUpdate()){
								cptOffreUpdate ++ ;
							} else {
								cptOffreNew ++ ;
							}
						}
						
						/*
						if (entity.getIsVisualiserNotifByClient() != null && !entity.getIsVisualiserNotifByClient()) {
							if (entity.getIsUpdate() != null && entity.getIsUpdate()) {
								cptOffreUpdate ++ ;
								entity.setIsVisualiserNotifByClient(false);
								entity.setIsVisualiserContenuByClient(false);
							} else {
								cptOffreNew ++ ;
							}
						}
						//*/
						
						
//						if (entity.getIsVisualiserNotifByClient()) {
//							if (entity.getIsUpdate() != null && entity.getIsUpdate()) {
//								//entity.setIsVisualiserNotifByClient(false);
//								//entity.setIsVisualiserContenuByClient(false);
//								cptOffreUpdate ++ ;
//							}
//						}else {
//							cptOffreNew ++ ;
//						}
					}
					offreRepository.save((Iterable<Offre>)offresUser);
					compteurOffreNew = cptOffreNew;
					compteurOffreUpdate = cptOffreUpdate;
				}
				
				
				//---------------- PARTIE FOURNISSEUR
				
				// creation 
				// maj aprs visualisation 
				// selection 
				data.setIsVisibleByClient(true);
			    data.setIsRetirer(false);
			    data.setIsLocked(false);
			    data.setUserFournisseurId(userId);
			    data.setIsAttribuer(false);
			    data.setIsVisualiserNotifByFournisseur(false);


				// recuperer tous les appelDoffres de sont domaines qui n'ont encore ete concluse
				// faire la difference entre les nouvelles, maj, et ou il a ete selectionné
				
			    // appelDoffres selectionner de ses secteurs d'activites (cad offre selctionner pour l'appelDoffres et conclu)
				data.setIsSelectedByClient(true);
			    //data.setIsVisualiserNotifByFournisseur(true);

				req.setData(data);
				appelDoffresRestreintsSelect = appelDoffresRestreintRepository.getByCriteria(req, em, locale) ;
				/*// MAJ
				if (Utilities.isNotEmpty(appelDoffresRestreintsSelect)) {
					for (AppelDoffresRestreint entity : appelDoffresRestreintsSelect) {
						entity.setIsVisualiserContenuByFournisseur(false);
						entity.setIsVisualiserNotifByFournisseur(false);
					}
					appelDoffresRestreintRepository.save(appelDoffresRestreintsSelect) ;
				}
				//*/
				
				// appelDoffres MAJ de ses secteurs d'activites (cad appelDoffres vu mais MAJ et non conclu)
				//data.setIsVisualiserNotifByFournisseur(true);
				//data.setIsVisualiserNotifByFournisseur(true);
				data.setIsUpdate(true);
				data.setIsUpdateAppelDoffres(true);
				data.setIsSelectedByClient(false);

			    
				req.setData(data);
				appelDoffresRestreintsMAJ = appelDoffresRestreintRepository.getByCriteria(req, em, locale) ;	
				/*// MAJ
				if (Utilities.isNotEmpty(appelDoffresRestreintsMAJ)) {
					for (AppelDoffresRestreint entity : appelDoffresRestreintsMAJ) {
						entity.setIsVisualiserContenuByFournisseur(false);
						entity.setIsVisualiserNotifByFournisseur(false);
					}
					appelDoffresRestreintRepository.save(appelDoffresRestreintsMAJ) ;
				}
				//*/
				
				
				// nouveaux appelDoffres de ses secteurs d'activites (cad appelDoffres non vu et non conclu)
			    //data.setIsVisualiserNotifByFournisseur(false);
				//data.setIsSelectedByClient(false);
				data.setIsUpdate(false);
				data.setIsUpdateAppelDoffres(false);
				

				req.setData(data);
				nouveauAppelDoffresRestreints = appelDoffresRestreintRepository.getByCriteria(req, em, locale) ;
				
				
				compteurAppelDoffresNew = (Utilities.isNotEmpty(nouveauAppelDoffresRestreints) ? nouveauAppelDoffresRestreints.size() : 0) ;
				compteurAppelDoffresUpdate = (Utilities.isNotEmpty(appelDoffresRestreintsMAJ) ? appelDoffresRestreintsMAJ.size() : 0) ;
				compteurAppelDoffresSelect = (Utilities.isNotEmpty(appelDoffresRestreintsSelect) ? appelDoffresRestreintsSelect.size() : 0) ;
				
				/*
				res = secteurDactiviteAppelDoffresBusiness.getByCriteriaCustom(req, locale) ;
				
				if (res.isHasError()) {
					response.setStatus(res.getStatus());
					response.setHasError(res.isHasError());
					return response ;
				}
				
				// entreprises fournisseurs
				// recuperer la liste des appels d'offres
				appelDoffresUser = res.getItems() ;
				if (Utilities.isNotEmpty(appelDoffresUser)) {
					
					int cptAppelDoffresNew = 0 ;
					int cptAppelDoffresSelect = 0 ;
					int cptAppelDoffresUpdate = 0 ;				
					List<AppelDoffresRestreint> appelDoffresRestreints = new ArrayList<>() ; 
					List<Offre> offres = new ArrayList<>() ; 
					
					for (AppelDoffresDto dto : appelDoffresUser) {
						if (dto.getIsUpdate() != null) {
							if(dto.getIsUpdate()) {										 
								appelDoffresRestreints = appelDoffresRestreintRepository.findByUserFournisseurIdAndAppelDoffresIdAndIsVisualiserNotifByFournisseurAndIsAppelRestreintAndIsVisibleByFournisseurAndIsDesactiver(userId, dto.getId(), true, false, false, false, false);
							}else {
								appelDoffresRestreints = appelDoffresRestreintRepository.findByUserFournisseurIdAndAppelDoffresIdAndIsVisualiserNotifByFournisseurAndIsAppelRestreintAndIsDesactiver(userId, dto.getId(), false, false, false, false);
							}
						}

						if (Utilities.isNotEmpty(appelDoffresRestreints)) { // => ancien ou nouveau appel d'offres
							if(dto.getIsUpdate() != null && dto.getIsUpdate()) {
								appelDoffresRestreints.get(0).setIsUpdateAppelDoffres(true);
								appelDoffresRestreints.get(0).setIsVisualiserNotifByFournisseur(false);
								appelDoffresRestreints.get(0).setIsVisualiserContenuByFournisseur(false);
								//appelDoffresRestreints.get(0).setIsUpdateAppelDoffres(true);
								//appelDoffresRestreints.get(0).setIsVisualiserNotifByFournisseur(true);
								//appelDoffresRestreints.get(0).setIsVisualiserContenuByFournisseur(true);
								//appelDoffresRestreints.get(0).setIsUpdateAppelDoffres(true);
								cptAppelDoffresUpdate ++ ;
								
								appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>)appelDoffresRestreints);
							}else{ // => nouvel appel d'offres
								cptAppelDoffresNew ++ ;
							}
						}else {
						}
						//isSelectedByClient  findByAppelDoffresIdAnduserFournisseurIdAndIsSelectedByClient
						//offres = offreRepository.findByAppelDoffresIdAndIsSelectedByClient(dto.getId(), true, false) ;
						offres = offreRepository.findByAppelDoffresIdAnduserFournisseurIdAndIsSelectedByClient(dto.getId(), userId, true, false) ;
						if (Utilities.isNotEmpty(offres)) {
							cptAppelDoffresSelect ++ ;
						}
					}
					
					compteurAppelDoffresNew = cptAppelDoffresNew ;
					compteurAppelDoffresUpdate = cptAppelDoffresUpdate ;
					compteurAppelDoffresSelect = cptAppelDoffresSelect ;
					
					
					
				}
				//*/
				break;

			default:
				break;
			}
			
			
//			if (typeEntrepriseCode.equals(TypeUserEnum.)) {
//				
//			}
			
			

			UserDto userDto = new UserDto() ;
			
			
			userDto.setCompteurOffreNew(compteurOffreNew);
			userDto.setCompteurOffreUpdate(compteurOffreUpdate);
			
			
			userDto.setCompteurAppelDoffresUpdate(compteurAppelDoffresUpdate);
			userDto.setCompteurAppelDoffresSelect(compteurAppelDoffresSelect);
			userDto.setCompteurAppelDoffresNew(compteurAppelDoffresNew);

			userDto.setTotalCompteur(compteurOffreNew + compteurOffreUpdate + compteurAppelDoffresUpdate + compteurAppelDoffresSelect + compteurAppelDoffresNew);
			// Verify if user to insert do not exist

			response.setItems(Arrays.asList(userDto));
			response.setHasError(false);

			slf4jLogger.info("----end getNotifications User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
	
	
	/**
	 * connexion User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> updateNotifications(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin updateNotifications User-----");

		response = new Response<UserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			
			String typeEntrepriseCode = utilisateur.getEntreprise().getTypeEntreprise().getCode();
			Integer entrepriseId = utilisateur.getEntreprise().getId();
			Integer userId = utilisateur.getId() ;
			
			//List<SecteurDactiviteEntreprise> secteurDactiviteEntreprises = new ArrayList<>();
			//secteurDactiviteEntreprises = secteurDactiviteEntrepriseRepository.findByEntrepriseId(entrepriseId, false);
			
			List<AppelDoffresDto> appelDoffresUser = new ArrayList<>();
			List<AppelDoffres> appelDoffres = new ArrayList<>();
			//List<OffreDto> offresUser = new ArrayList<>();
			List<Offre> offresUser = new ArrayList<>();

			Request<SecteurDactiviteAppelDoffresDto> req = new Request<>() ; 
			Response<AppelDoffresDto> res = new Response<>() ;
			UserDto dto = request.getData() ;

			List<AppelDoffresRestreint> appelDoffresRestreints = new ArrayList<>() ; 

			switch (typeEntrepriseCode) {
			case TypeUserEnum.ENTREPRISE_CLIENTE:
				// creation 
				// entreprises clientes
				offresUser = offreRepository.findByUserCliendId(userId, false);
				if (Utilities.isNotEmpty(offresUser)) {
					
					for (Offre entity : offresUser) {
						entity.setIsVisualiserNotifByClient(true);
					}
					offreRepository.save((Iterable<Offre>)offresUser);
				}
				// maj aprs visualisation 
				// selection 

				break;

			case TypeUserEnum.ENTREPRISE_FOURNISSEUR:
				// creation 
				// maj aprs visualisation 
				// selection 
				
				appelDoffresRestreints = appelDoffresRestreintRepository.findByUserFournisseurId(userId, false);
				if (Utilities.isNotEmpty(appelDoffresRestreints)) { // => ancien ou nouvel appel d'offres
					for (AppelDoffresRestreint data : appelDoffresRestreints) {
						data.setIsVisualiserNotifByFournisseur(true); 
					}
					appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>)appelDoffresRestreints);
				}

				break;

			case TypeUserEnum.ENTREPRISE_MIXTE:
				// creation 
				// maj aprs visualisation 
				// selection 

				if (dto.getLink().equals("/pages/appel-doffres")) {
					
					// entreprises clientes
					offresUser = offreRepository.findByUserCliendId(userId, false);
					if (Utilities.isNotEmpty(offresUser)) {
						
						for (Offre entity : offresUser) {
							entity.setIsVisualiserNotifByClient(true);
						}
						offreRepository.save((Iterable<Offre>)offresUser);
					}
				}else if (dto.getLink().equals("/pages/offres")) {
					appelDoffresRestreints = appelDoffresRestreintRepository.findByUserFournisseurId(userId, false);
					if (Utilities.isNotEmpty(appelDoffresRestreints)) { // => ancien ou nouveau appel d'offres
						for (AppelDoffresRestreint data : appelDoffresRestreints) {
							data.setIsVisualiserNotifByFournisseur(true);
						}
						appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>)appelDoffresRestreints);
					}
				}
				break;

			default:
				break;
			}
			
			
//			if (typeEntrepriseCode.equals(TypeUserEnum.)) {
//				
//			}
			
			// Verify if user to insert do not exist

			//response.setItems(Arrays.asList(userDto));
			response.setHasError(false);

			slf4jLogger.info("----end updateNotifications User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	


	/**
	 * get User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<UserDto> getByCriteria(Request<UserDto> request, Locale locale) {
		slf4jLogger.info("----begin get User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = null;
			items = userRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<UserDto> itemsDto = new ArrayList<UserDto>();
				for (User entity : items) {
					UserDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(userRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("user", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full UserDto by using User as object.
	 *
	 * @param entity, locale
	 * @return UserDto
	 *
	 */
	private UserDto getFullInfos(User entity, Integer size, Locale locale) throws Exception {
		UserDto dto = UserTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		dto.setPassword("");
		if (size > 1) {
			return dto;
		}
		
		// renvoi de la listes des fonctionnality
		List<RoleFonctionnalite> listRoleFonctionality = roleFonctionnaliteRepository.findByRoleId(dto.getRoleId(), false);
		if (Utilities.isNotEmpty(listRoleFonctionality)) {
			List<Fonctionnalite> datasFonctionnalite = new ArrayList<>();
			for (RoleFonctionnalite data : listRoleFonctionality) {
				datasFonctionnalite.add(data.getFonctionnalite());
			}
			dto.setDatasFonctionnalite(FonctionnaliteTransformer.INSTANCE.toDtos(datasFonctionnalite));
		}
		
		Entreprise entreprise = entrepriseRepository.findById(dto.getEntrepriseId(), false);
		if (entreprise != null ) {
			dto.setDataEntreprise(EntrepriseTransformer.INSTANCE.toDto(entreprise));
			if (Utilities.notBlank(dto.getDataEntreprise().getUrlLogo())) {
				dto.getDataEntreprise().setCheminFichier(Utilities.getSuitableFileUrl(Utilities.addExtensionInFileName(dto.getDataEntreprise().getUrlLogo(), dto.getDataEntreprise().getExtensionLogo()), paramsUtils));
				//dto.getDataEntreprise().setCheminFichier(Utilities.getSuitableFileUrl(dto.getDataEntreprise().getUrlLogo(), paramsUtils));
				//dto.getDataEntreprise().setCheminFichier(Utilities.getSuitableFileUrlCustom(dto.getDataEntreprise().getUrlLogo(), GlobalEnum.profils, paramsUtils));
				//dto.getDataEntreprise().setUrlLogo(Utilities.getSuitableFileUrlCustom(dto.getDataEntreprise().getUrlLogo(), GlobalEnum.profils, paramsUtils));
				
			}
			// nbre de ususer lier au compte
			if (!dto.getDataEntreprise().getTypeEntrepriseCode().equals(TypeUserEnum.ENTREPRISE_FOURNISSEUR)) {
				List<User> users = userRepository.findByEntrepriseIdAndTypeEntrepriseCode(dto.getDataEntreprise().getId(), dto.getDataEntreprise().getTypeEntrepriseCode(), false);
				dto.getDataEntreprise().setNombreUser(Utilities.isNotEmpty(users) ? users.size() : 0);
			}
			// nbre de ususer lier au compte
			if (!dto.getDataEntreprise().getTypeEntrepriseCode().equals(TypeUserEnum.ENTREPRISE_FOURNISSEUR)) {
				List<User> users = userRepository.findByEntrepriseIdAndTypeEntrepriseCode(dto.getDataEntreprise().getId(), dto.getDataEntreprise().getTypeEntrepriseCode(), false);
				dto.getDataEntreprise().setNombreUser(Utilities.isNotEmpty(users) ? users.size() : 0);
			}

			List<SecteurDactiviteEntreprise> datas = secteurDactiviteEntrepriseRepository.findByEntrepriseId(entreprise.getId(), false);
			
			if (Utilities.isNotEmpty(datas)) {
				List<SecteurDactivite> secteurDactivites = new ArrayList<>();
				for (SecteurDactiviteEntreprise data : datas) {
					secteurDactivites.add(data.getSecteurDactivite());
				}
				dto.getDataEntreprise().setDatasSecteurDactivite(SecteurDactiviteTransformer.INSTANCE.toDtos(secteurDactivites));
			}
			List<DomaineEntreprise> datasDomaine = domaineEntrepriseRepository.findByEntrepriseId(entreprise.getId(), false);
			
			if (Utilities.isNotEmpty(datasDomaine)) {
				List<Domaine> domaines = new ArrayList<>();
				for (DomaineEntreprise data : datasDomaine) {
					domaines.add(data.getDomaine());
				}
				dto.getDataEntreprise().setDatasDomaine(DomaineTransformer.INSTANCE.toDtos(domaines));
			}
		}

		return dto;
	}
}
