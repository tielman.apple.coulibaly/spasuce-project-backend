

/*
 * Java transformer for entity table nous_contacter 
 * Created on 2020-01-03 ( Time 10:54:34 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "nous_contacter"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface NousContacterTransformer {

	NousContacterTransformer INSTANCE = Mappers.getMapper(NousContacterTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.objetNousContacter.id", target="objetNousContacterId"),
				@Mapping(source="entity.objetNousContacter.code", target="objetNousContacterCode"),
				@Mapping(source="entity.objetNousContacter.libelle", target="objetNousContacterLibelle"),
	})
	NousContacterDto toDto(NousContacter entity) throws ParseException;

    List<NousContacterDto> toDtos(List<NousContacter> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.prenoms", target="prenoms"),
		@Mapping(source="dto.email", target="email"),
		@Mapping(source="dto.telephone", target="telephone"),
		@Mapping(source="dto.contenu", target="contenu"),
		@Mapping(source="dto.autre", target="autre"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="objetNousContacter", target="objetNousContacter"),
	})
    NousContacter toEntity(NousContacterDto dto, ObjetNousContacter objetNousContacter) throws ParseException;

    //List<NousContacter> toEntities(List<NousContacterDto> dtos) throws ParseException;

}
