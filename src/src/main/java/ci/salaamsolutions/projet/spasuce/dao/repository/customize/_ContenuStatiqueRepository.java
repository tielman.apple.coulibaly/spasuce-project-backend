package ci.salaamsolutions.projet.spasuce.dao.repository.customize;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;

/**
 * Repository customize : ContenuStatique.
 */
@Repository
public interface _ContenuStatiqueRepository {
	default List<String> _generateCriteria(ContenuStatiqueDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds List of ContenuStatique by using contenuStatiqueDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of ContenuStatique
	 * @throws DataAccessException,ParseException
	 */
	public default List<ContenuStatique> getByCriteriaCustom(Request<ContenuStatiqueDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from ContenuStatique e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		//req += " order by e.id desc";
		req += " order by e.ordre asc";
		TypedQuery<ContenuStatique> query = em.createQuery(req, ContenuStatique.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}
	
	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<ContenuStatiqueDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		ContenuStatiqueDto dto = request.getData() != null ? request.getData() : new ContenuStatiqueDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (ContenuStatiqueDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(ContenuStatiqueDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getContenu())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("contenu", dto.getContenu(), "e.contenu", "String", dto.getContenuParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTitre())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("titre", dto.getTitre(), "e.titre", "String", dto.getTitreParam(), param, index, locale));
			}
			if (dto.getOrdre()!= null && dto.getOrdre() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ordre", dto.getOrdre(), "e.ordre", "Integer", dto.getOrdreParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getTypeContenuStatiqueId()!= null && dto.getTypeContenuStatiqueId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeContenuStatiqueId", dto.getTypeContenuStatiqueId(), "e.typeContenuStatique.id", "Integer", dto.getTypeContenuStatiqueIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeContenuStatiqueCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeContenuStatiqueCode", dto.getTypeContenuStatiqueCode(), "e.typeContenuStatique.code", "String", dto.getTypeContenuStatiqueCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeContenuStatiqueLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeContenuStatiqueLibelle", dto.getTypeContenuStatiqueLibelle(), "e.typeContenuStatique.libelle", "String", dto.getTypeContenuStatiqueLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
	
	/**
	 * Finds ContenuStatique by using ordre as a search criteria.
	 *
	 * @param ordre
	 * @return An Object ContenuStatique whose ordre is equals to the given ordre. If
	 *         no ContenuStatique is found, this method returns null.
	 */
	@Query("select e from ContenuStatique e where e.ordre = :ordre and e.isDeleted = :isDeleted")
	ContenuStatique findContenuStatiqueByOrdre(@Param("ordre")Integer ordre, @Param("isDeleted")Boolean isDeleted);
	
	 /**
	   * Finds one ContenuStatique by using typeContenuStatiqueLibelle as a search criteria.
	   *
	   * @param typeContenuStatiqueLibelle
	   * @return An Object ContenuStatique whose typeContenuStatiqueLibelle is equals to the given typeContenuStatiqueLibelle. If
	   *         no ContenuStatique is found, this method returns null.
	   */
	  @Query("select e from ContenuStatique e where e.typeContenuStatique.libelle = :typeContenuStatiqueLibelle and e.isDeleted = :isDeleted")
	  ContenuStatique findContenuStatiqueByTypeContenuStatiqueLibelle(@Param("typeContenuStatiqueLibelle")String typeContenuStatiqueLibelle, @Param("isDeleted")Boolean isDeleted);

}
