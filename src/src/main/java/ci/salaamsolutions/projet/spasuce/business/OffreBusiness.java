


/*
 * Java transformer for entity table offre
 * Created on 2020-01-08 ( Time 09:47:27 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffres;
import ci.salaamsolutions.projet.spasuce.dao.entity.AppelDoffresRestreint;
import ci.salaamsolutions.projet.spasuce.dao.entity.AvertissementOffre;
import ci.salaamsolutions.projet.spasuce.dao.entity.ClasseNote;
import ci.salaamsolutions.projet.spasuce.dao.entity.CritereNoteOffre;
import ci.salaamsolutions.projet.spasuce.dao.entity.Entreprise;
import ci.salaamsolutions.projet.spasuce.dao.entity.Etat;
import ci.salaamsolutions.projet.spasuce.dao.entity.Fichier;
import ci.salaamsolutions.projet.spasuce.dao.entity.Offre;
import ci.salaamsolutions.projet.spasuce.dao.entity.Paiement;
import ci.salaamsolutions.projet.spasuce.dao.entity.User;
import ci.salaamsolutions.projet.spasuce.dao.entity.ValeurCritereOffre;
import ci.salaamsolutions.projet.spasuce.dao.repository.AppelDoffresRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.AppelDoffresRestreintRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.AvertissementOffreRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.ClasseNoteRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.CritereNoteOffreRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.EntrepriseRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.EtatRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.FichierRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.OffreRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.PaiementRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.UserRepository;
import ci.salaamsolutions.projet.spasuce.dao.repository.ValeurCritereOffreRepository;
import ci.salaamsolutions.projet.spasuce.helper.ExceptionUtils;
import ci.salaamsolutions.projet.spasuce.helper.FunctionalError;
import ci.salaamsolutions.projet.spasuce.helper.HostingUtils;
import ci.salaamsolutions.projet.spasuce.helper.ParamsUtils;
import ci.salaamsolutions.projet.spasuce.helper.TechnicalError;
import ci.salaamsolutions.projet.spasuce.helper.Utilities;
import ci.salaamsolutions.projet.spasuce.helper.Validate;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.dto.AvertissementOffreDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.CritereNoteOffreDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.FichierDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.OffreDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.PaiementDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.UserDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.ValeurCritereOffreDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.AvertissementOffreTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.CritereNoteOffreTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.FichierTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.OffreTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.PaiementTransformer;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.ValeurCritereOffreTransformer;
import ci.salaamsolutions.projet.spasuce.helper.enums.EmailEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.EtatEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.GlobalEnum;

/**
BUSINESS for table "offre"
 *
 * @author Back-End developper
 *
 */
@Component
@EnableAsync
public class OffreBusiness implements IBasicBusiness<Request<OffreDto>, Response<OffreDto>> {

	private Response<OffreDto> response;
	@Autowired
	private OffreRepository offreRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FichierRepository fichierRepository;
	@Autowired
	private AvertissementOffreRepository avertissementOffreRepository;
	@Autowired
	private AppelDoffresRepository appelDoffresRepository;
	@Autowired
	private ClasseNoteRepository classeNoteRepository;
	@Autowired
	private EtatRepository etatRepository;
	@Autowired
	private PaiementRepository paiementRepository;
	@Autowired
	private CritereNoteOffreRepository critereNoteOffreRepository;
	@Autowired
	private ValeurCritereOffreRepository valeurCritereOffreRepository;
	
	@Autowired
	private EntrepriseRepository entrepriseRepository ;
	
	@Autowired
	private AppelDoffresRestreintRepository appelDoffresRestreintRepository ;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private FichierBusiness fichierBusiness;


	@Autowired
	private AvertissementOffreBusiness avertissementOffreBusiness;


	@Autowired
	private PaiementBusiness paiementBusiness;


	@Autowired
	private CritereNoteOffreBusiness critereNoteOffreBusiness;


	@Autowired
	private ValeurCritereOffreBusiness valeurCritereOffreBusiness;

	@Autowired
	private ParamsUtils paramsUtils;

	private Context context;

	@Autowired
	private HostingUtils hostingUtils;


	public OffreBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create Offre by using OffreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<OffreDto> create(Request<OffreDto> request, Locale locale)  {
		slf4jLogger.info("----begin create Offre-----");

		response = new Response<OffreDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Offre> items = new ArrayList<Offre>();
			List<FichierDto> datasFichier = new ArrayList<>();
			List<ValeurCritereOffreDto> datasValeurCritereOffre = new ArrayList<>();

			for (OffreDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("messageClient", dto.getMessageClient());
				//fieldsToVerify.put("isSelectedByClient", dto.getIsSelectedByClient());
				//fieldsToVerify.put("commentaire", dto.getCommentaire());
				//fieldsToVerify.put("dateSoumission", dto.getDateSoumission());
				//fieldsToVerify.put("isPayer", dto.getIsPayer());
				//fieldsToVerify.put("isNoterByClient", dto.getIsNoterByClient());
				//fieldsToVerify.put("isVisualiserNoteByFournisseur", dto.getIsVisualiserNoteByFournisseur());
				//fieldsToVerify.put("isVisualiserNotifByClient", dto.getIsVisualiserNotifByClient());
				//fieldsToVerify.put("isVisualiserContenuByClient", dto.getIsVisualiserContenuByClient());
				//fieldsToVerify.put("isModifiable", dto.getIsModifiable());
				//fieldsToVerify.put("isRetirer", dto.getIsRetirer());
				//fieldsToVerify.put("prix", dto.getPrix());
				fieldsToVerify.put("appelDoffresId", dto.getAppelDoffresId());
				fieldsToVerify.put("userFournisseurId", dto.getUserFournisseurId());
				fieldsToVerify.put("etatCode", dto.getEtatCode());
				//fieldsToVerify.put("etatId", dto.getEtatId());
				
				fieldsToVerify.put("datasValeurCritereOffre", dto.getDatasValeurCritereOffre());
				//fieldsToVerify.put("datasDomaineAppelDoffres", dto.getDatasDomaineAppelDoffres());
				fieldsToVerify.put("datasFichier", dto.getDatasFichier());
				
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if offre to insert do not exist
				Offre existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("offre -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if user exist
				User existingUser = userRepository.findById(dto.getUserFournisseurId(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserFournisseurId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if appelDoffres exist
				AppelDoffres existingAppelDoffres = appelDoffresRepository.findById(dto.getAppelDoffresId(), false);
				if (existingAppelDoffres == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getAppelDoffresId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if classeNote exist
				ClasseNote existingClasseNote = null;
				if (Utilities.notBlank(dto.getClasseNoteCode())) {
				//if (dto.getClasseNoteId() != null && dto.getClasseNoteId() > 0) {
					existingClasseNote = classeNoteRepository.findByCode(dto.getClasseNoteCode(), false);
					if (existingClasseNote == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("classeNote -> " + dto.getClasseNoteCode(), locale));
						response.setHasError(true);
						return response;
					}
				}
				// Verify if etat exist
				//Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
				Etat existingEtat = etatRepository.findByCode(dto.getEtatCode(), false);
				if (existingEtat == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatCode(), locale));
					response.setHasError(true);
					return response;
				}
				
				if (dto.getIsPayer() == null ) {
					dto.setIsPayer(false);
				}
				dto.setIsNoterByClient(false);
				dto.setIsVisualiserNoteByFournisseur(false);
				dto.setIsVisualiserNotifByClient(false);
				dto.setIsVisualiserContenuByClient(false);
				dto.setIsModifiable(true);
				dto.setIsRetirer(false);
				dto.setIsUpdate(false);
				dto.setIsSelectedByClient(false);
			
				
				if (existingEtat.getCode().equals(EtatEnum.ENTRANT)) {
					existingAppelDoffres.setIsContenuNotificationDispo(true); // pour dire qu'un nouveau contenu est dispo 
					existingAppelDoffres.setIsModifiable(false); // pour dire qu'on ne peut plus modifier l'AO
					appelDoffresRepository.save(existingAppelDoffres);
					
					// maj d'appelDoffresRestreint
					List<AppelDoffresRestreint> appelDoffresRestreints = new ArrayList<AppelDoffresRestreint>() ;
					appelDoffresRestreints = appelDoffresRestreintRepository.findByUserFournisseurIdAndAppelDoffresId(dto.getUserFournisseurId(), dto.getAppelDoffresId(), false) ;
					if (Utilities.isNotEmpty(appelDoffresRestreints)) {
						for (AppelDoffresRestreint data : appelDoffresRestreints) {
							data.setIsSoumissionnerByFournisseur(true);
						}
						appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) appelDoffresRestreints) ;
						
					}else { // creer pour ceux qui n'ont pas de AppelDoffresRestreint avant  (cad il est passé par voir plus)
						AppelDoffresRestreint appelDoffresRestreint = new AppelDoffresRestreint();
						
						appelDoffresRestreint.setAppelDoffres(existingAppelDoffres);
						appelDoffresRestreint.setUser(existingUser);
						appelDoffresRestreint.setIsAppelRestreint(false); // pour dire que se sont des elements pour visualtisations
						appelDoffresRestreint.setIsSoumissionnerByFournisseur(true); // important ici
						appelDoffresRestreint.setIsDesactiver(false);
						appelDoffresRestreint.setIsVisualiserContenuByFournisseur(false);
						appelDoffresRestreint.setIsVisualiserNotifByFournisseur(false);
						//appelDoffresRestreint.setIsUpdateAppelDoffres(data.getIsUpdate()); // MAJ mis en evidence ne le concerne pas
						appelDoffresRestreint.setIsUpdateAppelDoffres(false);
						appelDoffresRestreint.setIsVisibleByFournisseur(true); // car il ne l'a pas encore supprimer logiquement
						appelDoffresRestreint.setCreatedAt(Utilities.getCurrentDate());
						appelDoffresRestreint.setCreatedBy(existingUser.getId());
						appelDoffresRestreint.setIsSelectedByClient(false);
						appelDoffresRestreint.setIsDeleted(false);
						appelDoffresRestreintRepository.save(appelDoffresRestreint) ;

					}

					// generation du code du contrat
					
					
					//List<Offre> offresSysteme = offreRepository.findByEtatCode(EtatEnum.ENTRANT, false);
					List<Offre> offresSysteme = offreRepository.findByEtatCodeAll(EtatEnum.ENTRANT);
					
					Integer nbre = (Utilities.isNotEmpty(offresSysteme) ? offresSysteme.size() : 0);
					
					String code = Utilities.getCodeByCritreria(GlobalEnum.code_of, nbre) ;
					
					slf4jLogger.info("********code*********", code);
					
					dto.setCode(code);
					
				}
				
				// prise en compte de la date limite des depots
				if (existingEtat.getCode().equals(EtatEnum.ENTRANT) && existingAppelDoffres.getDateLimiteDepot() != null && ( !new Date().equals(existingAppelDoffres.getDateLimiteDepot()) && new Date().after(existingAppelDoffres.getDateLimiteDepot()))) {
					response.setStatus(functionalError.DISALLOWED_OPERATION("La date de depôt de cet appel d'offres est passée -> " + dateFormat.format(existingAppelDoffres.getDateLimiteDepot()), locale));
					response.setHasError(true);
					return response;
				}
				
				Offre entityToSave = null;
				entityToSave = OffreTransformer.INSTANCE.toEntity(dto, existingUser, existingAppelDoffres, existingClasseNote, existingEtat);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				if (existingEtat.getCode().equals(EtatEnum.ENTRANT)) {
					entityToSave.setDateSoumission(Utilities.getCurrentDate());
				}
				items.add(entityToSave);
				
				
				for (FichierDto data : dto.getDatasFichier()) {
					data.setEntityOffre(entityToSave);
					//data.setIsFichierOffre(false);
				}
				datasFichier.addAll(dto.getDatasFichier());
				
				
				for (ValeurCritereOffreDto data : dto.getDatasValeurCritereOffre()) {
					data.setEntityOffre(entityToSave);
				}
				datasValeurCritereOffre.addAll(dto.getDatasValeurCritereOffre());
			
			}

			if (!items.isEmpty()) {
				List<Offre> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = offreRepository.save((Iterable<Offre>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("offre", locale));
					response.setHasError(true);
					return response;
				}
				
				
				for (ValeurCritereOffreDto data : datasValeurCritereOffre) {
					data.setOffreId(data.getEntityOffre().getId());
				}
				Request<ValeurCritereOffreDto> req = new Request<>();
				Response<ValeurCritereOffreDto> res = new Response<>();
				req.setUser(request.getUser());
				req.setDatas(datasValeurCritereOffre);
				
				res = valeurCritereOffreBusiness.create(req, locale);
				if (res != null && res.isHasError()) {
					response.setStatus(res.getStatus());
					response.setHasError(true);
					return response;
				}
				
				
				// creation des Fichier 
				for (FichierDto data : datasFichier) {
					data.setSource(GlobalEnum.fichiers_of);
					data.setSourceId(data.getEntityOffre().getId());
					//data.setOffreId(data.getEntityOffre().getId());
				}
				Request<FichierDto> reqFichier = new Request<>();
				Response<FichierDto> resFichier = new Response<>();
				reqFichier.setUser(request.getUser());
				reqFichier.setDatas(datasFichier);

				resFichier = fichierBusiness.create(reqFichier, locale);

				if (resFichier != null && resFichier.isHasError()) {
					response.setStatus(resFichier.getStatus());
					response.setHasError(true);
					return response;
				}
				
				
				
				List<OffreDto> itemsDto = new ArrayList<OffreDto>();
				for (Offre entity : itemsSaved) {
					OffreDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create Offre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Offre by using OffreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<OffreDto> update(Request<OffreDto> request, Locale locale)  {
		slf4jLogger.info("----begin update Offre-----");

		response = new Response<OffreDto>();
		List<String> listOfOldFiles = new 	ArrayList<>();
		List<String> listOfFilesCreate = new 	ArrayList<>();


		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Offre> items = new ArrayList<Offre>();
			
			List<ValeurCritereOffre> itemsValeurCritereOffreToDelete = new ArrayList<>();
			List<Fichier> itemsFichierToDelete = new ArrayList<>();
			

			List<ValeurCritereOffreDto> datasValeurCritereOffre = new ArrayList<>();
			List<FichierDto> datasFichier = new ArrayList<>();


			for (OffreDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la offre existe
				Offre entityToSave = null;
				entityToSave = offreRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("offre -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();
				//Integer entityAppelDoffresId = entityToSave.getAppelDoffres().getId();

				
				// l'offres n'est pas modifiable car deja vu par le client
				if ((!entityToSave.getIsModifiable() || entityToSave.getIsVisualiserContenuByClient()) 
						&& utilisateur.getId().equals(entityToSave.getCreatedBy())) {
					response.setStatus(functionalError.DISALLOWED_OPERATION("offre -> " + dto.getId() + " n'est pas modifiable", locale));
					response.setHasError(true);
					return response;
				}
				
				// Verify if user exist
				if (dto.getUserFournisseurId() != null && dto.getUserFournisseurId() > 0){
					User existingUser = userRepository.findById(dto.getUserFournisseurId(), false);
					if (existingUser == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserFournisseurId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUser(existingUser);
				}
				// Verify if appelDoffres exist
				if (dto.getAppelDoffresId() != null && dto.getAppelDoffresId() > 0){
					AppelDoffres existingAppelDoffres = appelDoffresRepository.findById(dto.getAppelDoffresId(), false);
					if (existingAppelDoffres == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getAppelDoffresId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setAppelDoffres(existingAppelDoffres);
				}
				// Verify if classeNote exist
				if (Utilities.notBlank(dto.getClasseNoteCode())){

				//if (dto.getClasseNoteId() != null && dto.getClasseNoteId() > 0) {
					
					// TODO : verifier que la classe correspond à la note
					ClasseNote existingClasseNote = classeNoteRepository.findByCode(dto.getClasseNoteCode(), false);
					if (existingClasseNote == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("classeNote -> " + dto.getClasseNoteCode(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setClasseNote(existingClasseNote);
				}
				// Verify if etat exist
				if (Utilities.notBlank(dto.getEtatCode())){
					//if (dto.getEtatId() != null && dto.getEtatId() > 0){
					//Etat existingEtat = etatRepository.findById(dto.getEtatId(), false);
						Etat existingEtat = etatRepository.findByCode(dto.getEtatCode(), false);
					if (existingEtat == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("etat -> " + dto.getEtatCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (!entityToSave.getEtat().getCode().equals(existingEtat.getCode()) 
							&& existingEtat.getCode().equals(EtatEnum.BROUILLON)) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("On ne peut pas passer de l'etat -> " + entityToSave.getEtat().getCode() + " à l'etat -> "+dto.getEtatCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (existingEtat.getCode().equals(EtatEnum.ENTRANT)
							&& entityToSave.getEtat().getCode().equals(EtatEnum.BROUILLON)) {
						
						// prise en compte de la date limite des depots
						if (entityToSave.getAppelDoffres().getDateLimiteDepot() != null && ( !new Date().equals(entityToSave.getAppelDoffres().getDateLimiteDepot()) && new Date().after(entityToSave.getAppelDoffres().getDateLimiteDepot()))) {
							response.setStatus(functionalError.DISALLOWED_OPERATION("La date de depôt de cet appel d'offres est passée -> " + dateFormat.format(entityToSave.getAppelDoffres().getDateLimiteDepot()), locale));
							response.setHasError(true);
							return response;
						}
						
						
						entityToSave.getAppelDoffres().setIsContenuNotificationDispo(true); // pour dire qu'un nouveau contenu est dispo 
						entityToSave.getAppelDoffres().setIsModifiable(false); // pour dire qu'on ne peut plus modifier l'AO
						appelDoffresRepository.save(entityToSave.getAppelDoffres());
						
						// maj d'appelDoffresRestreint
						List<AppelDoffresRestreint> appelDoffresRestreints = new ArrayList<AppelDoffresRestreint>() ;
						appelDoffresRestreints = appelDoffresRestreintRepository.findByUserFournisseurIdAndAppelDoffresId(dto.getUserFournisseurId(), dto.getAppelDoffresId(), false) ;
						if (Utilities.isNotEmpty(appelDoffresRestreints)) {
							for (AppelDoffresRestreint data : appelDoffresRestreints) {
								data.setIsSoumissionnerByFournisseur(true);
							}
							appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) appelDoffresRestreints) ;
							
						}else { // creer pour ceux qui n'ont pas de AppelDoffresRestreint avant  (cad il est passé par voir plus)
							AppelDoffresRestreint appelDoffresRestreint = new AppelDoffresRestreint();
							
							appelDoffresRestreint.setAppelDoffres(entityToSave.getAppelDoffres());
							appelDoffresRestreint.setUser(entityToSave.getUser());
							appelDoffresRestreint.setIsAppelRestreint(false); // pour dire que se sont des elements pour visualtisations
							appelDoffresRestreint.setIsSoumissionnerByFournisseur(true); // important ici
							appelDoffresRestreint.setIsDesactiver(false);
							appelDoffresRestreint.setIsVisualiserContenuByFournisseur(false);
							appelDoffresRestreint.setIsVisualiserNotifByFournisseur(false);
							//appelDoffresRestreint.setIsUpdateAppelDoffres(data.getIsUpdate()); // MAJ mis en evidence ne le concerne pas
							appelDoffresRestreint.setIsUpdateAppelDoffres(false);
							appelDoffresRestreint.setIsVisibleByFournisseur(true); // car il ne l'a pas encore supprimer logiquement
							appelDoffresRestreint.setCreatedAt(Utilities.getCurrentDate());
							appelDoffresRestreint.setCreatedBy(utilisateur.getId());
							appelDoffresRestreint.setIsSelectedByClient(false);
							appelDoffresRestreint.setIsDeleted(false);
							appelDoffresRestreintRepository.save(appelDoffresRestreint) ;

						}
						
						
						
						// generation du code du contrat
						
						//List<Offre> offresSysteme = offreRepository.findByEtatCode(EtatEnum.ENTRANT, false);
						List<Offre> offresSysteme = offreRepository.findByEtatCodeAll(EtatEnum.ENTRANT);
						Integer nbre = (Utilities.isNotEmpty(offresSysteme) ? offresSysteme.size() : 0);
						
						String code = Utilities.getCodeByCritreria(GlobalEnum.code_of, nbre) ;

						slf4jLogger.info("********code*********", code);
						
						entityToSave.setCode(code);

						entityToSave.setDateSoumission(Utilities.getCurrentDate());
					}
					entityToSave.setEtat(existingEtat);
				}
				if (Utilities.notBlank(dto.getMessageClient())) {
					entityToSave.setMessageClient(dto.getMessageClient());
				}
				if (dto.getIsSelectedByClient() != null) {
					entityToSave.setIsSelectedByClient(dto.getIsSelectedByClient());
					
					// setter que l'offre a été retenu dans AppelDoffresRestreint
					List<AppelDoffresRestreint> appelDoffresRestreints = new ArrayList<>() ; 
					appelDoffresRestreints = appelDoffresRestreintRepository.findByUserFournisseurIdAndAppelDoffresId(entityToSave.getUser().getId(), entityToSave.getAppelDoffres().getId(), false);
					if (Utilities.isNotEmpty(appelDoffresRestreints)) {
						for (AppelDoffresRestreint data : appelDoffresRestreints) {
							data.setIsSelectedByClient(dto.getIsSelectedByClient());
							//data.setIsSelectedByClient(true);
						}
						appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) appelDoffresRestreints);
					}
				}
				if (Utilities.notBlank(dto.getCommentaire())) {
					entityToSave.getAppelDoffres().setIsContenuNotificationDispo(true);
					if (entityToSave.getIsVisualiserContenuByClient() != null && entityToSave.getIsVisualiserContenuByClient()) {
						entityToSave.setIsUpdate(true);
						entityToSave.setIsVisualiserNotifByClient(false);
						entityToSave.setIsVisualiserContenuByClient(false);
					}
					if (entityToSave.getIsVisualiserNotifByClient() && entityToSave.getIsVisualiserNotifByClient()) {
						entityToSave.setIsUpdate(true);
						entityToSave.setIsVisualiserNotifByClient(false);
						entityToSave.setIsVisualiserContenuByClient(false); // surplus en realite
					}
					entityToSave.setCommentaire(dto.getCommentaire());
				}
				if (dto.getIsPayer() != null) {
					entityToSave.setIsPayer(dto.getIsPayer());
				}
				if (dto.getIsUpdate() != null) {
					entityToSave.setIsUpdate(dto.getIsUpdate());
				}
				if (dto.getIsNoterByClient() != null) {
					
					// TODO :  faire la moyenne du fournisseur en terme de note
					entityToSave.setIsNoterByClient(dto.getIsNoterByClient());
				}
				if (dto.getIsVisualiserNoteByFournisseur() != null) {
					entityToSave.setIsVisualiserNoteByFournisseur(dto.getIsVisualiserNoteByFournisseur());
				}
				if (dto.getIsVisualiserNotifByClient() != null) {
					entityToSave.setIsVisualiserNotifByClient(dto.getIsVisualiserNotifByClient());
				}
				if (dto.getIsVisualiserContenuByClient() != null) {
					entityToSave.setIsModifiable(false);
					entityToSave.setIsVisualiserContenuByClient(dto.getIsVisualiserContenuByClient());
					
					// mettre l'AO hors surbrillance si tous les offres ont ete vu
					List<Offre> offres = offreRepository.findByAppelDoffresIdAndIsVisualiserContenuByClient(entityToSave.getAppelDoffres().getId(), false, false) ;
					if (!Utilities.isNotEmpty(offres)) {
						entityToSave.getAppelDoffres().setIsContenuNotificationDispo(false);
					}
				}
				if (dto.getIsModifiable() != null) {
					entityToSave.setIsModifiable(dto.getIsModifiable());
				}
				if (dto.getIsRetirer() != null) {
					entityToSave.setIsRetirer(dto.getIsRetirer());
				}
				
				if (dto.getNoteOffre() != null && dto.getNoteOffre() > 0) {
					
					entityToSave.setNoteOffre(dto.getNoteOffre());

					
					// calculer la moyenne genenral du fournisseur et sa classe
					Entreprise entrepriseFournisseur = entityToSave.getUser().getEntreprise() ;
					List<Offre> offresSelectionner = offreRepository.findByUserFournisseurIdAndIsSelectedByClientAndIsNoterByClient(entityToSave.getUser().getId(), true, true, false);
					double totalNote = 0 ; 
					String classeNoteCode = "" ; 
					double noteADeterminer = 0 ; 
					if (Utilities.isNotEmpty(offresSelectionner)) {
						for (Offre data : offresSelectionner) {
							totalNote =+ data.getNoteOffre();
						}
						
						noteADeterminer = totalNote/offresSelectionner.size() ;
						
						DecimalFormat df = new DecimalFormat("0.00");
						df.setRoundingMode(RoundingMode.HALF_UP);
						slf4jLogger.info("df.format(noteADeterminer) :::"  + df.format(noteADeterminer));

						noteADeterminer = Double.parseDouble(df.format(noteADeterminer).replace(',', '.')); // arrondissement de la note

						if (noteADeterminer  <= 8) {
							classeNoteCode = GlobalEnum.CLASSE_C ;
						} else {
							if (8 < noteADeterminer && noteADeterminer <= 15) {
								classeNoteCode = GlobalEnum.CLASSE_B ;
							}else{
								classeNoteCode = GlobalEnum.CLASSE_A ;
							}
						}
						ClasseNote classeNote = classeNoteRepository.findByCode(classeNoteCode, false);
						
						if (classeNote != null) {
							entrepriseFournisseur.setClasseNote(classeNote);
							entrepriseFournisseur.setNote((float) noteADeterminer);
						}
						// enregister MAJ
						entrepriseRepository.save(entrepriseFournisseur) ;
					}
				}
				
				
				if (dto.getPrix() != null && dto.getPrix() > 0) {
					entityToSave.getAppelDoffres().setIsContenuNotificationDispo(true);
					if (entityToSave.getIsVisualiserContenuByClient() != null && entityToSave.getIsVisualiserContenuByClient()) {
						entityToSave.setIsUpdate(true);
						entityToSave.setIsVisualiserNotifByClient(false);
						entityToSave.setIsVisualiserContenuByClient(false);
					}
					 // TODO : a revoir que pr le changement du critere prix
					if (entityToSave.getIsVisualiserNotifByClient() && entityToSave.getIsVisualiserNotifByClient()) {
						entityToSave.setIsUpdate(true);
						entityToSave.setIsVisualiserNotifByClient(false);
						entityToSave.setIsVisualiserContenuByClient(false); // surplus en realite
					}
					entityToSave.setPrix(dto.getPrix());
				}
				

//				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
//					entityToSave.setCreatedBy(dto.getCreatedBy());
//				}
//				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
//					entityToSave.setUpdatedBy(dto.getUpdatedBy());
//				}
//				if (Utilities.notBlank(dto.getDeletedAt())) {
//					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
//				}
//				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
//					entityToSave.setDeletedBy(dto.getDeletedBy());
//				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
				
				if (Utilities.isNotEmpty(dto.getDatasFichier())) {
					entityToSave.getAppelDoffres().setIsContenuNotificationDispo(true);
					if (entityToSave.getIsVisualiserContenuByClient() != null && entityToSave.getIsVisualiserContenuByClient()) {
						entityToSave.setIsUpdate(true);
						entityToSave.setIsVisualiserNotifByClient(false);
						entityToSave.setIsVisualiserContenuByClient(false);
					}
					/*
					if (entityToSave.getIsVisualiserNotifByClient() && entityToSave.getIsVisualiserNotifByClient()) {
						entityToSave.setIsUpdate(true);
						entityToSave.setIsVisualiserNotifByClient(false);
						entityToSave.setIsVisualiserContenuByClient(false); // surplus en realite
					}
					//*/
					
					// recuperation des anciens pour suppression
					//List<Fichier> fichiers = fichierRepository.findByAppelDoffresId(entityToSaveId, false);
					//List<Fichier> fichiers = fichierRepository.findByOffreId(entityToSaveId, false);
					List<Fichier> fichiers = fichierRepository.findBySourceIdAndSource(entityToSaveId, GlobalEnum.fichiers_of, false);

					if (Utilities.isNotEmpty(fichiers)) {
						itemsFichierToDelete.addAll(fichiers);
					}
					
					for (FichierDto data : dto.getDatasFichier()){
						//data.setAppelDoffresId(entityToSaveId);
						//data.setOffreId(entityToSaveId);
						//data.setIsFichierOffre(true);
						data.setSource(GlobalEnum.fichiers_of);
						data.setSourceId(entityToSaveId);

					}
					datasFichier.addAll(dto.getDatasFichier());
				}
				if (Utilities.isNotEmpty(dto.getDatasValeurCritereOffre())) {
					entityToSave.getAppelDoffres().setIsContenuNotificationDispo(true);
					if (entityToSave.getIsVisualiserContenuByClient() != null && entityToSave.getIsVisualiserContenuByClient()) {
						entityToSave.setIsUpdate(true);
						entityToSave.setIsVisualiserNotifByClient(false);
						entityToSave.setIsVisualiserContenuByClient(false);
					}
					 // TODO : a revoir que pr le changement du critere prix / regler avec prix 
					/*
					if (entityToSave.getIsVisualiserNotifByClient() && entityToSave.getIsVisualiserNotifByClient()) {
						entityToSave.setIsUpdate(true);
						entityToSave.setIsVisualiserNotifByClient(false);
						entityToSave.setIsVisualiserContenuByClient(false); // surplus en realite
					}
					//*/
					// recuperation des anciens pour suppression
					List<ValeurCritereOffre> valeurCritereOffres = valeurCritereOffreRepository.findByOffreId(entityToSaveId, false);
					if (Utilities.isNotEmpty(valeurCritereOffres)) {
						itemsValeurCritereOffreToDelete.addAll(valeurCritereOffres);
					}
					for (ValeurCritereOffreDto data : dto.getDatasValeurCritereOffre()) {
						data.setOffreId(entityToSaveId);
					}
					datasValeurCritereOffre.addAll(dto.getDatasValeurCritereOffre());
				}
			}

			if (!items.isEmpty()) {
				List<Offre> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = offreRepository.save((Iterable<Offre>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("offre", locale));
					response.setHasError(true);
					return response;
				}
				
				// creation des ValeurCritereAppelDoffres
				if (!itemsValeurCritereOffreToDelete.isEmpty()) {
					List<ValeurCritereOffre> itemsToDelete = null;
					for (ValeurCritereOffre data : itemsValeurCritereOffreToDelete) {
						data.setIsDeleted(true);
						data.setDeletedBy(request.getUser());
						data.setDeletedAt(Utilities.getCurrentDate());
					}
					// maj les donnees en base
					itemsToDelete = valeurCritereOffreRepository.save((Iterable<ValeurCritereOffre>) itemsValeurCritereOffreToDelete);
					if (itemsToDelete == null || itemsToDelete.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("valeurCritereOffre", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!datasValeurCritereOffre.isEmpty()) {
					
					Request<ValeurCritereOffreDto> req = new Request<>();
					Response<ValeurCritereOffreDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasValeurCritereOffre);

					res = valeurCritereOffreBusiness.create(req, locale);

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}

				}
				
				
				// creation des Fichier 
				if (!itemsFichierToDelete.isEmpty()) {
					for (Fichier data : itemsFichierToDelete) {
						data.setIsDeleted(true);
						data.setDeletedBy(request.getUser());
						data.setDeletedAt(Utilities.getCurrentDate());
						
						//listOfOldFiles.add(data.getName());
						String fileNameByExtension = Utilities.addExtensionInFileName( data.getName() , data.getExtension()) ;
						listOfOldFiles.add(fileNameByExtension);
					}
					List<Fichier> itemsToDelete = null;
					// maj les donnees en base
					itemsToDelete = fichierRepository.save((Iterable<Fichier>) itemsFichierToDelete);
					if (itemsToDelete == null || itemsToDelete.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!datasFichier.isEmpty()) {
					
					Request<FichierDto> req = new Request<>();
					Response<FichierDto> res = new Response<>();
					req.setUser(request.getUser());
					req.setDatas(datasFichier);

					res = fichierBusiness.create(req, locale);

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
				}

				List<OffreDto> itemsDto = new ArrayList<OffreDto>();
				for (Offre entity : itemsSaved) {
					OffreDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update Offre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}else {
				// suppression des anciens fichiers quand le processus de update c'est bien dérouler
				slf4jLogger.info("listOfOldFiles ... " + listOfOldFiles);
				slf4jLogger.info("listOfOldFiles.toString() ... " + listOfOldFiles.toString());

				Utilities.deleteFileOnSever(listOfOldFiles, paramsUtils);
			}
		}
		return response;
	}


	/**
	 * delete Offre by using OffreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<OffreDto> delete(Request<OffreDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete Offre-----");

		response = new Response<OffreDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Offre> items = new ArrayList<Offre>();

			for (OffreDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la offre existe
				Offre existingEntity = null;
				existingEntity = offreRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("offre -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// fichier
				//List<Fichier> listOfFichier = fichierRepository.findByOffreId(existingEntity.getId(), false);
				List<Fichier> listOfFichier = fichierRepository.findBySourceIdAndSource(existingEntity.getId(), GlobalEnum.fichiers_of, false);

				if (listOfFichier != null && !listOfFichier.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfFichier.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// avertissementOffre
				List<AvertissementOffre> listOfAvertissementOffre = avertissementOffreRepository.findByOffreId(existingEntity.getId(), false);
				if (listOfAvertissementOffre != null && !listOfAvertissementOffre.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfAvertissementOffre.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// paiement
				List<Paiement> listOfPaiement = paiementRepository.findByOffreId(existingEntity.getId(), false);
				if (listOfPaiement != null && !listOfPaiement.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPaiement.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// critereNoteOffre
				List<CritereNoteOffre> listOfCritereNoteOffre = critereNoteOffreRepository.findByOffreId(existingEntity.getId(), false);
				if (listOfCritereNoteOffre != null && !listOfCritereNoteOffre.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfCritereNoteOffre.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// valeurCritereOffre
				List<ValeurCritereOffre> listOfValeurCritereOffre = valeurCritereOffreRepository.findByOffreId(existingEntity.getId(), false);
				if (listOfValeurCritereOffre != null && !listOfValeurCritereOffre.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfValeurCritereOffre.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				offreRepository.save((Iterable<Offre>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete Offre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete Offre by using OffreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<OffreDto> forceDelete(Request<OffreDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete Offre-----");

		response = new Response<OffreDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Offre> items = new ArrayList<Offre>();

			for (OffreDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la offre existe
				Offre existingEntity = null;
				existingEntity = offreRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("offre -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				
				if (existingEntity.getIsSelectedByClient() != null && existingEntity.getIsSelectedByClient()) {
					response.setStatus(functionalError.DISALLOWED_OPERATION("L'offre -> " + dto.getId() + " est déjà selectionnée !!!", locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// fichier
				//List<Fichier> listOfFichier = fichierRepository.findByOffreId(existingEntity.getId(), false);
				List<Fichier> listOfFichier = fichierRepository.findBySourceIdAndSource(existingEntity.getId(), GlobalEnum.fichiers_of, false);

				if (listOfFichier != null && !listOfFichier.isEmpty()){
					List<String> listFilesToDelete = new ArrayList<String>() ;
					
					for (Fichier data : listOfFichier) {
						listFilesToDelete.add(data.getName());
					}
					Request<FichierDto> deleteRequest = new Request<FichierDto>();
					deleteRequest.setDatas(FichierTransformer.INSTANCE.toDtos(listOfFichier));
					deleteRequest.setUser(request.getUser());
					Response<FichierDto> deleteResponse = fichierBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
					// suppression des fichiers
					Utilities.deleteFileOnSever(listFilesToDelete, paramsUtils);

				}
				// avertissementOffre
				List<AvertissementOffre> listOfAvertissementOffre = avertissementOffreRepository.findByOffreId(existingEntity.getId(), false);
				if (listOfAvertissementOffre != null && !listOfAvertissementOffre.isEmpty()){
					Request<AvertissementOffreDto> deleteRequest = new Request<AvertissementOffreDto>();
					deleteRequest.setDatas(AvertissementOffreTransformer.INSTANCE.toDtos(listOfAvertissementOffre));
					deleteRequest.setUser(request.getUser());
					Response<AvertissementOffreDto> deleteResponse = avertissementOffreBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// paiement
				List<Paiement> listOfPaiement = paiementRepository.findByOffreId(existingEntity.getId(), false);
				if (listOfPaiement != null && !listOfPaiement.isEmpty()){
					Request<PaiementDto> deleteRequest = new Request<PaiementDto>();
					deleteRequest.setDatas(PaiementTransformer.INSTANCE.toDtos(listOfPaiement));
					deleteRequest.setUser(request.getUser());
					Response<PaiementDto> deleteResponse = paiementBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// critereNoteOffre
				List<CritereNoteOffre> listOfCritereNoteOffre = critereNoteOffreRepository.findByOffreId(existingEntity.getId(), false);
				if (listOfCritereNoteOffre != null && !listOfCritereNoteOffre.isEmpty()){
					Request<CritereNoteOffreDto> deleteRequest = new Request<CritereNoteOffreDto>();
					deleteRequest.setDatas(CritereNoteOffreTransformer.INSTANCE.toDtos(listOfCritereNoteOffre));
					deleteRequest.setUser(request.getUser());
					Response<CritereNoteOffreDto> deleteResponse = critereNoteOffreBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// valeurCritereOffre
				List<ValeurCritereOffre> listOfValeurCritereOffre = valeurCritereOffreRepository.findByOffreId(existingEntity.getId(), false);
				if (listOfValeurCritereOffre != null && !listOfValeurCritereOffre.isEmpty()){
					Request<ValeurCritereOffreDto> deleteRequest = new Request<ValeurCritereOffreDto>();
					deleteRequest.setDatas(ValeurCritereOffreTransformer.INSTANCE.toDtos(listOfValeurCritereOffre));
					deleteRequest.setUser(request.getUser());
					Response<ValeurCritereOffreDto> deleteResponse = valeurCritereOffreBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				offreRepository.save((Iterable<Offre>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete Offre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get Offre by using OffreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<OffreDto> getByCriteria(Request<OffreDto> request, Locale locale) {
		slf4jLogger.info("----begin get Offre-----");

		response = new Response<OffreDto>();

		try {
			List<Offre> items = null;
			items = offreRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<OffreDto> itemsDto = new ArrayList<OffreDto>();
				for (Offre entity : items) {
					OffreDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(offreRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("offre", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get Offre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
	
	/**
	 * updateCustom Offre by using OffreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<OffreDto> updateCustom(Request<OffreDto> request, Locale locale)  {
		slf4jLogger.info("----begin updateCustom Offre-----");

		response = new Response<OffreDto>();
		List<String> listOfOldFiles = new 	ArrayList<>();
		List<String> listOfFilesCreate = new 	ArrayList<>();


		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Offre> items = new ArrayList<Offre>();
			
			for (OffreDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la offre existe
				Offre entityToSave = null;
				entityToSave = offreRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("offre -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();
				//Integer entityAppelDoffresId = entityToSave.getAppelDoffres().getId();

				
				if (Utilities.notBlank(dto.getMessageClient())) {
					entityToSave.setMessageClient(dto.getMessageClient());
				}
				
				
				if (dto.getIsPayer() != null) {
					entityToSave.setIsPayer(dto.getIsPayer());
				}
				if (dto.getIsUpdate() != null) {
					entityToSave.setIsUpdate(dto.getIsUpdate());
				}
				if (dto.getIsNoterByClient() != null) {
					
					// TODO :  faire la moyenne du fournisseur en terme de note
					entityToSave.setIsNoterByClient(dto.getIsNoterByClient());
				}
				if (dto.getIsVisualiserNoteByFournisseur() != null) {
					entityToSave.setIsVisualiserNoteByFournisseur(dto.getIsVisualiserNoteByFournisseur());
				}
				if (dto.getIsVisualiserNotifByClient() != null) {
					entityToSave.setIsVisualiserNotifByClient(dto.getIsVisualiserNotifByClient());
				}
				if (dto.getIsVisualiserContenuByClient() != null) {
					entityToSave.setIsModifiable(false);
					entityToSave.setIsVisualiserContenuByClient(dto.getIsVisualiserContenuByClient());
					
					// mettre l'AO hors surbrillance si tous les offres ont ete vu
					List<Offre> offres = offreRepository.findByAppelDoffresIdAndIsVisualiserContenuByClient(entityToSave.getAppelDoffres().getId(), false, false) ;
					if (!Utilities.isNotEmpty(offres)) {
						entityToSave.getAppelDoffres().setIsContenuNotificationDispo(false);
					}
				}
				if (dto.getIsModifiable() != null) {
					entityToSave.setIsModifiable(dto.getIsModifiable());
				}
				if (dto.getIsRetirer() != null) {
					entityToSave.setIsRetirer(dto.getIsRetirer());
				}
				
				if (dto.getNoteOffre() != null && dto.getNoteOffre() > 0) {
					entityToSave.setNoteOffre(dto.getNoteOffre());
				}

				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Offre> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = offreRepository.save((Iterable<Offre>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("offre", locale));
					response.setHasError(true);
					return response;
				}
				
				List<OffreDto> itemsDto = new ArrayList<OffreDto>();
				for (Offre entity : itemsSaved) {
					OffreDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end updateCustom Offre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}else {
				// suppression des anciens fichiers quand le processus de update c'est bien dérouler
				slf4jLogger.info("listOfOldFiles ... " + listOfOldFiles);
				slf4jLogger.info("listOfOldFiles.toString() ... " + listOfOldFiles.toString());

				Utilities.deleteFileOnSever(listOfOldFiles, paramsUtils);
			}
		}
		return response;
	}

	
	/**
	 * choisirOffre Offre by using OffreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<OffreDto> choisirOffre(Request<OffreDto> request, Locale locale)  {
		slf4jLogger.info("----begin choisirOffre Offre-----");

		response = new Response<OffreDto>();

		try {
			
			response = update(request, locale);
			
			if(response.isHasError()){
				return response;
			}
			
			// notifier les autres fournisseurs que l'offre a été attribuer
			AppelDoffres appelDoffres = appelDoffresRepository.findById(response.getItems().get(0).getAppelDoffresId(), false);
			if (appelDoffres != null) {
				appelDoffres.setIsAttribuer(true);
			}
			appelDoffresRepository.save(appelDoffres);

			
			
			List<Offre> autresOffres = new ArrayList<Offre>(); 
			
			autresOffres = offreRepository.findByAppelDoffresIdAndIsSelectedByClient(response.getItems().get(0).getAppelDoffresId(), false, false);
			
			// generation du code du contrat
			List<Offre> offresRetenu = offreRepository.findByCodeContratNotNullAndisSelectedByClient(true, false);
			Integer nbreContrat = (Utilities.isNotEmpty(offresRetenu) ? offresRetenu.size() : 0);
			
			String codeContrat = Utilities.getCodeByCritreria(GlobalEnum.code_ct, nbreContrat) ;

			slf4jLogger.info("********codeContrat*********", codeContrat);
			
			OffreDto offreDto = request.getDatas().get(0);
			Offre offreConcluse = offreRepository.findById(offreDto.getId(), false);
			offreConcluse.setCodeContrat(codeContrat);
			offreRepository.save(offreConcluse);
			
			
			

			
			
			
			/* 
			en commentaire pour le moment
			
			// mail des autres fournisseurs 
			if (Utilities.isNotEmpty(autresOffres)) {
				for (Offre offre : autresOffres) {
					offre.setMessageClient(GlobalEnum.DEFAULT_MESSAGE_OFFRE_REFUSEE);
				}
				offreRepository.save((Iterable<Offre>) autresOffres);
				
				/// Envoi de mail
				
				if (Utilities.isNotEmpty(autresOffres)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user" ,  paramsUtils.getSmtpMailUser());

					// choisir la vraie url
					String appLink = "";
					//			if (entityToSave.getAdmin() != null ) {
					//				appLink = paramsUtils.getUrlRootAdmin();
					//			}else {
					//				appLink = paramsUtils.getUrlRootUser();
					//			}

					appLink = paramsUtils.getUrlRootUser();

					for (Offre offre : autresOffres) {
						
						User data = offre.getUser() ;
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", data.getEmail());
						recipient.put("user", data.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_CREATION_COMPTE;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateCreationCompteParUser();
						//		        	context.setVariable("email", user.getEmail());
						//					context.setVariable("login", user.getLogin());
						//					context.setVariable("token", user.getToken());
						//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable(EmailEnum.nomEntreprise, data.getNom());
						context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail

					}
				}
			}
			
			//*/
			
			
//			User utilisateur = userRepository.findById(request.getUser(), false);
//			if (utilisateur == null) {
//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
//				response.setHasError(true);
//				return response;
//			}
//			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
//				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
//				response.setHasError(true);
//				return response;
//			}
	
			// mail du fournisseur  
			if (offreConcluse != null ) {

				//set mail to user
				Map<String, String> from=new HashMap<>();
				from.put("email", paramsUtils.getSmtpMailUsername());
				from.put("user" ,  paramsUtils.getSmtpMailUser());

				// choisir la vraie url
				String appLink = "";
				//			if (entityToSave.getAdmin() != null ) {
				//				appLink = paramsUtils.getUrlRootAdmin();
				//			}else {
				//				appLink = paramsUtils.getUrlRootUser();
				//			}

				appLink = paramsUtils.getUrlRootUser();
				

				//recipients
				List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
				//		        	for (User user : itemsSaved) {
				Map<String, String> recipient = new HashMap<String, String>();
				recipient = new HashMap<String, String>();
				recipient.put("email", offreConcluse.getUser().getEmail());
				recipient.put("user", offreConcluse.getUser().getLogin());
				//		        	recipient.put("user", user.getNom());

				toRecipients.add(recipient); 
				//					}

				//subject
				// ajout du champ objet a la newsletter
				//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
				String subject = EmailEnum.SUBJECT_CONTRAT + codeContrat;
				//String body = EmailEnum.BODY_PASSWORD_RESET;
				String body = "";
				context = new Context();
				String templateChangerPassword = paramsUtils.getTemplateSelectionOffreParClient();
				//		        	context.setVariable("email", user.getEmail());
				//					context.setVariable("login", user.getLogin());
				//					context.setVariable("token", user.getToken());
				//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
				context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
				
				context.setVariable(EmailEnum.nomEntrepriseCliente, appelDoffres.getUser().getEntreprise().getNom());
				context.setVariable(EmailEnum.nomEntrepriseOffre, offreConcluse.getUser().getEntreprise().getNom());
				context.setVariable(EmailEnum.codeAppelDoffres, appelDoffres.getCode());
				context.setVariable(EmailEnum.codeOffre, offreConcluse.getCode());
				context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);

				context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
				Response<UserDto> responseEnvoiEmail = new Response<>();
				responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
				// si jamais une exception c'est produite pour un mail
			}
			
			/*
			
			// mail du client
			if (offreConcluse != null ) {

				//set mail to user
				Map<String, String> from=new HashMap<>();
				from.put("email", paramsUtils.getSmtpMailUsername());
				from.put("user" ,  paramsUtils.getSmtpMailUser());

				// choisir la vraie url
				String appLink = "";
				//			if (entityToSave.getAdmin() != null ) {
				//				appLink = paramsUtils.getUrlRootAdmin();
				//			}else {
				//				appLink = paramsUtils.getUrlRootUser();
				//			}

				appLink = paramsUtils.getUrlRootUser();


				//recipients
				List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
				//		        	for (User user : itemsSaved) {
				Map<String, String> recipient = new HashMap<String, String>();
				recipient = new HashMap<String, String>();
				recipient.put("email", offreConcluse.getUser().getEmail());
				recipient.put("user", offreConcluse.getUser().getLogin());
				//		        	recipient.put("user", user.getNom());

				toRecipients.add(recipient); 
				//					}

				//subject
				// ajout du champ objet a la newsletter
				//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
				String subject = EmailEnum.SUBJECT_CONTRAT + codeContrat;
				//String body = EmailEnum.BODY_PASSWORD_RESET;
				String body = "";
				context = new Context();
				String templateChangerPassword = paramsUtils.getTemplateCreationCompteParUser();
				//		        	context.setVariable("email", user.getEmail());
				//					context.setVariable("login", user.getLogin());
				//					context.setVariable("token", user.getToken());
				//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
				context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
				context.setVariable(EmailEnum.nomEntreprise, offreConcluse.getUser().getNom());
				context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
				context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
				Response<UserDto> responseEnvoiEmail = new Response<>();
				responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
				// si jamais une exception c'est produite pour un mail
			}
			
			//*/
			
			//List<Offre> items = new ArrayList<Offre>();

			response.setHasError(false);

			slf4jLogger.info("----end choisirOffre Offre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full OffreDto by using Offre as object.
	 *
	 * @param entity, locale
	 * @return OffreDto
	 *
	 */
	private OffreDto getFullInfos(Offre entity, Integer size, Locale locale) throws Exception {
		OffreDto dto = OffreTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}
		List<ValeurCritereOffre> datasValeurCritereOffre = new ArrayList<>();
		datasValeurCritereOffre = valeurCritereOffreRepository.findByOffreId(dto.getId(), false);
		if (Utilities.isNotEmpty(datasValeurCritereOffre)) {
			dto.setDatasValeurCritereOffre(ValeurCritereOffreTransformer.INSTANCE.toDtos(datasValeurCritereOffre));
		}

		// TODO : Renvoyr l'url et non le nom du fichier
		List<Fichier> datasFichier = new ArrayList<>();
		//datasFichier = fichierRepository.findByOffreId(dto.getId(), false);
		datasFichier = fichierRepository.findBySourceIdAndSource(dto.getId(), GlobalEnum.fichiers_of, false);

		if (Utilities.isNotEmpty(datasFichier)) {
			dto.setDatasFichier(FichierTransformer.INSTANCE.toDtos(datasFichier));
			for (FichierDto data : dto.getDatasFichier()) {
				if (Utilities.notBlank(data.getName())) {
					// Repertoire où je depose mon fichier
					String filesDirectory = Utilities.getSuitableFileDirectory(data.getExtension(), paramsUtils);
					Utilities.createDirectory(filesDirectory);
					if (!filesDirectory.endsWith("/")) {
						filesDirectory += "/";
					}
					String fileNameByExtension = Utilities.addExtensionInFileName( data.getName() , data.getExtension()) ;
					filesDirectory=filesDirectory + fileNameByExtension;
					//filesDirectory=filesDirectory+data.getName();
					//data.setFichierBase64(Utilities.convertFileToBase64(filesDirectory));
					
					//data.setUrlFichier(filesDirectory);
					// formatter le name du fichier
					data.setName(Utilities.getBasicNameFile(data.getName()));
					data.setUrlFichier(Utilities.getSuitableFileUrl(fileNameByExtension, paramsUtils));
					//data.setUrlFichier(Utilities.getSuitableFileUrl(data.getName(), paramsUtils));
					//data.setUrlFichier(Utilities.getSuitableFileUrlCustom(data.getName(), GlobalEnum.offres, paramsUtils));

				}
			}
		}
		

		return dto;
	}
}
