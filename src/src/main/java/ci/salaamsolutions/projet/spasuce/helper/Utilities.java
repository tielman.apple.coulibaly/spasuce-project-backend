
/*
 * Created on 2020-01-02 ( Time 18:00:14 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.LocaleUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONObject;

import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.dto.customize._Image;
import ci.salaamsolutions.projet.spasuce.helper.enums.GlobalEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.MonthLetterEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeCritereAppelDoffresEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeDocumentEnum;

/**
 * Utilities
 * 
 * @author SFL Back-End developper
 *
 */
public class Utilities {

	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static SecureRandom rnd = new SecureRandom();

	public static String encryptPasswordBy_(String login, String password) throws Exception {
		return encrypt(login + "_" + password);
	}

	public static String randomString(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}

	public static Date getCurrentDate() {
		return new Date();
	}

	/**
	 * Check if a String given is an Integer.
	 *
	 * @param s
	 * @return isValidInteger
	 *
	 */
	public static boolean isInteger(String s) {
		boolean isValidInteger = false;
		try {
			Integer.parseInt(s);

			// s is a valid integer
			isValidInteger = true;
		} catch (NumberFormatException ex) {
			// s is not an integer
		}

		return isValidInteger;
	}

	public static String generateCodeDe8Caracetre() {
		String formatted = null;
		formatted = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
		return formatted;
	}

	public static String generateCode() {
		String formatted = null;
		SecureRandom secureRandom = new SecureRandom();
		int num = secureRandom.nextInt(100000000);
		formatted = String.format("%05d", num);
		return formatted;
	}

	public static boolean isNumeric(String str) {
		try {
			double d = Long.parseLong(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	/**
	 * Check if a String given is an Boolean.
	 *
	 * @param i
	 * @return isBoolean
	 *
	 */
	public static boolean isBoolean(String str) {
		boolean isBoolean = false;
		if (str != null && (str.equalsIgnoreCase("true") || str.equalsIgnoreCase("false"))) {
			isBoolean = true;
		}
		return isBoolean;
	}

	/**
	 * Check if a Integer given is an String.
	 *
	 * @param i
	 * @return isValidString
	 *
	 */
	public static boolean isString(Integer i) {
		boolean isValidString = true;
		try {
			Integer.parseInt(i + "");

			// i is a valid integer

			isValidString = false;
		} catch (NumberFormatException ex) {
			// i is not an integer
		}

		return isValidString;
	}

	public static boolean isValidEmail(String email) {
		String regex = "^(.+)@(.+)$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);

		return matcher.matches();
	}

	public static String encrypt(String str) throws Exception {
		MessageDigest digest = MessageDigest.getInstance("SHA-1");
		byte[] hashedBytes = digest.digest(str.getBytes("UTF-8"));

		return convertByteArrayToHexString(hashedBytes);
	}

	public static boolean isDateValid(String date) {
		try {
			String simpleDateFormat = "dd/MM/yyyy";

			if (date.contains("-"))
				simpleDateFormat = "dd-MM-yyyy";
			else if (date.contains("/"))
				simpleDateFormat = "dd/MM/yyyy";
			else
				return false;

			DateFormat df = new SimpleDateFormat(simpleDateFormat);
			df.setLenient(false);
			df.parse(date);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public static boolean isDateTimeValid(String date) {
		try {
			String simpleDateFormat = "dd/MM/yyyy HH:mm:ss";

			if (date.contains("-"))
				simpleDateFormat = "dd-MM-yyyy HH:mm:ss";
			else if (date.contains("/"))
				simpleDateFormat = "dd/MM/yyyy HH:mm:ss";
			else
				return false;

			DateFormat df = new SimpleDateFormat(simpleDateFormat);
			df.setLenient(false);
			df.parse(date);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public static String GenerateValueKey(String code) {
		String result = null;
		// String prefix = prefixe;
		String suffix = null;
		String middle = null;
		String separator = "-";
		final String defaut = "000001";
		try {

			SimpleDateFormat dt = new SimpleDateFormat("yy-MM-dd-ss");
			String _date = dt.format(new Date());
			String[] spltter = _date.split(separator);
			middle = spltter[0] + spltter[1] + spltter[2] + spltter[3];
			if (code != null) {
				// Splitter le code pour recuperer les parties
				// String[] parts = code(separator);
				String part = code.substring(1);
				System.out.println("part" + part);

				if (part != null) {
					int cpt = new Integer(part);
					cpt++;

					String _nn = String.valueOf(cpt);

					switch (_nn.length()) {
						case 1:
							suffix = "00000" + _nn;
							break;
						case 2:
							suffix = "0000" + _nn;
							break;
						case 3:
							suffix = "000" + _nn;
							break;
						case 4:
							suffix = "00" + _nn;
							break;
						case 5:
							suffix = "0" + _nn;
							break;
						default:
							suffix = _nn;
							break;
					}
					// result = prefix + separator + middle + separator +
					// suffix;
					result = middle + separator + suffix;
				}
			} else {
				// result = prefix + separator + middle + separator + defaut;
				result = middle + separator + defaut;
			}
		} catch (Exception e) {

		}
		return result;
	}

	public static Integer getAge(Date dateNaissance) throws ParseException, Exception {
		Integer annee = 0;

		if (dateNaissance == null) {
			annee = 0;
		}
		Calendar birth = new GregorianCalendar();
		birth.setTime(dateNaissance);
		Calendar now = new GregorianCalendar();
		now.setTime(new Date());
		int adjust = 0;
		if (now.get(Calendar.DAY_OF_YEAR) - birth.get(Calendar.DAY_OF_YEAR) < 0) {
			adjust = -1;
		}
		annee = now.get(Calendar.YEAR) - birth.get(Calendar.YEAR) + adjust;
		return annee;
	}

	public static Boolean AvailableCode(String code) {
		if (code == null || code.isEmpty()) {
			return false;
		}
		Locale local = new Locale(code, "");
		return LocaleUtils.isAvailableLocale(local);

	}

	public static String normalizeFileName(String fileName) {
		String fileNormalize = null;
		fileNormalize = fileName.trim().replaceAll("\\s+", "_");
		fileNormalize = fileNormalize.replace("'", "");
		fileNormalize = Normalizer.normalize(fileNormalize, Normalizer.Form.NFD);
		fileNormalize = fileNormalize.replaceAll("[^\\p{ASCII}]", "");

		return fileNormalize;
	}

	private static String convertByteArrayToHexString(byte[] arrayBytes) {
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < arrayBytes.length; i++) {
			stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16).substring(1));
		}
		return stringBuffer.toString();
	}

	public static SimpleDateFormat findDateFormat(String date) {
		SimpleDateFormat simpleDateFormat = null;
		String regex_dd_MM_yyyy = "\\A0?(?:3[01]|[12][0-9]|[1-9])[/.-]0?(?:1[0-2]|[1-9])[/.-][0-9]{4}\\z";

		if (date.matches(regex_dd_MM_yyyy))
			if (date.contains("-"))
				simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			else if (date.contains("/"))
				simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

		return simpleDateFormat;
	}

	/**
	 * @return Permet de retourner la date courante du système
	 *
	 */
	public static String getCurrentLocalDateTimeStamp() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}

	/**
	 * @param l liste de vérification de doublons
	 * @return retourne le nombre de doublon trouvé
	 *
	 */
	public static int getDupCount(List<String> l) {
		int cnt = 0;
		HashSet<String> h = new HashSet<>(l);

		for (String token : h) {
			if (Collections.frequency(l, token) > 1)
				cnt++;
		}

		return cnt;
	}

	public static boolean saveImage(String base64String, String nomCompletImage, String extension) throws Exception {

		BufferedImage image = decodeToImage(base64String);

		if (image == null) {

			return false;

		}

		File f = new File(nomCompletImage);

		// write the image

		ImageIO.write(image, extension, f);

		return true;

	}

	public static boolean saveVideo(String base64String, String nomCompletVideo) throws Exception {

		try {

			byte[] decodedBytes = Base64.getDecoder().decode(base64String);
			File file2 = new File(nomCompletVideo);
			FileOutputStream os = new FileOutputStream(file2, true);
			os.write(decodedBytes);
			os.close();

		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}

		return true;

	}

	public static BufferedImage decodeToImage(String imageString) throws Exception {

		BufferedImage image = null;

		byte[] imageByte;

		imageByte = Base64.getDecoder().decode(imageString);

		try (ByteArrayInputStream bis = new ByteArrayInputStream(imageByte)) {

			image = ImageIO.read(bis);

		}

		return image;

	}

	public static String encodeToString(BufferedImage image, String type) {

		String imageString = null;

		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		try {

			ImageIO.write(image, type, bos);

			byte[] imageBytes = bos.toByteArray();

			imageString = new String(Base64.getEncoder().encode(imageBytes));

			bos.close();

		} catch (IOException e) {

			e.printStackTrace();

		}

		return imageString;

	}

	public static String convertFileToBase64(String pathFichier) {
		File originalFile = new File(pathFichier);
		String encodedBase64 = null;
		try {
			FileInputStream fileInputStreamReader = new FileInputStream(originalFile);
			byte[] bytes = new byte[(int) originalFile.length()];
			fileInputStreamReader.read(bytes);
			encodedBase64 = new String(Base64.getEncoder().encodeToString((bytes)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return encodedBase64;
	}

	public static String addExtensionInFileName(String fileName, String fileExtension) {
		return fileName + "." + fileExtension;
	}

	public static String getImageExtension(String str) {
		String extension = "";
		int i = str.lastIndexOf('.');
		if (i >= 0) {
			extension = str.substring(i + 1);
			return extension;
		}
		return null;
	}

	public static boolean fileIsImage(String image) {

		String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp|jpeg))$)";
		Pattern pattern = Pattern.compile(IMAGE_PATTERN);
		Matcher matcher = pattern.matcher(image);

		return matcher.matches();

	}

	public static boolean fileIsVideo(String video) {

		String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(mp4|avi|camv|dvx|mpeg|mpg|wmv|3gp|mkv))$)";
		Pattern pattern = Pattern.compile(IMAGE_PATTERN);
		Matcher matcher = pattern.matcher(video);

		return matcher.matches();

	}

	public static void createDirectory(String chemin) {
		File file = new File(chemin);
		if (!file.exists()) {
			try {
				FileUtils.forceMkdir(file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static void deleteFolder(String chemin) {
		File file = new File(chemin);
		try {
			if (file.exists() && file.isDirectory()) {
				FileUtils.forceDelete(new File(chemin));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void deleteFile(String chemin) {
		File file = new File(chemin);
		try {
			if (file.exists() && file.getName() != null && !file.getName().isEmpty()) {

				FileUtils.forceDelete(new File(chemin));

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean notBlank(String str) {
		return str != null && !str.isEmpty() && !str.equals("\n");
	}

	public static boolean notEmpty(List<String> lst) {
		return lst != null && !lst.isEmpty() && lst.stream().noneMatch(s -> s.equals("\n"))
				&& lst.stream().noneMatch(s -> s.equals(null));
	}

	public static <T> boolean isNotEmpty(List<T> list) {
		return (list != null && !list.isEmpty());
	}

	static public String GetCode(String Value, Map<String, String> Table) {

		for (Entry<String, String> entry : Table.entrySet()) {
			if (entry.getValue().equals(Value)) {
				return entry.getKey();
			}
		}
		return Value;
	}

	public static boolean anObjectFieldsMapAllFieldsToVerify(List<Object> objets, Map<String, Object> fieldsToVerify) {
		for (Object objet : objets) {
			boolean oneObjectMapAllFields = true;
			JSONObject jsonObject = new JSONObject(objet);
			for (Map.Entry<String, Object> entry : fieldsToVerify.entrySet()) {
				// slf4jLogger.info("jsonObject " +jsonObject);
				String key = entry.getKey();
				Object value = entry.getValue();
				try {
					if (!jsonObject.get(key).equals(value)) {
						oneObjectMapAllFields = false;
						break;
					}
				} catch (Exception e) {
					oneObjectMapAllFields = false;
					break;
				}
			}
			if (oneObjectMapAllFields)
				return true;
		}

		return false;
	}

	public static String generateAlphanumericCode(Integer nbreCaractere) {
		String formatted = null;
		formatted = RandomStringUtils.randomAlphanumeric(nbreCaractere).toUpperCase();
		return formatted;
	}

	public static Boolean verifierEmail(String email) {
		Pattern emailPattern = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher emailMatcher = emailPattern.matcher(email);
		return emailMatcher.matches();
	}

	// add by tielman

	public static boolean fileIsTexteDocument(String textDocument) {

		String TEXT_DOCUMENT_PATTERN = "([^\\s]+(\\.(?i)(doc|docx|txt|odt|ods|pdf|xls|xlsx))$)";
		Pattern pattern = Pattern.compile(TEXT_DOCUMENT_PATTERN);
		Matcher matcher = pattern.matcher(textDocument);
		return matcher.matches();
	}

	public static void deleteFileOnSever(List<String> fileNameList, ParamsUtils paramsUtils) {
		if (fileNameList != null && !fileNameList.isEmpty()) {
			for (String fileName : fileNameList) {
				// Repertoire où se trouve le fichier
				if (fileName != null && fileName.contains(".")) {
					String file[] = fileName.split("\\.");
					if (file.length > 1) {
						String fileExtension = file[file.length - 1];
						String fullFileName = file[0];
						for (int k = 1; k < file.length - 1; k++) {
							fullFileName += "." + file[k];
						}
						file = fullFileName.split("/");
						fileName = file[file.length - 1];

						String filesDirectory = getSuitableFileDirectory(fileExtension, paramsUtils);
						deleteFile(filesDirectory + "/" + fileName);
					}
				}
			}
		}
	}

	public static String getSuitableFileDirectory(String fileExtension, ParamsUtils paramsUtils) {
		String suitableFileDirectory = null;
		if (fileIsImage("file." + fileExtension)) {
			suitableFileDirectory = paramsUtils.getGalerieImageFilesDirectory();
		} else {
			if (fileIsTexteDocument("file." + fileExtension)) {
				suitableFileDirectory = paramsUtils.getGalerieTextFilesDirectory();
			} else {
				if (fileIsVideo("file." + fileExtension)) {
					suitableFileDirectory = paramsUtils.getGalerieVideoFilesDirectory();
				}
			}
		}
		if (suitableFileDirectory == null) {
			suitableFileDirectory = paramsUtils.getOtherFilesDirectory();
		}
		return String.format("%s%s", paramsUtils.getRootPathFiles(), suitableFileDirectory);
	}

	public static String getSuitableFileDirectoryCustom(String sourceFile, String fileExtension,
			ParamsUtils paramsUtils) {
		String suitableFileDirectory = null;
		if (fileIsImage("file." + fileExtension)) {
			suitableFileDirectory = paramsUtils.getGalerieImageFilesDirectory();
		} else {
			if (fileIsTexteDocument("file." + fileExtension)) {
				suitableFileDirectory = paramsUtils.getGalerieTextFilesDirectory();
			} else {
				if (fileIsVideo("file." + fileExtension)) {
					suitableFileDirectory = paramsUtils.getGalerieVideoFilesDirectory();
				}
			}
		}
		if (suitableFileDirectory == null) {
			suitableFileDirectory = paramsUtils.getOtherFilesDirectory();
		}
		switch (sourceFile) {
			case GlobalEnum.appel_doffres:
				suitableFileDirectory = paramsUtils.getGalerieAppelsDoffres() + suitableFileDirectory;

				break;
			case GlobalEnum.offres:
				suitableFileDirectory = paramsUtils.getGalerieOffres() + suitableFileDirectory;

				break;
			case GlobalEnum.profils:
				suitableFileDirectory = paramsUtils.getGalerieProfils() + suitableFileDirectory;

				break;
			case GlobalEnum.autres:
			default:

				suitableFileDirectory = paramsUtils.getGalerieAutres() + suitableFileDirectory;

				break;
		}

		return String.format("%s%s", paramsUtils.getRootPathFiles(), suitableFileDirectory);
	}

	public static boolean isValidateIvorianPhoneNumber(String phoneNumber) {
		String regex = "^(\\(\\+225\\)|\\+225|\\(00225\\)|00225)?(\\s)?[0-9]{2}([ .-]?[0-9]{2}){3}$";
		return (phoneNumber != null && phoneNumber.matches(regex));
	}

	public static String ivorianPhoneNumberToStandardFormat(String phoneNumber) {
		String beginRegex = "^(\\(\\+225\\)|\\+225|\\(00225\\)|00225)?";
		String specialCharRegex = "[ .-]?";
		String simplePhoneNumber;

		if (phoneNumber == null)
			return null;

		simplePhoneNumber = phoneNumber.replaceAll(beginRegex, "");
		return simplePhoneNumber.replaceAll(specialCharRegex, "");
	}

	public static String saveFile(_Image dataImage, ParamsUtils paramsUtils) throws IOException, Exception {
		String filePath = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss_SSSSS");
		String fileName = null;
		if (dataImage != null) {
			String fileDirectory = null;
			if (notBlank(dataImage.getFileName()) && notBlank(dataImage.getFileBase64())
					&& notBlank(dataImage.getExtension())) {
				fileName = dataImage.getFileName();
				if (fileName.contains("/")) {
					String[] fileNames = dataImage.getFileName().split("/");
					fileName = fileNames[fileNames.length - 1];
				}
				fileName = normalizeFileName(fileName) + "_" + sdf.format(new Date()) + "." + dataImage.getExtension();

				// S'assurer que l'extension est bonne
				if (!fileIsImage(fileName) && !fileIsTexteDocument(fileName) && !fileIsVideo(fileName)) {
					System.out.println(
							"\n\nL'extension '" + dataImage.getExtension() + "' n'est pas prise en compte !\n\n");
					return null;
				}

				// Repertoire où je depose mon fichier
				String filesDirectory = getSuitableFileDirectory(dataImage.getExtension(), paramsUtils);
				createDirectory(filesDirectory);
				if (!filesDirectory.endsWith("/")) {
					filesDirectory += "/";
				}
				fileDirectory = filesDirectory + fileName;

				// Enregistrement du fichier
				boolean succes = false;
				succes = Utilities.saveVideo(dataImage.getFileBase64(), fileDirectory);

				// if(fileIsImage(fileName)) {
				// succes = Utilities.saveImage(dataImage.getFileBase64(), fileDirectory,
				// dataImage.getExtension());
				// }
				// else {
				// if(fileIsTexteDocument(fileName)) {
				// BufferedOutputStream stream = new BufferedOutputStream(new
				// FileOutputStream(new File(fileDirectory)));
				// stream.write(base64ToByte(dataImage.getFileBase64()));
				// stream.close();
				// }
				// else {
				// if(fileIsVideo(fileName)) {
				// succes = Utilities.saveVideo(dataImage.getFileBase64(), fileDirectory);
				// }
				// }
				// }

				if (!succes) {
					System.out.println("\n\nEchec de l'enregistrement du fichier '" + fileDirectory + "' !\n\n");
					return null;
				}
				filePath = fileDirectory;
			}
		}

		return fileName;
		// return filePath;
	}

	public static String saveFileCustomx(_Image dataImage, String sourceFichier, ParamsUtils paramsUtils)
			throws IOException, Exception {
		String filePath = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss_SSSSS");
		String fileName = null;
		if (dataImage != null) {
			String fileDirectory = null;
			if (notBlank(dataImage.getFileName()) && notBlank(dataImage.getFileBase64())
					&& notBlank(dataImage.getExtension())) {
				fileName = dataImage.getFileName();
				if (fileName.contains("/")) {
					String[] fileNames = dataImage.getFileName().split("/");
					fileName = fileNames[fileNames.length - 1];
				}
				fileName = normalizeFileName(fileName) + "_" + sdf.format(new Date()) + "." + dataImage.getExtension();

				// S'assurer que l'extension est bonne
				if (!fileIsImage(fileName) && !fileIsTexteDocument(fileName) && !fileIsVideo(fileName)) {
					System.out.println(
							"\n\nL'extension '" + dataImage.getExtension() + "' n'est pas prise en compte !\n\n");
					return null;
				}

				// Repertoire où je depose mon fichier
				// String filesDirectory = getSuitableFileDirectory(dataImage.getExtension(),
				// paramsUtils);
				String filesDirectory = getSuitableFileDirectoryCustom(sourceFichier, dataImage.getExtension(),
						paramsUtils);
				createDirectory(filesDirectory);
				if (!filesDirectory.endsWith("/")) {
					filesDirectory += "/";
				}
				fileDirectory = filesDirectory + fileName;

				// Enregistrement du fichier
				boolean succes = false;
				succes = Utilities.saveVideo(dataImage.getFileBase64(), fileDirectory);

				// if(fileIsImage(fileName)) {
				// succes = Utilities.saveImage(dataImage.getFileBase64(), fileDirectory,
				// dataImage.getExtension());
				// }
				// else {
				// if(fileIsTexteDocument(fileName)) {
				// BufferedOutputStream stream = new BufferedOutputStream(new
				// FileOutputStream(new File(fileDirectory)));
				// stream.write(base64ToByte(dataImage.getFileBase64()));
				// stream.close();
				// }
				// else {
				// if(fileIsVideo(fileName)) {
				// succes = Utilities.saveVideo(dataImage.getFileBase64(), fileDirectory);
				// }
				// }
				// }

				if (!succes) {
					System.out.println("\n\nEchec de l'enregistrement du fichier '" + fileDirectory + "' !\n\n");
					return null;
				}
				filePath = fileDirectory;
			}
		}

		return fileName;
		// return filePath;
	}

	public static String getSuitableFileUrl(String fileName, ParamsUtils paramsUtils) {
		String suitableFileDirectory = null;
		String file[] = fileName.split("\\.");
		int i = 0, j = 1;
		if (file.length > 0) {
			String fileExtension = (file.length > 2) ? file[(file.length - 1)] : file[j];
			if (fileIsImage("file." + fileExtension)) {
				suitableFileDirectory = paramsUtils.getGalerieImageFilesDirectory();
			} else {
				if (fileIsTexteDocument("file." + fileExtension)) {
					suitableFileDirectory = paramsUtils.getGalerieTextFilesDirectory();
				} else {
					if (fileIsVideo("file." + fileExtension)) {
						suitableFileDirectory = paramsUtils.getGalerieVideoFilesDirectory();
					}
				}
			}
		}
		if (suitableFileDirectory == null) {
			suitableFileDirectory = paramsUtils.getOtherFilesDirectory();
		}
		return String.format("%s%s%s", paramsUtils.getRootUrlFiles(), suitableFileDirectory, fileName);
	}

	public static String getSuitableFileUrlCustomx(String fileName, String sourceFichier, ParamsUtils paramsUtils) {
		String suitableFileDirectory = null;
		String file[] = fileName.split("\\.");
		int i = 0, j = 1;
		if (file.length > 0) {
			String fileExtension = (file.length > 2) ? file[(file.length - 1)] : file[j];
			if (fileIsImage("file." + fileExtension)) {
				suitableFileDirectory = paramsUtils.getGalerieImageFilesDirectory();
			} else {
				if (fileIsTexteDocument("file." + fileExtension)) {
					suitableFileDirectory = paramsUtils.getGalerieTextFilesDirectory();
				} else {
					if (fileIsVideo("file." + fileExtension)) {
						suitableFileDirectory = paramsUtils.getGalerieVideoFilesDirectory();
					}
				}
			}
		}
		if (suitableFileDirectory == null) {
			suitableFileDirectory = paramsUtils.getOtherFilesDirectory();
		}
		switch (sourceFichier) {
			case GlobalEnum.appel_doffres:
				suitableFileDirectory = paramsUtils.getGalerieAppelsDoffres() + suitableFileDirectory;

				break;
			case GlobalEnum.offres:
				suitableFileDirectory = paramsUtils.getGalerieOffres() + suitableFileDirectory;

				break;
			case GlobalEnum.profils:
				suitableFileDirectory = paramsUtils.getGalerieProfils() + suitableFileDirectory;

				break;
			case GlobalEnum.autres:
			default:

				suitableFileDirectory = paramsUtils.getGalerieAutres() + suitableFileDirectory;

				break;
		}
		return String.format("%s%s%s", paramsUtils.getRootUrlFiles(), suitableFileDirectory, fileName);
	}

	public static String getSuitableFileType(String fileExtension, ParamsUtils paramsUtils) {
		String suitableFileDirectory = null;
		if (fileExtension.contains(".")) {
			String file[] = fileExtension.split("\\.");
			int i = 0, j = 1;
			if (file.length > 0 && j < file.length) {
				fileExtension = (file.length > 2) ? file[(file.length - 1)] : file[j];
				// fileExtension=file[j];
			}
		}
		if (fileIsImage("file." + fileExtension)) {
			suitableFileDirectory = TypeDocumentEnum.IMAGE;
		} else {
			if (fileIsTexteDocument("file." + fileExtension)) {
				suitableFileDirectory = TypeDocumentEnum.DOC_TEXT;
			} else {
				if (fileIsVideo("file." + fileExtension)) {
					suitableFileDirectory = TypeDocumentEnum.VIDEO;
				}
			}
		}
		if (suitableFileDirectory == null) {
			suitableFileDirectory = TypeDocumentEnum.OTHER;
		}
		return suitableFileDirectory;
	}

	public static <T> Response<T> verifierSiContenuValeurCorrespondAuType(Response<T> response, String valeur,
			String type, Locale locale, FunctionalError functionalError) {

		if (notBlank(type) && notBlank(valeur)) {
			switch (type) {
				case TypeCritereAppelDoffresEnum.DATE:
					if (!Utilities.isDateValid(valeur)) {
						response.setStatus(functionalError
								.INVALID_DATA("la valeur -> " + valeur + " saisie n'est pas une date", locale));
						response.setHasError(true);
						return response;
					}
					break;

				case TypeCritereAppelDoffresEnum.DATETIME:

					if (!Utilities.isDateTimeValid(valeur)) {
						response.setStatus(functionalError.INVALID_DATA(
								"la valeur -> " + valeur + " saisie n'est pas une date avec heure", locale));
						response.setHasError(true);
						return response;
					}
					break;

				case TypeCritereAppelDoffresEnum.BOOLEAN:
					if (!Utilities.isBoolean(valeur)) {
						response.setStatus(functionalError
								.INVALID_DATA("la valeur -> " + valeur + " saisie n'est pas correcte", locale));
						response.setHasError(true);
						return response;
					}
					break;

				case TypeCritereAppelDoffresEnum.ENTIER:

					if (!Utilities.isInteger(valeur)) {
						response.setStatus(functionalError
								.INVALID_DATA("la valeur -> " + valeur + " saisie n'est pas un entier", locale));
						response.setHasError(true);
						return response;
					}
					break;
				case TypeCritereAppelDoffresEnum.NUMBER:

					if (!Utilities.isInteger(valeur)) {
						response.setStatus(functionalError
								.INVALID_DATA("la valeur -> " + valeur + " saisie n'est pas un entier", locale));
						response.setHasError(true);
						return response;
					}
					break;

				case TypeCritereAppelDoffresEnum.STRING:
					// rien a faire ici
					break;

				case TypeCritereAppelDoffresEnum.NOMBRE:
					if (!Utilities.isNumeric(valeur)) {
						response.setStatus(functionalError
								.INVALID_DATA("la valeur -> " + valeur + " saisie n'est pas nombre", locale));
						response.setHasError(true);
						return response;
					}
					break;

				default:
					response.setStatus(
							functionalError.DATA_NOT_EXIST("le type -> " + type + " n'existe pas !!!", locale));
					response.setHasError(true);
					return response;
			}
		} else {
			response.setStatus(functionalError.FIELD_EMPTY("type et/ou valeur", locale));
			response.setHasError(true);
		}
		return response;
	}

	public static String cleanHtmlFrom(String str) {
		String strRegEx = "<[^>]*>";

		str = str.replaceAll(strRegEx, "");
		// replace &nbsp; with space
		str = str.replace("&nbsp;", " ");
		// replace &amp; with &
		str = str.replace("&amp;", "&");
		// OR remove all HTML entities
		str = str.replaceAll("&.*?;", "");

		return str;
	}

	public static String truncateEncoding(String value, int length) throws UnsupportedEncodingException {
		// Ensure String length is longer than requested size.
		value = new String(value.getBytes(), Charset.forName("UTF-8"));
		value = StringEscapeUtils.unescapeHtml(value);
		if (value.length() > length) {
			return value.substring(0, length) + "...";
		} else {
			return value;
		}
	}

	public static String truncate(String value, int length) throws UnsupportedEncodingException {
		// Ensure String length is longer than requested size.
		if (value.length() > length) {
			return value.substring(0, length) + "...";
		} else {
			return value;
		}
	}

	public static String getCodeByCritreria(String codeStatic, Integer nbre) {

		Calendar calendar = new GregorianCalendar();

		String code = "";
		NumberFormat formatter = new DecimalFormat(GlobalEnum.nbreChiffre);

		NumberFormat formatterDeux = new DecimalFormat(GlobalEnum.nbreChiffreDeux);

		code = "" + calendar.get(Calendar.YEAR) + formatterDeux.format((calendar.get(Calendar.MONTH) + 1))
				+ formatterDeux.format(calendar.get(Calendar.DAY_OF_MONTH)) + codeStatic + formatter.format((nbre + 1));

		return code;
	}

	public static String getLibelleMonthByMonthId(int monthInInt) {

		String libelleMonth = null;
		switch (monthInInt) {
			case 0:
				libelleMonth = MonthLetterEnum.JANV;
				break;

			case 1:
				libelleMonth = MonthLetterEnum.FEV;
				break;

			case 2:
				libelleMonth = MonthLetterEnum.MARS;
				break;

			case 3:
				libelleMonth = MonthLetterEnum.AVRIL;
				break;

			case 4:
				libelleMonth = MonthLetterEnum.MAI;
				break;

			case 5:
				libelleMonth = MonthLetterEnum.JUIN;
				break;

			case 6:
				libelleMonth = MonthLetterEnum.JUIL;
				break;

			case 7:
				libelleMonth = MonthLetterEnum.AOUT;
				break;

			case 8:
				libelleMonth = MonthLetterEnum.SEPT;
				break;

			case 9:
				libelleMonth = MonthLetterEnum.OCT;
				break;

			case 10:
				libelleMonth = MonthLetterEnum.NOV;
				break;

			case 11:
				libelleMonth = MonthLetterEnum.DEC;
				break;

			default:
				break;
		}

		return libelleMonth;
	}

	public static int getMonthNumber(String month) {

		int monthNumber = 0;

		if (month == null) {
			return monthNumber;
		}
		switch (month.toUpperCase()) {
			case MonthLetterEnum.JANVIER:
				monthNumber = Calendar.JANUARY;
				break;
			case MonthLetterEnum.FEVRIER:
				monthNumber = Calendar.FEBRUARY;
				break;
			case MonthLetterEnum.MARS:
				monthNumber = Calendar.MARCH;
				break;
			case MonthLetterEnum.AVRIL:
				monthNumber = Calendar.APRIL;
				break;
			case MonthLetterEnum.MAI:
				monthNumber = Calendar.MAY;
				break;
			case MonthLetterEnum.JUIN:
				monthNumber = Calendar.JUNE;
				break;
			case MonthLetterEnum.JUILLET:
				monthNumber = Calendar.JULY;
				break;
			case MonthLetterEnum.AOUT:
				monthNumber = Calendar.AUGUST;
				break;
			case MonthLetterEnum.SEPTEMBRE:
				monthNumber = Calendar.SEPTEMBER;
				break;
			case MonthLetterEnum.OCTOBRE:
				monthNumber = Calendar.OCTOBER;
				break;
			case MonthLetterEnum.NOVEMBRE:
				monthNumber = Calendar.NOVEMBER;
				break;
			case MonthLetterEnum.DECEMBRE:
				monthNumber = Calendar.DECEMBER;
				break;
			default:
				monthNumber = 0;
				break;
		}

		return monthNumber;
	}

	public static String getBasicNameFile(String fullName) {
		if (notBlank(fullName)) {
			fullName = fullName.substring(0, fullName.length() - "_dd_MM_yyyy_HH_mm_ss_SSSSS".length());
		}
		return fullName;
	}

}
