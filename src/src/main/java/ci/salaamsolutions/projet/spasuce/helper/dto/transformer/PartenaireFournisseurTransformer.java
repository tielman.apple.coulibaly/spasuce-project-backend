

/*
 * Java transformer for entity table partenaire_fournisseur 
 * Created on 2020-01-02 ( Time 18:00:01 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "partenaire_fournisseur"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface PartenaireFournisseurTransformer {

	PartenaireFournisseurTransformer INSTANCE = Mappers.getMapper(PartenaireFournisseurTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.entreprise.id", target="entrepriseId"),
				@Mapping(source="entity.entreprise.nom", target="entrepriseNom"),
				@Mapping(source="entity.entreprise.login", target="entrepriseLogin"),
	})
	PartenaireFournisseurDto toDto(PartenaireFournisseur entity) throws ParseException;

    List<PartenaireFournisseurDto> toDtos(List<PartenaireFournisseur> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.urlLogo", target="urlLogo"),
		@Mapping(source="dto.extension", target="extension"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="entreprise", target="entreprise"),
	})
    PartenaireFournisseur toEntity(PartenaireFournisseurDto dto, Entreprise entreprise) throws ParseException;

    //List<PartenaireFournisseur> toEntities(List<PartenaireFournisseurDto> dtos) throws ParseException;

}
