


/*
 * Java transformer for entity table ville
 * Created on 2020-01-17 ( Time 17:33:13 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "ville"
 *
 * @author Back-End developper
 *
 */
@Component
public class VilleBusiness implements IBasicBusiness<Request<VilleDto>, Response<VilleDto>> {

	private Response<VilleDto> response;
	@Autowired
	private VilleRepository villeRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AppelDoffresRepository appelDoffresRepository;
	@Autowired
	private PaysRepository paysRepository;
	@Autowired
	private EntrepriseRepository entrepriseRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private AppelDoffresBusiness appelDoffresBusiness;


	@Autowired
	private EntrepriseBusiness entrepriseBusiness;



	public VilleBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create Ville by using VilleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<VilleDto> create(Request<VilleDto> request, Locale locale)  {
		slf4jLogger.info("----begin create Ville-----");

		response = new Response<VilleDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Ville> items = new ArrayList<Ville>();

			for (VilleDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("code", dto.getCode());
				//fieldsToVerify.put("libelle", dto.getLibelle());
				fieldsToVerify.put("datasVille", dto.getDatasVille());
				//fieldsToVerify.put("paysId", dto.getPaysId());
				fieldsToVerify.put("paysLibelle", dto.getPaysLibelle());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if ville to insert do not exist
				Ville existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("ville -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
//				existingEntity = villeRepository.findByCode(dto.getCode(), false);
//				if (existingEntity != null) {
//					response.setStatus(functionalError.DATA_EXIST("ville -> " + dto.getCode(), locale));
//					response.setHasError(true);
//					return response;
//				}
//				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
//					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les villes", locale));
//					response.setHasError(true);
//					return response;
//				}
//				existingEntity = villeRepository.findByLibelle(dto.getLibelle(), false);
//				if (existingEntity != null) {
//					response.setStatus(functionalError.DATA_EXIST("ville -> " + dto.getLibelle(), locale));
//					response.setHasError(true);
//					return response;
//				}
//				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
//					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les villes", locale));
//					response.setHasError(true);
//					return response;
//				}

				// Verify if pays exist
				Pays existingPays = paysRepository.findByLibelle(dto.getPaysLibelle(), false);
				if (existingPays == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pays -> " + dto.getPaysLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				
				//*
				for (VilleDto data : dto.getDatasVille()) {

					if (Utilities.notBlank(data.getCode())) {
						existingEntity = villeRepository.findByCodeAndPaysId(data.getCode(), dto.getPaysId(), false);
						if (existingEntity != null) {
							response.setStatus(functionalError.DATA_EXIST("pays -> " + data.getCode(), locale));
							response.setHasError(true);
							return response;
						}
						if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(data.getCode()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + data.getCode()+"' pour les pays", locale));
							response.setHasError(true);
							return response;
						}
					}
					existingEntity = villeRepository.findByLibelleAndPaysId(data.getLibelle(), dto.getPaysId(),false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("pays -> " + data.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					//slf4jLogger.info("avanerreur items :::" + items + "/"+items.size());

					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(data.getLibelle()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + data.getLibelle()+"' pour les pays", locale));
						response.setHasError(true);
						return response;
					}
					//slf4jLogger.info("erreur items :::" + items + "/"+items.size());


					Ville entityToSave = null;
					entityToSave = VilleTransformer.INSTANCE.toEntity(data, existingPays);
					entityToSave.setCreatedAt(Utilities.getCurrentDate());
					entityToSave.setCreatedBy(request.getUser());
					entityToSave.setIsDeleted(false);
					items.add(entityToSave);
				}
				//*/
				
				
				
				
				/*
				if (Utilities.notBlank(dto.getCode())) {
					existingEntity = villeRepository.findByCodeAndPaysId(dto.getCode(), dto.getPaysId(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("pays -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les pays", locale));
						response.setHasError(true);
						return response;
					}
				}
				existingEntity = villeRepository.findByLibelleAndPaysId(dto.getLibelle(), dto.getPaysId(),false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("pays -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				//slf4jLogger.info("avanerreur items :::" + items + "/"+items.size());

				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les pays", locale));
					response.setHasError(true);
					return response;
				}
				//slf4jLogger.info("erreur items :::" + items + "/"+items.size());


				Ville entityToSave = null;
				entityToSave = VilleTransformer.INSTANCE.toEntity(dto, existingPays);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
				
				//*/
			}

			if (!items.isEmpty()) {
				List<Ville> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = villeRepository.save((Iterable<Ville>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("ville", locale));
					response.setHasError(true);
					return response;
				}
				List<VilleDto> itemsDto = new ArrayList<VilleDto>();
				for (Ville entity : itemsSaved) {
					VilleDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create Ville-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Ville by using VilleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<VilleDto> update(Request<VilleDto> request, Locale locale)  {
		slf4jLogger.info("----begin update Ville-----");

		response = new Response<VilleDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Ville> items = new ArrayList<Ville>();

			for (VilleDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la ville existe
				Ville entityToSave = null;
				entityToSave = villeRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ville -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				Integer entityToSaveId = entityToSave.getId();

				// Verify if pays exist
				if (Utilities.notBlank(dto.getPaysLibelle())){
					//if (dto.getPaysId() != null && dto.getPaysId() > 0){
					//Pays existingPays = paysRepository.findById(dto.getPaysId(), false);
					Pays existingPays = paysRepository.findByLibelle(dto.getPaysLibelle(), false);

					if (existingPays == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("pays -> " + dto.getPaysLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setPays(existingPays);
				}
				
				Integer paysId = entityToSave.getPays().getId();

				if (Utilities.notBlank(dto.getCode())) {
					Ville existingEntity = villeRepository.findByCodeAndPaysId(dto.getCode(), paysId, false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("ville -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les villes", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCode(dto.getCode());
				}
				if (Utilities.notBlank(dto.getLibelle())) {
					Ville existingEntity = villeRepository.findByLibelleAndPaysId(dto.getLibelle(), paysId, false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("ville -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les villes", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
//				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
//					entityToSave.setCreatedBy(dto.getCreatedBy());
//				}
//				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
//					entityToSave.setUpdatedBy(dto.getUpdatedBy());
//				}
//				if (Utilities.notBlank(dto.getDeletedAt())) {
//					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
//				}
//				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
//					entityToSave.setDeletedBy(dto.getDeletedBy());
//				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Ville> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = villeRepository.save((Iterable<Ville>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("ville", locale));
					response.setHasError(true);
					return response;
				}
				List<VilleDto> itemsDto = new ArrayList<VilleDto>();
				for (Ville entity : itemsSaved) {
					VilleDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update Ville-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete Ville by using VilleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<VilleDto> delete(Request<VilleDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete Ville-----");

		response = new Response<VilleDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Ville> items = new ArrayList<Ville>();

			for (VilleDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la ville existe
				Ville existingEntity = null;
				existingEntity = villeRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ville -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// appelDoffres
				List<AppelDoffres> listOfAppelDoffres = appelDoffresRepository.findByVilleId(existingEntity.getId(), false);
				if (listOfAppelDoffres != null && !listOfAppelDoffres.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfAppelDoffres.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// entreprise
				List<Entreprise> listOfEntreprise = entrepriseRepository.findByVilleId(existingEntity.getId(), false);
				if (listOfEntreprise != null && !listOfEntreprise.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfEntreprise.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				villeRepository.save((Iterable<Ville>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete Ville-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete Ville by using VilleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<VilleDto> forceDelete(Request<VilleDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete Ville-----");

		response = new Response<VilleDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Ville> items = new ArrayList<Ville>();

			for (VilleDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la ville existe
				Ville existingEntity = null;
				existingEntity = villeRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ville -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// appelDoffres
				List<AppelDoffres> listOfAppelDoffres = appelDoffresRepository.findByVilleId(existingEntity.getId(), false);
				if (listOfAppelDoffres != null && !listOfAppelDoffres.isEmpty()){
					Request<AppelDoffresDto> deleteRequest = new Request<AppelDoffresDto>();
					deleteRequest.setDatas(AppelDoffresTransformer.INSTANCE.toDtos(listOfAppelDoffres));
					deleteRequest.setUser(request.getUser());
					Response<AppelDoffresDto> deleteResponse = appelDoffresBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// entreprise
				List<Entreprise> listOfEntreprise = entrepriseRepository.findByVilleId(existingEntity.getId(), false);
				if (listOfEntreprise != null && !listOfEntreprise.isEmpty()){
					Request<EntrepriseDto> deleteRequest = new Request<EntrepriseDto>();
					deleteRequest.setDatas(EntrepriseTransformer.INSTANCE.toDtos(listOfEntreprise));
					deleteRequest.setUser(request.getUser());
					Response<EntrepriseDto> deleteResponse = entrepriseBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				villeRepository.save((Iterable<Ville>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete Ville-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get Ville by using VilleDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<VilleDto> getByCriteria(Request<VilleDto> request, Locale locale) {
		slf4jLogger.info("----begin get Ville-----");

		response = new Response<VilleDto>();

		try {
			List<Ville> items = null;
			items = villeRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<VilleDto> itemsDto = new ArrayList<VilleDto>();
				for (Ville entity : items) {
					VilleDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(villeRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("ville", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get Ville-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full VilleDto by using Ville as object.
	 *
	 * @param entity, locale
	 * @return VilleDto
	 *
	 */
	private VilleDto getFullInfos(Ville entity, Integer size, Locale locale) throws Exception {
		VilleDto dto = VilleTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
