


/*
 * Java transformer for entity table appel_doffres_restreint
 * Created on 2020-01-03 ( Time 08:37:04 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeUserEnum;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "appel_doffres_restreint"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class AppelDoffresRestreintBusiness implements IBasicBusiness<Request<AppelDoffresRestreintDto>, Response<AppelDoffresRestreintDto>> {

	private Response<AppelDoffresRestreintDto> response;
	@Autowired
	private AppelDoffresRestreintRepository appelDoffresRestreintRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AppelDoffresRepository appelDoffresRepository;
	@Autowired
	private OffreRepository offreRepository;

	
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public AppelDoffresRestreintBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create AppelDoffresRestreint by using AppelDoffresRestreintDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<AppelDoffresRestreintDto> create(Request<AppelDoffresRestreintDto> request, Locale locale)  {
		slf4jLogger.info("----begin create AppelDoffresRestreint-----");

		response = new Response<AppelDoffresRestreintDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			// type de user autorisé
			if (utilisateur.getEntreprise().getTypeEntreprise().getCode().equals(TypeUserEnum.ENTREPRISE_FOURNISSEUR)) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un client" , locale));
				response.setHasError(true);
				return response;
			}


			List<AppelDoffresRestreint> items = new ArrayList<AppelDoffresRestreint>();

			for (AppelDoffresRestreintDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("isDesactiver", dto.getIsDesactiver());
				//fieldsToVerify.put("isVisualiserByFournisseur", dto.getIsVisualiserByFournisseur());
				fieldsToVerify.put("userFournisseurId", dto.getUserFournisseurId());
				fieldsToVerify.put("appelDoffresId", dto.getAppelDoffresId());
				fieldsToVerify.put("isAppelRestreint", dto.getIsAppelRestreint());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if appelDoffresRestreint to insert do not exist
				AppelDoffresRestreint existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("appelDoffresRestreint -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if user exist
				User existingUser = userRepository.findById(dto.getUserFournisseurId(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserFournisseurId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if appelDoffres exist
				AppelDoffres existingAppelDoffres = appelDoffresRepository.findById(dto.getAppelDoffresId(), false);
				if (existingAppelDoffres == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getAppelDoffresId(), locale));
					response.setHasError(true);
					return response;
				}
				// doublons de critere pour les appelDoffres
				if (!items.isEmpty()) {
					if (items.stream().anyMatch(a->a.getUser().getId().equals(dto.getUserFournisseurId()) && a.getAppelDoffres().getId().equals(dto.getAppelDoffresId()) )) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du fournisseur '" + dto.getUserFournisseurId()+"' pour les appelDoffress", locale));
						response.setHasError(true);
						return response;
					}
				}
				dto.setIsSelectedByClient(false);
				dto.setIsSoumissionnerByFournisseur(false);
				dto.setIsDesactiver(false);
				dto.setIsVisualiserContenuByFournisseur(false);
				dto.setIsVisualiserNotifByFournisseur(false);
				dto.setIsUpdateAppelDoffres(existingAppelDoffres.getIsUpdate());
				dto.setIsVisibleByFournisseur(true); // car il ne l'a pas encore supprimer logiquement
				
				
				AppelDoffresRestreint entityToSave = null;
				entityToSave = AppelDoffresRestreintTransformer.INSTANCE.toEntity(dto, existingUser, existingAppelDoffres);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<AppelDoffresRestreint> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("appelDoffresRestreint", locale));
					response.setHasError(true);
					return response;
				}
				List<AppelDoffresRestreintDto> itemsDto = new ArrayList<AppelDoffresRestreintDto>();
				for (AppelDoffresRestreint entity : itemsSaved) {
					AppelDoffresRestreintDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create AppelDoffresRestreint-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update AppelDoffresRestreint by using AppelDoffresRestreintDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<AppelDoffresRestreintDto> update(Request<AppelDoffresRestreintDto> request, Locale locale)  {
		slf4jLogger.info("----begin update AppelDoffresRestreint-----");

		response = new Response<AppelDoffresRestreintDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<AppelDoffresRestreint> items = new ArrayList<AppelDoffresRestreint>();

			for (AppelDoffresRestreintDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la appelDoffresRestreint existe
				AppelDoffresRestreint entityToSave = null;
				entityToSave = appelDoffresRestreintRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffresRestreint -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if user exist
				if (dto.getUserFournisseurId() != null && dto.getUserFournisseurId() > 0){
					User existingUser = userRepository.findById(dto.getUserFournisseurId(), false);
					if (existingUser == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserFournisseurId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUser(existingUser);
				}
				// Verify if appelDoffres exist
				if (dto.getAppelDoffresId() != null && dto.getAppelDoffresId() > 0){
					AppelDoffres existingAppelDoffres = appelDoffresRepository.findById(dto.getAppelDoffresId(), false);
					if (existingAppelDoffres == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getAppelDoffresId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setAppelDoffres(existingAppelDoffres);
				}
				if (dto.getIsDesactiver() != null) {
					entityToSave.setIsDesactiver(dto.getIsDesactiver());
				}
				if (dto.getIsVisibleByFournisseur() != null) {
					entityToSave.setIsVisibleByFournisseur(dto.getIsVisibleByFournisseur());
				}
				
				if (dto.getIsVisualiserNotifByFournisseur() != null) {
					entityToSave.setIsVisualiserNotifByFournisseur(dto.getIsVisualiserNotifByFournisseur());
				}
				if (dto.getIsVisualiserContenuByFournisseur() != null) {
					entityToSave.setIsVisualiserContenuByFournisseur(dto.getIsVisualiserContenuByFournisseur());
				}
				if (dto.getIsUpdateAppelDoffres() != null) {
					entityToSave.setIsUpdateAppelDoffres(dto.getIsUpdateAppelDoffres());
				}
				if (dto.getIsSoumissionnerByFournisseur() != null) {
					entityToSave.setIsSoumissionnerByFournisseur(dto.getIsSoumissionnerByFournisseur());
				}
				if (dto.getIsAppelRestreint() != null) {
					entityToSave.setIsAppelRestreint(dto.getIsAppelRestreint());
				}
				
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<AppelDoffresRestreint> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("appelDoffresRestreint", locale));
					response.setHasError(true);
					return response;
				}
				List<AppelDoffresRestreintDto> itemsDto = new ArrayList<AppelDoffresRestreintDto>();
				for (AppelDoffresRestreint entity : itemsSaved) {
					AppelDoffresRestreintDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update AppelDoffresRestreint-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete AppelDoffresRestreint by using AppelDoffresRestreintDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<AppelDoffresRestreintDto> delete(Request<AppelDoffresRestreintDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete AppelDoffresRestreint-----");

		response = new Response<AppelDoffresRestreintDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<AppelDoffresRestreint> items = new ArrayList<AppelDoffresRestreint>();

			for (AppelDoffresRestreintDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la appelDoffresRestreint existe
				AppelDoffresRestreint existingEntity = null;
				existingEntity = appelDoffresRestreintRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffresRestreint -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				existingEntity.setIsVisibleByFournisseur(false);

				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete AppelDoffresRestreint-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete AppelDoffresRestreint by using AppelDoffresRestreintDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<AppelDoffresRestreintDto> forceDelete(Request<AppelDoffresRestreintDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete AppelDoffresRestreint-----");

		response = new Response<AppelDoffresRestreintDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<AppelDoffresRestreint> items = new ArrayList<AppelDoffresRestreint>();

			for (AppelDoffresRestreintDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la appelDoffresRestreint existe
				AppelDoffresRestreint existingEntity = null;
				existingEntity = appelDoffresRestreintRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffresRestreint -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------


				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				appelDoffresRestreintRepository.save((Iterable<AppelDoffresRestreint>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete AppelDoffresRestreint-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get AppelDoffresRestreint by using AppelDoffresRestreintDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<AppelDoffresRestreintDto> getByCriteria(Request<AppelDoffresRestreintDto> request, Locale locale) {
		slf4jLogger.info("----begin get AppelDoffresRestreint-----");

		response = new Response<AppelDoffresRestreintDto>();

		try {
			List<AppelDoffresRestreint> items = null;
			items = appelDoffresRestreintRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<AppelDoffresRestreintDto> itemsDto = new ArrayList<AppelDoffresRestreintDto>();
				for (AppelDoffresRestreint entity : items) {
					AppelDoffresRestreintDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(appelDoffresRestreintRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("appelDoffresRestreint", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get AppelDoffresRestreint-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full AppelDoffresRestreintDto by using AppelDoffresRestreint as object.
	 *
	 * @param entity, locale
	 * @return AppelDoffresRestreintDto
	 *
	 */
	private AppelDoffresRestreintDto getFullInfos(AppelDoffresRestreint entity, Integer size, Locale locale) throws Exception {
		AppelDoffresRestreintDto dto = AppelDoffresRestreintTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		List<Offre> offres = offreRepository.findByAppelDoffresId(dto.getAppelDoffresId(), false);
		dto.setNombreOffre(Utilities.isNotEmpty(offres) ? ( offres.size() == 1 ?  offres.size() + " offre" : offres.size() + " offres") : "Aucune offre");
		
		
		
		// TODO : RENVOYER LE NOMBRE DE FOURNISSEUR QUI ONT DEJA VU L'APPEL D'OFFRES
		List<AppelDoffresRestreint> appelDoffresRestreints = appelDoffresRestreintRepository.findByAppelDoffresIdAndIsVisualiserContenuByFournisseurAndIsAppelRestreintAndIsDesactiver(dto.getAppelDoffresId(), true, false, false, false) ;
		dto.setNombreVisualisation(Utilities.isNotEmpty(appelDoffresRestreints) ? ( appelDoffresRestreints.size() == 1 ?  appelDoffresRestreints.size() + " vu" : appelDoffresRestreints.size() + " vus") : "Aucune vu");

 		
		
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
