package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._DomaineEntrepriseRepository;

/**
 * Repository : DomaineEntreprise.
 */
@Repository
public interface DomaineEntrepriseRepository extends JpaRepository<DomaineEntreprise, Integer>, _DomaineEntrepriseRepository {
	/**
	 * Finds DomaineEntreprise by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object DomaineEntreprise whose id is equals to the given id. If
	 *         no DomaineEntreprise is found, this method returns null.
	 */
	@Query("select e from DomaineEntreprise e where e.id = :id and e.isDeleted = :isDeleted")
	DomaineEntreprise findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds DomaineEntreprise by using valeurAutre as a search criteria.
	 *
	 * @param valeurAutre
	 * @return An Object DomaineEntreprise whose valeurAutre is equals to the given valeurAutre. If
	 *         no DomaineEntreprise is found, this method returns null.
	 */
	@Query("select e from DomaineEntreprise e where e.valeurAutre = :valeurAutre and e.isDeleted = :isDeleted")
	List<DomaineEntreprise> findByValeurAutre(@Param("valeurAutre")String valeurAutre, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DomaineEntreprise by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object DomaineEntreprise whose createdAt is equals to the given createdAt. If
	 *         no DomaineEntreprise is found, this method returns null.
	 */
	@Query("select e from DomaineEntreprise e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<DomaineEntreprise> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DomaineEntreprise by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object DomaineEntreprise whose createdBy is equals to the given createdBy. If
	 *         no DomaineEntreprise is found, this method returns null.
	 */
	@Query("select e from DomaineEntreprise e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<DomaineEntreprise> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DomaineEntreprise by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object DomaineEntreprise whose updatedAt is equals to the given updatedAt. If
	 *         no DomaineEntreprise is found, this method returns null.
	 */
	@Query("select e from DomaineEntreprise e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<DomaineEntreprise> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DomaineEntreprise by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object DomaineEntreprise whose updatedBy is equals to the given updatedBy. If
	 *         no DomaineEntreprise is found, this method returns null.
	 */
	@Query("select e from DomaineEntreprise e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<DomaineEntreprise> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DomaineEntreprise by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object DomaineEntreprise whose deletedAt is equals to the given deletedAt. If
	 *         no DomaineEntreprise is found, this method returns null.
	 */
	@Query("select e from DomaineEntreprise e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<DomaineEntreprise> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DomaineEntreprise by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object DomaineEntreprise whose deletedBy is equals to the given deletedBy. If
	 *         no DomaineEntreprise is found, this method returns null.
	 */
	@Query("select e from DomaineEntreprise e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<DomaineEntreprise> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DomaineEntreprise by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object DomaineEntreprise whose isDeleted is equals to the given isDeleted. If
	 *         no DomaineEntreprise is found, this method returns null.
	 */
	@Query("select e from DomaineEntreprise e where e.isDeleted = :isDeleted")
	List<DomaineEntreprise> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds DomaineEntreprise by using entrepriseId as a search criteria.
	 *
	 * @param entrepriseId
	 * @return A list of Object DomaineEntreprise whose entrepriseId is equals to the given entrepriseId. If
	 *         no DomaineEntreprise is found, this method returns null.
	 */
	@Query("select e from DomaineEntreprise e where e.entreprise.id = :entrepriseId and e.isDeleted = :isDeleted")
	List<DomaineEntreprise> findByEntrepriseId(@Param("entrepriseId")Integer entrepriseId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one DomaineEntreprise by using entrepriseId as a search criteria.
   *
   * @param entrepriseId
   * @return An Object DomaineEntreprise whose entrepriseId is equals to the given entrepriseId. If
   *         no DomaineEntreprise is found, this method returns null.
   */
  @Query("select e from DomaineEntreprise e where e.entreprise.id = :entrepriseId and e.isDeleted = :isDeleted")
  DomaineEntreprise findDomaineEntrepriseByEntrepriseId(@Param("entrepriseId")Integer entrepriseId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds DomaineEntreprise by using domaineId as a search criteria.
	 *
	 * @param domaineId
	 * @return A list of Object DomaineEntreprise whose domaineId is equals to the given domaineId. If
	 *         no DomaineEntreprise is found, this method returns null.
	 */
	@Query("select e from DomaineEntreprise e where e.domaine.id = :domaineId and e.isDeleted = :isDeleted")
	List<DomaineEntreprise> findByDomaineId(@Param("domaineId")Integer domaineId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one DomaineEntreprise by using domaineId as a search criteria.
   *
   * @param domaineId
   * @return An Object DomaineEntreprise whose domaineId is equals to the given domaineId. If
   *         no DomaineEntreprise is found, this method returns null.
   */
  @Query("select e from DomaineEntreprise e where e.domaine.id = :domaineId and e.isDeleted = :isDeleted")
  DomaineEntreprise findDomaineEntrepriseByDomaineId(@Param("domaineId")Integer domaineId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of DomaineEntreprise by using domaineEntrepriseDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of DomaineEntreprise
	 * @throws DataAccessException,ParseException
	 */
	public default List<DomaineEntreprise> getByCriteria(Request<DomaineEntrepriseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from DomaineEntreprise e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<DomaineEntreprise> query = em.createQuery(req, DomaineEntreprise.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of DomaineEntreprise by using domaineEntrepriseDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of DomaineEntreprise
	 *
	 */
	public default Long count(Request<DomaineEntrepriseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from DomaineEntreprise e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<DomaineEntrepriseDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		DomaineEntrepriseDto dto = request.getData() != null ? request.getData() : new DomaineEntrepriseDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (DomaineEntrepriseDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(DomaineEntrepriseDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getValeurAutre())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("valeurAutre", dto.getValeurAutre(), "e.valeurAutre", "String", dto.getValeurAutreParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getEntrepriseId()!= null && dto.getEntrepriseId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseId", dto.getEntrepriseId(), "e.entreprise.id", "Integer", dto.getEntrepriseIdParam(), param, index, locale));
			}
			if (dto.getDomaineId()!= null && dto.getDomaineId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("domaineId", dto.getDomaineId(), "e.domaine.id", "Integer", dto.getDomaineIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEntrepriseNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseNom", dto.getEntrepriseNom(), "e.entreprise.nom", "String", dto.getEntrepriseNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEntrepriseLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("entrepriseLogin", dto.getEntrepriseLogin(), "e.entreprise.login", "String", dto.getEntrepriseLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDomaineCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("domaineCode", dto.getDomaineCode(), "e.domaine.code", "String", dto.getDomaineCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDomaineLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("domaineLibelle", dto.getDomaineLibelle(), "e.domaine.libelle", "String", dto.getDomaineLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
