package ci.salaamsolutions.projet.spasuce.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.dao.entity.Offre;
import ci.salaamsolutions.projet.spasuce.dao.entity.ValeurCritereOffre;
import ci.salaamsolutions.projet.spasuce.helper.CriteriaUtils;
import ci.salaamsolutions.projet.spasuce.helper.Utilities;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.dto.ValeurCritereOffreDto;

/**
 * Repository customize : ValeurCritereOffre.
 */
@Repository
public interface _ValeurCritereOffreRepository {
	default List<String> _generateCriteria(ValeurCritereOffreDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE
		// ajouter pour les filtres
		if (Utilities.notBlank(dto.getPaysLibelle())) {
			listOfQuery.add(CriteriaUtils.generateCriteria("paysLibelle", dto.getPaysLibelle(), "e.offre.user.entreprise.ville.pays.libelle", "String", dto.getPaysLibelleParam(), param, index, locale));
		}
		
		// ajouter pour les filtres
		if (dto.getAppelDoffresId()!= null && dto.getAppelDoffresId() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("appelDoffresId", dto.getAppelDoffresId(), "e.offre.appelDoffres.id", "Integer", dto.getAppelDoffresIdParam(), param, index, locale));
		}
		
		return listOfQuery;
	}
	
	
	/**
	 * Finds ValeurCritereOffre by using offreId as a search criteria.
	 *
	 * @param offreId
	 * @return A list of Object ValeurCritereOffre whose offreId is equals to the given offreId. If
	 *         no ValeurCritereOffre is found, this method returns null.
	 */
	@Query("select e from ValeurCritereOffre e where e.offre.id = :offreId and e.critereAppelDoffres.code = :critereAppelDoffresCode and e.isDeleted = :isDeleted")
	ValeurCritereOffre findValeurCritereOffreByOffreIdAndCritereAppelDoffresCode(@Param("offreId")Integer offreId, @Param("critereAppelDoffresCode")String critereAppelDoffresCode, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds List of ValeurCritereOffre by using valeurCritereOffreDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of ValeurCritereOffre
	 * @throws DataAccessException,ParseException
	 */
	public default List<ValeurCritereOffre> getByCriteriaCustom(Request<ValeurCritereOffreDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from ValeurCritereOffre e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<ValeurCritereOffre> query = em.createQuery(req, ValeurCritereOffre.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}
	
	
	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<ValeurCritereOffreDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		ValeurCritereOffreDto dto = request.getData() != null ? request.getData() : new ValeurCritereOffreDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (ValeurCritereOffreDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}
	
	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(ValeurCritereOffreDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getValeur())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("valeur", dto.getValeur(), "e.valeur", "String", dto.getValeurParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getOffreId()!= null && dto.getOffreId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("offreId", dto.getOffreId(), "e.offre.id", "Integer", dto.getOffreIdParam(), param, index, locale));
			}
			if (dto.getCritereId()!= null && dto.getCritereId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("critereId", dto.getCritereId(), "e.critereAppelDoffres.id", "Integer", dto.getCritereIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCritereAppelDoffresLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("critereAppelDoffresLibelle", dto.getCritereAppelDoffresLibelle(), "e.critereAppelDoffres.libelle", "String", dto.getCritereAppelDoffresLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
