

/*
 * Java transformer for entity table valeur_critere_appel_doffres 
 * Created on 2020-01-02 ( Time 18:00:05 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "valeur_critere_appel_doffres"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface ValeurCritereAppelDoffresTransformer {

	ValeurCritereAppelDoffresTransformer INSTANCE = Mappers.getMapper(ValeurCritereAppelDoffresTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.appelDoffres.id", target="appelDoffresId"),
		@Mapping(source="entity.appelDoffres.libelle", target="appelDoffresLibelle"),
		@Mapping(source="entity.critereAppelDoffres.id", target="critereId"),
		@Mapping(source="entity.critereAppelDoffres.libelle", target="critereAppelDoffresLibelle"),
		@Mapping(source="entity.critereAppelDoffres.typeCritereAppelDoffres.code", target="typeCritereAppelDoffresCode"), //Ajouter 


	})
	ValeurCritereAppelDoffresDto toDto(ValeurCritereAppelDoffres entity) throws ParseException;

	List<ValeurCritereAppelDoffresDto> toDtos(List<ValeurCritereAppelDoffres> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.valeur", target="valeur"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="appelDoffres", target="appelDoffres"),
		@Mapping(source="critereAppelDoffres", target="critereAppelDoffres"),
	})
	ValeurCritereAppelDoffres toEntity(ValeurCritereAppelDoffresDto dto, AppelDoffres appelDoffres, CritereAppelDoffres critereAppelDoffres) throws ParseException;

	//List<ValeurCritereAppelDoffres> toEntities(List<ValeurCritereAppelDoffresDto> dtos) throws ParseException;

}
