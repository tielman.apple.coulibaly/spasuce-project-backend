


/*
 * Java transformer for entity table critere_filtre
 * Created on 2020-01-02 ( Time 17:59:55 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "critere_filtre"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class CritereFiltreBusiness implements IBasicBusiness<Request<CritereFiltreDto>, Response<CritereFiltreDto>> {

	private Response<CritereFiltreDto> response;
	@Autowired
	private CritereFiltreRepository critereFiltreRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FiltreRepository filtreRepository;
	@Autowired
	private TypeCritereAppelDoffresRepository typeCritereAppelDoffresRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public CritereFiltreBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create CritereFiltre by using CritereFiltreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<CritereFiltreDto> create(Request<CritereFiltreDto> request, Locale locale)  {
		slf4jLogger.info("----begin create CritereFiltre-----");

		response = new Response<CritereFiltreDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<CritereFiltre> items = new ArrayList<CritereFiltre>();

			for (CritereFiltreDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				fieldsToVerify.put("code", dto.getLibelle());
				fieldsToVerify.put("typeCritereFiltreId", dto.getTypeCritereFiltreId());
				fieldsToVerify.put("filtreId", dto.getFiltreId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if critereFiltre to insert do not exist
				CritereFiltre existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("critereFiltre -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = critereFiltreRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("critereFiltre -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les critereFiltres", locale));
					response.setHasError(true);
					return response;
				}

				existingEntity = critereFiltreRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("critereFiltre -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les critereFiltres", locale));
					response.setHasError(true);
					return response;
				}

				// Verify if filtre exist
				Filtre existingFiltre = filtreRepository.findById(dto.getFiltreId(), false);
				if (existingFiltre == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("filtre -> " + dto.getFiltreId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if typeCritereAppelDoffres exist
				TypeCritereAppelDoffres existingTypeCritereAppelDoffres = typeCritereAppelDoffresRepository.findById(dto.getTypeCritereFiltreId(), false);
				if (existingTypeCritereAppelDoffres == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeCritereAppelDoffres -> " + dto.getTypeCritereFiltreId(), locale));
					response.setHasError(true);
					return response;
				}
				CritereFiltre entityToSave = null;
				entityToSave = CritereFiltreTransformer.INSTANCE.toEntity(dto, existingFiltre, existingTypeCritereAppelDoffres);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<CritereFiltre> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = critereFiltreRepository.save((Iterable<CritereFiltre>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("critereFiltre", locale));
					response.setHasError(true);
					return response;
				}
				List<CritereFiltreDto> itemsDto = new ArrayList<CritereFiltreDto>();
				for (CritereFiltre entity : itemsSaved) {
					CritereFiltreDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create CritereFiltre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update CritereFiltre by using CritereFiltreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<CritereFiltreDto> update(Request<CritereFiltreDto> request, Locale locale)  {
		slf4jLogger.info("----begin update CritereFiltre-----");

		response = new Response<CritereFiltreDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<CritereFiltre> items = new ArrayList<CritereFiltre>();

			for (CritereFiltreDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la critereFiltre existe
				CritereFiltre entityToSave = null;
				entityToSave = critereFiltreRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("critereFiltre -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if filtre exist
				if (dto.getFiltreId() != null && dto.getFiltreId() > 0){
					Filtre existingFiltre = filtreRepository.findById(dto.getFiltreId(), false);
					if (existingFiltre == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("filtre -> " + dto.getFiltreId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setFiltre(existingFiltre);
				}
				// Verify if typeCritereAppelDoffres exist
				if (dto.getTypeCritereFiltreId() != null && dto.getTypeCritereFiltreId() > 0){
					TypeCritereAppelDoffres existingTypeCritereAppelDoffres = typeCritereAppelDoffresRepository.findById(dto.getTypeCritereFiltreId(), false);
					if (existingTypeCritereAppelDoffres == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("typeCritereAppelDoffres -> " + dto.getTypeCritereFiltreId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setTypeCritereAppelDoffres(existingTypeCritereAppelDoffres);
				}
				if (Utilities.notBlank(dto.getLibelle())) {
					CritereFiltre existingEntity = critereFiltreRepository.findByLibelle(dto.getLibelle(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("critereFiltre -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les critereFiltres", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}

				if (Utilities.notBlank(dto.getCode())) {
					CritereFiltre existingEntity = critereFiltreRepository.findByCode(dto.getCode(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("critereFiltre -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les critereFiltres", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCode(dto.getCode());
				}

				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<CritereFiltre> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = critereFiltreRepository.save((Iterable<CritereFiltre>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("critereFiltre", locale));
					response.setHasError(true);
					return response;
				}
				List<CritereFiltreDto> itemsDto = new ArrayList<CritereFiltreDto>();
				for (CritereFiltre entity : itemsSaved) {
					CritereFiltreDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update CritereFiltre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete CritereFiltre by using CritereFiltreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<CritereFiltreDto> delete(Request<CritereFiltreDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete CritereFiltre-----");

		response = new Response<CritereFiltreDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<CritereFiltre> items = new ArrayList<CritereFiltre>();

			for (CritereFiltreDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la critereFiltre existe
				CritereFiltre existingEntity = null;
				existingEntity = critereFiltreRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("critereFiltre -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				critereFiltreRepository.save((Iterable<CritereFiltre>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete CritereFiltre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete CritereFiltre by using CritereFiltreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<CritereFiltreDto> forceDelete(Request<CritereFiltreDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete CritereFiltre-----");

		response = new Response<CritereFiltreDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<CritereFiltre> items = new ArrayList<CritereFiltre>();

			for (CritereFiltreDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la critereFiltre existe
				CritereFiltre existingEntity = null;
				existingEntity = critereFiltreRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("critereFiltre -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				critereFiltreRepository.save((Iterable<CritereFiltre>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete CritereFiltre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get CritereFiltre by using CritereFiltreDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<CritereFiltreDto> getByCriteria(Request<CritereFiltreDto> request, Locale locale) {
		slf4jLogger.info("----begin get CritereFiltre-----");

		response = new Response<CritereFiltreDto>();

		try {
			List<CritereFiltre> items = null;
			items = critereFiltreRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<CritereFiltreDto> itemsDto = new ArrayList<CritereFiltreDto>();
				for (CritereFiltre entity : items) {
					CritereFiltreDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(critereFiltreRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("critereFiltre", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get CritereFiltre-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full CritereFiltreDto by using CritereFiltre as object.
	 *
	 * @param entity, locale
	 * @return CritereFiltreDto
	 *
	 */
	private CritereFiltreDto getFullInfos(CritereFiltre entity, Integer size, Locale locale) throws Exception {
		CritereFiltreDto dto = CritereFiltreTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
