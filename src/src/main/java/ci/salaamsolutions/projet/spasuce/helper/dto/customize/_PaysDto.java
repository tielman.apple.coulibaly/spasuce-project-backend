
/*
 * Java dto for entity table pays 
 * Created on 2020-01-03 ( Time 08:47:10 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.customize;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.helper.dto.PaysDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.VilleDto;
import lombok.Data;
/**
 * DTO customize for table "pays"
 * 
 * @author SFL Back-End developper
 *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _PaysDto {
	
	protected List<PaysDto> datasPays;
	protected List<VilleDto> datasVille;
	

}
