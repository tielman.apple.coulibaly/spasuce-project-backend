

/*
 * Java transformer for entity table sous_critere_note 
 * Created on 2020-01-02 ( Time 18:00:03 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "sous_critere_note"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface SousCritereNoteTransformer {

	SousCritereNoteTransformer INSTANCE = Mappers.getMapper(SousCritereNoteTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.critereNote.id", target="critereNoteId"),
				@Mapping(source="entity.critereNote.libelle", target="critereNoteLibelle"),
	})
	SousCritereNoteDto toDto(SousCritereNote entity) throws ParseException;

    List<SousCritereNoteDto> toDtos(List<SousCritereNote> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.coefficient", target="coefficient"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="critereNote", target="critereNote"),
	})
    SousCritereNote toEntity(SousCritereNoteDto dto, CritereNote critereNote) throws ParseException;

    //List<SousCritereNote> toEntities(List<SousCritereNoteDto> dtos) throws ParseException;

}
