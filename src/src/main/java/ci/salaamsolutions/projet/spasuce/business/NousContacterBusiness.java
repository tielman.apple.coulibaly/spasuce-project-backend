


/*
 * Java transformer for entity table nous_contacter
 * Created on 2020-01-03 ( Time 10:54:34 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.enums.EmailEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeUserEnum;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "nous_contacter"
 *
 * @author SFL Back-End developper
 *
 */
@Component
@EnableAsync // pour pouviur executer une methode asynchrone
public class NousContacterBusiness implements IBasicBusiness<Request<NousContacterDto>, Response<NousContacterDto>> {

	private Response<NousContacterDto> response;
	@Autowired
	private NousContacterRepository nousContacterRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ObjetNousContacterRepository objetNousContacterRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;

	@Autowired
	private ParamsUtils paramsUtils;
	private Context context;

	@Autowired
	private HostingUtils hostingUtils;





	public NousContacterBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create NousContacter by using NousContacterDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<NousContacterDto> create(Request<NousContacterDto> request, Locale locale)  {
		slf4jLogger.info("----begin create NousContacter-----");

		response = new Response<NousContacterDto>();

		try {
			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}
			//
			//			User utilisateur = userRepository.findById(request.getUser(), false);
			//			if (utilisateur == null) {
			//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}
			//			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
			//				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			List<NousContacter> items = new ArrayList<NousContacter>();

			for (NousContacterDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("nom", dto.getNom());
				//fieldsToVerify.put("prenoms", dto.getPrenoms());
				fieldsToVerify.put("email", dto.getEmail());
				//fieldsToVerify.put("telephone", dto.getTelephone());
				fieldsToVerify.put("contenu", dto.getContenu());
				//fieldsToVerify.put("autre", dto.getAutre());
				fieldsToVerify.put("objetNousContacterId", dto.getObjetNousContacterId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if nousContacter to insert do not exist
				NousContacter existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("nousContacter -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				//				existingEntity = nousContacterRepository.findByEmail(dto.getEmail(), false);
				//				if (existingEntity != null) {
				//					response.setStatus(functionalError.DATA_EXIST("nousContacter -> " + dto.getEmail(), locale));
				//					response.setHasError(true);
				//					return response;
				//				}
				//				if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()))) {
				//					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les nousContacters", locale));
				//					response.setHasError(true);
				//					return response;
				//				}
				//				existingEntity = nousContacterRepository.findByTelephone(dto.getTelephone(), false);
				//				if (existingEntity != null) {
				//					response.setStatus(functionalError.DATA_EXIST("nousContacter -> " + dto.getTelephone(), locale));
				//					response.setHasError(true);
				//					return response;
				//				}
				//				if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()))) {
				//					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les nousContacters", locale));
				//					response.setHasError(true);
				//					return response;
				//				}

				// Verify if objetNousContacter exist
				ObjetNousContacter existingObjetNousContacter = objetNousContacterRepository.findById(dto.getObjetNousContacterId(), false);
				if (existingObjetNousContacter == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("objetNousContacter -> " + dto.getObjetNousContacterId(), locale));
					response.setHasError(true);
					return response;
				}
				NousContacter entityToSave = null;
				entityToSave = NousContacterTransformer.INSTANCE.toEntity(dto, existingObjetNousContacter);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<NousContacter> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = nousContacterRepository.save((Iterable<NousContacter>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("nousContacter", locale));
					response.setHasError(true);
					return response;
				}
				List<NousContacterDto> itemsDto = new ArrayList<NousContacterDto>();
				for (NousContacter entity : itemsSaved) {
					NousContacterDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);


				// l'envoi de mail au contacteur
				// envoi de mail 
				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user" ,  paramsUtils.getSmtpMailUser());

					// choisir la vraie url
					String appLink = "";
					//			if (entityToSave.getAdmin() != null ) {
					//				appLink = paramsUtils.getUrlRootAdmin();
					//			}else {
					//				appLink = paramsUtils.getUrlRootUser();
					//			}

					appLink = paramsUtils.getUrlRootUser();

					for (NousContacter data : itemsSaved) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", data.getEmail());
						recipient.put("user", (Utilities.notBlank(data.getNom()) ? data.getNom() : data.getEmail()));
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						//String subject = EmailEnum.SUBJECT_CREATION_COMPTE;
						String subject = EmailEnum.SUBJECT_NOUS_CONTACTER_DEFAULT_RETOUR;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateNousContacterRetourParDefaut();
						//		        	context.setVariable("email", user.getEmail());
						//					context.setVariable("login", user.getLogin());
						//					context.setVariable("token", user.getToken());
						//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable(EmailEnum.nomEntreprise, EmailEnum.NOM_ENTREPRISE);
						context.setVariable(EmailEnum.nom, data.getNom());
						context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail

					}
				}



				// l'envoi de mail au administrateur
				// envoi de mail 
				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user" ,  paramsUtils.getSmtpMailUser());

					// choisir la vraie url
					String appLink = "";
					//			if (entityToSave.getAdmin() != null ) {
					//				appLink = paramsUtils.getUrlRootAdmin();
					//			}else {
					//				appLink = paramsUtils.getUrlRootUser();
					//			}

					appLink = paramsUtils.getUrlRootUser();


					for (NousContacter nousContacter : itemsSaved) {

//						List<User> usersAdmin = new ArrayList<User>() ;
//
//						usersAdmin = userRepository.findByRoleCode(TypeUserEnum.USER_ADMIN, false);
//
//						slf4jLogger.info("usersAdmin:: " + usersAdmin);
//
//						if (Utilities.isNotEmpty(usersAdmin)) {
//							slf4jLogger.info("usersAdmin:: " + usersAdmin.size());
//
//
//							for (User data : usersAdmin) {
								//recipients
								List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
								//		        	for (User user : itemsSaved) {
								Map<String, String> recipient = new HashMap<String, String>();
								recipient = new HashMap<String, String>();
//								recipient.put("email", data.getEmail());
//								recipient.put("user", data.getNom());
								recipient.put("email", paramsUtils.getSmtpMailUsername());
								recipient.put("user" ,  paramsUtils.getSmtpMailUser());

								//		        	recipient.put("user", user.getNom());
								// slf4jLogger.info("data.getEmail():: " + data.getEmail());


								toRecipients.add(recipient); 
								//					}

								//subject
								// ajout du champ objet a la newsletter
								//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
								//String subject = EmailEnum.SUBJECT_CREATION_COMPTE;
								String subject = EmailEnum.SUBJECT_NOUS_CONTACTER;
								//String body = EmailEnum.BODY_PASSWORD_RESET;
								String body = EmailEnum.Objet + nousContacter.getObjetNousContacter().getLibelle() +"<br/>" +
										"De: " + nousContacter.getNom() +"<br/>" +
										EmailEnum.Email + nousContacter.getEmail() + "<br/>" + 
										EmailEnum.Telephone +  (Utilities.notBlank(nousContacter.getTelephone()) ? nousContacter.getTelephone() : "")+ "<br/>" + 
										EmailEnum.Message +nousContacter.getContenu();
								context = new Context();
								//String templateChangerPassword = paramsUtils.getTemplateNousContacterRetourParDefaut();
								//		        	context.setVariable("email", user.getEmail());
								//					context.setVariable("login", user.getLogin());
								//					context.setVariable("token", user.getToken());
								//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
								//context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
								//context.setVariable(EmailEnum.nomEntreprise, EmailEnum.NOM_ENTREPRISE);
								//context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
								//context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
								Response<UserDto> responseEnvoiEmail = new Response<>();
								responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,null, locale);
								//responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
								// si jamais une exception c'est produite pour un mail

							//}
						}
					}



					/*
					for (NousContacter nousContacter : itemsSaved) {

						List<User> usersAdmin = new ArrayList<User>() ;

						usersAdmin = userRepository.findByRoleCode(TypeUserEnum.USER_ADMIN, false);

						slf4jLogger.info("usersAdmin:: " + usersAdmin);

						if (Utilities.isNotEmpty(usersAdmin)) {
							slf4jLogger.info("usersAdmin:: " + usersAdmin.size());


							for (User data : usersAdmin) {
								//recipients
								List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
								//		        	for (User user : itemsSaved) {
								Map<String, String> recipient = new HashMap<String, String>();
								recipient = new HashMap<String, String>();
								recipient.put("email", data.getEmail());
								recipient.put("user", data.getNom());
								//		        	recipient.put("user", user.getNom());
								slf4jLogger.info("data.getEmail():: " + data.getEmail());


								toRecipients.add(recipient); 
								//					}

								//subject
								// ajout du champ objet a la newsletter
								//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
								//String subject = EmailEnum.SUBJECT_CREATION_COMPTE;
								String subject = EmailEnum.SUBJECT_NOUS_CONTACTER;
								//String body = EmailEnum.BODY_PASSWORD_RESET;
								String body = EmailEnum.Objet + nousContacter.getObjetNousContacter().getLibelle() +"<br/>" +
											"De: " + nousContacter.getNom() +"<br/>" +
											EmailEnum.Email + nousContacter.getEmail() + "<br/>" + 
											EmailEnum.Telephone +  (Utilities.notBlank(nousContacter.getTelephone()) ? nousContacter.getTelephone() : "")+ "<br/>" + 
											EmailEnum.Message +nousContacter.getContenu();
								context = new Context();
								//String templateChangerPassword = paramsUtils.getTemplateNousContacterRetourParDefaut();
								//		        	context.setVariable("email", user.getEmail());
								//					context.setVariable("login", user.getLogin());
								//					context.setVariable("token", user.getToken());
								//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
								//context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
								//context.setVariable(EmailEnum.nomEntreprise, EmailEnum.NOM_ENTREPRISE);
								//context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
								//context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
								Response<UserDto> responseEnvoiEmail = new Response<>();
								responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,null, locale);
								//responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
								// si jamais une exception c'est produite pour un mail

							}
						}
					
					//*/
					}
//				}
//			}

			slf4jLogger.info("----end create NousContacter-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update NousContacter by using NousContacterDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<NousContacterDto> update(Request<NousContacterDto> request, Locale locale)  {
		slf4jLogger.info("----begin update NousContacter-----");

		response = new Response<NousContacterDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<NousContacter> items = new ArrayList<NousContacter>();

			for (NousContacterDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la nousContacter existe
				NousContacter entityToSave = null;
				entityToSave = nousContacterRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("nousContacter -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if objetNousContacter exist
				if (dto.getObjetNousContacterId() != null && dto.getObjetNousContacterId() > 0){
					ObjetNousContacter existingObjetNousContacter = objetNousContacterRepository.findById(dto.getObjetNousContacterId(), false);
					if (existingObjetNousContacter == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("objetNousContacter -> " + dto.getObjetNousContacterId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setObjetNousContacter(existingObjetNousContacter);
				}
				if (Utilities.notBlank(dto.getNom())) {
					entityToSave.setNom(dto.getNom());
				}
				if (Utilities.notBlank(dto.getPrenoms())) {
					entityToSave.setPrenoms(dto.getPrenoms());
				}
				if (Utilities.notBlank(dto.getEmail())) {
					NousContacter existingEntity = nousContacterRepository.findByEmail(dto.getEmail(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("nousContacter -> " + dto.getEmail(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les nousContacters", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEmail(dto.getEmail());
				}
				if (Utilities.notBlank(dto.getTelephone())) {
					NousContacter existingEntity = nousContacterRepository.findByTelephone(dto.getTelephone(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("nousContacter -> " + dto.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les nousContacters", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setTelephone(dto.getTelephone());
				}
				if (Utilities.notBlank(dto.getContenu())) {
					entityToSave.setContenu(dto.getContenu());
				}
				if (Utilities.notBlank(dto.getAutre())) {
					entityToSave.setAutre(dto.getAutre());
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				if (Utilities.notBlank(dto.getDeletedAt())) {
					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
				}
				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
					entityToSave.setDeletedBy(dto.getDeletedBy());
				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<NousContacter> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = nousContacterRepository.save((Iterable<NousContacter>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("nousContacter", locale));
					response.setHasError(true);
					return response;
				}
				List<NousContacterDto> itemsDto = new ArrayList<NousContacterDto>();
				for (NousContacter entity : itemsSaved) {
					NousContacterDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update NousContacter-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete NousContacter by using NousContacterDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<NousContacterDto> delete(Request<NousContacterDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete NousContacter-----");

		response = new Response<NousContacterDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<NousContacter> items = new ArrayList<NousContacter>();

			for (NousContacterDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la nousContacter existe
				NousContacter existingEntity = null;
				existingEntity = nousContacterRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("nousContacter -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				nousContacterRepository.save((Iterable<NousContacter>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete NousContacter-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete NousContacter by using NousContacterDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<NousContacterDto> forceDelete(Request<NousContacterDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete NousContacter-----");

		response = new Response<NousContacterDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<NousContacter> items = new ArrayList<NousContacter>();

			for (NousContacterDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la nousContacter existe
				NousContacter existingEntity = null;
				existingEntity = nousContacterRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("nousContacter -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				nousContacterRepository.save((Iterable<NousContacter>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete NousContacter-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get NousContacter by using NousContacterDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<NousContacterDto> getByCriteria(Request<NousContacterDto> request, Locale locale) {
		slf4jLogger.info("----begin get NousContacter-----");

		response = new Response<NousContacterDto>();

		try {
			List<NousContacter> items = null;
			items = nousContacterRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<NousContacterDto> itemsDto = new ArrayList<NousContacterDto>();
				for (NousContacter entity : items) {
					NousContacterDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(nousContacterRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("nousContacter", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get NousContacter-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full NousContacterDto by using NousContacter as object.
	 *
	 * @param entity, locale
	 * @return NousContacterDto
	 *
	 */
	private NousContacterDto getFullInfos(NousContacter entity, Integer size, Locale locale) throws Exception {
		NousContacterDto dto = NousContacterTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
