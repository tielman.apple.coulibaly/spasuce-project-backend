package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._CritereFiltreRepository;

/**
 * Repository : CritereFiltre.
 */
@Repository
public interface CritereFiltreRepository extends JpaRepository<CritereFiltre, Integer>, _CritereFiltreRepository {
	/**
	 * Finds CritereFiltre by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object CritereFiltre whose id is equals to the given id. If
	 *         no CritereFiltre is found, this method returns null.
	 */
	@Query("select e from CritereFiltre e where e.id = :id and e.isDeleted = :isDeleted")
	CritereFiltre findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds CritereFiltre by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object CritereFiltre whose libelle is equals to the given libelle. If
	 *         no CritereFiltre is found, this method returns null.
	 */
	@Query("select e from CritereFiltre e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	CritereFiltre findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereFiltre by using code as a search criteria.
	 *
	 * @param code
	 * @return An Object CritereFiltre whose code is equals to the given code. If
	 *         no CritereFiltre is found, this method returns null.
	 */
	@Query("select e from CritereFiltre e where e.code = :code and e.isDeleted = :isDeleted")
	CritereFiltre findByCode(@Param("code")String code, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds CritereFiltre by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object CritereFiltre whose createdAt is equals to the given createdAt. If
	 *         no CritereFiltre is found, this method returns null.
	 */
	@Query("select e from CritereFiltre e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<CritereFiltre> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereFiltre by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object CritereFiltre whose createdBy is equals to the given createdBy. If
	 *         no CritereFiltre is found, this method returns null.
	 */
	@Query("select e from CritereFiltre e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<CritereFiltre> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereFiltre by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object CritereFiltre whose updatedAt is equals to the given updatedAt. If
	 *         no CritereFiltre is found, this method returns null.
	 */
	@Query("select e from CritereFiltre e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<CritereFiltre> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereFiltre by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object CritereFiltre whose updatedBy is equals to the given updatedBy. If
	 *         no CritereFiltre is found, this method returns null.
	 */
	@Query("select e from CritereFiltre e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<CritereFiltre> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereFiltre by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object CritereFiltre whose deletedAt is equals to the given deletedAt. If
	 *         no CritereFiltre is found, this method returns null.
	 */
	@Query("select e from CritereFiltre e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<CritereFiltre> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereFiltre by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object CritereFiltre whose deletedBy is equals to the given deletedBy. If
	 *         no CritereFiltre is found, this method returns null.
	 */
	@Query("select e from CritereFiltre e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<CritereFiltre> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds CritereFiltre by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object CritereFiltre whose isDeleted is equals to the given isDeleted. If
	 *         no CritereFiltre is found, this method returns null.
	 */
	@Query("select e from CritereFiltre e where e.isDeleted = :isDeleted")
	List<CritereFiltre> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds CritereFiltre by using filtreId as a search criteria.
	 *
	 * @param filtreId
	 * @return A list of Object CritereFiltre whose filtreId is equals to the given filtreId. If
	 *         no CritereFiltre is found, this method returns null.
	 */
	@Query("select e from CritereFiltre e where e.filtre.id = :filtreId and e.isDeleted = :isDeleted")
	List<CritereFiltre> findByFiltreId(@Param("filtreId")Integer filtreId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one CritereFiltre by using filtreId as a search criteria.
   *
   * @param filtreId
   * @return An Object CritereFiltre whose filtreId is equals to the given filtreId. If
   *         no CritereFiltre is found, this method returns null.
   */
  @Query("select e from CritereFiltre e where e.filtre.id = :filtreId and e.isDeleted = :isDeleted")
  CritereFiltre findCritereFiltreByFiltreId(@Param("filtreId")Integer filtreId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds CritereFiltre by using typeCritereFiltreId as a search criteria.
	 *
	 * @param typeCritereFiltreId
	 * @return A list of Object CritereFiltre whose typeCritereFiltreId is equals to the given typeCritereFiltreId. If
	 *         no CritereFiltre is found, this method returns null.
	 */
	@Query("select e from CritereFiltre e where e.typeCritereAppelDoffres.id = :typeCritereFiltreId and e.isDeleted = :isDeleted")
	List<CritereFiltre> findByTypeCritereFiltreId(@Param("typeCritereFiltreId")Integer typeCritereFiltreId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one CritereFiltre by using typeCritereFiltreId as a search criteria.
   *
   * @param typeCritereFiltreId
   * @return An Object CritereFiltre whose typeCritereFiltreId is equals to the given typeCritereFiltreId. If
   *         no CritereFiltre is found, this method returns null.
   */
  @Query("select e from CritereFiltre e where e.typeCritereAppelDoffres.id = :typeCritereFiltreId and e.isDeleted = :isDeleted")
  CritereFiltre findCritereFiltreByTypeCritereFiltreId(@Param("typeCritereFiltreId")Integer typeCritereFiltreId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of CritereFiltre by using critereFiltreDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of CritereFiltre
	 * @throws DataAccessException,ParseException
	 */
	public default List<CritereFiltre> getByCriteria(Request<CritereFiltreDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from CritereFiltre e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<CritereFiltre> query = em.createQuery(req, CritereFiltre.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of CritereFiltre by using critereFiltreDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of CritereFiltre
	 *
	 */
	public default Long count(Request<CritereFiltreDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from CritereFiltre e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<CritereFiltreDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		CritereFiltreDto dto = request.getData() != null ? request.getData() : new CritereFiltreDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (CritereFiltreDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(CritereFiltreDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("code", dto.getCode(), "e.code", "String", dto.getCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getFiltreId()!= null && dto.getFiltreId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("filtreId", dto.getFiltreId(), "e.filtre.id", "Integer", dto.getFiltreIdParam(), param, index, locale));
			}
			if (dto.getTypeCritereFiltreId()!= null && dto.getTypeCritereFiltreId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeCritereFiltreId", dto.getTypeCritereFiltreId(), "e.typeCritereAppelDoffres.id", "Integer", dto.getTypeCritereFiltreIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getFiltreCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("filtreCode", dto.getFiltreCode(), "e.filtre.code", "String", dto.getFiltreCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getFiltreLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("filtreLibelle", dto.getFiltreLibelle(), "e.filtre.libelle", "String", dto.getFiltreLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeCritereAppelDoffresCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeCritereAppelDoffresCode", dto.getTypeCritereAppelDoffresCode(), "e.typeCritereAppelDoffres.code", "String", dto.getTypeCritereAppelDoffresCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeCritereAppelDoffresLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeCritereAppelDoffresLibelle", dto.getTypeCritereAppelDoffresLibelle(), "e.typeCritereAppelDoffres.libelle", "String", dto.getTypeCritereAppelDoffresLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
