
/*
 * Java dto for entity table appel_doffres_restreint
 * Created on 2020-02-08 ( Time 09:23:24 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.customize._AppelDoffresRestreintDto;

import lombok.*;
/**
 * DTO for table "appel_doffres_restreint"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class AppelDoffresRestreintDto extends _AppelDoffresRestreintDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * Pour retirer un fournisseur
	 */
    private Boolean    isDesactiver         ;
	/*
	 * Si l'offre du . fournisseur a été retenu pr cet AO
	 */
    private Boolean    isSelectedByClient   ;
	/*
	 * Si le fournisseur ne veut plus voir l'offre
	 */
    private Boolean    isVisibleByFournisseur ;
	/*
	 * Quand le fournisseur aura cliqué sur la notification	
	 */
    private Boolean    isVisualiserNotifByFournisseur ;
	/*
	 * Quand le fournisseur aura regarder le contenu de la notification	
	 */
    private Boolean    isVisualiserContenuByFournisseur ;
	/*
	 * Pour dire que l'AO a été modifier après soumission dans le système
	 */
    private Boolean    isUpdateAppelDoffres ;
	/*
	 * Quand le fournisseur aura postuler pour un appel d'offre	
	 */
    private Boolean    isSoumissionnerByFournisseur ;
	/*
	 * 
	 */
    private Boolean    isAppelRestreint     ;
	/*
	 * 
	 */
    private Integer    userFournisseurId    ;
	/*
	 * 
	 */
    private Integer    appelDoffresId       ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String userNom;
	private String userPrenoms;
	private String userLogin;
	private String appelDoffresLibelle;
	private String appelDoffresCode;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Boolean>  isDesactiverParam     ;                     
	private SearchParam<Boolean>  isSelectedByClientParam;                     
	private SearchParam<Boolean>  isVisibleByFournisseurParam;                     
	private SearchParam<Boolean>  isVisualiserNotifByFournisseurParam;                     
	private SearchParam<Boolean>  isVisualiserContenuByFournisseurParam;                     
	private SearchParam<Boolean>  isUpdateAppelDoffresParam;                     
	private SearchParam<Boolean>  isSoumissionnerByFournisseurParam;                     
	private SearchParam<Boolean>  isAppelRestreintParam ;                     
	private SearchParam<Integer>  userFournisseurIdParam;                     
	private SearchParam<Integer>  appelDoffresIdParam   ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   userNomParam          ;                     
	private SearchParam<String>   userPrenomsParam      ;                     
	private SearchParam<String>   userLoginParam        ;                     
	private SearchParam<String>   appelDoffresLibelleParam;                     
	private SearchParam<String>   appelDoffresCodeParam;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		AppelDoffresRestreintDto other = (AppelDoffresRestreintDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
