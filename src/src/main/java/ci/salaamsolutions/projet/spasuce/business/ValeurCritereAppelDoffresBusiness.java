


/*
 * Java transformer for entity table valeur_critere_appel_doffres
 * Created on 2020-01-02 ( Time 18:00:05 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.transformer.*;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeCritereAppelDoffresEnum;
import ci.salaamsolutions.projet.spasuce.helper.enums.TypeUserEnum;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.IBasicBusiness;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.*;

/**
BUSINESS for table "valeur_critere_appel_doffres"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class ValeurCritereAppelDoffresBusiness implements IBasicBusiness<Request<ValeurCritereAppelDoffresDto>, Response<ValeurCritereAppelDoffresDto>> {

	private Response<ValeurCritereAppelDoffresDto> response;
	@Autowired
	private ValeurCritereAppelDoffresRepository valeurCritereAppelDoffresRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AppelDoffresRepository appelDoffresRepository;
	@Autowired
	private CritereAppelDoffresRepository critereAppelDoffresRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public ValeurCritereAppelDoffresBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create ValeurCritereAppelDoffres by using ValeurCritereAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ValeurCritereAppelDoffresDto> create(Request<ValeurCritereAppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin create ValeurCritereAppelDoffres-----");

		response = new Response<ValeurCritereAppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			// type de user autorisé
			if (utilisateur.getEntreprise().getTypeEntreprise().getCode().equals(TypeUserEnum.ENTREPRISE_FOURNISSEUR)) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un client" , locale));
				response.setHasError(true);
				return response;
			}

			List<ValeurCritereAppelDoffres> items = new ArrayList<ValeurCritereAppelDoffres>();

			for (ValeurCritereAppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("valeur", dto.getValeur());
				fieldsToVerify.put("appelDoffresId", dto.getAppelDoffresId());
				fieldsToVerify.put("critereId", dto.getCritereId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if valeurCritereAppelDoffres to insert do not exist
				ValeurCritereAppelDoffres existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("valeurCritereAppelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if appelDoffres exist
				AppelDoffres existingAppelDoffres = appelDoffresRepository.findById(dto.getAppelDoffresId(), false);
				if (existingAppelDoffres == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getAppelDoffresId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if critereAppelDoffres exist
				CritereAppelDoffres existingCritereAppelDoffres = critereAppelDoffresRepository.findById(dto.getCritereId(), false);
				if (existingCritereAppelDoffres == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("critereAppelDoffres -> " + dto.getCritereId(), locale));
					response.setHasError(true);
					return response;
				}
				
				// verifier le contenu de la valeuur selon le type du critere
				/*
				String valeur = dto.getValeur();
				String typeCritere = existingCritereAppelDoffres.getTypeCritereAppelDoffres().getCode();
				
				
				
				response = Utilities.verifierSiContenuValeurCorrespondAuType(response, valeur, typeCritere, locale, functionalError);
				
				if (response.isHasError()) {
					return response;
				}
				//*/
				
				
				
//				switch (existingCritereAppelDoffres.getTypeCritereAppelDoffres().getCode()) {
//				case TypeCritereAppelDoffresEnum.DATE:
//					if (!Utilities.isDateValid(valeur)) {
//						response.setStatus(functionalError.INVALID_DATA("la valeur -> " + dto.getValeur() + " saisie n'est pas une date", locale));
//						response.setHasError(true);
//						return response;
//					}
//
//					break;
//
//				case TypeCritereAppelDoffresEnum.DATETIME:
//					try {
//						Date dateTime = dateFormat.parse(dto.getValeur());
//					} catch (Exception e) {
//						// TODO: handle exception
//						response.setStatus(functionalError.INVALID_DATA("la valeur -> " + dto.getValeur() + " saisie n'est pas une date avec heure", locale));
//						response.setHasError(true);
//						return response;
//					}
//
//					break;
//
//				case TypeCritereAppelDoffresEnum.BOOLEAN:
//					if (!Utilities.isBoolean(valeur)) {
//						response.setStatus(functionalError.INVALID_DATA("la valeur -> " + dto.getValeur() + " saisie n'est pas correcte", locale));
//						response.setHasError(true);
//						return response;
//					}
//					break;
//
//				case TypeCritereAppelDoffresEnum.ENTIER:
//
//					if (!Utilities.isInteger(valeur)) {
//						response.setStatus(functionalError.INVALID_DATA("la valeur -> " + dto.getValeur() + " saisie n'est pas un entier", locale));
//						response.setHasError(true);
//						return response;
//					}
//					break;
//
//				case TypeCritereAppelDoffresEnum.STRING:
//					// rien a faire ici
//					break;
//
//				case TypeCritereAppelDoffresEnum.NOMBRE:
//					if (!Utilities.isNumeric(valeur)) {
//						response.setStatus(functionalError.INVALID_DATA("la valeur -> " + dto.getValeur() + " saisie n'est pas nombre", locale));
//						response.setHasError(true);
//						return response;
//					}
//					break;
//
//				default:
//					break;
//				}
				
				// doublons de critere pour les appelDoffres
				if (!items.isEmpty()) {
					if (items.stream().anyMatch(a->a.getCritereAppelDoffres().getId().equals(dto.getCritereId()) && a.getAppelDoffres().getId().equals(dto.getAppelDoffresId()) )) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du critere '" + dto.getCritereId()+"' pour les appelDoffress", locale));
						response.setHasError(true);
						return response;
					}
				}
				
				
				ValeurCritereAppelDoffres entityToSave = null;
				entityToSave = ValeurCritereAppelDoffresTransformer.INSTANCE.toEntity(dto, existingAppelDoffres, existingCritereAppelDoffres);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ValeurCritereAppelDoffres> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = valeurCritereAppelDoffresRepository.save((Iterable<ValeurCritereAppelDoffres>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("valeurCritereAppelDoffres", locale));
					response.setHasError(true);
					return response;
				}
				List<ValeurCritereAppelDoffresDto> itemsDto = new ArrayList<ValeurCritereAppelDoffresDto>();
				for (ValeurCritereAppelDoffres entity : itemsSaved) {
					ValeurCritereAppelDoffresDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create ValeurCritereAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update ValeurCritereAppelDoffres by using ValeurCritereAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ValeurCritereAppelDoffresDto> update(Request<ValeurCritereAppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin update ValeurCritereAppelDoffres-----");

		response = new Response<ValeurCritereAppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ValeurCritereAppelDoffres> items = new ArrayList<ValeurCritereAppelDoffres>();

			for (ValeurCritereAppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la valeurCritereAppelDoffres existe
				ValeurCritereAppelDoffres entityToSave = null;
				entityToSave = valeurCritereAppelDoffresRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("valeurCritereAppelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if appelDoffres exist
				if (dto.getAppelDoffresId() != null && dto.getAppelDoffresId() > 0){
					AppelDoffres existingAppelDoffres = appelDoffresRepository.findById(dto.getAppelDoffresId(), false);
					if (existingAppelDoffres == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("appelDoffres -> " + dto.getAppelDoffresId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setAppelDoffres(existingAppelDoffres);
				}
				// Verify if critereAppelDoffres exist
				if (dto.getCritereId() != null && dto.getCritereId() > 0){
					CritereAppelDoffres existingCritereAppelDoffres = critereAppelDoffresRepository.findById(dto.getCritereId(), false);
					if (existingCritereAppelDoffres == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("critereAppelDoffres -> " + dto.getCritereId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCritereAppelDoffres(existingCritereAppelDoffres);
				}
				if (Utilities.notBlank(dto.getValeur())) {
					entityToSave.setValeur(dto.getValeur());
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				if (Utilities.notBlank(dto.getDeletedAt())) {
					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
				}
				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
					entityToSave.setDeletedBy(dto.getDeletedBy());
				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ValeurCritereAppelDoffres> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = valeurCritereAppelDoffresRepository.save((Iterable<ValeurCritereAppelDoffres>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("valeurCritereAppelDoffres", locale));
					response.setHasError(true);
					return response;
				}
				List<ValeurCritereAppelDoffresDto> itemsDto = new ArrayList<ValeurCritereAppelDoffresDto>();
				for (ValeurCritereAppelDoffres entity : itemsSaved) {
					ValeurCritereAppelDoffresDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update ValeurCritereAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete ValeurCritereAppelDoffres by using ValeurCritereAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ValeurCritereAppelDoffresDto> delete(Request<ValeurCritereAppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete ValeurCritereAppelDoffres-----");

		response = new Response<ValeurCritereAppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ValeurCritereAppelDoffres> items = new ArrayList<ValeurCritereAppelDoffres>();

			for (ValeurCritereAppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la valeurCritereAppelDoffres existe
				ValeurCritereAppelDoffres existingEntity = null;
				existingEntity = valeurCritereAppelDoffresRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("valeurCritereAppelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				valeurCritereAppelDoffresRepository.save((Iterable<ValeurCritereAppelDoffres>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete ValeurCritereAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete ValeurCritereAppelDoffres by using ValeurCritereAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<ValeurCritereAppelDoffresDto> forceDelete(Request<ValeurCritereAppelDoffresDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete ValeurCritereAppelDoffres-----");

		response = new Response<ValeurCritereAppelDoffresDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ValeurCritereAppelDoffres> items = new ArrayList<ValeurCritereAppelDoffres>();

			for (ValeurCritereAppelDoffresDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la valeurCritereAppelDoffres existe
				ValeurCritereAppelDoffres existingEntity = null;
				existingEntity = valeurCritereAppelDoffresRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("valeurCritereAppelDoffres -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				valeurCritereAppelDoffresRepository.save((Iterable<ValeurCritereAppelDoffres>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete ValeurCritereAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get ValeurCritereAppelDoffres by using ValeurCritereAppelDoffresDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<ValeurCritereAppelDoffresDto> getByCriteria(Request<ValeurCritereAppelDoffresDto> request, Locale locale) {
		slf4jLogger.info("----begin get ValeurCritereAppelDoffres-----");

		response = new Response<ValeurCritereAppelDoffresDto>();

		try {
			List<ValeurCritereAppelDoffres> items = null;
			items = valeurCritereAppelDoffresRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<ValeurCritereAppelDoffresDto> itemsDto = new ArrayList<ValeurCritereAppelDoffresDto>();
				for (ValeurCritereAppelDoffres entity : items) {
					ValeurCritereAppelDoffresDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(valeurCritereAppelDoffresRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("valeurCritereAppelDoffres", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get ValeurCritereAppelDoffres-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ValeurCritereAppelDoffresDto by using ValeurCritereAppelDoffres as object.
	 *
	 * @param entity, locale
	 * @return ValeurCritereAppelDoffresDto
	 *
	 */
	private ValeurCritereAppelDoffresDto getFullInfos(ValeurCritereAppelDoffres entity, Integer size, Locale locale) throws Exception {
		ValeurCritereAppelDoffresDto dto = ValeurCritereAppelDoffresTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
