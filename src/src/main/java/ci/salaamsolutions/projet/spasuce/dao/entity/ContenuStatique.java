/*
 * Created on 2020-03-23 ( Time 12:05:53 )
 * Generated by Telosys Tools Generator ( version 3.1.2 )
 */
// This Bean has a basic Primary Key (not composite) 

package ci.salaamsolutions.projet.spasuce.dao.entity;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.*;

import lombok.*;

/**
 * Persistent class for entity stored in table "contenu_statique"
 *
* @author Back-End developper
   *
 */

@Entity
@Table(name="contenu_statique" )
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
public class ContenuStatique implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
	/*
	 * 
	 */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
	/*
	 * 
	 */
    @Column(name="contenu")
    private String     contenu      ;

	/*
	 * 
	 */
    @Column(name="titre", length=255)
    private String     titre        ;

	/*
	 * 
	 */
    @Column(name="ordre")
    private Integer    ordre        ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at")
    private Date       createdAt    ;

	/*
	 * 
	 */
    @Column(name="created_by")
    private Integer    createdBy    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_at")
    private Date       updatedAt    ;

	/*
	 * 
	 */
    @Column(name="updated_by")
    private Integer    updatedBy    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="deleted_at")
    private Date       deletedAt    ;

	/*
	 * 
	 */
    @Column(name="deleted_by")
    private Integer    deletedBy    ;

	/*
	 * 
	 */
    @Column(name="is_deleted")
    private Boolean    isDeleted    ;

	// "typeContenuStatiqueId" (column "type_contenu_statique_id") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="type_contenu_statique_id", referencedColumnName="id")
        private TypeContenuStatique typeContenuStatique;

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
