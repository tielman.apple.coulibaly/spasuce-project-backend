package ci.salaamsolutions.projet.spasuce.helper.enums;

public class GlobalEnum {
	public static final String	CLASSE_A							= "CLASSE_A";
	public static final String	CLASSE_B							= "CLASSE_B";
	public static final String	CLASSE_C							= "CLASSE_C";


	public static final String	DEMD_							= "DEMD_";
	public static final String	APPRO_						= "APPRO_";
	public static final String	CMD_						= "CMD_";
	public static final String	PANIER_							= "PANIER_";
	public static final int nbre_appre_produit = 5;
	public static final int NBRE_MAXI_USER_PAR_ENTREPRISE_CLIENTE = 5;
	public static final int NBRE_MAXI_USER_PAR_ENTREPRISE_MIXTE = 6;
	public static final String	DEFAULT_MESSAGE_OFFRE_REFUSEE	 = "L'appel d'offres a été attribué à un fournisseur";


	// non des sources des fichiers

	public static final String	profils							= "profils";
	public static final String	appel_doffres					= "appel_doffres";
	public static final String	offres							= "offres";
	public static final String	autres							= "autres";


	// liste des criteres de filtre
	public static final String	plus_recent					= "plus_recent";
	public static final String	par_date_limite				= "par_date_limite";
	public static final String	plus_ancien					= "plus_ancien";


	// liste des criteres de appel d'offre
	public static final String	prix					= "prix";
	public static final String	delai_dexecution				= "delai_dexecution";
	public static final String	date_de_livraison				= "date_de_livraison";
	public static final String	dure_de_mise_a_disponibilite					= "dure_de_mise_a_disponibilite";


	// liste des codes
	public static final String	code_ao					= "AO";
	public static final String	code_of					= "OF";
	public static final String	code_ct					= "CT";
	public static final String	nbreChiffre				= "00000";
	public static final String	nbreChiffreDeux				= "00";
	

	// liste des TYPE DE FICHIER
	public static final String	fichiers_ao					= "fichiers_ao";
	public static final String	fichiers_of					= "fichiers_of";
	public static final String	fichiers_ins_description					= "fichiers_ins_description";
	public static final String	fichiers_ins_partenaire					= "fichiers_ins_partenaire";
	public static final String	fichiers_ins_logo					= "fichiers_ins_logo";



}
