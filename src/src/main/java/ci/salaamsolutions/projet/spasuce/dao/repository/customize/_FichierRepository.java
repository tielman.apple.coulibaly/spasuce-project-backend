package ci.salaamsolutions.projet.spasuce.dao.repository.customize;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;

/**
 * Repository customize : Fichier.
 */
@Repository
public interface _FichierRepository {
	default List<String> _generateCriteria(FichierDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds Fichier by using sourceId as a search criteria.
	 *
	 * @param sourceId
	 * @return An Object Fichier whose sourceId is equals to the given sourceId. If
	 *         no Fichier is found, this method returns null.
	 */
	@Query("select e from Fichier e where e.sourceId = :sourceId and e.source = :source and e.isDeleted = :isDeleted")
	List<Fichier> findBySourceIdAndSource(@Param("sourceId")Integer sourceId, @Param("source")String source, @Param("isDeleted")Boolean isDeleted);
	
}
