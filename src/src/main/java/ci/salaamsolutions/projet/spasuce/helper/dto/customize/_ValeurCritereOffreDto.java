
/*
 * Java dto for entity table valeur_critere_offre 
 * Created on 2020-01-03 ( Time 08:47:11 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.customize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.dao.entity.Offre;
import ci.salaamsolutions.projet.spasuce.helper.contract.SearchParam;
import lombok.Data;
/**
 * DTO customize for table "valeur_critere_offre"
 * 
 * @author SFL Back-End developper
 *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _ValeurCritereOffreDto {
	
	protected Offre entityOffre ;
	
	
	protected String typeCritereAppelDoffresCode ;
	protected String dateSoumission ;
	protected String commentaire ;
	protected String entrepriseNom ;
	
	protected String prix ;
	protected String     libelle               ;
	protected String     code               ;

	
	
	
	protected String paysLibelle ;
	protected SearchParam<String> paysLibelleParam ;
	protected Integer appelDoffresId ;
	protected SearchParam<Integer> appelDoffresIdParam ;
	


}
