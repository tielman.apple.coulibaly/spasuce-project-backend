package ci.salaamsolutions.projet.spasuce.dao.repository.customize;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;

/**
 * Repository customize : Offre.
 */
@Repository
public interface _OffreRepository {
	default List<String> _generateCriteria(OffreDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE
		
		if (dto.getUserClientId()!= null && dto.getUserClientId() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("userClientId", dto.getUserClientId(), "e.appelDoffres.user.id", "Integer", dto.getUserClientIdParam(), param, index, locale));
		}

		// ajouter pour les filtres
		if (Utilities.notBlank(dto.getPaysLibelle())) {
			listOfQuery.add(CriteriaUtils.generateCriteria("paysLibelle", dto.getPaysLibelle(), "e.user.ville.pays.libelle", "String", dto.getPaysLibelleParam(), param, index, locale));
		}

		return listOfQuery;
	}
	
	/**
	 * Finds Offre by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object Offre whose etatId is equals to the given etatId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.etat.code = :etatCode and e.isDeleted = :isDeleted")
	List<Offre> findByEtatCode(@Param("etatCode")String etatCode, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds Offre by using etatId as a search criteria.
	 *
	 * @param etatId
	 * @return A list of Object Offre whose etatId is equals to the given etatId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.etat.code = :etatCode")
	List<Offre> findByEtatCodeAll(@Param("etatCode")String etatCode);

	
	/**
	 * Finds Offre by using codeContrat as a search criteria.
	 *
	 * @param codeContrat
	 * @return An Object Offre whose codeContrat is equals to the given codeContrat. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.codeContrat IS NOT NULL and e.isSelectedByClient = :isSelectedByClient and e.isDeleted = :isDeleted")
	List<Offre> findByCodeContratNotNullAndisSelectedByClient(@Param("isSelectedByClient")Boolean isSelectedByClient,  @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds Offre by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object Offre whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.appelDoffres.id = :appelDoffresId and e.etat.code = :etatCode and e.isRetirer = :isRetirer and e.isDeleted = :isDeleted")
	List<Offre> findByAppelDoffresIdAndEtatCodeAndIsRetirer(@Param("appelDoffresId")Integer appelDoffresId, @Param("etatCode")String etatCode, @Param("isRetirer")Boolean isRetirer, @Param("isDeleted")Boolean isDeleted);

	
//	/**
//	 * Finds Offre by using appelDoffresId as a search criteria.
//	 *
//	 * @param appelDoffresId
//	 * @return A list of Object Offre whose appelDoffresId is equals to the given appelDoffresId. If
//	 *         no Offre is found, this method returns null.
//	 */
//	@Query("select e from Offre e where e.appelDoffres.id = :appelDoffresId and e.etat.code = :etatCode and e.isRetirer = :isRetirer and e.isDeleted = :isDeleted")
//	List<Offre> findByAppelDoffresIdAndEtatCodeAndIsRetirer(@Param("appelDoffresId")Integer appelDoffresId, @Param("etatCode")String etatCode, @Param("isRetirer")Boolean isRetirer, @Param("isDeleted")Boolean isDeleted);
//
//	
	/**
	 * Finds Offre by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object Offre whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.appelDoffres.id = :appelDoffresId and e.isSelectedByClient = :isSelectedByClient and  e.isDeleted = :isDeleted")
	List<Offre> findByAppelDoffresIdAndIsSelectedByClient(@Param("appelDoffresId")Integer appelDoffresId, @Param("isSelectedByClient")Boolean isSelectedByClient, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds Offre by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object Offre whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.user.id = :userFournisseurId and e.isSelectedByClient = :isSelectedByClient and  e.isDeleted = :isDeleted")
	List<Offre> findByUserFournisseurIdAndIsSelectedByClient(@Param("userFournisseurId")Integer userFournisseurId, @Param("isSelectedByClient")Boolean isSelectedByClient, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Offre by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object Offre whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.user.id = :userFournisseurId and e.isSelectedByClient = :isSelectedByClient and e.isNoterByClient = :isNoterByClient and  e.isDeleted = :isDeleted")
	List<Offre> findByUserFournisseurIdAndIsSelectedByClientAndIsNoterByClient(@Param("userFournisseurId")Integer userFournisseurId, @Param("isSelectedByClient")Boolean isSelectedByClient, @Param("isNoterByClient") Boolean isNoterByClient , @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds Offre by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object Offre whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.appelDoffres.id = :appelDoffresId and e.user.id = :userFournisseurId and e.isSelectedByClient = :isSelectedByClient and  e.isDeleted = :isDeleted")
	List<Offre> findByAppelDoffresIdAnduserFournisseurIdAndIsSelectedByClient(@Param("appelDoffresId")Integer appelDoffresId, @Param("userFournisseurId")Integer userFournisseurId, @Param("isSelectedByClient")Boolean isSelectedByClient, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds Offre by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object Offre whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.appelDoffres.user.id = :userClientId and e.isDeleted = :isDeleted")
	List<Offre> findByUserCliendId(@Param("userClientId")Integer userClientId, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds Offre by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object Offre whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.appelDoffres.id = :appelDoffresId and e.isVisualiserContenuByClient = :isVisualiserContenuByClient and  e.isDeleted = :isDeleted")
	List<Offre> findByAppelDoffresIdAndIsVisualiserContenuByClient(@Param("appelDoffresId")Integer appelDoffresId, @Param("isVisualiserContenuByClient")Boolean isVisualiserContenuByClient, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds Offre by using appelDoffresId as a search criteria.
	 *
	 * @param appelDoffresId
	 * @return A list of Object Offre whose appelDoffresId is equals to the given appelDoffresId. If
	 *         no Offre is found, this method returns null.
	 */
	@Query("select e from Offre e where e.appelDoffres.id = :appelDoffresId and dateSoumission >= :dateDebut and dateSoumission <= :dateFin and e.isDeleted = :isDeleted")
	List<Offre> findByAppelDoffresIdAndDateSoumission(@Param("appelDoffresId")Integer appelDoffresId, @Param("dateDebut")Date dateDebut, @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

}
