package ci.salaamsolutions.projet.spasuce.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.contract.Request;
import ci.salaamsolutions.projet.spasuce.helper.contract.Response;
import ci.salaamsolutions.projet.spasuce.helper.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;
import ci.salaamsolutions.projet.spasuce.dao.repository.customize._EntrepriseRepository;

/**
 * Repository : Entreprise.
 */
@Repository
public interface EntrepriseRepository extends JpaRepository<Entreprise, Integer>, _EntrepriseRepository {
	/**
	 * Finds Entreprise by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object Entreprise whose id is equals to the given id. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.id = :id and e.isDeleted = :isDeleted")
	Entreprise findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Entreprise by using nom as a search criteria.
	 *
	 * @param nom
	 * @return An Object Entreprise whose nom is equals to the given nom. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.nom = :nom and e.isDeleted = :isDeleted")
	List<Entreprise> findByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using email as a search criteria.
	 *
	 * @param email
	 * @return An Object Entreprise whose email is equals to the given email. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.email = :email and e.isDeleted = :isDeleted")
	Entreprise findByEmail(@Param("email")String email, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using numeroImmatriculation as a search criteria.
	 *
	 * @param numeroImmatriculation
	 * @return An Object Entreprise whose numeroImmatriculation is equals to the given numeroImmatriculation. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.numeroImmatriculation = :numeroImmatriculation and e.isDeleted = :isDeleted")
	List<Entreprise> findByNumeroImmatriculation(@Param("numeroImmatriculation")String numeroImmatriculation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using numeroDuns as a search criteria.
	 *
	 * @param numeroDuns
	 * @return An Object Entreprise whose numeroDuns is equals to the given numeroDuns. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.numeroDuns = :numeroDuns and e.isDeleted = :isDeleted")
	List<Entreprise> findByNumeroDuns(@Param("numeroDuns")String numeroDuns, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using password as a search criteria.
	 *
	 * @param password
	 * @return An Object Entreprise whose password is equals to the given password. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.password = :password and e.isDeleted = :isDeleted")
	List<Entreprise> findByPassword(@Param("password")String password, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using numeroFix as a search criteria.
	 *
	 * @param numeroFix
	 * @return An Object Entreprise whose numeroFix is equals to the given numeroFix. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.numeroFix = :numeroFix and e.isDeleted = :isDeleted")
	List<Entreprise> findByNumeroFix(@Param("numeroFix")String numeroFix, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using numeroFax as a search criteria.
	 *
	 * @param numeroFax
	 * @return An Object Entreprise whose numeroFax is equals to the given numeroFax. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.numeroFax = :numeroFax and e.isDeleted = :isDeleted")
	List<Entreprise> findByNumeroFax(@Param("numeroFax")String numeroFax, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using telephone as a search criteria.
	 *
	 * @param telephone
	 * @return An Object Entreprise whose telephone is equals to the given telephone. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.telephone = :telephone and e.isDeleted = :isDeleted")
	Entreprise findByTelephone(@Param("telephone")String telephone, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using login as a search criteria.
	 *
	 * @param login
	 * @return An Object Entreprise whose login is equals to the given login. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.login = :login and e.isDeleted = :isDeleted")
	Entreprise findByLogin(@Param("login")String login, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using urlLogo as a search criteria.
	 *
	 * @param urlLogo
	 * @return An Object Entreprise whose urlLogo is equals to the given urlLogo. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.urlLogo = :urlLogo and e.isDeleted = :isDeleted")
	List<Entreprise> findByUrlLogo(@Param("urlLogo")String urlLogo, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using extensionLogo as a search criteria.
	 *
	 * @param extensionLogo
	 * @return An Object Entreprise whose extensionLogo is equals to the given extensionLogo. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.extensionLogo = :extensionLogo and e.isDeleted = :isDeleted")
	List<Entreprise> findByExtensionLogo(@Param("extensionLogo")String extensionLogo, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using description as a search criteria.
	 *
	 * @param description
	 * @return An Object Entreprise whose description is equals to the given description. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.description = :description and e.isDeleted = :isDeleted")
	List<Entreprise> findByDescription(@Param("description")String description, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using note as a search criteria.
	 *
	 * @param note
	 * @return An Object Entreprise whose note is equals to the given note. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.note = :note and e.isDeleted = :isDeleted")
	List<Entreprise> findByNote(@Param("note")Float note, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using isLocked as a search criteria.
	 *
	 * @param isLocked
	 * @return An Object Entreprise whose isLocked is equals to the given isLocked. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.isLocked = :isLocked and e.isDeleted = :isDeleted")
	List<Entreprise> findByIsLocked(@Param("isLocked")Boolean isLocked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object Entreprise whose createdAt is equals to the given createdAt. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Entreprise> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object Entreprise whose createdBy is equals to the given createdBy. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Entreprise> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object Entreprise whose updatedBy is equals to the given updatedBy. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Entreprise> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object Entreprise whose updatedAt is equals to the given updatedAt. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Entreprise> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object Entreprise whose isDeleted is equals to the given isDeleted. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.isDeleted = :isDeleted")
	List<Entreprise> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object Entreprise whose deletedBy is equals to the given deletedBy. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Entreprise> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Entreprise by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object Entreprise whose deletedAt is equals to the given deletedAt. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Entreprise> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Entreprise by using villeId as a search criteria.
	 *
	 * @param villeId
	 * @return A list of Object Entreprise whose villeId is equals to the given villeId. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.ville.id = :villeId and e.isDeleted = :isDeleted")
	List<Entreprise> findByVilleId(@Param("villeId")Integer villeId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Entreprise by using villeId as a search criteria.
   *
   * @param villeId
   * @return An Object Entreprise whose villeId is equals to the given villeId. If
   *         no Entreprise is found, this method returns null.
   */
  @Query("select e from Entreprise e where e.ville.id = :villeId and e.isDeleted = :isDeleted")
  Entreprise findEntrepriseByVilleId(@Param("villeId")Integer villeId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Entreprise by using statutJuridiqueId as a search criteria.
	 *
	 * @param statutJuridiqueId
	 * @return A list of Object Entreprise whose statutJuridiqueId is equals to the given statutJuridiqueId. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.statutJuridique.id = :statutJuridiqueId and e.isDeleted = :isDeleted")
	List<Entreprise> findByStatutJuridiqueId(@Param("statutJuridiqueId")Integer statutJuridiqueId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Entreprise by using statutJuridiqueId as a search criteria.
   *
   * @param statutJuridiqueId
   * @return An Object Entreprise whose statutJuridiqueId is equals to the given statutJuridiqueId. If
   *         no Entreprise is found, this method returns null.
   */
  @Query("select e from Entreprise e where e.statutJuridique.id = :statutJuridiqueId and e.isDeleted = :isDeleted")
  Entreprise findEntrepriseByStatutJuridiqueId(@Param("statutJuridiqueId")Integer statutJuridiqueId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Entreprise by using classeNoteId as a search criteria.
	 *
	 * @param classeNoteId
	 * @return A list of Object Entreprise whose classeNoteId is equals to the given classeNoteId. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.classeNote.id = :classeNoteId and e.isDeleted = :isDeleted")
	List<Entreprise> findByClasseNoteId(@Param("classeNoteId")Integer classeNoteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Entreprise by using classeNoteId as a search criteria.
   *
   * @param classeNoteId
   * @return An Object Entreprise whose classeNoteId is equals to the given classeNoteId. If
   *         no Entreprise is found, this method returns null.
   */
  @Query("select e from Entreprise e where e.classeNote.id = :classeNoteId and e.isDeleted = :isDeleted")
  Entreprise findEntrepriseByClasseNoteId(@Param("classeNoteId")Integer classeNoteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Entreprise by using typeEntrepriseId as a search criteria.
	 *
	 * @param typeEntrepriseId
	 * @return A list of Object Entreprise whose typeEntrepriseId is equals to the given typeEntrepriseId. If
	 *         no Entreprise is found, this method returns null.
	 */
	@Query("select e from Entreprise e where e.typeEntreprise.id = :typeEntrepriseId and e.isDeleted = :isDeleted")
	List<Entreprise> findByTypeEntrepriseId(@Param("typeEntrepriseId")Integer typeEntrepriseId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Entreprise by using typeEntrepriseId as a search criteria.
   *
   * @param typeEntrepriseId
   * @return An Object Entreprise whose typeEntrepriseId is equals to the given typeEntrepriseId. If
   *         no Entreprise is found, this method returns null.
   */
  @Query("select e from Entreprise e where e.typeEntreprise.id = :typeEntrepriseId and e.isDeleted = :isDeleted")
  Entreprise findEntrepriseByTypeEntrepriseId(@Param("typeEntrepriseId")Integer typeEntrepriseId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of Entreprise by using entrepriseDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Entreprise
	 * @throws DataAccessException,ParseException
	 */
	public default List<Entreprise> getByCriteria(Request<EntrepriseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Entreprise e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Entreprise> query = em.createQuery(req, Entreprise.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Entreprise by using entrepriseDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Entreprise
	 *
	 */
	public default Long count(Request<EntrepriseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Entreprise e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<EntrepriseDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		EntrepriseDto dto = request.getData() != null ? request.getData() : new EntrepriseDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (EntrepriseDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(EntrepriseDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nom", dto.getNom(), "e.nom", "String", dto.getNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEmail())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("email", dto.getEmail(), "e.email", "String", dto.getEmailParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNumeroImmatriculation())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numeroImmatriculation", dto.getNumeroImmatriculation(), "e.numeroImmatriculation", "String", dto.getNumeroImmatriculationParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNumeroDuns())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numeroDuns", dto.getNumeroDuns(), "e.numeroDuns", "String", dto.getNumeroDunsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPassword())) {
				dto.setPassword(Utilities.encrypt(dto.getPassword())); // hasher le password avant de verifier
				listOfQuery.add(CriteriaUtils.generateCriteria("password", dto.getPassword(), "e.password", "String", dto.getPasswordParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNumeroFix())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numeroFix", dto.getNumeroFix(), "e.numeroFix", "String", dto.getNumeroFixParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNumeroFax())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numeroFax", dto.getNumeroFax(), "e.numeroFax", "String", dto.getNumeroFaxParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTelephone())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephone", dto.getTelephone(), "e.telephone", "String", dto.getTelephoneParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("login", dto.getLogin(), "e.login", "String", dto.getLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUrlLogo())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("urlLogo", dto.getUrlLogo(), "e.urlLogo", "String", dto.getUrlLogoParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getExtensionLogo())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("extensionLogo", dto.getExtensionLogo(), "e.extensionLogo", "String", dto.getExtensionLogoParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDescription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (dto.getNote()!= null && dto.getNote() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("note", dto.getNote(), "e.note", "Float", dto.getNoteParam(), param, index, locale));
			}
			if (dto.getIsLocked()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isLocked", dto.getIsLocked(), "e.isLocked", "Boolean", dto.getIsLockedParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getVilleId()!= null && dto.getVilleId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("villeId", dto.getVilleId(), "e.ville.id", "Integer", dto.getVilleIdParam(), param, index, locale));
			}
			if (dto.getStatutJuridiqueId()!= null && dto.getStatutJuridiqueId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("statutJuridiqueId", dto.getStatutJuridiqueId(), "e.statutJuridique.id", "Integer", dto.getStatutJuridiqueIdParam(), param, index, locale));
			}
			if (dto.getClasseNoteId()!= null && dto.getClasseNoteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("classeNoteId", dto.getClasseNoteId(), "e.classeNote.id", "Integer", dto.getClasseNoteIdParam(), param, index, locale));
			}
			if (dto.getTypeEntrepriseId()!= null && dto.getTypeEntrepriseId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeEntrepriseId", dto.getTypeEntrepriseId(), "e.typeEntreprise.id", "Integer", dto.getTypeEntrepriseIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getVilleCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("villeCode", dto.getVilleCode(), "e.ville.code", "String", dto.getVilleCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getVilleLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("villeLibelle", dto.getVilleLibelle(), "e.ville.libelle", "String", dto.getVilleLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getStatutJuridiqueCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("statutJuridiqueCode", dto.getStatutJuridiqueCode(), "e.statutJuridique.code", "String", dto.getStatutJuridiqueCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getStatutJuridiqueLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("statutJuridiqueLibelle", dto.getStatutJuridiqueLibelle(), "e.statutJuridique.libelle", "String", dto.getStatutJuridiqueLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getClasseNoteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("classeNoteCode", dto.getClasseNoteCode(), "e.classeNote.code", "String", dto.getClasseNoteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getClasseNoteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("classeNoteLibelle", dto.getClasseNoteLibelle(), "e.classeNote.libelle", "String", dto.getClasseNoteLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeEntrepriseCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeEntrepriseCode", dto.getTypeEntrepriseCode(), "e.typeEntreprise.code", "String", dto.getTypeEntrepriseCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeEntrepriseLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeEntrepriseLibelle", dto.getTypeEntrepriseLibelle(), "e.typeEntreprise.libelle", "String", dto.getTypeEntrepriseLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
