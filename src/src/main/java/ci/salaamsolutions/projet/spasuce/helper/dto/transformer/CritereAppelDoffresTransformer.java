

/*
 * Java transformer for entity table critere_appel_doffres 
 * Created on 2020-03-03 ( Time 08:47:42 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.salaamsolutions.projet.spasuce.helper.dto.*;
import ci.salaamsolutions.projet.spasuce.dao.entity.*;


/**
TRANSFORMER for table "critere_appel_doffres"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface CritereAppelDoffresTransformer {

	CritereAppelDoffresTransformer INSTANCE = Mappers.getMapper(CritereAppelDoffresTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.typeCritereAppelDoffres.id", target="typeCritereAppelDoffresId"),
				@Mapping(source="entity.typeCritereAppelDoffres.code", target="typeCritereAppelDoffresCode"),
				@Mapping(source="entity.typeCritereAppelDoffres.libelle", target="typeCritereAppelDoffresLibelle"),
	})
	CritereAppelDoffresDto toDto(CritereAppelDoffres entity) throws ParseException;

    List<CritereAppelDoffresDto> toDtos(List<CritereAppelDoffres> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="typeCritereAppelDoffres", target="typeCritereAppelDoffres"),
	})
    CritereAppelDoffres toEntity(CritereAppelDoffresDto dto, TypeCritereAppelDoffres typeCritereAppelDoffres) throws ParseException;

    //List<CritereAppelDoffres> toEntities(List<CritereAppelDoffresDto> dtos) throws ParseException;

}
