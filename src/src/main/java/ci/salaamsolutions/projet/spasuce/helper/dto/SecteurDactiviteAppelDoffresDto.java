
/*
 * Java dto for entity table secteur_dactivite_appel_doffres
 * Created on 2020-01-31 ( Time 08:34:48 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.helper.contract.*;
import ci.salaamsolutions.projet.spasuce.helper.dto.customize._SecteurDactiviteAppelDoffresDto;

import lombok.*;
/**
 * DTO for table "secteur_dactivite_appel_doffres"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class SecteurDactiviteAppelDoffresDto extends _SecteurDactiviteAppelDoffresDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    appelDoffresId       ;
	/*
	 * 
	 */
    private Integer    secteurDactiviteId   ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String secteurDactiviteCode;
	private String secteurDactiviteLibelle;
	private String appelDoffresLibelle;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  appelDoffresIdParam   ;                     
	private SearchParam<Integer>  secteurDactiviteIdParam;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   secteurDactiviteCodeParam;                     
	private SearchParam<String>   secteurDactiviteLibelleParam;                     
	private SearchParam<String>   appelDoffresLibelleParam;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		SecteurDactiviteAppelDoffresDto other = (SecteurDactiviteAppelDoffresDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
