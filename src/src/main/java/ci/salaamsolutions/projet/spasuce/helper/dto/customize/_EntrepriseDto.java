
/*
 * Java dto for entity table entreprise 
 * Created on 2020-01-03 ( Time 08:47:10 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.salaamsolutions.projet.spasuce.helper.dto.customize;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.salaamsolutions.projet.spasuce.helper.dto.DomaineDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.DomaineEntrepriseDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.FichierDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.PartenaireDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteDto;
import ci.salaamsolutions.projet.spasuce.helper.dto.SecteurDactiviteEntrepriseDto;
import lombok.Data;
/**
 * DTO customize for table "entreprise"
 * 
 * @author SFL Back-End developper
 *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _EntrepriseDto {
	
	protected String fichierBase64;
	protected String cheminFichier;
	protected String paysLibelle;
    private String     entrepriseNom           ;
    private String     oldUrlLogo           ;

	
	//protected List<FichierDto> datasFichierDescription ;
	protected List<FichierDto> datasFichierPartenaire ;

	
	
	protected Integer nombreUser;
	protected Integer paysId;

	
	
	protected List<SecteurDactiviteEntrepriseDto> datasSecteurDactiviteEntreprise;
	protected List<SecteurDactiviteDto> datasSecteurDactivite;
	protected List<DomaineEntrepriseDto> datasDomaineEntreprise;
	protected List<DomaineDto> datasDomaine;
	protected List<FichierDto> datasFichierDescription;
	protected List<PartenaireDto> datasPartenaire;


}
