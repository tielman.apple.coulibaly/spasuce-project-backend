##----------------------------------------------------------------------------------------------------
## JPA beans suffix

#set ( $dto = ${entity.name} + "Dto" )
#set ( $transformer = ${entity.name} + "Transformer" )
#set ( $listOfForeignAttrib = ["code", "name", "firstName", "lastName", "libelle", "intitule", "login", "nom", "prenoms", "prenom", "denomination", "codeGroupe", "libelleGroupe", "raisonSociale"])

/*
 * Java transformer for entity table ${entity.databaseTable} 
 * Created on $today.date ( Time $today.time )
 * Generator tool : $generator.name ( version $generator.version )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
## * Copyright 2020 Savoir Faire Linux. All Rights Reserved.
 */

package ${target.javaPackageFromFolder(${SRC})};

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ${ROOT_PKG}.helper.dto.*;
import ${ENTITY_PKG}.*;


/**
TRANSFORMER for table "${entity.databaseTable}"
 * 
* @author Back-End developper
 ##* @author SFL Back-End developper
  *
 */
@Mapper(componentModel="spring")
public interface ${transformer} {

	${transformer} INSTANCE = Mappers.getMapper(${transformer}.class);

	@Mappings({
#foreach( $field in $entity.attributes )
##if( $field.isUtilDateType() )
##if( $field.isDateType() )
#if ( $field.getDateType() != 0 )

		@Mapping(source="entity.${field.name}", dateFormat="dd/MM/yyyy",target="${field.name}"),
#end
#end
#foreach( $link in $entity.selectedLinks )
#if($link.isCardinalityManyToOne() || $link.isCardinalityOneToOne())
#set( $entityFk = $link.targetEntity)
#foreach($joinColumn in $link.joinColumns)
##$joinColumn.name
#set( $refFieldName = $entity.getAttributeByColumnName($joinColumn.name) )
#end
##/*$refFieldName.name*/
##$refFieldName.name
		@Mapping(source="entity.${link.fieldName}.#foreach($field in $entityFk.keyAttributes)$field.name#end", target="$refFieldName.name"),
##--------------------
#foreach($attrib in $entityFk.attributes)
#if($listOfForeignAttrib.indexOf($attrib.name) != -1)
		##@Mapping(source="entity.${link.fieldName}.$attrib.name", target="$fn.uncapitalize($refFieldName.name)$fn.capitalize($attrib.name)"),
		@Mapping(source="entity.${link.fieldName}.$attrib.name", target="$fn.uncapitalize($link.fieldName)$fn.capitalize($attrib.name)"),
#end
#end
##--------------------
#end
#end
	})
	${dto} toDto(${entity.name} entity) throws ParseException;

    List<${dto}> toDtos(List<${entity.name}> entities) throws ParseException;

	@Mappings({
#foreach( $field in $entity.getAttributesByCriteria( $const.NOT_IN_SELECTED_LINKS ) )
##if( $field.isUtilDateType() )
##if( $field.isDateType() )
#if ( $field.getDateType() != 0 )
		@Mapping(source="dto.${field.name}", dateFormat="dd/MM/yyyy",target="${field.name}"),
#end
##if( !$field.isUtilDateType() )
##if( !$field.isDateType() )
#if ( $field.getDateType() == 0 )
		@Mapping(source="dto.${field.name}", target="${field.name}"),
#end
#end
#foreach( $link in $entity.selectedLinks )
#if( !$link.isCardinalityOneToMany() )
		@Mapping(source="$link.fieldName", target="$link.fieldName"),
#end
#end
	})
    ${entity.name} toEntity(${dto} dto#foreach($link in $entity.selectedLinks)#if(!$link.isCardinalityOneToMany()), $link.fieldType $link.fieldName#end#end) throws ParseException;

    //List<${entity.name}> toEntities(List<${dto}> dtos) throws ParseException;

}
